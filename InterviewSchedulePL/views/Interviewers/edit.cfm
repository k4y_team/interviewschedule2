<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>	
	<form id="interviewerFrm" action="#event.buildLink(prc.xeh.save)#" method="POST" target="_self">		
		<input type="hidden" name="INTERVIEWER_ID" value="#rc.interviewerData.getINTERVIEWER_ID()#">
		<input type="hidden" name="INTERVIEW_SCHEDULE_ID" value="#rc.interviewerData.getINTERVIEW_SCHEDULE_ID()#">
		<table border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="textLine_noBorder">Registrant ID:</td>
				<td class="textLine_noBorder" colspan="3"><input type="text" name="REGISTRANT_ID" class="large" value="#rc.interviewerData.getREGISTRANT_ID()#"></td>
			</tr>
			<tr>
				<td class="textLine_noBorder" width="80px">First Name:</td>
				<td class="textLine_noBorder"><input class="large" type="text" name="FIRST_NAME" value="#rc.interviewerData.getFIRST_NAME()#"></td>
				<td class="textLine_noBorder" width="80px">Last Name:</td>
				<td class="textLine_noBorder"><input class="large" type="text" name="LAST_NAME" value="#rc.interviewerData.getLAST_NAME()#"></td>			
			</tr>
			<tr>
				<td class="textLine_noBorder">Email</td>
				<td class="textLine_noBorder" colspan="3"><input type="text" name="EMAIL" class="xxxlarge" value="#rc.interviewerData.getEMAIL()#" style="width:317px"></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Type:</td>
				<td class="textLine_noBorder" colspan="3">
					<select name="TYPE" id="type" class="large">
						<option val="faculty" <cfif 'FACULTY' EQ #rc.interviewerData.getTYPE()#>selected</cfif>>Faculty</option>
						<option val="student" <cfif 'STUDENT' EQ #rc.interviewerData.getTYPE()#>selected</cfif>>Student</option>
						<option val="resident" <cfif 'RESIDENT' EQ #rc.interviewerData.getTYPE()#>selected</cfif>>Resident</option>
					</select></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Student ID:</td>
				<td class="textLine_noBorder"><input type="text" name="STUDENT_ID" class="large" value="#rc.interviewerData.getSTUDENT_ID()#"></td>
				<td class="textLine_noBorder">CPSO ##:</td>
				<td class="textLine_noBorder"><input type="text" name="CPSO" class="large" value="#rc.interviewerData.getCPSO()#"></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Department:</td>
				<td class="textLine_noBorder" colspan="3"><input type="text" name="DEPARTMENT" class="xxxlarge" value="#rc.interviewerData.getDEPARTMENT()#" style="width:317px"></td>
			</tr>
			<tr>
				<td class="textLine_noBorder_Wrap">Availability</td>
				<td class="textLine_noBorder" colspan="3">
					<cfset loopIndex = 1>
					<cfif prc.days.recordcount eq 0>
						<span style="color:red">Interview days not defined</span>
						<input type="hidden" name="AVAILABLE_DATES" value="">
					<cfelse>
					<cfloop query="prc.days">
						<cfset isChecked = false>
						<cfloop query="prc.availableDays">
							<cfif DateFormat(prc.days.DAY_DT,'ddmmyyyy') eq DateFormat(prc.availableDays.DAY_DT,'ddmmyyyy')>
								<cfset isChecked = true>
							</cfif>
						</cfloop>
						<label><input type="checkbox" name="AVAILABLE_DATES" id="day#loopIndex#" <cfif isChecked>checked</cfif> value="#DateFormat(prc.days.DAY_DT,'dd-mmm-yyyy')#">
							#prc.days.DAY_NAME# / #DateFormat(prc.days.DAY_DT,'dd-mmm-yyyy')#
						</label><br>
						<cfset loopIndex++>
					</cfloop>
					</cfif>
				</td>
			</tr>				
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<hr>
				</td>
			</tr>
			<tr>
				<td class="padding5" width="100%" align="right">
					<input id="saveBtn" class="save" type="button" name="Save" value="Save">
					<cfif rc.interviewerData.getINTERVIEWER_ID() NEQ ''>
						<input id="deleteBtn" class="delete" type="button" name="Delete" value="Delete">
					</cfif>
					<input class="cancel" type="button" name="Cancel" value="Cancel" onclick="closeDialog()">
				</td>
			</tr>
		</table>
	</form>
	<script>
		$('##saveBtn').on('click',function(){
			$('##saveBtn').attr("disabled", "disabled");
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##interviewerFrm').serialize(),
	          	success: function(data){
					if (data == "") {
						closeDialog();
					} else {
						var $parentContainer = $('##interviewerFrm').parents()[0];
						$($parentContainer).html(data);
					}
					reloadPage(false);
	           	}
	        });
	  	});		
		$('##deleteBtn').on('click',function(){
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.delete)#",
	          	data: {'interviewerCD':'#rc.interviewerData.getINTERVIEWER_ID()#'},
				dataType: 'json',
	          	success: function(data){
				   if(data == true){
						closeDialog();
						reloadPage(true);
					} else {
						refreshDialog();
					}
	           	}
	        });
		});
	</script>
</cfoutput>