<cfoutput>
	#runEvent(event=prc.xeh.tabs, eventArguments=prc)#
	<table class="body_tabs" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="padding10" valign="top">
					<cfif isDefined("prc.MsgBox")>
						#prc.MsgBox.renderIt()#
					</cfif>
					<form action="#event.buildLink(prc.xeh.upload)#" method="POST" enctype="multipart/form-data">
						<table  border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="textLine_noBorder" nowrap="true">Session:</td>
								<td class="textLine_noBorder">
									<select name="sessionID" id="sessionID" class="large" onChange="submitFilter();">
										<cfloop query="#prc.uiFilter.getSessionQuery()#">
											<option value="#prc.uiFilter.getSessionQuery().SESSION_ID#" <cfif prc.uiFilter.getSessionValue() EQ prc.uiFilter.getSessionQuery().SESSION_ID>selected</cfif>>
												#prc.uiFilter.getSessionQuery().SESSION_NAME#
											</option>
										</cfloop>
									</select>
								</td>
							</tr>
							<tr>
								<td class="textLine_noBorder" nowrap="true">File:</td>
								<td class="textLine_noBorder">
									<input type="file" name="xls_file" id="xls_file" class="xxlarge">
								</td>
								<td class="textLine_noBorder">
									<input name="upload" type="submit" id="upload" class="load" value="Upload">
								</td>
								<td class="textLine_noBorder">
									<input class="return" type="button" name="back" value="Back" onclick="window.location='#event.buildLink(prc.xeh.interviewers)#?interviewScheduleID=#rc.interviewScheduleID#'">
								</td>
							</tr>
						</table>
					</form>
					<table width="850px" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="textLine_noBorder_black" width="100%"><strong>Notes:</strong>Interviewers to be imported are to be in Excel spreadsheets with columns as shown in the example below. <a href="#event.buildLink(prc.xeh.downloadTemplate)#">Download template</a></td>
						</tr>
					</table>
					<table class="act" width="850px" border="0" cellspacing="1" cellpadding="0">
						<tr>
							<td class="bm_content_header" nowrap>Registrant ID</td>
							<td class="bm_content_header" nowrap>Last Name</td>
							<td class="bm_content_header" nowrap>First Name</td>
							<td class="bm_content_header" nowrap>Email</td>
							<td class="bm_content_header" nowrap>Type</td>
							<td class="bm_content_header" nowrap>Department</td>
							<td class="bm_content_header" nowrap>CPSO ##</td>
							<td class="bm_content_header" nowrap>Student ID</td>
							<td class="bm_content_header" nowrap>Availability</td>
						</tr>
						<tr>
							<td class="bm_content_body_alt">123</td>							
							<td class="bm_content_body_alt">User 10</td>
							<td class="bm_content_body_alt">Interviewer</td>
							<td class="bm_content_body_alt">reviewer10@k4y.ca</td>
							<td class="bm_content_body_alt">Psychiatry</td>
							<td class="bm_content_body_alt">Faculty</td>
							<td class="bm_content_body_alt">300978</td>
							<td class="bm_content_body_alt">324534</td>
							<td class="bm_content_body_alt">01-Dec-2013,02-Dec,2013,03-Dec,2013,04-Dec,2013</td>
						</tr>		
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</cfoutput>