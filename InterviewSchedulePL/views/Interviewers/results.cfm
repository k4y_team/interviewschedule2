<cfoutput>
	<cfif prc.qInterviewers.recordcount eq 0>
		<cfset totalPages = 0>
		<cfset totalRows = 0>
	<cfelse>
		<cfset totalPages = prc.qInterviewers.total_rows\25>
		<cfset totalRows = prc.qInterviewers.total_rows>
	</cfif>
	<cfif totalRows mod 25 gt 0>
		<cfset totalPages = totalPages + 1>
	</cfif>

	<input type="hidden" name="current_page" id="current_page" value="#rc.current_page#">
	<input type="hidden" name="total_pages" id="total_pages" value="#totalPages#">
	<input type="hidden" name="total_rows" id="total_rows" value="#totalRows#">	
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<table width="850px" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="act" width="100%">Interviewers</td>
			<cfif totalRows gt 0>
				<td class="act" style="text-align:right;" >
					<table width="10%" border="0" cellspacing="1" cellpadding="0" style="float: right;">
						<tr>
							<cfif totalPages gt 1>
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="first" class="bm navigation ico-nav-first" href="##">&nbsp;</a></td>
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="previous" class="bm navigation ico-nav-prev" href="##">&nbsp;</a></td>
								<td class="act" width="6%" style="text-align:center">Page #rc.current_page# of #totalPages#</td>
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="next" class="bm navigation ico-nav-next" href="##">&nbsp;</a></td>
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="last" class="bm navigation ico-nav-last" href="##">&nbsp;</a></td>
							</cfif>	
							<td>&nbsp;&nbsp;&nbsp;&nbsp;Total:#totalRows#</td>
						</tr>
					</table>
				</td>
			</cfif>	
		</tr>
	</table>
	<table class="act" width="850px" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td class="bm_content_header" nowrap>Registrant ID</td>							
			<td class="bm_content_header" nowrap>Last Name</td>
			<td class="bm_content_header" nowrap>First Name</td>
			<td class="bm_content_header" nowrap>Email</td>
			<td class="bm_content_header" nowrap>Type</td>
			<td class="bm_content_header" nowrap>CPSO ##</td>
			<td class="bm_content_header" nowrap>Student ID</td>
			<td class="bm_content_header" nowrap>Status</td>
		</tr>
		<cfloop query="#prc.qInterviewers#">
			<tr>
				<td class="bm_content_body_alt">
					<cfif prc.userAccess eq "READ-WRITE">
						<span class="ico-edit" onclick="editInterviewer('#INTERVIEWER_ID#')"></span>
					</cfif>					
					#REGISTRANT_ID#
				</td>
				<td class="bm_content_body_alt">#LAST_NAME#</td>
				<td class="bm_content_body_alt">#FIRST_NAME#</td>
				<td class="bm_content_body_alt">#EMAIL#</td>
				<td class="bm_content_body_alt">#TYPE#</td>
				<td class="bm_content_body_alt">#CPSO#</td>
				<td class="bm_content_body_alt">#STUDENT_ID#</td>
				<td class="bm_content_body_alt">
					<cfif STATUS EQ 'Y'>
						<a href="#event.buildLink(linkTo=prc.xeh.schedule, queryString='interviewer=Interviewer #LAST_NAME#, #FIRST_NAME#&search_string=I_#INTERVIEWER_ID#')#">Scheduled</a>
					<cfelse>
						Not Scheduled
					</cfif>
				</td>
			</tr>
		</cfloop>
		
	</table>
	<script>
		function editInterviewer(interviewerCD) {
			if (interviewerCD === '') {
				dialogTitle = 'Add Interviewer';
			} else {
				dialogTitle = 'Edit Interviewer';
			}
			createDialog("#event.buildLink(linkTo=prc.xeh.editInterviewer, queryString='interviewScheduleID=#rc.interviewScheduleID#&interviewerCD=')#" + interviewerCD, {
				title: dialogTitle, 
				width: 500, 
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
	</script>
</cfoutput>