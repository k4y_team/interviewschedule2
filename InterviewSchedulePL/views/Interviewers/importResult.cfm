<cfoutput>
	#runEvent(event=prc.xeh.tabs, eventArguments=prc)#
	<cfset badRecordsQry = prc.importResult.getBAD_RECORDS_DATA()>
	<cfset goodRecordsQry = prc.importResult.getGOOD_RECORDS_DATA()>

	<table class="body_tabs" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="padding10" valign="top">
					<div id="addButtonDiv" style="display:block;">
						<table width="850px" border="0" cellpadding="0" cellspacing="0">
							<tr style="height:25px;">
								<td>
									<b>#prc.importTitle#</b>
								</td>
								<td class="textLine_noBorder" nowrap style="text-align:right">
									<form name="submitFrm" action="#event.buildLink(prc.xeh.submit)#" method="post">
										<input type="hidden" name="interviewScheduleID" value="#rc.interviewScheduleID#">
										<input type="hidden" name="filename" value="#prc.importFileName#">
										<input class="return" type="button" name="back" value="Back" onclick="window.location='#event.buildLink(prc.xeh.ImportForm)#?interviewScheduleID=#rc.interviewScheduleID#'">
										<cfif prc.importResult.getGOOD_RECORDS() gt 0>
											<input class="load" type="submit" name="submit" value="Submit">
										</cfif>	
									</form>
								</td>
							</tr>
						</table>
					</div>
					<table width="850px" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="act" width="50%">Valid Interviewers</td>
							<td class="act" style="text-align:right">Total: #prc.importResult.getGOOD_RECORDS()#</td>
						</tr>
					</table>
					<table class="act" width="850px" border="0" cellspacing="1" cellpadding="0">
						<tr>
							<td class="bm_content_header" nowrap>Registrant ID</td>
							<td class="bm_content_header" nowrap>Last Name</td>
							<td class="bm_content_header" nowrap>First Name</td>
							<td class="bm_content_header" nowrap>Email</td>
							<td class="bm_content_header" nowrap>Type</td>
							<td class="bm_content_header" nowrap>CPSO ##</td>
							<td class="bm_content_header" nowrap>Student ID</td>
						</tr>
						<tbody>
						<cfloop query="goodRecordsQry">
							<tr>
								<td class="bm_content_body_alt" nowrap>
									<cfif isNumeric(goodRecordsQry["Registrant ID"][goodRecordsQry.currentrow])>
										#NumberFormat(goodRecordsQry["Registrant ID"][goodRecordsQry.currentrow],'_')#
									<cfelse>
										#goodRecordsQry["Registrant ID"][goodRecordsQry.currentrow]#
									</cfif>
								</td>								
								<td class="bm_content_body_alt" nowrap>#goodRecordsQry["Last Name"][goodRecordsQry.currentrow]#</td>
								<td class="bm_content_body_alt" nowrap>#goodRecordsQry["First Name"][goodRecordsQry.currentrow]#</td>
								<td class="bm_content_body_alt" nowrap>#goodRecordsQry["Email"][goodRecordsQry.currentrow]#</td>
								<td class="bm_content_body_alt" nowrap>#goodRecordsQry["Type"][goodRecordsQry.currentrow]#</td>
								<td class="bm_content_body_alt" nowrap>
									<cfif isNumeric(goodRecordsQry["CPSO"][goodRecordsQry.currentrow])>
										#NumberFormat(goodRecordsQry["CPSO"][goodRecordsQry.currentrow],'_')#
									<cfelse>
										#goodRecordsQry["CPSO"][goodRecordsQry.currentrow]#
									</cfif>
								</td>
								<td class="bm_content_body_alt" nowrap>
									<cfif isNumeric(goodRecordsQry["Student ID"][goodRecordsQry.currentrow])>
										#NumberFormat(goodRecordsQry["Student ID"][goodRecordsQry.currentrow],'_')#
									<cfelse>
										#goodRecordsQry["Student ID"][goodRecordsQry.currentrow]#
									</cfif>
								</td>
							</tr>
						</cfloop>	
						</tbody>
					</table>
					
					<br>					
					<cfif prc.importResult.getBAD_RECORDS() gt 0>	
						<table width="850px" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="act" width="50%" style="background-color:##CC0000">Invalid Interviewers</td>
								<td class="act" style="text-align:right;background-color:##CC0000">Total: #prc.importResult.getBAD_RECORDS()#</td>
							</tr>
						</table>
						<table class="act" width="850px" border="0" cellspacing="1" cellpadding="0">
							<tr>
								<td class="bm_content_header" nowrap>Registrant ID</td>	
								<td class="bm_content_header" nowrap>Last Name</td>
								<td class="bm_content_header" nowrap>First Name</td>
								<td class="bm_content_header" nowrap>Email</td>
								<td class="bm_content_header" nowrap>Type</td>
								<td class="bm_content_header" nowrap>CPSO ##</td>
								<td class="bm_content_header" nowrap>Student ID</td>
								<td class="bm_content_header" nowrap>Error</td>
							</tr>
							<tbody>
							<cfloop query="badRecordsQry">
								<tr>
									<td class="bm_content_body_alt" nowrap>
										<cfif isNumeric(badRecordsQry["Registrant ID"][badRecordsQry.currentrow])>
											#NumberFormat(badRecordsQry["Registrant ID"][badRecordsQry.currentrow],'_')#
										<cfelse>
											#badRecordsQry["Registrant ID"][badRecordsQry.currentrow]#
										</cfif>
									</td>									
									<td class="bm_content_body_alt" nowrap>#badRecordsQry["First Name"][badRecordsQry.currentrow]#</td>
									<td class="bm_content_body_alt" nowrap>#badRecordsQry["Last Name"][badRecordsQry.currentrow]#</td>
									<td class="bm_content_body_alt" nowrap>#badRecordsQry["Email"][badRecordsQry.currentrow]#</td>
									<td class="bm_content_body_alt" nowrap>#badRecordsQry["Type"][badRecordsQry.currentrow]#</td>
									<td class="bm_content_body_alt" nowrap>
										<cfif isNumeric(badRecordsQry["CPSO"][badRecordsQry.currentrow])>
											#NumberFormat(badRecordsQry["CPSO"][badRecordsQry.currentrow],'_')#
										<cfelse>
											#badRecordsQry["CPSO"][badRecordsQry.currentrow]#
										</cfif>
									<td class="bm_content_body_alt" nowrap>
										<cfif isNumeric(badRecordsQry["Student ID"][badRecordsQry.currentrow])>
											#NumberFormat(badRecordsQry["Student ID"][badRecordsQry.currentrow],'_')#
										<cfelse>
											#badRecordsQry["Student ID"][badRecordsQry.currentrow]#
										</cfif>
									</td>
									<td class="bm_content_body_alt" style="color:red">#badRecordsQry["errormessages"][badRecordsQry.currentrow]#</td>
								</tr>
							</cfloop>	
							</tbody>
						</table>
						<br>
					</cfif>						
				</td>
			</tr>
		</tbody>
	</table>
</cfoutput>