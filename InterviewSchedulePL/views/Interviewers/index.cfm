<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>

	<K4Y:includesFile fileName="javascript/lookup.js" moduleName="#event.getCurrentModule()#" fileType="js"/>	
	#runEvent(event=prc.xeh.tabs, eventArguments=prc)#
	<table class="body_tabs" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="padding10" valign="top">
					#runEvent(event=prc.xeh.showmessage, eventArguments=rc)#
					<div style="display:block;margin-bottom:5px;">
						<form name="filter" id="filter" action="#event.buildLink(prc.xeh.self)#" method="post">	
							<input type="hidden" name="interviewScheduleID" value="#rc.interviewScheduleID#">
							<table width="850px" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="50px" class="textLine_noBorder" nowrap="true">Session:</td>
									<td class="textLine_noBorder" style="width:130px;">
										<select name="sessionID" id="sessionID" class="large" onChange="submitFilter();">
											<cfloop query="#prc.uiFilter.getSessionQuery()#">
												<option value="#prc.uiFilter.getSessionQuery().SESSION_ID#" <cfif prc.uiFilter.getSessionValue() EQ prc.uiFilter.getSessionQuery().SESSION_ID>selected</cfif>>
													#prc.uiFilter.getSessionQuery().SESSION_NAME#
												</option>
											</cfloop>
										</select>
									</td>
									<td class="textLine_noBorder" nowrap="true" style="width:50px;vertical-align:middle;">Status:</td>
									<td class="textLine_noBorder">
										<select name="interviewerStatus" id="interviewerStatus" class="large" onChange="submitFilter();">
											<option value="all" <cfif rc.interviewerStatus eq 'all'>selected</cfif>>All</option>
											<option value="scheduled" <cfif rc.interviewerStatus eq 'scheduled'>selected</cfif>>Scheduled</option>
											<option value="not_scheduled" <cfif rc.interviewerStatus eq 'not_scheduled'>selected</cfif>>Not Scheduled</option>
										</select>
									</td>
									<td>
										<cfif isDefined("prc.MsgBox")>
											#prc.MsgBox.renderIt()#
										</cfif>	
									</td>
									<cfif prc.userAccess eq "READ-WRITE">
										<td class="textLine_right_noBorder" nowrap>
											<input class="load" type="button" name="Import" value="Import" onclick="window.location='#event.buildLink(prc.xeh.import)#?interviewScheduleID=#rc.interviewScheduleID#'">
										</td>
									</cfif>
								</tr>
							</table>
						</form>
					</div>
					<form name="fake_filterFrm" id="fake_filterFrm" method="post" action=""></form>
					<div id="results"></div>
				</td>
			</tr>
		</tbody>
	</table>
	<script>
		function reloadPage(bResetPage) {
			LOOKUP.reload(bResetPage);			
		}		
		$(function() {
			var options = {};
			options.triggerSearch = 'true';
			options.loadResultsUrl = '#event.buildLink(prc.xeh.load)#';
			LOOKUP.setup(options);
		});		
		function submitFilter(bResetPage){
			$('##filter').submit();
		}
	</script>
</cfoutput>