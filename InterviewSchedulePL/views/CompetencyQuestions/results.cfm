<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<table width="550px" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="act" width="100%">Score Questions</td>
		</tr>
	</table>
	<table class="act" width="550px" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td class="bm_content_header" nowrap>Name</td>
			<td class="bm_content_header_center" nowrap>Rank</td>
			<td class="bm_content_header_center" nowrap>Type</td>
		</tr>
		<cfloop query="prc.qQuestions" >
			<tr>
				<td class="bm_content_body_alt">
					<cfif prc.userAccess eq "READ-WRITE">
					<span class="ico-edit" onclick="editQuestion(#QUESTION_ID#)"></span>&nbsp;
					</cfif>
					#QUESTION_NAME#
				</td>
				<td class="bm_content_body_alt_center">#QUESTION_RANK#</td>
				<td class="bm_content_body_alt_center">#QUESTION_TYPE_NAME#</td>
			</tr>
		</cfloop>	
	</table>
	<script>
		function editQuestion(questionID) {
			if (questionID === '') {
				dialogTitle = 'Add Score Question';
			} else {
				dialogTitle = 'Edit Score Question';
			}
			createDialog("#event.buildLink(linkTo=prc.xeh.editQuestion, queryString='sessionID=#rc.sessionID#&competencyID=#rc.competencyID#&questionID=')#" + questionID , {
				title: dialogTitle, 
				width: 500, 
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
	</script>
</cfoutput>