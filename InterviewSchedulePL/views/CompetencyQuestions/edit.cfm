<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>		
	<form name="questionForm" id="questionForm" action="#event.buildLink(prc.xeh.save)#" method="POST">
		<input type="hidden" name="QUESTION_ID" value="#rc.questionData.getQUESTION_ID()#">
		<input type="hidden" name="COMPETENCY_ID" value="#rc.competencyData.getCOMPETENCY_ID()#">
		<input type="hidden" name="sessionID" value="#rc.sessionID#">
		<table  width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="textLine_noBorder" width="70px">Session:</td>
				<td class="textLine_noBorder">
					<input type="text" class="large" name="session_name" id="session_name" value="#prc.sessionQry.SESSION_NAME#" disabled>
				</td>
				
				<td class="textLine_right_noBorder" width="70px">Competency:</td>
				<td class="textLine_noBorder">
					<input type="text" class="large" name="competency_name" id="competency_name" value="#rc.competencyData.getCOMPETENCY_NAME()#" disabled>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Name:</td>
				<td class="textLine_noBorder" colspan="3"><input class="large" type="text" name="QUESTION_NAME" value="#rc.questionData.getQUESTION_NAME()#" style="width:348px"></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Type:</td>
				<td class="textLine_noBorder">
					<select class="large" name="QUESTION_TYPE_ID" id="questionType">
						<cfloop query="prc.questionTypesQry">
							<option data-code="#QUESTION_TYPE_CODE#" value="#QUESTION_TYPE_ID#" <cfif rc.questionData.getQUESTION_TYPE_ID() EQ QUESTION_TYPE_ID>selected</cfif>>#QUESTION_TYPE_NAME#</option>
						</cfloop>
					</select>
				</td>
				<td class="textLine_right_noBorder">Required:</td>
				<td class="textLine_left_noBorder"><input type="checkbox" name="ISREQUIRED" <cfif rc.questionData.getIS_REQUIRED() eq 'Y'> checked</cfif> value="Y"></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Rank:</td>
				<td class="textLine_noBorder"><input class="large" type="text" name="QUESTION_RANK" value="#rc.questionData.getQUESTION_RANK()#"></td>
				<td class="textLine_right_noBorder isEditable">Editable:</td>
				<td class="textLine_left_noBorder isEditable"><input type="checkbox" name="IS_EDITABLE" <cfif rc.questionData.getIS_EDITABLE() eq 'Y'> checked</cfif> value="Y"></td>
			</tr>						
			<tr>
				<td colspan="4"><hr></td>
			</tr>
			<tr>
				<td class="padding5" width="100%" align="right" colspan="4">
					<input id="saveBtn" class="save" type="button" name="Save" value="Save">
					<cfif rc.questionData.getQUESTION_ID() NEQ ''>
						<input id="deleteBtn" class="delete" type="button" name="Delete" value="Delete">
					</cfif>
					<input class="cancel" type="button" name="Cancel" value="Cancel" onclick="closeDialog();">
				</td>
			</tr>
		</table>
	</form>
	<script>
		$('##saveBtn').on('click',function(){
			$('##saveBtn').attr("disabled", "disabled");
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##questionForm').serialize(),
	          	success: function(data){
					if (data == "") {
						closeDialog();
					} else {
						var $parentContainer = $('##questionForm').parents()[0];
						$($parentContainer).html(data);
					}
					reloadPage();
	           	}
	        });
	  	});		
		$('##deleteBtn').on('click',function(){
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.delete)#",
	          	data: {'questionID':'#rc.questionData.getQUESTION_ID()#'},
				dataType: 'json',
	          	success: function(data){
				   	if(data == true){
						closeDialog(true);
						reloadPage();
					} else {
						refreshDialog();
					}
	           	}
	        });
		});
		$('##questionType').on('change',function(){
			hideEditable($('##questionType option:selected').attr("data-code"));	
		});
		
		function hideEditable(questionType){
			if(questionType != "TEXT"){
				$(".isEditable").hide();
			} else $(".isEditable").show();
		}
		$(function(){
			hideEditable($('##questionType option:selected').attr("data-code"));		
		});
	</script>
</cfoutput>