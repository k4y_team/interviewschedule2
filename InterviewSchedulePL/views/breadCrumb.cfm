<cfoutput>
    <ul class="breadcrumb">
        <li><a href="#event.buildLink(prc.xeh.home)#">Home</a> <span class="divider">></span></li>
		<cfloop from="#ArrayLen(prc.breadCrumbArr)#" to="2" index="i" step="-1">
	        <li <cfif prc.breadCrumbArr[i].code eq "ISCH_MODULE">id="ISCH_MODULE"</cfif>>
		        <cfif prc.breadCrumbArr[i].showLink><a href="#event.buildLink(prc.breadCrumbArr[i].link)#"></cfif>
		        #prc.breadCrumbArr[i].title#
		        <cfif prc.breadCrumbArr[i].showLink></a></cfif>
		        <span class="divider">></span>
			</li>
		</cfloop>
		<li class="active">#prc.breadCrumbArr[1].title#</li>
    </ul>
</cfoutput>
