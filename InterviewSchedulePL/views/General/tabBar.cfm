<cfparam name="prc.activetab" default="">
<cfparam name="prc.tabList" default="">
<cfparam name="prc.linksList" default="">
<cfparam name="prc.colorList" default="">
<cfparam name="prc.colorDefault" default="B">
<cfparam name="prc.urlParams" default="">

<cfset colorListLen = ListLen(prc.colorList)>
<cfif colorListLen LT ListLen(prc.tabList)>
	<cfloop from="#Val(colorListLen+1)#" to="#ListLen(prc.tabList)#" index="indx">
		<cfset prc.colorList = ListAppend(prc.colorList,prc.colorDefault)>
	</cfloop>
</cfif>

<cfset tabExt = "">
<cfif ListLen(prc.activetab) EQ 2>
	<cfset tabExt = ListGetAt(prc.activetab,2)>
	<cfset prc.activetab = ListGetAt(prc.activetab,1)>
</cfif>
<cfoutput>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tabBar">
		<tr height="5"><td></td></tr>
		<tr>
			<cfset iter = 0>
			<cfset filteredTabList = "">
			<cfset filteredLinksList = "">
			<cfset filteredColorList = "">			
			<cfloop list="#prc.tabList#" index="tab">
				<cfset iter = iter + 1>
				<cfif prc.Security.checkUserAccess(session.user_id,tab) neq "DENIED">
					<cfset filteredTabList = ListAppend(filteredTabList,tab)>
					<cfset filteredLinksList = ListAppend(filteredLinksList,ListGetAt(prc.linksList,iter))>
					<cfset filteredColorList = ListAppend(filteredColorList,ListGetAt(prc.colorList,iter))>					
				</cfif>
			</cfloop>
			
			<cfset iter = 0>
			<cfloop list="#filteredTabList#" index="tab">
				<cfset iter = iter + 1>
				<cfset colorLeft = "X">
				<cfif iter GT 1>
					<cfset colorLeft = ListGetAt(filteredColorList,iter-1)>
				</cfif>	
				<cfset colorTab = ListGetAt(filteredColorList,iter)>
				<cfif tab EQ prc.activetab>
					<cfset tabActive = "R">
					<cfset tabDesc = Trim(ListGetAt(filteredTabList,iter) & " " & tabExt)>
				<cfelse>
					<cfset tabActive = "X">
					<cfset tabDesc = tab>
					<cfif iter GT 1>
						<cfif ListGetAt(filteredTabList,iter-1) EQ prc.activeTab>
							<cfset tabActive = "L">
						</cfif>
					</cfif>	
				</cfif>
				<cfset linkDesc = ListGetAt(filteredLinksList,iter) & prc.urlParams>
				<cfset tabIter = iter>
				
				<cfset prc.tabActive = tabActive>
				<cfset prc.tabDesc = tabDesc>
				<cfset prc.linkDesc = linkDesc>
				<cfset prc.colorLeft = colorLeft>
				<cfset prc.colorTab = colorTab>
				<cfset prc.tabIter = tabIter>
				
				#runEvent(event=prc.xeh.tab, eventArguments=prc)#
			</cfloop>
			
			<!--- displays the right end of tab bar --->
			<cfset iter = iter + 1>
			<cfset colorLeft = "X">
			<cfif iter GT 1>
				<cfset colorLeft = ListGetAt(filteredColorList,iter-1)>
			</cfif>	
			<cfset tabActive = "X">
			<cfif iter GT 1>
				<cfif ListGetAt(filteredTabList,iter-1) EQ prc.activetab>
					<cfset tabActive = "L">
				</cfif>
			</cfif>
			<cfset tabDesc = "">
			<cfset colorTab = "X">
			<cfset linkDesc = "">
			<cfset tabIter = iter>
				
			<cfset prc.tabActive = tabActive>
			<cfset prc.tabDesc = tabDesc>
			<cfset prc.linkDesc = linkDesc>
			<cfset prc.colorLeft = colorLeft>
			<cfset prc.colorTab = colorTab>
			<cfset prc.tabIter = tabIter>
			#runEvent(event=prc.xeh.tab, eventArguments=prc)#
		</tr>
	</table>
</cfoutput>