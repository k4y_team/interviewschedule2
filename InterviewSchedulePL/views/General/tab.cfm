<cfparam name="prc.tabActive">
<cfparam name="prc.tabDesc" default="">
<cfparam name="prc.linkDesc" default="">
<cfparam name="prc.colorLeft" default="">
<cfparam name="prc.colorTab" default="">
<cfparam name="prc.tabIter" default="1">

<!--- ****************************************************************** 
	Create Tab class in format "tab_<tabActive>_<colorLeft>_<colorTab>" 
	Use "X" for inactive tab and transparent color on very left/right tabs 
	Use "L"/"R" for active Left/Right tab 

	Create Tab background class in format "tabBgd_<tabActive>_<colorTab>" 
	Use "X" for inactive tab and transparent color on very right tab
	Use "A" for active tab 
--->

<cfset classTab = "tab_#prc.tabActive#_#prc.colorLeft#_#prc.colorTab#">
<cfif UCase(prc.tabActive) EQ "R">
	<cfset actBgd = "A">
<cfelse>
	<cfset actBgd = "X">
</cfif>
<cfset classTabBgd = "tabBgd_#actBgd#_#prc.colorTab#">

<cfoutput>
	<!--- displays the left side of the tab --->
	<td id="tab#prc.tabIter#_lft" class="#classTab#">
		<div class="spacer"></div>
	</td>

	<!--- displays the middle part of each tab --->
	<cfif UCase(prc.colorTab) EQ "X">
		<td class="tab_spacer" width="100%">
			<div class="spacer"></div>
		</td>
	<cfelse>
		<td id="tab#prc.tabIter#_mid" class="#classTabBgd#">
			<cfif actBgd EQ "A">
				#prc.tabDesc#
			<cfelse>
				<a class="tabLnk" href="javascript:;" onclick="this.style.cursor='wait';window.location='#event.buildLink(prc.linkDesc)#';">#prc.tabDesc#</a>
			</cfif>
		</td>	
	</cfif>
</cfoutput>

