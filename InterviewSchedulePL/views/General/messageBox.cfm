<cfoutput>
	<cfif rc.messageType IS "success">
		<div id="msg_box" class="messageSuccess">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="messageSuccess"><span class="ico-success" title="Confirmation"></span></td>
					<td class="messageSuccess">#rc.message#</td>
				</tr>
			</table>
		</div>
	<cfelseif rc.messageType NEQ "">
		<div id="msg_box" class="messageError">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="messageError"><span class="ico-error" title="Confirmation"></span></td>
					<td class="messageError">#rc.message#</td>
				</tr>
			</table>
		</div>
	</cfif>
	<script type="text/javascript">
		$(function(){
			$('##msg_box').on('click',function(){
				hideMessage($(this));
			});
			<cfif rc.timeOut NEQ 0>
				window.setTimeout( hideMessage($('##msg_box')), #rc.timeOut#);
			</cfif>
		});
		
		function hideMessage(selector) {
			selector.hide();
			selector.remove();
		}
	</script>
</cfoutput>