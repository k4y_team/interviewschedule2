<cfparam name="rc.Title" default=""> 
<cfparam name="rc.Scroll" default="no"> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title><cfoutput><cfif rc.Title NEQ ''>#rc.Title#<cfelse>MedSIS</cfif></cfoutput></title>
    	<script type="text/javascript" src="#application.cbcontroller.getsetting('applicationBaseURL')#includes/js/jquery-1.10.2.min.js"></script>
	</head>
	<body>
		<cfset strUrl = CGI.PATH_INFO>
		<cfset pos = FindNoCase('sourceUrl',cgi.path_info) + Len('sourceUrl')>
		<cfset temp = Left(cgi.path_info, pos)>
		<cfset strUrl = Replace(strUrl, temp, "", "All")>
		<cfset strUrl = Replace(strUrl, ".", "/")>

		<cfoutput>
			<input type="hidden" name="modal" id="modal" value="yes">
			<iframe src="#event.buildLink(strUrl)#" width="100%" height="100%" frameborder="0" scrolling="#rc.Scroll#" id="iframeModal"></iframe>
		</cfoutput> 
		<script type="text/javascript">
			$('#iframeModal').load(function() {
				if ($('#iframeModal').contents().find('.cancel').length > 0) {
					currOnClick = $('#iframeModal').contents().find('.cancel').get(0).getAttributeNode('onclick').value;
					if (currOnClick.search('parent.window.close') == -1 && currOnClick.search('window.parent.close') == -1) {
						$('#iframeModal').contents().find('.cancel').click(function(){
							window.close();
						});
					}
				}
		    });
		</script>
	</body>
</html>