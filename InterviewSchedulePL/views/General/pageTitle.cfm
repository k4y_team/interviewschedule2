<cfoutput>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="fullbar">
		<tr>
			<td class="title_bar" width="100%" ondblclick="javascript:showHideMenu();">#prc.pageTitle#</td>
			<cfif prc.resize>
				<td class="title_bar">
					<a href="javascript:showHideMenu();">
						<span class="ico-maximize" id="restoreImg" title="Maximize"></span>
					</a>
				</td>
			</cfif>
		</tr>
	</table>
</cfoutput>
<script type="text/javascript">
	function showHideMenu() {
		if (parent.document.getElementById("detail_frameset")) {
	   		if(parent.document.getElementById("detail_frameset").cols == "0,*") {
	   			$('#restoreImg').removeClass('ico-resize').addClass('ico-maximize');
	   			parent.document.getElementById('detail_frameset').cols = "143,*";
	   			parent.parent.document.getElementById('main_frameset').rows = "80,*";
	   		} else {
	   			$('#restoreImg').removeClass('ico-maximize').addClass('ico-resize');
	   			parent.document.getElementById('detail_frameset').cols = "0,*";
	   			parent.parent.document.getElementById('main_frameset').rows = "0,*";
	   		}
	   	}
	}
</script>