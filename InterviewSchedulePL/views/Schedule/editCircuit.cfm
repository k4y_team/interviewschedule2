<cfoutput>
	<style type="text/css">
		##color {
			text-transform: uppercase;
		}
	</style>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<form id="circuitForm" name="circuitForm" action="#event.buildLink(prc.xeh.save)#" method="post">
		<input type="hidden" name="circuit_id" id="circuit_id" value="#prc.circuitData.getCIRCUIT_ID()#">
		<input type="hidden" name="interviewScheduleID" id="interviewScheduleID" value="#rc.interviewScheduleID#">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="textLine_noBorder" width="80">Session:</td>
				<td class="textLine_noBorder">
					<input type="hidden" name="sessionID" id="sessionID" value="#prc.sessionQry.SESSION_ID#">
					<input type="text" name="session_name" id="session_name" value="#prc.sessionQry.SESSION_NAME#" disabled>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Day:</td>
				<td class="textLine_noBorder">
					<select name="day_id" id="day_id" class="xlarge">
						<cfloop query="prc.dayQry">
							<option value="#prc.dayQry.DAY_ID#" <cfif prc.circuitData.getDAY_ID() EQ prc.dayQry.DAY_ID>selected</cfif>>
								#prc.dayQry.DAY_NAME# / #DateFormat(prc.dayQry.DAY_DT, application.cbcontroller.getSetting('formatStruct').DateFormat)#
							</option>
						</cfloop>
					</select>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Color:</td>
				<td class="textLine_noBorder">
					<input type="text" maxlength="6" size="6" id="color" name="color" value="#prc.circuitData.getCOLOR()#" />
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Name:</td>
				<td class="textLine_noBorder">
					<input type="text" class="xxlarge" name="circuit_name" id="circuit_name" value="#prc.circuitData.getCIRCUIT_NAME()#">
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Location:</td>
				<td class="textLine_noBorder">
					<input type="text" class="xxlarge" name="location" id="location" value="#prc.circuitData.getLOCATION()#">
				</td>
			</tr>
			<cfif prc.circuitData.getCIRCUIT_ID() EQ '' AND (prc.shiftsQry.recordCount GT 0 OR prc.competenciesQry.recordCount GT 0)>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				<cfif prc.shiftsQry.recordCount GT 0>
					<tr>
						<td class="textLine_noBorder alignTop" width="80">Shifts:</td>
						<td class="textLine_noBorder">
							<table border="0" cellpadding="0" cellspacing="0">
								<cfloop query="prc.shiftsQry">
									<cfif prc.shiftsQry.currentRow MOD 2 EQ 1>
										<tr>
									</cfif>
									<td width="200">
										<label for="shift_#prc.shiftsQry.SHIFT_ID#">
											<input type="checkbox" name="shift_id_list" id="shift_#prc.shiftsQry.SHIFT_ID#" value="#prc.shiftsQry.SHIFT_ID#" <cfif ListFind(prc.circuitData.getSHIFT_ID_LIST(), prc.shiftsQry.SHIFT_ID)>checked</cfif>>
											#NumberFormat(prc.shiftsQry.START_HOUR,"00")#:#NumberFormat(prc.shiftsQry.START_MINUTE,"00")# - #NumberFormat(prc.shiftsQry.END_HOUR,"00")#:#NumberFormat(prc.shiftsQry.END_MINUTE,"00")#
										</label>
									</td>
									<cfif prc.shiftsQry.currentRow MOD 2 EQ 0>
										</tr>
									</cfif>
								</cfloop>
							</table>
						</td>
					</tr>
				</cfif>
				<cfif prc.competenciesQry.recordCount GT 0>
					<tr>
						<td class="textLine_noBorder alignTop" width="80">Stations:</td>
						<td class="textLine_noBorder">
							<table border="0" cellpadding="0" cellspacing="0">
								<cfloop query="prc.competenciesQry">
									<cfif prc.competenciesQry.currentRow MOD 2 EQ 1>
										<tr>
									</cfif>
									<td width="200">
										<label for="competency_#prc.competenciesQry.COMPETENCY_ID#">
											<input type="checkbox" name="competency_id_list" id="competency_#prc.competenciesQry.COMPETENCY_ID#" value="#prc.competenciesQry.COMPETENCY_ID#" <cfif ListFind(prc.circuitData.getCOMPETENCY_ID_LIST(), prc.competenciesQry.COMPETENCY_ID)>checked</cfif>>#prc.competenciesQry.COMPETENCY_NAME#
										</label>
									</td>
									<cfif prc.competenciesQry.currentRow MOD 2 EQ 0>
										</tr>
									</cfif>
								</cfloop>
							</table>
						</td>
					</tr>
				</cfif>
			</cfif>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<td class="padding5" align="right" colspan="2">
					<input type="button" class="save" name="Save" value="Save" onclick="saveCircuit()">
					<cfif prc.circuitData.getCIRCUIT_ID() NEQ ''>
						<input type="button" class="delete" name="Delete" value="Delete" onclick="deleteCircuit()">
					</cfif>
					<input type="button" class="cancel" name="Cancel" value="Cancel" onclick="closeDialog();">
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		$(function(){
			$('##color').ColorPicker({
				onSubmit: function(hsb, hex, rgb, el) {
					$(el).val(hex);
					$(el).ColorPickerHide();
				},
				onBeforeShow: function () {
					$(this).ColorPickerSetColor(this.value);
				}
			})
			.bind('keyup', function(){
				$(this).ColorPickerSetColor(this.value);
			});	
		});
		function saveCircuit() {
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##circuitForm').serialize(),
	          	success: function(data){
					if (data == "") {
						submitFilter();
						closeDialog();
					} else {
						var $parentContainer = $('##circuitForm').parents()[0];
						$($parentContainer).html(data);
						loadCircuit($('##circuit_id').val(),'');
					}
	           	}
	        });
	  	};				
		<cfif prc.circuitData.getCIRCUIT_ID() NEQ ''>
			function deleteCircuit() {
				$.ajax({
					type: "POST",
					url: "#event.buildLink(prc.xeh.delete)#",
					data: $('##circuit_id').serialize(),
					dataType: "json",
					success: function(data) {
						if(data == true){
							$('##circ' + $('##circuit_id').val()).remove();
							closeDialog();
						} else {
							refreshDialog();
						}
					}
				});
			}
		</cfif>
	</script>
</cfoutput>