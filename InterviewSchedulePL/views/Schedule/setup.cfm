<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<cfif prc.circuitQry.recordcount gt 0>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<cfset detailQry = prc.detailStruct['detail_' & prc.circuitQry.CIRCUIT_ID]>
					<table class="schedule_table" cellpadding="0" cellspacing="0">
						<tr>
							<th class="circuit_util" style="<cfif prc.circuitQry.COLOR NEQ ''>background-color: ###prc.circuitQry.COLOR#;</cfif>min-width:100px;">
								<strong>#prc.circuitQry.DAY_NAME# / #DateFormat(prc.circuitQry.DAY_DT, application.cbcontroller.getSetting('formatStruct').DateFormat)#</strong><br />
								<strong>#prc.circuitQry.CIRCUIT_NAME#</strong><br />
								<span class="ico-edit" onclick="editCircuit(#prc.circuitQry.CIRCUIT_ID#)"></span>
							</th>
							<cfif prc.detailStruct['avail_station_' & prc.circuitQry.CIRCUIT_ID].recordCount GT 0>
								<th class="circuit_util">
									<span class="ico-add" onclick="editStation(#prc.circuitQry.CIRCUIT_ID#, '')"></span>&nbsp;&nbsp;<a href="javascript:;" onclick="editStation(#prc.circuitQry.CIRCUIT_ID#, '')">Add Station</a>
								</th>
							</cfif>
							<cfset stationNR = 0>
							<cfif detailQry.recordCount GT 0>
								<cfquery name="stationQry" dbtype="query">
									SELECT * FROM detailQry ORDER BY ORDER_NUMBER
								</cfquery>
								<cfloop query="stationQry" group="COMPETENCY_ID">
									<cfif stationQry.COMPETENCY_ID NEQ ''>
										<cfset stationNR++>
										<th class="circuit_util accept">
											<table class="defaultTable" width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td nowrap>
														<input type="hidden" name="station_id" value="#stationQry.STATION_ID#">
														Competency: #stationQry.COMPETENCY_NAME#
													</td>
													<td align="right">
														<span class="handle floatr no-padding"></span>
													</td>
												</tr>
												<tr>
													<td colspan="2">Room: 
														<cfif stationQry.ROOM NEQ ''>
															<span <cfif "R_#stationQry.room#" eq rc.search_string>style="background-color:yellow"</cfif>>
																#stationQry.ROOM#
															</span>
														<cfelse>
															<span class="red">Not Selected</span>
														</cfif>
													</td>
												</tr>
												<tr>
													<td colspan="2">Question ID: #stationQry.QUESTION_ID#</td>
												</tr>
												<tr>
													<td>Interviewer: 
														<cfif stationQry.FIRST_NAME NEQ ''>
															<span <cfif rc.search_string EQ "I_#stationQry.INTERVIEWER_ID#">style="background-color:yellow;"</cfif>>
																#stationQry.FIRST_NAME# #stationQry.LAST_NAME#
															</span>
														<cfelse>
															<span style="color:red">Not Selected</span>
														</cfif>
													</td>
													<td align="right"><span class="ico-edit floatr no-padding" onclick="editStation(#prc.circuitQry.CIRCUIT_ID#, #stationQry.STATION_ID#)"></span></td>
												</tr>
											</table>
										</th>
									</cfif>
								</cfloop>
							</cfif>
						</tr>
						<tr>
							<td class="circuit_util">
								<span class="ico-add" onclick="editShift(#prc.circuitQry.CIRCUIT_ID#, '')"></span>&nbsp;&nbsp;<a href="javascript:;" onclick="editShift(#prc.circuitQry.CIRCUIT_ID#, '')">Add Shift</a>
							</td>
							<cfif prc.detailStruct['avail_station_' & prc.circuitQry.CIRCUIT_ID].recordCount GT 0>
								<td>&nbsp;</td>
							</cfif>
							<cfif stationNR GT 0>
								<cfloop from="1" to="#stationNR#" step="1" index="j">
									<td>&nbsp;</td>
								</cfloop>
							</cfif>
						</tr>
						<cfif detailQry.recordCount GT 0>
							<cfquery name="shiftQry" dbtype="query">
								SELECT * FROM detailQry ORDER BY START_HOUR, START_MINUTE, END_HOUR, END_MINUTE
							</cfquery>
							<cfloop query="shiftQry" group="SHIFT_ID">
								<cfif shiftQry.SHIFT_ID NEQ "">
									<cfif shiftQry.START_HOUR EQ 0 OR Log10(shiftQry.START_HOUR) LT 1>
										<cfset shiftTime = '0' & shiftQry.START_HOUR>
									<cfelse>
										<cfset shiftTime = shiftQry.START_HOUR>
									</cfif>
									<cfif shiftQry.START_MINUTE EQ 0 OR Log10(shiftQry.START_MINUTE) LT 1>
										<cfset shiftTime &= ':0' & shiftQry.START_MINUTE>
									<cfelse>
										<cfset shiftTime &= ':' & shiftQry.START_MINUTE>
									</cfif>
									<cfset shiftTime &= ' - '>
									<cfif shiftQry.END_HOUR EQ 0 OR Log10(shiftQry.END_HOUR) LT 1>
										<cfset shiftTime &= '0' & shiftQry.END_HOUR>
									<cfelse>
										<cfset shiftTime &= shiftQry.END_HOUR>
									</cfif>
									<cfif shiftQry.END_MINUTE EQ 0 OR Log10(shiftQry.END_MINUTE) LT 1>
										<cfset shiftTime &= ':0' & shiftQry.END_MINUTE>
									<cfelse>
										<cfset shiftTime &= ':' & shiftQry.END_MINUTE>
									</cfif>
									<tr>
										<td class="circuit_util">
											<strong>#shiftTime#</strong><span class="ico-edit text" onclick="editShift(#prc.circuitQry.CIRCUIT_ID#, #shiftQry.SHIFT_ID#)"></span>
										</td>
										<cfif prc.detailStruct['avail_station_' & prc.circuitQry.CIRCUIT_ID].recordCount GT 0>
											<td>&nbsp;</td>
										</cfif>
										<cfif stationNR GT 0>
											<cfquery name="applicantQry" dbtype="query">
												SELECT * FROM shiftQry WHERE SHIFT_ID = #shiftQry.SHIFT_ID# ORDER BY ORDER_NUMBER
											</cfquery>
											<cfloop query="applicantQry" group="ORDER_NUMBER">
												<td <cfif rc.search_string eq "A_#applicantQry.applicant_id#">style="background-color:yellow"</cfif>>
													<cfset applicantName = applicantQry.APPLICANT_NAME>
													<cfset applicantRefNo =applicantQry.REFERENCE_ID>
													<cfif applicantQry.APPLICANT_ID EQ "">
														<cfset applicantName = 'Not Scheduled'>
													</cfif>
													<input type="hidden" name="applicant_id" value="#applicantQry.APPLICANT_ID#">
													<input type="hidden" name="circuit_id" value="#applicantQry.CIRCUIT_ID#">
													<input type="hidden" name="station_id" value="#applicantQry.STATION_ID#">
													<input type="hidden" name="shift_id" value="#applicantQry.SHIFT_ID#">
													<input type="text" class="combobox" name="applicant_name" value="#applicantName#">
													<span id="applicant_refNo" name="applicant_refNo">
														<cfif applicantQry.APPLICANT_ID NEQ "">
															(Ref.No.: #applicantRefNo#)
														</cfif>
													</span>
												</td>
											</cfloop>
										</cfif>
									</tr>
								</cfif>
							</cfloop>
						</cfif>
					</table>
					<br />
				</td>
			</tr>
		</table>
	</cfif>
</cfoutput>