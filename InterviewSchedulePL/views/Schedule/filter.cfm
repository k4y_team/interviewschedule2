<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<style type="text/css">
		body {
			overflow:hidden;
		}
		.ui-dialog-content {
			min-height: 0 !important;
		}
	</style>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<form name="filter" id="filter" action="#event.buildLink(prc.xeh.data)#" method="post" onsubmit="onSubmitFnc();return false;">
		<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 10px;">
			<tr>
				<td class="textLine_noBorder" nowrap="true">Session:</td>
				<td class="textLine_noBorder">
					<select name="sessionID" id="sessionID" class="large" onChange="reloadFilter();">
						<cfloop query="#prc.uiFilter.getSessionQuery()#">
							<option value="#prc.uiFilter.getSessionQuery().SESSION_ID#" <cfif prc.uiFilter.getSessionValue() EQ prc.uiFilter.getSessionQuery().SESSION_ID>selected</cfif>>
								#prc.uiFilter.getSessionQuery().SESSION_NAME#
							</option>
						</cfloop>
					</select>
				</td>
				<cfif prc.uiFilter.getDayQuery().recordCount GT 0>
					<td class="textLine_right_noBorder" width="60" nowrap="true" align="right">Day:</td>
					<td class="textLine_noBorder">
						<select name="dayID" id="dayID" class="xlarge" onChange="reloadFilter();">
							<option value="">All Days</option>
							<cfloop query="#prc.uiFilter.getDayQuery()#">
								<option value="#prc.uiFilter.getDayQuery().DAY_ID#" <cfif prc.uiFilter.getDayValue() EQ prc.uiFilter.getDayQuery().DAY_ID>selected</cfif>>
									#prc.uiFilter.getDayQuery().DAY_NAME# / #DateFormat(prc.uiFilter.getDayQuery().DAY_DT, application.cbcontroller.getSetting('formatStruct').DateFormat)#
								</option>
							</cfloop>
						</select>
					</td>
				<cfelse>
					<input type="hidden" id="dayID" name="dayID" value="">
				</cfif>
				<cfif prc.uiFilter.getCircuitQuery().recordCount GT 0 AND prc.uiFilter.getDayValue() NEQ ''>
					<td class="textLine_right_noBorder" width="60" nowrap="true" align="right">Circuit:</td>
					<td class="textLine_noBorder">
						<select name="circuitID" id="circuitID" class="xlarge" onChange="submitFilter();">
							<option value="">All Circuits</option>
							<cfloop query="#prc.uiFilter.getCircuitQuery()#">
								<option value="#prc.uiFilter.getCircuitQuery().CIRCUIT_ID#" <cfif prc.uiFilter.getCircuitValue() EQ prc.uiFilter.getCircuitQuery().CIRCUIT_ID>selected</cfif>>
									#prc.uiFilter.getCircuitQuery().CIRCUIT_NAME#
								</option>
							</cfloop>
						</select>
					</td>
				<cfelse>
					<input type="hidden" id="circuitID" name="circuitID" value="">
				</cfif>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" style="background-color:##dadada;width:100%;border:1px solid ##426CB4">
			<tr>
				<cfif prc.userAccess eq "READ-WRITE">
					<td style="height:40px;padding-left:10px;width:130px" valign="middle">
						<div id="radio">
							<input type="radio" id="schedule_mode_setup" name="schedule_mode" <cfif rc.schedule_mode eq "setup">checked="checked"</cfif> onclick="switchToSetupMode()" value="setup"><label for="schedule_mode_setup">Setup</label><input type="radio" id="schedule_mode_details" name="schedule_mode" <cfif rc.schedule_mode eq "details">checked="checked"</cfif> onclick="switchToDetailsMode()" value="details"><label for="schedule_mode_details">Details</label>
						</div>
					</td>
				</cfif>
				<td class="textLine_noBorder" style="vertical-align:middle;width:50px;padding-left:10px;height:40px;">
					Search:
				</td>
				<td valign="middle">
					<k4y:autocomplete
						name="interviewer"
						value="#rc.interviewer#"
						placeholder="Search for applicants, interviewers, rooms"
						hiddenName="search_string"
						hiddenValue="#rc.search_string#"
						style="height:18px;width:250px;"
						requestURL="#event.buildLink(linkTo=prc.xeh.getData4Search)#?interviewScheduleID=#rc.interviewScheduleID#&dayID=#rc.dayID#&circuitID=#rc.circuitID#"
						minLen="1"/>
				</td>
				<td align="right" valign="middle" style="padding-right:10px">
					<input type="button" name="ExportBtn" id="ExportBtn" class="load" value="Export" onclick="exportSchedule()">
					<cfif rc.schedule_mode eq "setup">
						<input type="button" name="ScheduleBtn" class="submitLarge" onclick="showScheduleAllDlg()" value="Schedule Applicants">
						<input type="button" name="addNew" class="altContact" onclick="createCircuit()" value="Create Circuit">
					<cfelse>
						<input type="button" name="PrintBtn" class="print" value="Print" onclick="window.print()">
					</cfif>
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		(function( $ ) {
			$.ajaxSetup({
			    // Disable caching of AJAX responses
			    cache: false
			});
			$.widget( "custom.combobox", {
				_create: function() {
					this.wrapper = $( "<span>" )
						.addClass( "custom-combobox" )
						.insertAfter( this.element );

					this.element.hide();
					this._createAutocomplete();
					this._createShowAllButton();
				},
				_createAutocomplete: function() {
					value = $(this.element.get()).val();

					this.input = $( "<input>" )
						.appendTo( this.wrapper )
						.val( value )
						.attr( "title", "" )
						.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
						.autocomplete({
							delay: 0,
							minLength: 0,
							source: $.proxy( this, "_source" )
						})
						.tooltip({
							tooltipClass: "ui-state-highlight"
						});
					this._on( this.input, {
						autocompleteselect: function( event, ui ) {
							$(event.currentTarget).closest('td').find('input[name="applicant_id"]').val(ui.item.id);
							$(this.element.get()).val(ui.item.value);
							this.input.blur();
							$(event.currentTarget).closest('td').find('span[id="applicant_refNo"]').html('(Ref.No.: '+ui.item.referenceId+')');
							// schedule applicant
							$.ajax({
								type: "POST",
								url: "#event.buildLink(prc.xeh.schedule)#",
								data: $(event.currentTarget).closest('td').find('input').serialize(),
								dataType: "json",
								success: function(data) {
									if (!data.SUCCESS) {
										submitFilter();
										$(event.currentTarget).val('Not Scheduled');
										$(event.currentTarget).closest('td').find('input[name="applicant_id"]').val('');
										$(event.currentTarget).closest('td').find('span[id="applicant_refNo"]').html('');
									}
								}
							});
						},
						autocompletechange: "_removeIfInvalid"
					});
				},
				_createShowAllButton: function() {
					var input = this.input,
					wasOpen = false;

					$( "<a>" )
						.attr( "tabIndex", -1 )
						.appendTo( this.wrapper )
						.button({
							icons: {
								primary: "ui-icon-triangle-1-s"
							},
							text: false
						})
						.removeClass( "ui-corner-all" )
						.addClass( "custom-combobox-toggle ui-corner-right" )
						.mousedown(function() {
							wasOpen = input.autocomplete( "widget" ).is( ":visible" );
						})
						.click(function() {
							input.focus();
							// Close if already visible
							if ( wasOpen ) {
								return;
							}
							// Pass empty string as value to search for, displaying all results
							input.autocomplete( "search", "" );
						});
				},
				_source: function( request, response ) {
					that = $(this);
					$.getJSON(
						"#event.buildLink(linkTo=prc.xeh.getApplicants)#?interviewScheduleID=#rc.interviewScheduleID#&dayID=#rc.dayID#&circuitID=#rc.circuitID#"
						, $.param(request)
						, function (data) {
							if (data.length === 0) {
								that[0].bindings.addClass('red');
							} else {
								that[0].bindings.removeClass('red');
							}
							response(data);
						}
					);
				},
				_removeIfInvalid: function( event, ui ) {
					$(event.currentTarget).removeClass('red');
					if (!ui.item) {
						if ($(event.currentTarget).val() === '') {
							if ($(event.currentTarget).closest('td').find('input[name="applicant_id"]').val() !== '') {
								// un schedule applicant
								$.ajax({
									type: "POST",
									url: "#event.buildLink(prc.xeh.unSchedule)#",
									data: $(event.currentTarget).closest('td').find('input[name="applicant_id"]').serialize(),
									dataType: "json",
									success: function(data) {
										if (!data.SUCCESS) {
											submitFilter();
											$(event.currentTarget).val('Not Scheduled');
											//$(event.currentTarget).closest('td').find('input[name="applicant_id"]').val('');
											//$(event.currentTarget).closest('td').find('span[id="applicant_refNo"]').html('');
										} else {

										}
									}
								});
							}
						} else {
							$(event.currentTarget).val($(this.element.get()).val());
						}
					}
				},
				_destroy: function() {
					this.wrapper.remove();
					this.element.show();
				}
			});
		})( jQuery );

		var content = '';
		var circuitsArr = [];
		var currentCircuitIdx = 0;
		var nCircuitsLoading = 0;
		$(function(){
			$(".content").scroll(function() {
				if ($(this).outerHeight() >= ($(this).get(0).scrollHeight - $(this).scrollTop())) {
					if (nCircuitsLoading == 0) {
						for (var i=0;i<3;i++) {
							if (currentCircuitIdx < circuitsArr.length-1) {
								currentCircuitIdx++;
								loadCircuit(circuitsArr[currentCircuitIdx].id,'');
							}
						}
					}
				}
			});
			$( "##radio" ).buttonset();
			$( "##interviewer" ).keyup(function() {
				if($("##search_string").val() != '' && $("##interviewer" ).val() == '')
				{
					$( "##interviewer" ).val('');
					setTimeout(function() {
						submitFilter();
					}, 100);
				}
			});
			$("##interviewer").autocomplete("option", {
				select: function(event, ui) {
					if (ui.item.id == 0) {
						event.preventDefault();
					} else {
						$("##search_string").val(ui.item.id).change();
						setTimeout(function() {
							submitFilter();
						}, 100);
					}
				}
			});
			$("##interviewer").bind('blur', function(e) {
				if ($("##interviewer").val() == "" && $('##search_string').val() != '') {
					autocompleteClean();
					setTimeout(function() {
						submitFilter();
					}, 100);
				}
			});
		});
		function exportSchedule() {
			$.fileDownload('#event.buildLink(linkTo=prc.xeh.export)#', {
           		dialogOptions: { modal: true, title: 'Export Schedule Data', height: 'auto', resizable: false },
		        preparingMessageHtml: "<div style='margin-top: 20px; margin-bottom: 20px; text-align:center;'>We are preparing your export, please wait...</div>",
		        failMessageHtml: "<div style='margin-top: 20px; margin-bottom: 20px; text-align:center;'>There was a problem generating your excel file, please try again.</div>",
		        httpMethod: "POST",
		        data: $('##filter').serialize()
		    });
		}
		function onSubmitFnc() {
			if ($("##interviewer").val() == '') {
				$("##search_string").val($("##interviewer").val());
			}
			submitFilter();
		}
		function switchToSetupMode() {
			reloadFilter();
		}
		function switchToDetailsMode() {
			reloadFilter();
		}
		function reloadFilter() {
			$('.filter').load(
				'#event.buildLink(linkTo=prc.xeh.filter)#',
				$('##filter').serialize(),
				function() {
					submitFilter();
				}
			);
		}
		function submitFilter() {
			bCircuitLoading = false;
			$('.content').scrollTop(0);
			if ($("##interviewer").val() == '') {
				$("##search_string").val($("##interviewer").val());
			}
			$('.content').html('');
			if (content != '')
				content.abort();

			content = $.ajax({
				type: "POST",
				url: '#event.buildLink(linkTo=prc.xeh.data)#',
				data: $('##filter').serialize(),
				dataType: "json",
				success: function(data) {
					circuitsArr = data;
					if (circuitsArr.length > 0) {
						currentCircuitIdx = 0;
						nCircuitsLoading = 0;
						for (var i=0;i<5;i++) {
							if (i <= circuitsArr.length-1) {
								currentCircuitIdx = i;
								loadCircuit(circuitsArr[currentCircuitIdx].id,'');
							}
						}
					} else {
						$('.content').html('<div style="font-size:12px">No interview schedule data found for the selected search criteria.</div>');
					}
				}
			});
		}
		function loadCircuit(circuit_id,label) {
			nCircuitsLoading++;
			if ($('##circ' + circuit_id).length == 0) {
				$('.content').append('<div id="circ' + circuit_id + '" class="circuit_container"><div class="textLine_noBorder"></div></div>');
				if ($('.loading').length == 0) {
					$('##circ' + circuit_id).addClass('loading').html('Loading...');
				}
			}
			var urlParam = new Object();
			urlParam.search_string = $('##search_string').val();
			urlParam.schedule_mode = $('input:radio[name="schedule_mode"]:checked').val();
			urlParam.circuitID = circuit_id;
			$.ajax({
				async: true,
				type: "GET",
				url: '#event.buildLink(linkTo=prc.xeh.content)#',
				data: urlParam,
				success: function(data) {
					var clicky;
				    $(document).mousedown(function(e) {
				        clicky = $(e.target);
				    });
					$('##circ' + circuit_id).html(data);
					$('##circ' + circuit_id).removeClass("loading");
					nCircuitsLoading--;
					adjustViewportSize();
					$('##circ' + circuit_id + ' .combobox').combobox();
					$('##circ' + circuit_id + ' .custom-combobox-input').on('focus', function(){
						$('##circ' + circuit_id + ' .custom-combobox').removeClass('focus');
						$(this).closest('td').find('.custom-combobox').addClass('focus');
						if ($(this).val() === 'Not Scheduled')
							$(this).val('');
					});
					$('##circ' + circuit_id + ' .custom-combobox-input').on('blur', function(ev){
						if (clicky.closest("a.custom-combobox-toggle:visible").length === 0) {
							$(this).closest('td').find('.custom-combobox').removeClass('focus');
							if ($(this).val() === '')
								$(this).val('Not Scheduled');
								if($(this).val() === 'Not Scheduled') {
									$(this).closest('td').find('span[id="applicant_refNo"]').html('');
								}

						}
					});
				    $('##circ' + circuit_id + ' .toggle').click(function() {
						$('##circ' + circuit_id + ' .combobox').toggle();
				    });
					$('##circ' + circuit_id + ' .schedule_table').dragtable({
						dragaccept:'.accept',
						dragHandle:'.handle',
						beforeStop: function(table) {
							// save station order
							$.ajax({
								type: "POST",
								url: "#event.buildLink(prc.xeh.reorder)#",
								data: table.el.find('th input[name="station_id"]').serialize(),
								dataType: "json",
								success: function(data) {
									if (!data.SUCCESS) {
										submitFilter();
									}
								}
							});
						}
					});
				}
			});
		}
		function createCircuit() {
			var $dialogTitle = 'Create Circuit';
			createDialog("#event.buildLink(linkTo=prc.xeh.createCircuit, queryString='interviewScheduleID=#rc.interviewScheduleID#&circuitID=')#", {
				title: $dialogTitle,
				width: 500,
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
		function showScheduleAllDlg() {
			var $dialogTitle = 'Schedule Applicants ' + $('##sessionID option:selected').html();
			if ($('##dayID').val() !== '') {
				$dialogTitle += '; ' + $('##dayID option:selected').html();
			}
			if ($('##circuitID').val() !== '') {
				$dialogTitle += '; ' + $('##circuitID option:selected').html();
			}
			createDialog("#event.buildLink(prc.xeh.scheduleAll)#?interviewScheduleID=#rc.interviewScheduleID#&dayID=" + $('##dayID').val() + "&circuitID=" + $('##circuitID').val(), {
				title: $dialogTitle,
				width: 450,
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
		function adjustViewportSize() {
			var windowWidth = $('.title_bar:first').width();
			var windowHeight = $(window).height();
			$('.main_container td:first').css('height',windowHeight - 40 + 'px');
			$('.filter').css({'width':windowWidth + 'px'});
			$('.content').css({'height': windowHeight - 170 + 'px','width':windowWidth + 'px'});
		}
		function reloadPage() {
			submitFilter();
		}
		function editCircuit(circuitID) {
			var $dialogTitle = 'Edit Circuit';
			createDialog("#event.buildLink(linkTo=prc.xeh.editCircuit, queryString='interviewScheduleID=#rc.interviewScheduleID#&circuitID=')#" + circuitID, {
				title: $dialogTitle,
				width: 500,
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}

		function editShift(circuitID, shiftID) {
			if (shiftID === '') {
				$dialogTitle = 'Add Shift';
			} else {
				$dialogTitle = 'Edit Shift';
			}
			createDialog("#event.buildLink(linkTo=prc.xeh.editShift, queryString='interviewScheduleID=#rc.interviewScheduleID#&circuitID=')#" + circuitID + "/shiftID/" + shiftID, {
				title: $dialogTitle,
				width: 510,
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
		function editStation(circuitID, stationID) {
			if (stationID === '') {
				$dialogTitle = 'Add Station';
			} else {
				$dialogTitle = 'Edit Station';
			}
			createDialog("#event.buildLink(linkTo=prc.xeh.editStation, queryString='interviewScheduleID=#rc.interviewScheduleID#&circuitID=')#" + circuitID + "/stationID/" + stationID, {
				title: $dialogTitle,
				width: 450,
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
	</script>
</cfoutput>