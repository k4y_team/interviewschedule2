<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	#runEvent(event=prc.xeh.tabs, eventArguments=prc)#
	<table class="body_tabs" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 10px 10px 0px 10px;">
				<div class="filter"></div>
			</td>
		</tr>
		<tr>
			<td class="padding10">
				<div class="content" style="overflow:auto;"></div>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
		function reloadFilter() {
			$('.filter').load(
				'#event.buildLink(prc.xeh.filter)#?interviewer=#URLEncodedFormat(rc.interviewer)#&search_string=#rc.search_string#',
				function() {
					submitFilter();
				}
			);
		}
		
		$(document).ready(function() {
			reloadFilter();
		});
	</script>
</cfoutput>