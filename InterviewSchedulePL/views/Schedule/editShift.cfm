<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<form name="shiftForm" id="shiftForm" action="#event.buildLink(prc.xeh.save)#" method="post">
		<input type="hidden" name="shift_id" id="shift_id" value="#prc.ShiftData.getSHIFT_ID()#">
		<input type="hidden" name="interview_schedule_id" id="interview_schedule_id" value="#prc.ShiftData.getINTERVIEW_SCHEDULE_ID()#">
		<input type="hidden" name="circuit_id" id="circuit_id" value="#prc.ShiftData.getCIRCUIT_ID()#">
		<input type="hidden" name="day_name" id="day_name" value="#prc.ShiftData.getDAY_NAME()#">
		<input type="hidden" name="circuit_name" id="circuit_name" value="#prc.ShiftData.getCIRCUIT_NAME()#">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="textLine_noBorder" width="80">Session:</td>
				<td class="textLine_noBorder xlarge">
					<input type="hidden" name="sessionID" id="sessionID" value="#prc.sessionQry.SESSION_ID#">
					<input type="text" class="large" name="session_name" id="session_name" value="#prc.sessionQry.SESSION_NAME#" disabled>
				</td>
				<td class="textLine_noBorder" width="60">Day:</td>
				<td class="textLine_noBorder">
					<input type="text" class="xlarge" name="day_name" id="day_name" value="#prc.ShiftData.getDAY_NAME()#" disabled>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Circuit:</td>
				<td class="textLine_noBorder" colspan="3">
					<input type="text" class="large" name="circuit_name" id="circuit_name" value="#prc.ShiftData.getCIRCUIT_NAME()#" disabled>
				</td>
			</tr>
			<tr>
				<td colspan="4"><hr></td>
			</tr>
			<tr>
				<td class="textLine_noBorder" width="80">Start:</td>
				<td class="textLine_noBorder xsmall" colspan="3">
					<select name="start_hour" id="start_hour" class="xsmall">
						<cfloop query="prc.hourQry">
							<option value="#prc.hourQry.ID#" <cfif prc.ShiftData.getSTART_HOUR() EQ prc.hourQry.id>selected<cfelseif prc.ShiftData.getSTART_HOUR() eq "" and prc.hourQry.VALUE eq 9>selected</cfif>>#NumberFormat(prc.hourQry.VALUE,"00")#</option>
						</cfloop>
					</select>&nbsp;:&nbsp;
					<select name="start_minute" id="start_minute" class="xsmall">
						<cfloop query="prc.minutesQry">
							<option value="#prc.minutesQry.ID#" <cfif prc.ShiftData.getSTART_MINUTE() EQ prc.minutesQry.id>selected</cfif>>#NumberFormat(prc.minutesQry.VALUE,"00")#</option>
						</cfloop>
					</select>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder" width="80">End:</td>
				<td class="textLine_noBorder" colspan="3">
					<select name="end_hour" id="end_hour" class="xsmall">
						<cfloop query="prc.hourQry">
							<option value="#prc.hourQry.ID#" <cfif prc.ShiftData.getEND_HOUR() EQ prc.hourQry.id>selected<cfelseif prc.ShiftData.getEND_HOUR() eq "" and prc.hourQry.VALUE eq 10>selected</cfif>>#NumberFormat(prc.hourQry.VALUE,"00")#</option>
						</cfloop>
					</select>&nbsp;:&nbsp;
					<select name="end_minute" id="end_minute" class="xsmall">
						<cfloop query="prc.minutesQry">
							<option value="#prc.minutesQry.ID#" <cfif prc.ShiftData.getEND_MINUTE() EQ prc.minutesQry.id>selected</cfif>>#NumberFormat(prc.minutesQry.VALUE,"00")#</option>
						</cfloop>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="4"><hr></td>
			</tr>
			<tr>
				<td class="padding5" align="right" colspan="4">
					<input type="button" class="save" name="Save" value="Save" onclick="saveShift()">
					<cfif prc.ShiftData.getSHIFT_ID() NEQ ''>
						<input type="button" class="delete" name="Delete" value="Delete" onclick="deleteShift(#prc.ShiftData.getSHIFT_ID()#);">
					</cfif>
					<input type="button" class="cancel" name="Cancel" value="Cancel" onclick="closeDialog();">
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		function saveShift() {
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##shiftForm').serialize(),
	          	success: function(data){
					if (data == "") {
						closeDialog();
					} else {
						var $parentContainer = $('##shiftForm').parents()[0];
						$($parentContainer).html(data);
					}
					loadCircuit($('##circuit_id').val(),'');
	           	}
	        });
	  	};
		<cfif prc.ShiftData.getSHIFT_ID() NEQ ''>
			function deleteShift(stationID) {
				$.ajax({
					type: "POST",
					url: "#event.buildLink(prc.xeh.delete)#",
					data: $('##circuit_id').serialize() + '&' + $('##shift_id').serialize(),
					dataType: "json",
					success: function(data) {
						if(data == true){
							loadCircuit($('##circuit_id').val(),'');
							closeDialog();
						} else {
							refreshDialog();
						}
					}
				});
			}
		</cfif>
	</script>
</cfoutput>