<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<cfif FindNoCase("A_",rc.search_string)>
		<cfset scheduleDetailsQry = prc.schedule>
		<table class="schedule_table print_view" cellpadding="0" cellspacing="0">
			<tr>
				<td class="circuit_util" width="100" style="<cfif scheduleDetailsQry.COLOR NEQ ''>background-color: ###scheduleDetailsQry.COLOR#;</cfif> font-weight:bold;">
					#scheduleDetailsQry.day_name# / #DateFormat(scheduleDetailsQry.day_dt,"dd-mmm-yyyy")#<br>
					#scheduleDetailsQry.CIRCUIT_NAME#
				</td>
				<cfloop query="scheduleDetailsQry">
					<td class="circuit_util left" width="200">
						Competency: #scheduleDetailsQry.COMPETENCY_NAME#<br />
						Room: 
						<cfif scheduleDetailsQry.room NEQ ''>
							#scheduleDetailsQry.room#
						<cfelse>
							<span class="red">Not Selected</span>
						</cfif><br />
						Question ID: #scheduleDetailsQry.question_id#<br />
						Interviewer: 
						<cfif scheduleDetailsQry.interviewer_fname NEQ ''>
							#scheduleDetailsQry.interviewer_fname# #scheduleDetailsQry.interviewer_lname#
						<cfelse>
							<span style="color:red">Not Selected</span>
						</cfif>
					</td>
				</cfloop>
			</tr>
			<cfset shiftID = "">
			<cfset shiftBreakDownID = "">
			<cfloop query="scheduleDetailsQry">
				<cfif shiftID neq scheduleDetailsQry.shift_id>
					<cfif shiftID neq "">
						</tr>
					</cfif>
					<tr>
				</cfif>
				<cfif shiftID neq scheduleDetailsQry.shift_id>
					<td>
						#scheduleDetailsQry.applicant_fname# #scheduleDetailsQry.applicant_lname# 
					</td>
				</cfif>
				<cfif shiftBreakDownID neq "#scheduleDetailsQry.start_hour##scheduleDetailsQry.start_minute#">
					<cfset st_hour = #scheduleDetailsQry.start_hour#>
					<cfset st_min = #scheduleDetailsQry.start_minute#>
					<cfset e_hour = #scheduleDetailsQry.end_hour#>
					<cfset e_min = #scheduleDetailsQry.end_minute#>
					<cfif st_hour lt 10>
						<cfset st_hour = '0'&'#st_hour#'>
					</cfif>
					<cfif st_min lt 10>
						<cfset st_min = '0'&'#st_min#'>
					</cfif>
					<cfif e_hour lt 10>
						<cfset e_hour = '0'&'#e_hour#'>
					</cfif>
					<cfif e_min lt 10>
						<cfset e_min = '0'&'#e_min#'>
					</cfif>
					<td>
						#st_hour#:#st_min#-#e_hour#:#e_min# 
					</td>
				</cfif>
				<cfset shiftID = scheduleDetailsQry.shift_id>
				<cfset shiftBreakDownID = "#scheduleDetailsQry.start_hour##scheduleDetailsQry.start_minute#">
			</cfloop>
			</tr>
		</table>				
	<cfelseif FindNoCase("I_",rc.search_string)>
		<cfset scheduleDetailsQry = prc.schedule>
		<cfset idx = 1>
		<cfloop query="prc.schedule" group="circuit_id">	
		<cfquery name="oneCircuitQry" dbtype="query">
			select distinct *
			from
				scheduleDetailsQry
			where
				scheduleDetailsQry.circuit_id = #scheduleDetailsQry.circuit_id#
		</cfquery>
		<cfset tblClasses = "schedule_table print_view">
		<cfif idx neq 1>
			<cfset tblClasses = tblClasses & " page-break-before">
		</cfif>
		<table class="#tblClasses#" cellpadding="0" cellspacing="0">
			<tr>
				<td class="circuit_util" width="100" colspan="2" style="<cfif oneCircuitQry.COLOR NEQ ''>background-color: ###oneCircuitQry.COLOR#;</cfif> font-weight:bold;">
					#oneCircuitQry.day_name# / #DateFormat(oneCircuitQry.day_dt,"dd-mmm-yyyy")#<br>
					#oneCircuitQry.CIRCUIT_NAME#
					<br />
				</td>
				<td class="circuit_util left" width="200">
					Competency: #oneCircuitQry.competency_name#<br />
					Room: 
					<cfif oneCircuitQry.room NEQ ''>
						#oneCircuitQry.room#
					<cfelse>
						<span class="red">Not Selected</span>
					</cfif><br />
					Question ID: #oneCircuitQry.question_id#<br />
					Interviewer: 
					<cfif oneCircuitQry.interviewer_fname NEQ ''>
						#oneCircuitQry.interviewer_fname# #oneCircuitQry.interviewer_lname#
					<cfelse>
						<span style="color:red">Not Selected</span>
					</cfif>					
				</td>
			</tr>
			<cfset shiftID = "">
			<cfset shiftBreakDownID = "">
			<cfloop query="oneCircuitQry">
				<cfif shiftID neq oneCircuitQry.shift_id or shiftBreakDownID neq "#oneCircuitQry.start_hour##oneCircuitQry.start_minute#">
					<cfif shiftID neq "">
						</tr>
					</cfif>
					<tr>
				</cfif>
				<cfif shiftID neq oneCircuitQry.shift_id>
					<cfquery name="shiftQry" dbtype="query">
						select distinct
							oneCircuitQry.start_hour,
							oneCircuitQry.start_minute
						from oneCircuitQry
						where 
							oneCircuitQry.shift_id = #oneCircuitQry.shift_id#
					</cfquery>
					<td rowspan="#shiftQry.recordcount#">
						<cfset sh_start_hour = #oneCircuitQry.shift_start_hour#>
						<cfset sh_start_min = #oneCircuitQry.shift_start_minute#>
						<cfset sh_end_hour = #oneCircuitQry.shift_end_hour#>
						<cfset sh_end_min = #oneCircuitQry.shift_end_minute#>
						<cfif sh_start_hour lt 10>
							<cfset sh_start_hour = '0'&'#shift_start_hour#'>
						</cfif>
						<cfif sh_start_min lt 10>
							<cfset sh_start_min = '0'&'#sh_start_min#'>
						</cfif>
						<cfif sh_end_hour lt 10>
							<cfset sh_end_hour = '0'&'#sh_end_hour#'>
						</cfif>
						<cfif sh_end_min lt 10>
							<cfset sh_end_min = '0'&'#sh_end_min#'>
						</cfif>
							#sh_start_hour#:#sh_start_min#-#sh_end_hour#:#sh_end_min# 
					</td>
				</cfif>
				<cfif shiftBreakDownID neq "#oneCircuitQry.start_hour##oneCircuitQry.start_minute#">
					<cfset st_hour = #oneCircuitQry.start_hour#>
					<cfset st_min = #oneCircuitQry.start_minute#>
					<cfset e_hour = #oneCircuitQry.end_hour#>
					<cfset e_min = #oneCircuitQry.end_minute#>
					<cfif st_hour lt 10>
						<cfset st_hour = '0'&'#st_hour#'>
					</cfif>
					<cfif st_min lt 10>
						<cfset st_min = '0'&'#st_min#'>
					</cfif>
					<cfif e_hour lt 10>
						<cfset e_hour = '0'&'#e_hour#'>
					</cfif>
					<cfif e_min lt 10>
						<cfset e_min = '0'&'#e_min#'>
					</cfif>
					<td>
						#st_hour#:#st_min#-#e_hour#:#e_min# 
					</td>
				</cfif>
				<td>
					#oneCircuitQry.applicant_fname# #oneCircuitQry.applicant_lname#
				</td>
				<cfset shiftID = oneCircuitQry.shift_id>
				<cfset shiftBreakDownID = "#oneCircuitQry.start_hour##oneCircuitQry.start_minute#">
			</cfloop>
			</tr>
		</table>
		<cfset idx++>
		<br/>
		</cfloop>
	<cfelse>
		<cfloop from="1" to="#ArrayLen(prc.detailsArr)#" index="idx">
			<cfset prc.schedule = prc.detailsArr[idx]>
			<cfset tblClasses = "schedule_table print_view">
			<cfif idx neq 1>
				<cfset tblClasses = tblClasses & " page-break-before">
			</cfif>
			<table class="#tblClasses#" cellpadding="0" cellspacing="0">
				<tr>
					<td class="circuit_util" colspan="2" style="<cfif prc.schedule.COLOR NEQ ''>background-color: ###prc.schedule.COLOR#;</cfif> font-weight:bold;">
						#prc.schedule.day_name# / #DateFormat(prc.schedule.day_dt,"dd-mmm-yyyy")#<br>
						#prc.schedule.CIRCUIT_NAME#
					</td>
					<cfset scheduleDetailsQry = prc.schedule>
					<cfquery name="competenciesQry" dbtype="query">
						select distinct 
							scheduleDetailsQry.competency_name,
							scheduleDetailsQry.room,
							scheduleDetailsQry.question_id,
							scheduleDetailsQry.interviewer_id,
							scheduleDetailsQry.interviewer_lname,
							scheduleDetailsQry.interviewer_fname
						from
							scheduleDetailsQry
						order by
							scheduleDetailsQry.order_number		
					</cfquery>
					<cfloop query="competenciesQry">
						<cfif not FindNoCase("R_",rc.search_string) or "R_#competenciesQry.room#" eq rc.search_string>
						<td class="circuit_util left" style="200px">
							Competency: #competenciesQry.COMPETENCY_NAME#<br />
							Room: 
							<cfif competenciesQry.room NEQ ''>
								#competenciesQry.room#
							<cfelse>
								<span class="red">Not Selected</span>
							</cfif><br />
							Question ID: #competenciesQry.question_id#<br />
							Interviewer: 
							<cfif competenciesQry.interviewer_fname NEQ ''>
								#competenciesQry.interviewer_fname# #competenciesQry.interviewer_lname#
							<cfelse>
								<span style="color:red">Not Selected</span>
							</cfif>
						</td>
						</cfif>
					</cfloop>
				</tr>
				<cfset shiftID = "">
				<cfset shiftBreakDownID = "">
				<cfloop query="scheduleDetailsQry">
					<cfif not FindNoCase("R_",rc.search_string) or "R_#scheduleDetailsQry.room#" eq rc.search_string>								
						<cfif shiftID neq scheduleDetailsQry.shift_id or shiftBreakDownID neq "#scheduleDetailsQry.start_hour##scheduleDetailsQry.start_minute#">
							<cfif shiftID neq "">
								</tr>
							</cfif>
							<tr>
						</cfif>
						<cfif shiftID neq scheduleDetailsQry.shift_id>
							<cfquery name="shiftQry" dbtype="query">
								select distinct
									scheduleDetailsQry.start_hour,
									scheduleDetailsQry.start_minute
								from scheduleDetailsQry
								where 
									scheduleDetailsQry.shift_id = #scheduleDetailsQry.shift_id#
							</cfquery>
							<td rowspan="#shiftQry.recordcount#" style="width:90px;">
								<cfset sh_start_hour = #scheduleDetailsQry.shift_start_hour#>
								<cfset sh_start_min = #scheduleDetailsQry.shift_start_minute#>
								<cfset sh_end_hour = #scheduleDetailsQry.shift_end_hour#>
								<cfset sh_end_min = #scheduleDetailsQry.shift_end_minute#>
								<cfif sh_start_hour lt 10>
									<cfset sh_start_hour = '0'&'#shift_start_hour#'>
								</cfif>
								<cfif sh_start_min lt 10>
									<cfset sh_start_min = '0'&'#sh_start_min#'>
								</cfif>
								<cfif sh_end_hour lt 10>
									<cfset sh_end_hour = '0'&'#sh_end_hour#'>
								</cfif>
								<cfif sh_end_min lt 10>
									<cfset sh_end_min = '0'&'#sh_end_min#'>
								</cfif>
									#sh_start_hour#:#sh_start_min#-#sh_end_hour#:#sh_end_min# 
							</td>
						</cfif>
						<cfif shiftBreakDownID neq "#scheduleDetailsQry.start_hour##scheduleDetailsQry.start_minute#">
							<cfset st_hour = #scheduleDetailsQry.start_hour#>
							<cfset st_min = #scheduleDetailsQry.start_minute#>
							<cfset e_hour = #scheduleDetailsQry.end_hour#>
							<cfset e_min = #scheduleDetailsQry.end_minute#>
							<cfif st_hour lt 10>
								<cfset st_hour = '0'&'#st_hour#'>
							</cfif>
							<cfif st_min lt 10>
								<cfset st_min = '0'&'#st_min#'>
							</cfif>
							<cfif e_hour lt 10>
								<cfset e_hour = '0'&'#e_hour#'>
							</cfif>
							<cfif e_min lt 10>
								<cfset e_min = '0'&'#e_min#'>
							</cfif>
							<td style="width:90px;">
								#st_hour#:#st_min#-#e_hour#:#e_min# 
							</td>
						</cfif>
						<td>
							#scheduleDetailsQry.applicant_fname# #scheduleDetailsQry.applicant_lname#
						</td>
						<cfset shiftID = scheduleDetailsQry.shift_id>
						<cfset shiftBreakDownID = "#scheduleDetailsQry.start_hour##scheduleDetailsQry.start_minute#">
					</cfif>	
				</cfloop>
				</tr>
			</table>
			<br/>
		</cfloop>
	</cfif>
</cfoutput>