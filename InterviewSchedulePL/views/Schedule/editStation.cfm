<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<form name="stationForm" id="stationForm" action="#event.buildLink(prc.xeh.save)#" method="post">
		<input type="hidden" name="station_id" id="station_id" value="#prc.StationData.getSTATION_ID()#">
		<input type="hidden" name="circuit_id" id="circuit_id" value="#prc.StationData.getCIRCUIT_ID()#">
		<input type="hidden" name="day_id" id="day_id" value="#prc.StationData.getDAY_ID()#">
		<input type="hidden" name="day_name" id="day_name" value="#prc.StationData.getDAY_NAME()#">
		<input type="hidden" name="circuit_name" id="circuit_name" value="#prc.StationData.getCIRCUIT_NAME()#">
		<input type="hidden" name="order_number" value="#prc.StationData.getORDER_NUMBER()#">		
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="textLine_noBorder" width="80">Session:</td>
				<td class="textLine_noBorder xlarge">
					<input type="hidden" name="sessionID" id="sessionID" value="#prc.sessionQry.SESSION_ID#">
					<input type="text" class="large" name="session_name" id="session_name" value="#prc.sessionQry.SESSION_NAME#" disabled>
				</td>
				<td class="textLine_noBorder" width="60">Day:</td>
				<td class="textLine_noBorder">
					<input type="text" class="xlarge" name="day_name" id="day_name" value="#prc.StationData.getDAY_NAME()#" disabled>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Circuit:</td>
				<td class="textLine_noBorder" colspan="3">
					<input type="text" class="large" name="circuit_name" id="circuit_name" value="#prc.StationData.getCIRCUIT_NAME()#" disabled>
				</td>
			</tr>
			<tr>
				<td colspan="4"><hr></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Competency:</td>
				<td class="textLine_noBorder" colspan="3">
					<select name="competency_id" id="competency_id" class="xlarge">
						<cfif prc.competencyQry.recordcount eq 0>
							<option value="">-- N/A --</option>
						<cfelse>
							<cfloop query="prc.competencyQry">
								<option value="#prc.competencyQry.COMPETENCY_ID#" <cfif prc.StationData.getCOMPETENCY_ID() EQ prc.competencyQry.COMPETENCY_ID>selected</cfif>>#prc.competencyQry.COMPETENCY_NAME#</option>
							</cfloop>
						</cfif>
					</select>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Room:</td>
				<td class="textLine_noBorder" colspan="3">
					<input type="text" class="xlarge" name="room" id="room" value="#prc.StationData.getROOM()#">
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Question ID:</td>
				<td class="textLine_noBorder" colspan="3">
					<input type="text" class="xlarge" name="question_id" id="question_id" value="#prc.StationData.getQUESTION_ID()#">
				</td>
			</tr>
			<tr>
				<td colspan="4"><hr></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Interviewer:</td>
				<td class="textLine_noBorder" colspan="3">
					<select name="interviewer_id" id="interviewer_id" class="xlarge">
						<cfif prc.interviewerQry.recordcount eq 0>
							<option value="">-- N/A --</option>
						<cfelse>	
							<option value="">-- Select --</option>
							<cfloop query="prc.interviewerQry">
								<option value="#prc.interviewerQry.INTERVIEWER_ID#" <cfif prc.StationData.getINTERVIEWER_ID() EQ prc.interviewerQry.INTERVIEWER_ID>selected</cfif>>
									#prc.interviewerQry.LAST_NAME#, #prc.interviewerQry.FIRST_NAME# (#prc.interviewerQry.TYPE#)
								</option>
							</cfloop>
						</cfif>	
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="4"><hr></td>
			</tr>
			<tr>
				<td class="padding5" align="right" colspan="4">
					<input type="button" class="save" name="Save" value="Save" onclick="saveStation()">
					<cfif prc.StationData.getSTATION_ID() NEQ ''>
						<input type="button" class="delete" name="Delete" value="Delete" onclick="deleteStation(#prc.StationData.getSTATION_ID()#)">
					</cfif>
					<input type="button" class="cancel" name="Cancel" value="Cancel" onclick="closeDialog();">
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		function saveStation() {
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##stationForm').serialize(),
	          	success: function(data){
					if (data == "") {
						closeDialog();
					} else {
						var $parentContainer = $('##stationForm').parents()[0];
						$($parentContainer).html(data);
					}
					loadCircuit($('##circuit_id').val(),'');
	           	}
	        });
	  	};
		<cfif prc.StationData.getSTATION_ID() NEQ ''>
			function deleteStation(stationID) {
				$.ajax({
					type: "POST",
					url: "#event.buildLink(prc.xeh.delete)#",
					data: $('##station_id').serialize(),
					dataType: "json",
					success: function(data) {
						if(data == true){
							loadCircuit($('##circuit_id').val(),'');
							closeDialog();
						} else {
							refreshDialog();
						}
					}
				});
			}
		</cfif>
	</script>
</cfoutput>