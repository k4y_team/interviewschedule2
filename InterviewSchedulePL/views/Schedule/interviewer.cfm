<cfset scheduleDetailsQry = prc.schedule>
<cfoutput>
	<table  border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td class="textLine_noBorder_black" width="100%" nowrap><h3>Interview Schedule Station View, #scheduleDetailsQry.session_name#, #scheduleDetailsQry.interviewer_fname# #scheduleDetailsQry.interviewer_lname#</h3></td>
			<td class="textLine_right_noBorder" width="100%" nowrap>
				<input class="return" type="button" name="back" value="Back" onclick="window.location='#event.buildLink(prc.xeh.back)#'">
			</td>
		</tr>
	</table>
	
	<cfloop query="scheduleDetailsQry" group="circuit_id">
		<cfquery name="oneCircuitQry" dbtype="query">
			select distinct *
			from
				scheduleDetailsQry
			where
				scheduleDetailsQry.circuit_id = #scheduleDetailsQry.circuit_id#
		</cfquery>
		<table class="schedule_table print_view" cellpadding="0" cellspacing="0">
			<tr>
				<td class="circuit_util" width="100">
					<strong>
						#scheduleDetailsQry.day_name# / #DateFormat(scheduleDetailsQry.day_dt,"dd-mmm-yyyy")#<br>
					#scheduleDetailsQry.CIRCUIT_NAME#</strong>
						<br />
				</td>
				<td class="circuit_util" width="100">
					&nbsp;
				</td>
				
	
				<td class="circuit_util left" width="200">
					Competency: #oneCircuitQry.competency_name#<br />
					Room: #oneCircuitQry.room#<br />
					Question ID: #oneCircuitQry.question_id#
				</td>
			</tr>
			<cfset shiftID = "">
			<cfset shiftBreakDownID = "">
			<cfloop query="oneCircuitQry">
				<cfif shiftID neq oneCircuitQry.shift_id or shiftBreakDownID neq "#oneCircuitQry.start_hour##oneCircuitQry.start_minute#">
					<cfif shiftID neq "">
						</tr>
					</cfif>
					<tr>
				</cfif>
				<cfif shiftID neq oneCircuitQry.shift_id>
					<cfquery name="shiftQry" dbtype="query">
						select distinct
							oneCircuitQry.start_hour,
							oneCircuitQry.start_minute
						from oneCircuitQry
						where 
							oneCircuitQry.shift_id = #oneCircuitQry.shift_id#
					</cfquery>
					<td rowspan="#shiftQry.recordcount#">
						<cfset sh_start_hour = #oneCircuitQry.shift_start_hour#>
						<cfset sh_start_min = #oneCircuitQry.shift_start_minute#>
						<cfset sh_end_hour = #oneCircuitQry.shift_end_hour#>
						<cfset sh_end_min = #oneCircuitQry.shift_end_minute#>
						<cfif sh_start_hour lt 10>
							<cfset sh_start_hour = '0'&'#shift_start_hour#'>
						</cfif>
						<cfif sh_start_min lt 10>
							<cfset sh_start_min = '0'&'#sh_start_min#'>
						</cfif>
						<cfif sh_end_hour lt 10>
							<cfset sh_end_hour = '0'&'#sh_end_hour#'>
						</cfif>
						<cfif sh_end_min lt 10>
							<cfset sh_end_min = '0'&'#sh_end_min#'>
						</cfif>
							#sh_start_hour#:#sh_start_min#-#sh_end_hour#:#sh_end_min# 
					</td>
				</cfif>
				<cfif shiftBreakDownID neq "#oneCircuitQry.start_hour##oneCircuitQry.start_minute#">
					<cfset st_hour = #oneCircuitQry.start_hour#>
					<cfset st_min = #oneCircuitQry.start_minute#>
					<cfset e_hour = #oneCircuitQry.end_hour#>
					<cfset e_min = #oneCircuitQry.end_minute#>
					<cfif st_hour lt 10>
						<cfset st_hour = '0'&'#st_hour#'>
					</cfif>
					<cfif st_min lt 10>
						<cfset st_min = '0'&'#st_min#'>
					</cfif>
					<cfif e_hour lt 10>
						<cfset e_hour = '0'&'#e_hour#'>
					</cfif>
					<cfif e_min lt 10>
						<cfset e_min = '0'&'#e_min#'>
					</cfif>
					<td>
						#st_hour#:#st_min#-#e_hour#:#e_min# 
					</td>
				</cfif>
				<td>
					<cfif oneCircuitQry.applicant_id eq "">
						Not Scheduled
					<cfelse>	
						#oneCircuitQry.applicant_fname# #oneCircuitQry.applicant_lname#
					</cfif>	
				</td>
				<cfset shiftID = oneCircuitQry.shift_id>
				<cfset shiftBreakDownID = "#oneCircuitQry.start_hour##oneCircuitQry.start_minute#">
				
			</cfloop>
			</tr>
		</table>
		<br>
	</cfloop>
</cfoutput>