<cfoutput>
	#prc.MsgBox.renderIt()#
	<form name="scheduleAll" id="scheduleAll" action="#event.buildLink(prc.xeh.autoSchedule)#" method="post">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="textLine_noBorder">
				<span id="scheduledNr">#prc.scheduleInfo["SCHEDULED_APPLICANTS"]#</span> applicants scheduled.
			</td>
		</tr>
		<tr>
			<td class="textLine_noBorder">
				<span id="remainingNr" class="orange">#prc.scheduleInfo["REMAINING_APPLICANTS"]#</span> applicants remaining.
			</td>
		</tr>
		<tr>
			<td class="textLine_noBorder">
				<span id="availableNr" class="green">#prc.scheduleInfo["AVAILABLE_SPOTS"]#</span> spots available.
			</td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><hr></td>
		</tr>
		<tr>
			<td class="padding5" width="100%" align="right">
				<input type="hidden" name="interviewScheduleID" value="#rc.interviewScheduleID#">
				<input type="hidden" name="dayID" value="#rc.dayID#">
				<input type="hidden" name="circuitID" value="#rc.circuitID#">
				<cfif prc.scheduleInfo["AVAILABLE_SPOTS"] GT 0>
					<input type="button" class="submit" name="schedule" value="Schedule" onclick="runAutoSchedule();">
				</cfif>
				<input type="button" class="cancel" name="close" value="Close" onclick="closeDialog();">
			</td>
		</tr>
	</table>
	</form>
	<script type="text/javascript">
		function runAutoSchedule() {
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.autoSchedule)#",
	          	data: $('##scheduleAll').serialize(),
	          	success: function(data){
					var $parentContainer = $('##scheduleAll').parents()[0];
					$($parentContainer).html(data);
					submitFilter();
	           	}
	        });
	  	};	
	</script>
</cfoutput>