<cfoutput>
	#getMyPlugin("MsgBox").renderIt(header=true)#
	#getMyPlugin('HTMLHelper').renderTabs(codeList=prc.tabListCodes, xmlObj=prc.xmlContent, activeTab=prc.activeTab, loadInitial=false)#

	<script type="text/javascript" >
		$(function() {
			angular.bootstrap($("div.workplace"), ['tabBarApp']);
		});
	</script>
	<div class="manualLoaded">
	<form name="editForm" id="editForm" action="" method="POST" class="validation">
		<input id="INTERVIEWER_ID" type="hidden" value="#prc.interviewerData.getINTERVIEWER_ID()#" name="INTERVIEWER_ID">
		<input type="hidden" name="interview_schedule_id" value="#prc.interviewerData.getINTERVIEW_SCHEDULE_ID()#" />
	<div class="row-fluid no-marg-bot">
		<div class="span12">
			<div class="head clearfix">
				<div class="isw-edit">
				</div>
				<h1>Edit Interviewer</h1>
			</div>

			<div class="block-fluid clearfix">
				<div class="row-form clearfix no-border">
					<div class="span1">
						Registrant ID:<span class="mand">*</span>
					</div>
					<div class="span2">
						<input name="registrant_id" value="#prc.interviewerData.getREGISTRANT_ID()#" type="text" class="validate[required]" >
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						First Name:<span class="mand">*</span>
					</div>
					<div class="span2">
						<input name="first_name"value="#prc.interviewerData.getFIRST_NAME()#" type="text" class="validate[required]" >
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						Last Name:<span class="mand">*</span>
					</div>
					<div class="span2">
						<input name="last_name" value="#prc.interviewerData.getLAST_NAME()#" type="text" class="validate[required]" >
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						Email:<span class="mand">*</span>
					</div>
					<div class="span2">
						<input name="email" value="#prc.interviewerData.getEMAIL()#" type="text" class="validate[required,custom[email]]" >
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						Type:<span class="mand">*</span>
					</div>
					<div class="span2">
						<select name="TYPE" class="large">
							<option val="faculty" <cfif 'FACULTY' EQ prc.interviewerData.getTYPE()>selected</cfif>>Faculty</option>
							<option val="student" <cfif 'STUDENT' EQ prc.interviewerData.getTYPE()>selected</cfif>>Student</option>
							<option val="resident" <cfif 'RESIDENT' EQ prc.interviewerData.getTYPE()>selected</cfif>>Resident</option>
						</select>
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						Student ID:<span class="mand">*</span>
					</div>
					<div class="span2">
						<input name="student_id" value="#prc.interviewerData.getSTUDENT_ID()#" type="text" class="validate[required]" >
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						CPSO ##:<span class="mand">*</span>
					</div>
					<div class="span2">
						<input name="cpso" value="#prc.interviewerData.getCPSO()#" type="text" class="validate[required,numeric]" >
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						Department:<span class="mand">*</span>
					</div>
					<div class="span2">
						<input name="department" value="#prc.interviewerData.getDEPARTMENT()#" type="text" class="validate[required]" >
					</div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span1">
						Availability:<span class="mand">*</span>
					</div>
					<div class="span2">
						<cfset loopIndex = 1>
						<cfif prc.days.recordcount eq 0>
							<span style="color:red">Interview days not defined</span>
							<input type="hidden" name="AVAILABLE_DATES" value="">
						<cfelse>
						<cfloop query="prc.days">
							<cfset isChecked = false>
							<cfloop query="prc.availableDays">
								<cfif DateFormat(prc.days.DAY_DT,'ddmmyyyy') eq DateFormat(prc.availableDays.DAY_DT,'ddmmyyyy')>
									<cfset isChecked = true>
								</cfif>
							</cfloop>
							<label><input type="checkbox" name="AVAILABLE_DATES" id="day#loopIndex#" <cfif isChecked>checked</cfif> value="#DateFormat(prc.days.DAY_DT,'dd-mmm-yyyy')#">
								#prc.days.DAY_NAME# / #DateFormat(prc.days.DAY_DT,'dd-mmm-yyyy')#
							</label><br>
							<cfset loopIndex++>
						</cfloop>
						</cfif>
					</div>
				</div>

				<div class="footer">
					<button id="saveUser" type="button" class="btn btn-mini">
						<span class="ico-save"></span>Save
					</button>
					<cfif prc.interviewerData.getINTERVIEWER_ID() neq "">
						<button id="deleteUser" type="button" class="btn btn-mini">
							<span class="ico-delete"></span>Delete
						</button>
					</cfif>
					<button id="btnCancel" type="button" class="btn btn-mini">
						<span class="ico-cancel"></span>Cancel
					</button>
				</div>
			</div>
		</div>
	</div>
	</form>
	</div>

	<script type="text/javascript">
		$(function(){
			$("form.validation").validationEngine({promptPosition : "bottomLeft", scroll: true});
			$('##saveUser').on('click',function(ev){
				ev.preventDefault();
				if ($("form.validation").validationEngine("validate") ) {
					$.ajax({
						type: "POST",
						dataType: "json",
						url: '#event.buildLink(prc.xeh.save)#',
						data: $("##editForm").serialize() + '&ajaxSubmit=true',
						success: function(data) {
							drawMessage('##message-area', data.TYPE, data.MSGARRAY);
							refreshTab('Interviewers');
						}
					});
				}
			});
			$('##deleteUser').on('click',function(ev){
				ev.preventDefault();
				if(confirm("Are you sure you want to delete the details?")) {
					$('##editForm').attr('action', '#event.buildLink(prc.xeh.delete)#');
					$.ajax({
						type: "POST",
						dataType: "json",
						url: '#event.buildLink(prc.xeh.delete)#',
						data: $("##editForm").serialize() + '&ajaxSubmit=true',
						success: function(data) {
							drawMessage('##message-area', data.TYPE, data.MSGARRAY);
							refreshTab('Interviewers');
						}
					});
				}
			});
			$('##btnCancel').on('click', function(e) {
				e.preventDefault();
				refreshTab('Interviewers');
			});
		});

		function validateEmail($email){
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if (!emailReg.test($email)) {
				return false;
			}
			else {
				return true;
			}
		}
	</script>
</cfoutput>