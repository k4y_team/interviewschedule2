<cfimport prefix="k4y" taglib="/sis_core/cftags">
<cfoutput>
	<div class="row-fluid">
		<div class="span12">
			<div class="headInfo">
				<div class="row-fluid no-marg-bot">
					<div class="span1">Interview Schedule:</div>
					<div class="span2">
						<k4y:select
							name="filter_interviewScheduleID"
							id="interviewScheduleId"
							class="filterSelect"
							query="prc.interviewScheduleQry"
							keyField="interview_schedule_id"
							descField="interview_schedule_name"
						/>
					</div>
					<div class="span1">Status:</div>
					<div class="span2">
						<select name="filter_interviewerStatus" class="filterSelect" id="status">
							<option value="">-- All --</option>
							<option value="scheduled">Scheduled</option>
							<option value="not_scheduled">Not Scheduled</option>
						</select>
					</div>
				</div>
			</div>
			<div class="head clearfix marg-top10">
				<div class=" isw-list"></div>
				<h1>Interviewers</h1>
			</div>
			<div class="block-fluid table-sorting clearfix no-marg-bot">
				<form id="interviewFilter"></form>
				<table cellpadding="0" cellspacing="0" width="100%" class="table" id="interviewTbl">
				</table>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function createFilterData(){
		    let filterArray = [];

 			let filterStruct = {};
			filterStruct["name"] = $('##interviewScheduleId').prop("name");
			filterStruct["value"] = $('##interviewScheduleId').val();
			filterArray.push(filterStruct);
			filterStruct = {};
			filterStruct["name"] = $('##status').prop("name");
			filterStruct["value"] = $('##status').val();
			filterArray.push(filterStruct);

		    return filterArray;
		}


		function initGrid(tableID, filterData, isRefresh){
		    return $(tableID).dataTable({
		        "bProcessing": true,
		        "bServerSide": true,
		        "sPaginationType": "full_numbers",
		        "sDom": '<"top"f>rt<"bottom"ilpr><"clear">',

		        "aaSorting": [[1, "asc"]],
		        "aoColumns": [{
		            "sTitle": "Registrant ID",
		            "sName": "registrant_id",
		            "mData": "REGISTRANT_ID"
		        }, {
		            "sTitle": "Last Name",
		            "sName": "last_name",
		            "mData": "LAST_NAME"
		        }, {
		            "sTitle": "First Name",
		            "sName": "first_name",
		            "mData": "FIRST_NAME"
		        }, {
		            "sTitle": "Email",
		            "sName": "email",
		            "mData": "EMAIL"
		        }, {
		            "sTitle": "Type",
		            "sName": "type",
		            "mData": "TYPE"
		        }, {
		            "sTitle": "CPSO ##",
		            "sName": "cpso",
		            "mData": "CPSO"
		        }, {
		            "sTitle": "Student ID",
		            "sName": "student_id",
		            "mData": "STUDENT_ID"
		        }, {
		            "sTitle": "Status",
		            "sName": "status",
		            "mData": "STATUS"
		        }],

		        "aoColumnDefs": [
		        	{
		        		"mRender": function(data, type, row){
			                return '<a href="#event.buildLink(prc.xeh.editInterviewer)#/interviewerId/' + row.INTERVIEWER_ID + '">' + data + '</a>';
			            },
			            "aTargets": [0]
		        	},
		        	{
		        		"mRender": function(data, type, row){
		        			if (row.STATUS === 'Y')
				                return 'Scheduled';
				            else
				            	return 'Not Scheduled';
			            },
			            "aTargets": [7]
		        	}
		        ],

		        "fnServerParams": function(aoData){
		            $.merge(aoData, createFilterData());
		        },

		        "fnServerData": function(sSource, aoData, fnCallback, oSettings){
		            oSettings.jqXHR = $.ajax({
		                "dataType": 'json',
		                "type": "POST",
		                "url": "#event.buildLink(prc.xeh.loadInterviewers)#",
		                "data": aoData,
		                "success": function(result){
		                    $("##total_records").html('Total: ' + result.iTotalRecords);
		                    fnCallback(result);
		                    if (!isRefresh)
		                        $("##table-div").show();
		                }
		            });
		        }
		    });
		}

		$(function(){
		    var oTable = initGrid("##interviewTbl", createFilterData(), false);
		    $('.filterSelect').on('change', function(e) {
		    	e.preventDefault();
		    	oTable.fnDraw();
		    });
		});

	</script>

</cfoutput>