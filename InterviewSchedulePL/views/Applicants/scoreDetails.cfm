<cfoutput>
	<cfset isEditable = false>
	#prc.MsgBox.renderIt()#
	<form name="scoreForm" id="scoreForm" action="" method="POST">
		<input type="hidden" name="applicant_id" value="#prc.scoresQry.APPLICANT_ID#">
		<input type="hidden" name="competency_id" value="#prc.scoresQry.COMPETENCY_ID#">
		<table  width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="textLine_noBorder" width="250px"><b>#prc.scoresQry.COMPETENCY_NAME# Score:</b></td>
				<td class="textLine_right_noBorder" style="text-align:center">
					<b><cfif prc.scoresQry.total_score neq "">#prc.scoresQry.total_score#<cfelse>N/A</cfif></b>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<cfloop query="prc.scoresQry">
				<cfif prc.scoresQry.question_type_code neq "TEXT">
					<cfset score = "">
					<cfswitch expression="#prc.scoresQry.question_type_code#">
						<cfcase value="RATING_1_TO_7">
							<cfif prc.scoresQry.SCORE_VALUE neq "">
								<cfset score = prc.scoresQry.score_value>
							<cfelse>
								<cfset score = "">
							</cfif>
						</cfcase>
						<cfcase value="YES_NO">
							<cfif prc.scoresQry.SCORE_VALUE eq 1>
								<cfset score = "Yes">
							<cfelseif prc.scoresQry.SCORE_VALUE eq 0>
								<cfset score = "No">
							<cfelse>
								<cfset score = "">
							</cfif>
						</cfcase>
					</cfswitch>
					<tr>
						<td class="textLine_noBorder" width="70px">#prc.scoresQry.QUESTION_NAME#:</td>
						<td class="textLine_right_noBorder">
							<input type="text" class="small" name="q_#prc.scoresQry.QUESTION_RANK#" id="q_#prc.scoresQry.QUESTION_RANK#" value="#score#" disabled style="text-align:center">
						</td>
					</tr>
				<cfelse>
					<cfif prc.scoresQry.IS_EDITABLE eq "Y">
						<cfset isEditable = true>
					</cfif>
					<tr>
						<td colspan="2" class="textLine_noBorder" width="70px">Comment:</td>
					</tr>
					<tr>
						<td colspan="2" class="textLine_noBorder">
							<input type="hidden" name="question_id" value="#prc.scoresQry.QUESTION_ID#">
							<textarea <cfif not isEditable>disabled</cfif> maxlength="4000" style="width:100%;height:70px;resize: none;" name="answer_text" id="q_#prc.scoresQry.QUESTION_RANK#">#prc.scoresQry.ANSWER_TEXT#</textarea>
						</td>
					</tr>
				</cfif>
			</cfloop>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<td class="padding5" width="100%" align="right" colspan="2">
					<cfif isEditable>
						<input id="saveBtn" class="save" type="button" name="Save" value="Save" onclick="saveForm();">
					</cfif>
					<input class="cancel" type="button" name="Cancel" value="Cancel" onclick="closeDialog();">
				</td>
			</tr>
		</table>
	</form>
</cfoutput>