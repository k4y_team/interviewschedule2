<cfoutput>
	<cfif prc.applicants.recordcount eq 0>
		<cfset totalPages = 0>
		<cfset totalRows = 0>
	<cfelse>
		<cfset totalPages = prc.applicants.total_rows\25>
		<cfset totalRows = prc.applicants.total_rows>
	</cfif>
	<cfif totalRows mod 25 gt 0>
		<cfset totalPages = totalPages + 1>
	</cfif>
	<input type="hidden" name="current_page" id="current_page" value="#rc.current_page#">
	<input type="hidden" name="total_pages" id="total_pages" value="#totalPages#">
	<input type="hidden" name="total_rows" id="total_rows" value="#totalRows#">
	<table width="900px" border="0" cellspacing="0" cellpadding="0">	
		<tr>
			<td class="act">
				<cfif rc.applicantStatus eq "scheduled">
					Scheduled
				<cfelseif rc.applicantStatus eq "not_scheduled">
					Not Scheduled	
				</cfif>
				 Applicants		
			</td>
			<cfif totalRows gt 0>
				<td class="act" style="text-align:right;" >
					<table width="10%" border="0" cellspacing="1" cellpadding="0" style="float: right;">
						<tr>
							<cfif totalPages gt 1>						
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="first" class="bm navigation ico-nav-first" href="##">&nbsp;</a></td>
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="previous" class="bm navigation ico-nav-prev" href="##">&nbsp;</a></td>
								<td class="act" width="6%" style="text-align:center">Page #rc.current_page# of #totalPages#</td>
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="next" class="bm navigation ico-nav-next" href="##">&nbsp;</a></td>
								<td class="nav_bar" width="1%" style="text-align:center" nowrap=""><a id="last" class="bm navigation ico-nav-last" href="##">&nbsp;</a></td>
							</cfif>	
							<td>&nbsp;&nbsp;&nbsp;&nbsp;Total:#totalRows#</td>
						</tr>
					</table>
				</td>
			</cfif>	
		</tr>	
	</table>
	<table class="act" width="900px" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td class="bm_content_header" nowrap rowspan="2" style="vertical-align:middle;width:80px">Ref.##</td>
			<td class="bm_content_header" nowrap rowspan="2" style="vertical-align:middle;width:150px">Last Name</td>
			<td class="bm_content_header" nowrap rowspan="2" style="vertical-align:middle;width:150px">First Name</td>
			<td class="bm_content_header_center" nowrap rowspan="2" style="vertical-align:middle;width:80px">Status</td>
			<td class="bm_content_header_center" colspan="#prc.competencies.recordcount+1#">Scores</td>
		</tr>
		<tr>	
			<cfloop query="prc.competencies">
				<td class="bm_content_header_center" nowrap>#prc.competencies.competency_name#</td>
			</cfloop>
			<td class="bm_content_header_center" nowrap>Final (out of #prc.applicants.SCORE_WEIGHT#)</td>
		</tr>
		<cfloop query="prc.applicants" >
			<tr>
				<td class="bm_content_body_alt">#prc.applicants.reference_no#</td>
				<td class="bm_content_body_alt">#prc.applicants.LAST_NAME#</td>
				<td class="bm_content_body_alt">#prc.applicants.FIRST_NAME#</td>
				<td class="bm_content_body_alt_center">
					<cfif prc.applicants.STATUS EQ 'Scheduled'>
						<a href="#event.buildLink(linkTo=prc.xeh.schedule, queryString='interviewer=Applicant #LAST_NAME#, #FIRST_NAME#&search_string=A_#APPLICANT_ID#')#">#prc.applicants.STATUS#</a>
					<cfelse>
						#prc.applicants.STATUS#
					</cfif>
				</td>
				<cfloop query="prc.competencies">
					<cfset scoreValue = Evaluate("prc.applicants.competency_#prc.competencies.competency_id#_score")>
					<td class="bm_content_body_alt_center" nowrap>
						<cfif scoreValue eq 0 or scoreValue eq "">
							<cfset scoreValue = ''>
						</cfif>
						<cfif prc.applicants.STATUS EQ 'Scheduled' AND prc.competencies.questions_number neq 0>
							<a href="javascript:;" onClick="competencyScore(#prc.applicants.applicant_id#,#prc.competencies.competency_id#,'#prc.competencies.competency_name#')">#scoreValue#</a>
						<cfelse>
							#scoreValue#
						</cfif>
					</td>
				</cfloop>
				<td class="bm_content_body_alt_center" nowrap>
					<cfif total_score neq "">
						<b>#total_score#</b>
					</cfif>
				</td>
			</tr>
		</cfloop>
	</table>
	<script>
		function competencyScore(applicantID,competencyID,competencyName) {
			createDialog("#event.buildLink(linkTo=prc.xeh.scoreDetails)#/competencyID/"+competencyID+"/applicantID/"+applicantID , {
				title: competencyName+' Score Details', 
				width: 350, 
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
		function saveForm(){
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##scoreForm').serialize(),
	          	success: function(data){
					refreshDialog();
	           	}
	        });
	  	}
	</script>
</cfoutput>