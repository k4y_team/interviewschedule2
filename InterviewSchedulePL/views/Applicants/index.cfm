<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<K4Y:includesFile fileName="javascript/lookup.js" moduleName="#event.getCurrentModule()#" fileType="js"/>
	#runEvent(event=prc.xeh.tabs, eventArguments=prc)#
	<table class="body_tabs" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="padding10" valign="top">
					<div style="margin-bottom:5px;">
						<form name="filter" id="filter" action="#event.buildLink(prc.xeh.self)#" method="post">
						<input type="hidden" name="interviewScheduleID" value="#rc.interviewScheduleID#">
						<table width="900px" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="textLine_noBorder" nowrap="true" style="width:50px;vertical-align:middle;">Session:</td>
								<td class="textLine_noBorder" style="width:130px">
									<select name="sessionID" id="sessionID" class="large" onChange="submitFilter();">
										<cfloop query="#prc.uiFilter.getSessionQuery()#">
											<option value="#prc.uiFilter.getSessionQuery().SESSION_ID#" <cfif prc.uiFilter.getSessionValue() EQ prc.uiFilter.getSessionQuery().SESSION_ID>selected</cfif>>
												#prc.uiFilter.getSessionQuery().SESSION_NAME#
											</option>
										</cfloop>
									</select>
								</td>
								<td class="textLine_noBorder" nowrap="true" style="width:50px;vertical-align:middle;">Status:</td>
								<td class="textLine_noBorder">
									<select name="applicantStatus" id="applicantStatus" class="large" onChange="submitFilter();">
										<option value="all" <cfif rc.applicantStatus eq 'all'>selected</cfif>>All</option>
										<option value="scheduled" <cfif rc.applicantStatus eq 'scheduled'>selected</cfif>>Scheduled</option>
										<option value="not_scheduled" <cfif rc.applicantStatus eq 'not_scheduled'>selected</cfif>>Not Scheduled</option>
									</select>
								</td>
								<cfif prc.userAccess eq "READ-WRITE">
									<td style="text-align:right">
										<input class="load" type="button" name="Import" value="Load Scores" onclick="window.location='#event.buildLink(prc.xeh.import)#?interviewScheduleID=#rc.interviewScheduleID#'">
									</td>
								</cfif>	
							</tr>
						</table>
						</form>
					</div>
					<form name="fake_filterFrm" id="fake_filterFrm" method="post" action=""></form>
					<div id="results"></div>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
			$(function() {
				var options = {};
				options.triggerSearch = 'true';
				options.loadResultsUrl = '#event.buildLink(prc.xeh.load)#';
				LOOKUP.setup(options);
			});		
			function submitFilter(){
				$('##filter').submit();
			}
			function reloadPage(){
				submitFilter();	
			}
		</script>	
	</script>
</cfoutput>