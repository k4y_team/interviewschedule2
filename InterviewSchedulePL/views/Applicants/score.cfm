<cfoutput>
	<table width="800" style="padding:0px">
		<tr>
			<td class="act">Interview</td>
		</tr>
		<cfif prc.scheduleDetailsQry.recordCount GT 0>
			<cfset st_hour = prc.scheduleDetailsQry.SHIFT_START_HOUR>
			<cfset st_min = prc.scheduleDetailsQry.SHIFT_START_MINUTE>
			<cfset e_hour = prc.scheduleDetailsQry.SHIFT_END_HOUR>
			<cfset e_min = prc.scheduleDetailsQry.SHIFT_END_MINUTE>
			<cfif st_hour lt 10>
				<cfset st_hour = '0'&'#st_hour#'>
			</cfif>
			<cfif st_min lt 10>
				<cfset st_min = '0'&'#st_min#'>
			</cfif>
			<cfif e_hour lt 10>
				<cfset e_hour = '0'&'#e_hour#'>
			</cfif>
			<cfif e_min lt 10>
				<cfset e_min = '0'&'#e_min#'>
			</cfif>
			<tr>
				<td class="textLine_noBorder" style="padding:10px 0;">
					Applicant scheduled on <strong>#prc.scheduleDetailsQry.DAY_NAME#/#DateFormat(prc.scheduleDetailsQry.DAY_DT, application.cbController.getSetting('formatStruct').dateFormat)#</strong>, <strong>#prc.scheduleDetailsQry.CIRCUIT_NAME#</strong>, between <strong>#st_hour#:#st_min#</strong> and <strong>#e_hour#:#e_min#</strong><cfif prc.scheduleDetailsQry.LOCATION neq "">, at <strong>#prc.scheduleDetailsQry.LOCATION#</cfif></strong>. 
				</td>
			</tr>
			<tr>
				<td>
					<table class="act" width="100%" cellpadding="0" cellspacing="1" border="0">
						<tr>
							<td class="bm_content_header">Time</td>
							<td class="bm_content_header">Interviewer</td>
							<td class="bm_content_header" style="text-align:center">Competency</td>
							<td class="bm_content_header" style="text-align:center">Room</td>
							<td class="bm_content_header" style="text-align:center">Score</td>
							<td class="bm_content_header" style="text-align:center">Definite Admission Offer</td>
							<td class="bm_content_header" style="text-align:center">Red Flag</td>
						</tr>
						<cfset altClass="bm_content_body_alt">
						<cfloop query="prc.scheduleDetailsQry">
							<cfif altClass EQ "bm_content_body_alt">
								<cfset altClass="bm_content_body">
							<cfelse>
								<cfset altClass="bm_content_body_alt">
							</cfif>
							<cfset st_hour = prc.scheduleDetailsQry.START_HOUR>
							<cfset st_min = prc.scheduleDetailsQry.START_MINUTE>
							<cfset e_hour = prc.scheduleDetailsQry.END_HOUR>
							<cfset e_min = prc.scheduleDetailsQry.END_MINUTE>
							<cfif st_hour lt 10>
								<cfset st_hour = '0'&'#st_hour#'>
							</cfif>
							<cfif st_min lt 10>
								<cfset st_min = '0'&'#st_min#'>
							</cfif>
							<cfif e_hour lt 10>
								<cfset e_hour = '0'&'#e_hour#'>
							</cfif>
							<cfif e_min lt 10>
								<cfset e_min = '0'&'#e_min#'>
							</cfif>
							<tr>
								<td class="#altClass#">#st_hour#:#st_min# - #e_hour#:#e_min#</td>
								<td class="#altClass#">
									<cfif prc.scheduleDetailsQry.INTERVIEWER_LNAME NEQ ''>
										#prc.scheduleDetailsQry.INTERVIEWER_LNAME#, #prc.scheduleDetailsQry.INTERVIEWER_FNAME#
									<cfelse>
										N/A
									</cfif>
								</td>
								<td class="#altClass#" style="text-align:center">#prc.scheduleDetailsQry.COMPETENCY_NAME#</td>
								<td class="#altClass#" style="text-align:center">
									<cfif prc.scheduleDetailsQry.ROOM NEQ ''>
										#prc.scheduleDetailsQry.ROOM#
									<cfelse>
										N/A
									</cfif>
								</td>
								<td class="#altClass#" style="text-align:center">
									<a href="javascript:editCompetencyScore(#prc.scheduleDetailsQry.APPLICANT_ID#,#prc.scheduleDetailsQry.COMPETENCY_ID#);">
										<cfif prc.scheduleDetailsQry.SCORE_VALUE NEQ ''>
											#prc.scheduleDetailsQry.SCORE_VALUE#
										</cfif>
									</a>
								</td>
								<td class="#altClass#" style="text-align:center">
									<cfswitch expression="#prc.scheduleDetailsQry.ADMISSION_OFFER#">
										<cfcase value="Y">Yes</cfcase>
										<cfcase value="N">No</cfcase>
										<cfdefaultcase></cfdefaultcase>
									</cfswitch>
								</td>
								<td class="#altClass#" style="text-align:center">
									<cfswitch expression="#prc.scheduleDetailsQry.RED_FLAG#">
										<cfcase value="Y">Yes</cfcase>
										<cfcase value="N">No</cfcase>
										<cfdefaultcase></cfdefaultcase>
									</cfswitch>
								</td>
							</tr>
						</cfloop>
					</table>
				</td>
			</tr>
			<tr>
				<td class="textLine_noBorder" style="padding:10px 0;">
					<cfif isNumeric(prc.scheduleDetailsQry.TOTAL_SCORE)>
						Interview Score: <b>#prc.scheduleDetailsQry.TOTAL_SCORE#</b> (out of 50)
					<cfelse>
						Interview Score: <span style="color:red;font-weight:bold">Not calculated</span>
					</cfif>	
				</td>
			</tr>
		<cfelse>
			<tr>
				<td class="textLine_noBorder" style="padding:10px 0;">Applicant not yet scheduled.</td>
			</tr>
		</cfif>
	</table>
</cfoutput>