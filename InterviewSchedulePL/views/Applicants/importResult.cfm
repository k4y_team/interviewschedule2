<cfoutput>
	#runEvent(event=prc.xeh.tabs, eventArguments=prc)#
	<cfset badRecordsQry = prc.importResult.getBAD_RECORDS_DATA()>
	<cfset goodRecordsQry = prc.importResult.getGOOD_RECORDS_DATA()>
	<cfset warnRecordsQry = prc.importResult.getWARNING_RECORDS_DATA()>

	<table class="body_tabs" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="padding10" valign="top">
					<div id="addButtonDiv" style="display:block;">
						<table width="850px" border="0" cellpadding="0" cellspacing="0">
							<tr style="height:25px;">
								<td>
									<b>#prc.importTitle#</b>
								</td>
								<td class="textLine_noBorder" nowrap style="text-align:right">
									<form name="submitFrm" action="#event.buildLink(prc.xeh.submit)#" method="post">
										<input type="hidden" name="interviewScheduleID" value="#rc.interviewScheduleID#">
										<input type="hidden" name="filename" value="#prc.importFileName#">
										<input class="return" type="button" name="back" value="Back" onclick="window.location='#event.buildLink(prc.xeh.ImportForm)#?interviewScheduleID=#rc.interviewScheduleID#'">
										<cfif prc.importResult.getGOOD_RECORDS() gt 0>
											<input class="load" type="submit" name="submit" value="Submit">
										</cfif>	
									</form>
								</td>
							</tr>
						</table>
					</div>
					<table width="850px" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="act" width="50%">Valid Scores</td>
							<td class="act" style="text-align:right">Total: #prc.importResult.getGOOD_RECORDS()#</td>
						</tr>
					</table>
					<table class="act" width="850px" border="0" cellspacing="1" cellpadding="0">
						<tr>
							<cfloop list="#prc.fields#" index="headerItem">
								<td class="bm_content_header" nowrap>#headerItem#</td>
							</cfloop>
						</tr>
						<tbody>
						<cfloop query="goodRecordsQry">
							<tr>
								<cfloop list="#prc.fields#" index="headerItem">
									<td class="bm_content_body_alt" nowrap>
										<cfif isNumeric(goodRecordsQry[headerItem][goodRecordsQry.currentrow])>
											#NumberFormat(goodRecordsQry[headerItem][goodRecordsQry.currentrow],'_')#
										<cfelse>
											<cfif Len(Trim(goodRecordsQry[headerItem][goodRecordsQry.currentrow])) gt 100>
												#Mid(goodRecordsQry[headerItem][goodRecordsQry.currentrow],1,100)#...
											<cfelse>										
												#goodRecordsQry[headerItem][goodRecordsQry.currentrow]#
											</cfif>	
										</cfif>
									</td>
								</cfloop>
							</tr>
						</cfloop>	
						</tbody>
					</table>
					<br>					
					<cfif prc.importResult.getBAD_RECORDS() gt 0>	
						<table width="850px" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="act" width="50%" style="background-color:##CC0000">Invalid Scores (These scores are not imported) </td>
								<td class="act" style="text-align:right;background-color:##CC0000">Total: #prc.importResult.getBAD_RECORDS()#</td>
							</tr>
						</table>
						<table class="act" width="850px" border="0" cellspacing="1" cellpadding="0">
							<tr>
								<cfloop list="#prc.fields#" index="headerItem">
									<td class="bm_content_header" nowrap>#headerItem#</td>
								</cfloop>
								<td class="bm_content_header" nowrap>Error</td>
							</tr>
							<tbody>
							<cfloop query="badRecordsQry">
								<tr>
									<cfloop list="#prc.fields#" index="headerItem">
										<td class="bm_content_body_alt" nowrap>
											<cfif isNumeric(badRecordsQry[headerItem][badRecordsQry.currentrow])>
												#NumberFormat(badRecordsQry[headerItem][badRecordsQry.currentrow],'_')#
											<cfelse>
												<cfif Len(Trim(badRecordsQry[headerItem][badRecordsQry.currentrow])) gt 100>
													#Mid(badRecordsQry[headerItem][badRecordsQry.currentrow],1,100)#...
												<cfelse>											
													#badRecordsQry[headerItem][badRecordsQry.currentrow]#
												</cfif>
											</cfif>
										</td>
									</cfloop>
									<td class="bm_content_body_alt" style="color:red">#badRecordsQry["errormessages"][badRecordsQry.currentrow]#</td>
								</tr>
							</cfloop>	
							</tbody>
						</table>
						<br>
					</cfif>
					<cfif prc.importResult.getWARNING_RECORDS() gt 0>	
						<table width="850px" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="act" width="50%" style="background-color:##FE9A2E">Warning Scores</td>
								<td class="act" style="text-align:right;background-color:##FE9A2E">Total: #prc.importResult.getWARNING_RECORDS()#</td>
							</tr>
						</table>
						<table class="act" width="850px" border="0" cellspacing="1" cellpadding="0">
							<tr>
								<cfloop list="#prc.fields#" index="headerItem">
									<td class="bm_content_header" nowrap>#headerItem#</td>
								</cfloop>
								<td class="bm_content_header" nowrap>Warning</td>
							</tr>
							<tbody>
							<cfloop query="warnRecordsQry">
								<tr>
									<cfloop list="#prc.fields#" index="headerItem">
										<td class="bm_content_body_alt" nowrap>
											<cfif isNumeric(warnRecordsQry[headerItem][warnRecordsQry.currentrow])>
												#NumberFormat(warnRecordsQry[headerItem][warnRecordsQry.currentrow],'_')#
											<cfelse>
												#warnRecordsQry[headerItem][warnRecordsQry.currentrow]#
											</cfif>
										</td>
									</cfloop>
									<td class="bm_content_body_alt" style="color:##DF7401">#warnRecordsQry["errormessages"][warnRecordsQry.currentrow]#</td>
								</tr>
							</cfloop>	
							</tbody>
						</table>
						<br>
					</cfif>	
				</td>
			</tr>
		</tbody>
	</table>
</cfoutput>