<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>		
	<form name="competencyForm" id="competencyForm" action="#event.buildLink(prc.xeh.save)#" method="POST">
		<input type="hidden" name="COMPETENCY_ID" value="#rc.competencyData.getCOMPETENCY_ID()#">
		<input type="hidden" name="INTERVIEW_SCHEDULE_ID" value="#rc.interviewScheduleID#">
		<input type="hidden" name="sessionID" value="#rc.sessionID#">
		<table  width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="textLine_noBorder" width="70px">Session:</td>
				<td class="textLine_noBorder">
					<input type="text" class="large" name="session_name" id="session_name" value="#prc.sessionQry.SESSION_NAME#" disabled>
				</td>
				
			</tr>
			<tr>
				<td class="textLine_noBorder">Name:</td>
				<td class="textLine_noBorder"><input class="large" type="text" name="COMPETENCY_NAME" value="#rc.competencyData.getCOMPETENCY_NAME()#"></td>
			</tr>
			<tr>
				<td class="textLine_noBorder">Duration:</td>
				<td class="textLine_noBorder"><input class="large" type="text" name="DURATION_MINS" value="#rc.competencyData.getDURATION_MINS()#">&nbsp;minutes</td>
			</tr>						
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<td class="padding5" width="100%" align="right" colspan="2">
					<input id="saveBtn" class="save" type="button" name="Save" value="Save">
					<cfif rc.competencyData.getCOMPETENCY_ID() NEQ ''>
						<input id="deleteBtn" class="delete" type="button" name="Delete" value="Delete">
					</cfif>
					<input class="cancel" type="button" name="Cancel" value="Cancel" onclick="closeDialog();">
				</td>
			</tr>
		</table>
	</form>
	<script>
		$('##saveBtn').on('click',function(){
			$('##saveBtn').attr("disabled", "disabled");
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##competencyForm').serialize(),
	          	success: function(data){
					if (data == "") {
						closeDialog();
					} else {
						var $parentContainer = $('##competencyForm').parents()[0];
						$($parentContainer).html(data);
					}
					reloadPage();
	           	}
	        });
	  	});		
		$('##deleteBtn').on('click',function(){
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.delete)#",
	          	data: {'competencyCD':'#rc.competencyData.getCOMPETENCY_ID()#'},
				dataType: 'json',
	          	success: function(data){
				   	if(data == true){
						closeDialog(true);
						reloadPage();
					} else {
						refreshDialog();
					}
	           	}
	        });
		});
	</script>
</cfoutput>