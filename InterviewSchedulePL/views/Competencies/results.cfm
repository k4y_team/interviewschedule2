<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<table width="450px" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="act" width="100%">Competencies</td>
		</tr>
	</table>
	<table class="act" width="450px" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td class="bm_content_header" nowrap>Name</td>
			<td class="bm_content_header_center" nowrap>Duration</td>
		</tr>
		<cfloop query="prc.qCompetencies" >
			<tr>
				<td class="bm_content_body_alt">
					<cfif prc.userAccess eq "READ-WRITE">
					<span class="ico-edit" onclick="editCompetency(#COMPETENCY_ID#)"></span>&nbsp;
					</cfif>
					#COMPETENCY_NAME#
				</td>
				<td class="bm_content_body_alt_center">#DURATION_MINS#</td>
			</tr>
		</cfloop>	
	</table>
	<script>
		function editCompetency(competencyCD) {
			if (competencyCD === '') {
				dialogTitle = 'Add Competency';
			} else {
				dialogTitle = 'Edit Competency';
			}
			createDialog("#event.buildLink(linkTo=prc.xeh.editCompetency, queryString='sessionID=#rc.sessionID#&competencyCD=')#" + competencyCD, {
				title: dialogTitle, 
				width: 400, 
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}
	</script>
</cfoutput>