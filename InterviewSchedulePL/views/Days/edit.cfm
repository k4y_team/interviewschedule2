<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>				
	<form name="dayForm" id="dayForm" action="#event.buildLink(prc.xeh.save)#" method="POST">
		<input type="hidden" name="DAY_ID" value="#rc.dayData.getDAY_ID()#">
		<input type="hidden" name="sessionID" value="#rc.sessionID#">
		<input type="hidden" name="INTERVIEW_SCHEDULE_ID" value="#rc.interviewScheduleID#">
		<table  width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="textLine_noBorder" width="50px">Session:</td>
				<td class="textLine_noBorder">
					<input type="text" class="large" name="session_name" id="session_name" value="#prc.sessionQry.SESSION_NAME#" disabled>
				</td>
				
			</tr>
			<tr>
				<td class="textLine_noBorder">Name:</td>
				<td class="textLine_noBorder"><input class="xlarge" type="text" name="DAY_NAME" value="#rc.dayData.getDAY_NAME()#"></td>
			</tr>
			<tr style="height:210px;">
				<td class="textLine_noBorder">Date:</td>
				<td class="textLine_noBorder">
					<div id="datepicker" style="width:200px"></div>
					<input type="hidden" name="DAY_DT" id="DAY_DT" value="#DateFormat(rc.dayData.getDAY_DT(),"dd-mmm-yyyy")#">
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<td class="padding5" width="100%" align="right" colspan="2">
					<input id="saveBtn" class="save" type="button" name="Save" value="Save">
					<cfif rc.dayData.getDAY_ID() NEQ ''>
						<input id="deleteBtn" class="delete" type="button" name="Delete" value="Delete">
					</cfif>
					<input class="cancel" type="button" name="Cancel" value="Cancel" onclick="closeDialog();">
				</td>
			</tr>
		</table>
	</form>
	<script>
		$(function() {
	    	$( "##datepicker").datepicker({
	    		inline: true,
	    		dateFormat: 'dd-M-yy',
				changeMonth: true,
				changeYear: true,
				defaultDate: '#DateFormat(rc.dayData.getDAY_DT(),"dd-mmm-yyyy")#',
				onSelect: function(dateText, inst) {
					$('##DAY_DT').val(dateText);
				}
			});
	  	});
		$('##saveBtn').on('click',function(){
			$('##saveBtn').attr("disabled", "disabled");
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##dayForm').serialize(),
	          	success: function(data){
					if (data == "") {
						closeDialog();
					} else {
						var $parentContainer = $('##dayForm').parents()[0];
						$($parentContainer).html(data);
					}
					reloadPage();
	           	}
	        });
	  	});
		$('##deleteBtn').on('click',function(){
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.delete)#",
	          	data: {'dayCD':'#rc.dayData.getDAY_ID()#'},
				dataType: 'json',
	          	success: function(data){
				   	if(data == true){
						closeDialog(true);
						reloadPage();
					} else {
						refreshDialog();
					}
	           	}
	        });
		});
	</script>
</cfoutput>
