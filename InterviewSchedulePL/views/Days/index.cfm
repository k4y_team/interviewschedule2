<cfoutput>
	#runEvent(event=prc.xeh.tabs, eventArguments=prc)#
	<table class="body_tabs" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="padding10" valign="top">
					<div style="margin-bottom:5px;">
						<form name="daysFilter" id="daysFilter" action="#event.buildLink(prc.xeh.self)#" method="post">
						<table width="450px" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="textLine_noBorder" nowrap="true" style="width:50px;">Session:</td>
								<td class="textLine_noBorder">
									<select name="sessionID" id="sessionID" class="large" onChange="submitFilter();">
										<cfloop query="#prc.uiFilter.getSessionQuery()#">
											<option value="#prc.uiFilter.getSessionQuery().SESSION_ID#" <cfif prc.uiFilter.getSessionValue() EQ prc.uiFilter.getSessionQuery().SESSION_ID>selected</cfif>>
												#prc.uiFilter.getSessionQuery().SESSION_NAME#
											</option>
										</cfloop>
									</select>
								</td>
								<cfif prc.userAccess eq "READ-WRITE">
									<td class="textLine_right_noBorder" nowrap>
										<input class="addNew" id="newDay" type="button" name="Add" value="Add New" onclick="editDay('')">
									</td>
								</cfif>	
							</tr>
						</table>
						</form>
					</div>
					<div class="content">
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		function reloadPage() {
			submitFilter();
		}		
		function submitFilter(){
			$('.content').html('<span class="loading">Loading...</span>');
			$('.content').load(
				'#event.buildLink(linkTo=prc.xeh.load)#',
				$('##daysFilter').serialize()
			);
		}
		$(document).ready(function() {
			submitFilter();
		});
	</script>
</cfoutput>