<cfoutput>
	<cfif isDefined("prc.MsgBox")>
		#prc.MsgBox.renderIt()#
	</cfif>
	<table width="450px" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="act" width="100%">Interview Days</td>
		</tr>
	</table>
	<table class="act" width="450px" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td class="bm_content_header" nowrap>Name</td>
			<td class="bm_content_header_center" nowrap>Date</td>
		</tr>
		<cfloop query="prc.qDays" >
			<tr>
				<td class="bm_content_body_alt">
					<cfif prc.userAccess eq "READ-WRITE"><span class="ico-edit" onclick="editDay(#DAY_ID#)"></span>&nbsp;</cfif>
					#DAY_NAME#
				</td>
				<td class="bm_content_body_alt_center">#dateFormat(DAY_DT,"dd-mmm-yyyy")#</td>
			</tr>
		</cfloop>			
	</table>
	<script type="text/javascript">
		function editDay(dayCD) {
			if (dayCD === '') {
				dialogTitle = 'Add Interview Day';
			} else {
				dialogTitle = 'Edit Interview Day';
			}
			createDialog("#event.buildLink(linkTo=prc.xeh.editDay, queryString='sessionID=#rc.sessionID#&dayCD=')#" + dayCD, {
				title: dialogTitle, 
				width: 400, 
				height: 'auto',
				autoOpen:true,
				modal: true,
				resizable: false});
		}		
	</script>
</cfoutput>