<cfoutput>
	<style type="text/css">
		TD.textLine_noBorder {
			padding: 2px 0px !important;
		}
		table.settings_tbl {
			border: none !important;
		}
	</style>
	<form name="editForm" id="editForm" action="#event.buildLink(prc.xeh.save)#" method="POST">
		<input type="hidden" name="INTERVIEW_SCHEDULE_ID" value="#rc.interviewQry.INTERVIEW_SCHEDULE_ID#">
		<input type="hidden" name="interview_schedule_name" value="#rc.interviewQry.INTERVIEW_SCHEDULE_NAME#">		
		<table  class="settings_tbl" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="textLine_noBorder" style="width:140px;">Break between stations:</td>
				<td class="textLine_noBorder"><input class="large" type="text" name="break_between_stations" value="#rc.interviewQry.break_between_stations#" style="width:50px">&nbsp;minutes</td>
			</tr>
			<tr>
				<td class="textLine_noBorder" style="width:140px;">Interview score weight:</td>
				<td class="textLine_noBorder"><input class="large" type="text" name="score_weight" value="#rc.interviewQry.SCORE_WEIGHT#" style="width:50px">&nbsp;out of 100</td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<td class="padding5" width="100%" align="right" colspan="2">
					<input id="saveBtn" class="save" type="button" name="Save" value="Save">
				</td>
			</tr>
		</table>
	</form>
	<script>
		$('##saveBtn').on('click',function(){
			$.ajax({
	          	type: "POST",
	           	url: "#event.buildLink(prc.xeh.save)#",
	          	data: $('##editForm').serialize(),
	          	success: function(data){
					location.reload();
	           	}
	        });
	  	});
	</script>
</cfoutput>