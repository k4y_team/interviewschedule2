<cfoutput>
	<style>
		.errorPage .name {
		    color: ##CB2C1A;
		    font-size: 50px;
		    line-height: 70px;
		    text-shadow: none !important;
		}
		body {
			background: ##FFFFFF !important;
		}
		.content {
			margin-left:0px;
		}				
	</style>
	<div class="errorPage">        
        <p class="name">Access Denied!</p>
        <p class="description">You are not authorized to view this page.</p>        
        <p><button class="btn btn-danger" onClick="document.location.href = '#application.cbController.getSetting("applicationBaseURL")#';">Back to Home page</button> <button class="btn btn-warning" onClick="history.back();">Previous page</button></p>
		<p>&copy; 1999  - #Year(Now())# <a href="http://www.knowledge4you.com" target="_blank">Knowledge4You Corporation</a>. All rights reserved.</p>		
    </div>	
</cfoutput>	