<cfoutput>
<div class="menu">
	<ul class="navigation">
		<li <cfif rc.activeMenuOption eq "Home">class="active"</cfif>>
			<a href="#event.buildLink(prc.xeh.home)#">
				<span class="isw-home"></span><span class="text">Home</span>
			</a>
		</li>
		<cfif prc.UserSecurity.checkAccess("ISCH_INTERVIEW_SCHEDULE")>
			<li <cfif rc.activeMenuOption eq "Interview Schedule">class="active"</cfif>>
				<a href="#event.buildLink(prc.xeh.interviewSchedule)#">
					<span class="isw-isch-interview"></span><span class="text">Interview Schedule</span>
				</a>
			</li>
		</cfif>
		<cfif prc.UserSecurity.checkAccess("ISCH_IMPORT")>
			<li <cfif rc.activeMenuOption eq "Import">class="active"</cfif>>
				<a href="#event.buildLink(prc.xeh.import)#">
					<span class="isw-import"></span><span class="text">Import</span>
				</a>
			</li>
		</cfif>
	</ul>
</div>
</cfoutput>