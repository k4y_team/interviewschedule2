var LOOKUP = (function ($)
	{
		var my = {};
		var controls = {};
		
		function init() {
			initControls();
			bindControls();
						
			if (my.settings.triggerSearch) {
				loadResultsPage("SEARCH");
			}
		}

		function initControls() {
			controls.clearButtons = $('.clear');
			controls.navigationButtons = $('.navigation');
			controls.searchButton = $('#searchBtn');
			controls.clearFormButton = $('#clearBtn');
			controls.searchForm = $('#filter');
			controls.clearSearchFormFields = [];
			controls.searchFormFields = $("#filter :input");
			controls.fakeSearchForm = $('#fake_filterFrm');
			controls.resultsContainer = $('#results');
		}
		
		/* type - 'SEARCH', search submitted by button or 'PAGE', page browsing */
		function loadResultsPage(type, current_page, total_rows) {
			var dataString = "";
			
			if (type == "SEARCH") {
				dataString = controls.searchForm.serialize(); 
			}
			else if (type == "PAGE") {
				dataString = controls.fakeSearchForm.serialize() + "&current_page=" + current_page + "&total_rows=" + total_rows;
			} else if (type == "RELOAD") {
				var $currentPage = $('#current_page').val();
				var $totalRows = $('#total_rows').val();
				dataString = controls.fakeSearchForm.serialize() + "&current_page=" + $currentPage + "&total_rows=" + $totalRows;
			}
			$.ajax({
				url: my.settings.loadResultsUrl,
				type: "POST",
				cache: false,
				dataType: "HTML",
				data: dataString,
				success: function(data) {
					controls.resultsContainer.empty();
					controls.resultsContainer.html(data);
					controls.resultsContainer.show();
					if (type == "SEARCH") {
						createFakeForm();
					}
					else if (type == "PAGE") {
						$('#current_page').val(current_page);
					}
				}
			});			
		}
		
		function createFakeForm() {
			controls.fakeSearchForm.empty();
			filterData = createFilterData();
			for (var i = 0; i < filterData.length; i++) {
				var input = '<input type="hidden" name="' + filterData[i].name + '" value="' + filterData[i].value + '"/>';
				controls.fakeSearchForm.append(input);
			}			
		}
		
		function createFilterData() {
			var filterArray = [];
			controls.searchFormFields.each(function() {
				if (! $(this).is(":button")) {
					if ($(this).is(":checkbox") && $(this).is(":checked")) {
						var found = false;
						for (var i = 0; i < filterArray.length; i++) {
							if (filterArray[i].name == $(this).prop("name")) {
								filterArray[i].value = filterArray[i].value + "," + $(this).val();
								found = true;
								break;
							}
						}
						
						if (!found) {
							var filterStruct = {};
							filterStruct["name"] = $(this).prop("name");
							filterStruct["value"] = $(this).val();								
							filterArray.push(filterStruct);							
						}					
					}
					else if (!$(this).is(":checkbox")) {
						var filterStruct = {};
						filterStruct["name"] = $(this).prop("name");
						filterStruct["value"] = $(this).val();
						filterArray.push(filterStruct);
					}
				}				
			});
			
			return filterArray;
		}
		
		function clearFilter() {
			(controls.searchForm)[0].reset();
			controls.clearSearchFormFields.each(function() {
	            switch(this.type) {
	                case 'hidden':
	                    $(this).val('');
	            }
			});
		}
		
		function bindControls() {			
			controls.searchButton.bind('click', function() {
				loadResultsPage("SEARCH");
			});
									
			controls.clearFormButton.bind('click', function() {
				clearFilter();	
			});
			
			controls.resultsContainer.on("click", ".navigation", function(ev) {
				ev.preventDefault();
				controls.currentPage = $('#current_page');
				controls.totalRows = $('#total_rows');
				controls.totalPages = $('#total_pages');
				var current_page, total_rows;
				switch ($(ev.currentTarget).attr('id')) {
					case 'next': {
						current_page = parseInt(controls.currentPage.val());
						total_rows = controls.totalRows.val();
						if (current_page < controls.totalPages.val()) {
							current_page++;
							loadResultsPage("PAGE", current_page, total_rows);
						}
						break;
					}
					case 'previous': {
						current_page = parseInt(controls.currentPage.val());
						total_rows = controls.totalRows.val();
						if (current_page > 1) {
							current_page--;
							loadResultsPage("PAGE", current_page, total_rows);
						}
						break;
					}
					case 'first': {
						current_page = 1;
						total_rows = controls.totalRows.val();
						loadResultsPage("PAGE", current_page, total_rows);
						break;
					}
					case 'last': {
						current_page = parseInt(controls.totalPages.val());
						total_rows = controls.totalRows.val();
						loadResultsPage("PAGE", current_page, total_rows);
					}
				}
			});
		}
			
		my.setup = function (options) {
			my.settings = options;
			init();
		}
		my.reload = function (bResetPage) {
			if (bResetPage) {
				loadResultsPage("SEARCH");
			} else {
				loadResultsPage("RELOAD");			
			}	
		}
		
		//$(init);
		return my;
	}
(jQuery));