var Dialog = {
	url: '',
	options: {}
} 
function createDialog($url, $options) {
	Dialog.url = $url;
	Dialog.options = $options;
	
	$('.dlg').remove();
	$('<div class="dlg" style="display:none"></div>').html('<div class="dlg_content"></div>').appendTo('body');
	$('.dlg .dlg_content').load(
		Dialog.url,
		function() {
			$('.dlg').dialog(Dialog.options);
		}
	)
}
function openDialog() {
	$('.dlg').dialog("open");
}

function closeDialog() {
	$('.dlg').dialog("close");
	$('.dlg').dialog("destroy");
}

function refreshDialog() {
	$('.dlg .dlg_content').load(
		Dialog.url
	)
}