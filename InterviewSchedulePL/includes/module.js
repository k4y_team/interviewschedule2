$(document).ready(function() {
	$(".datepicker").datepicker({
		autoclose: true,
		forceParse: true,
		format: "dd-M-yyyy",
		viewMode: 0,
		minViewMode: 0
	});
	
	$("table.tblEditable tr").on('dblclick', function() {
		$("#tablePopup").trigger('click');
	});
	
	$("#addNew").on('click', function() {
		$("#tablePopup").trigger('click');
	});	
	
	/* Forgot password*/
	$('#sent').hide();
	
	$('#submitPsw').on('click',function() {
		$('#sent').show();
		$('#forgot').hide();
	});
});

function refreshTab(tab) {
	$('.Tab').each(function(){
		if ($(this).text() == tab) {
			$(this).click();
		}
	});
}