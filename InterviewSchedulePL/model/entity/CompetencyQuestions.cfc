/**
 * CompetencyQuestions
 *
 * @author zoltan.stir
 * @date 9/12/18
 **/
component tableName="ISCH_COMPETENCY_QUESTION" title="Competency Questions" gridSql="SELECT A.QUESTION_ID,D.INTERVIEW_SCHEDULE_NAME AS INTERVIEW_SCHEDULE_ID,C.COMPETENCY_NAME AS COMPETENCY_ID,A.QUESTION_NAME,A.IS_EDITABLE,A.QUESTION_RANK,B.QUESTION_TYPE_NAME AS QUESTION_TYPE_ID,A.IS_REQUIRED FROM ISCH_COMPETENCY_QUESTION A,ISCH_QUESTION_TYPE B,ISCH_COMPETENCY C,ISCH_INTERVIEW_SCHEDULE D,OMSAS_SESSION E WHERE A.QUESTION_TYPE_ID    = B.QUESTION_TYPE_ID AND A.COMPETENCY_ID = C.COMPETENCY_ID AND C.INTERVIEW_SCHEDULE_ID = D.INTERVIEW_SCHEDULE_ID AND D.SESSION_ID= E.SESSION_CD ORDER BY E.SESSION_NAME DESC,D.INTERVIEW_SCHEDULE_NAME,C.COMPETENCY_NAME,A.QUESTION_NAME" {
    property name="question_id" isPK=true sqltype="numeric" size="22";
    property name="interview_schedule_id" sqltype="numeric" size="20" displayOnGrid=true displayName="Schedule Name"  validations="required" renderType="select" hasEditLink=true fktablesql="SELECT interview_schedule_id as OptionID, interview_schedule_name as OptionValue FROM ISCH_INTERVIEW_SCHEDULE a, OMSAS_SESSION b where a.session_id=b.session_cd ORDER BY b.session_name desc, interview_schedule_name";
    property name="competency_id" sqltype="numeric" size="20" displayOnGrid=true displayName="Competency Name" renderType="select" linkTo="interview_schedule_id" fktablesql="SELECT a.competency_id as OptionID, a.competency_name as OptionValue, b.interview_schedule_id as linkToID FROM ISCH_COMPETENCY a, ISCH_INTERVIEW_SCHEDULE b WHERE a.interview_schedule_id = b.interview_schedule_id ORDER BY UPPER(competency_name)" hasEditLink=true validations="required";
    property name="question_name" sqltype="varchar2" size="50" displayOnGrid=true displayName="Name" renderType="input" validations="required,maxSize[50]" hasEditLink=true;
    property name="question_type_id" sqltype="numeric" size="20" displayOnGrid=true displayName="Type" validations="required" renderType="select" fktablesql="SELECT question_type_id as OptionID, question_type_name as OptionValue FROM ISCH_QUESTION_TYPE ORDER BY rating_id";
    property name="question_rank" sqltype="numeric" size="50" displayOnGrid=true displayName="Rank" renderType="input" validations="required,number" hasEditLink=false;
    property name="is_required" sqltype="varchar2" size="1" displayOnGrid=true displayName="Is Required" renderType="checkbox"  values='[ { "OptionID" : "Y" , "OptionValue" : "" }  ]';
}