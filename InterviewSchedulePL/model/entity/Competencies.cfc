/**
 * Competencies
 *
 * @author zoltan.stir
 * @date 9/12/18
 **/
component tableName="ISCH_COMPETENCY" title="Competencies" gridSql="SELECT A.COMPETENCY_ID, B.INTERVIEW_SCHEDULE_NAME AS INTERVIEW_SCHEDULE_ID, A.COMPETENCY_NAME, A.DURATION_MINS FROM ISCH_COMPETENCY A, ISCH_INTERVIEW_SCHEDULE B, OMSAS_SESSION C WHERE A.INTERVIEW_SCHEDULE_ID = B.INTERVIEW_SCHEDULE_ID AND B.SESSION_ID = C.SESSION_CD ORDER BY C.SESSION_NAME DESC, A.COMPETENCY_NAME" {
    property name="competency_id" isPK=true sqltype="numeric" size="22";
    property name="interview_schedule_id" sqltype="numeric" size="20" displayOnGrid=true displayName="Schedule Name" renderType="select" validations="required" fktablesql="SELECT interview_schedule_id as OptionID, interview_schedule_name as OptionValue FROM ISCH_INTERVIEW_SCHEDULE ORDER BY interview_schedule_name";
    property name="competency_name" sqltype="varchar2" size="100" displayOnGrid=true displayName="Name" renderType="input" validations="required,maxSize[100]" hasEditLink=true;
    property name="duration_mins" sqltype="numeric" size="22" displayOnGrid=true displayName="Duration (minutes)" renderType="input" validations="required,number" hasEditLink=false;
}