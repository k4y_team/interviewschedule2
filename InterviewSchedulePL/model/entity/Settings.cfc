/**
 * Title
 *
 * @author zoltan.stir
 * @date 9/11/18
 **/
component tableName="ISCH_INTERVIEW_SCHEDULE" title="Settings" gridSql="SELECT a.interview_schedule_id, a.interview_schedule_name, b.session_name as session_id, a.break_between_stations, a.score_weight FROM isch_interview_schedule a, omsas_session b WHERE a.session_id = b.session_cd ORDER BY b.session_name, a.interview_schedule_name" {
    property name="interview_schedule_id" isPK=true sqltype="numeric" size="22";
    property name="session_id" sqltype="numeric" size="20" displayName="Session" displayOnGrid=true renderType="select"  validations="required" fktablesql="SELECT session_cd as OptionID, session_name as OptionValue FROM OMSAS_SESSION ORDER BY session_name";
    property name="interview_schedule_name" sqltype="varchar2" size="100" displayOnGrid=true displayName="Schedule Name" renderType="input" validations="required,maxSize[100]" hasEditLink=true;
    property name="break_between_stations" sqltype="numeric" size="22" displayOnGrid=true displayName="Break between stations (minutes)" renderType="input" validations="required,number" hasEditLink=false;
    property name="score_weight" sqltype="numeric" size="22" displayOnGrid=true displayName="Interview score weight (out of 100)" renderType="input" validations="required,number" hasEditLink=false;


    function customValidation(formData){
		var message = [];
		var sqlString = "";
		var queryService = new query();
		var interviewScheduleId = formData.interview_schedule_id;

		queryService.setName("checkInterviewSchedule");
		queryService.setDatasource(request.app.mainDSN);
		queryService.addParam(name="sessionId", value=formData.session_id, cfsqltype="cf_sql_numeric");
		sqlString = "SELECT * FROM isch_interview_schedule WHERE session_id = :sessionId";
		if(interviewScheduleId neq ""){
			queryService.addParam(name="interviewScheduleId", value=interview_schedule_id, cfsqltype="cf_sql_numeric");
			sqlString = sqlString & " AND interview_schedule_id != :interviewScheduleId";
		}
		queryService.setSQL(sqlString);
		checkSettingRes = queryService.execute().getResult();

		if (checkSettingRes.recordCount GT 0) {
			msgType = "error";
			message = [{message = "Duplicate entry.", msgtype="error", tag="session_id"}];
		}

		return message;
	}
}