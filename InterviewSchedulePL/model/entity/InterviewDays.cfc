/**
 * Title
 *
 * @author zoltan.stir
 * @date 9/11/18
 **/
component tableName="ISCH_DAY" title="Interview Days" gridSql="SELECT A.DAY_ID, B.INTERVIEW_SCHEDULE_NAME AS INTERVIEW_SCHEDULE_ID, A.DAY_NAME, A.DAY_DT, A.CREATION_ID, A.CREATION_DT, A.MODIFICATION_ID, A.MODIFICATION_DT FROM ISCH_DAY A, ISCH_INTERVIEW_SCHEDULE B, OMSAS_SESSION C WHERE A.INTERVIEW_SCHEDULE_ID = B.INTERVIEW_SCHEDULE_ID AND B.SESSION_ID = C.SESSION_CD ORDER BY C.SESSION_NAME DESC, A.DAY_DT" {
    property name="day_id" isPK=true sqltype="numeric" size="22";
    property name="interview_schedule_id" sqltype="numeric" size="20" displayOnGrid=true displayName="Schedule Name" renderType="select" validations="required" fktablesql="SELECT interview_schedule_id as OptionID, interview_schedule_name as OptionValue FROM ISCH_INTERVIEW_SCHEDULE ORDER BY interview_schedule_name";
    property name="day_name" sqltype="varchar2" size="50" displayOnGrid=true displayName="Name" renderType="input" validations="required,maxSize[50]" hasEditLink=true;
    property name="day_dt" sqltype="date" size="50" displayOnGrid=true displayName="Date" renderType="date" validations="required,maxSize[50]" hasEditLink=false;
}