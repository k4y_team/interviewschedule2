<cfcomponent displayname="Security" output="false">

<cfproperty name="Security" inject="medsis.model.UserSecurity" scope="variables" />

<cffunction name="init" returntype="Security" output="true">
	<cfreturn this>
</cffunction>

<cffunction name="checkSession" output="true">
	<!--- <cfset variables.Security.checkSession()> --->
</cffunction>

<cffunction name="checkUserAccess" output="false" returntype="string">
	<cfargument name="userID" type="numeric" required="true">
	<cfargument name="permissionCode" type="string" required="true" default="">
	
 	<cfreturn variables.Security.checkUserAccess(userID=arguments.userID,sectionName="Admission",groupName="Interview Schedule",functionName=arguments.permissionCode)>
</cffunction>

</cfcomponent>