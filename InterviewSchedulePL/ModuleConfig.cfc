<!-----------------------------------------------------------------------
Author 	 :	Walkovszky Norbert
Date     :	November 14, 2013
Description : Interview Schedule presentation layer and business layer for Admission

----------------------------------------------------------------------->
<cfcomponent output="false" hint="Interview Schedule PL Configuration">
<cfscript>
	// Module Properties
	this.title 				= "Interview Schedule PL";
	this.author				= "Walkovszky Norbert";
	this.webUrl				= "";
	this.description 		= "";
	this.version			= "1.0";
	this.viewParentLookup 	= false;
	this.layoutParentLookup = true;
	this.entryPoint			= "InterviewSchedule";

	/**
	* Configure the InterviewSchedule Module
	*/
	function configure(){
		settings = {
			version = "1.0",
			loginPageEvent = "Authentication:Main.index",
			moduleMapXML = XmlParse("#modulePath#\moduleMap.xml")
		};
	}

	function preProcess(event, interceptData) {
		if (event.getCurrentModule() EQ "InterviewSchedulePL"){
			var rc = event.getCollection();
			var prc = event.getCollection(private=true);
			var loadedModules = ArrayToList(controller.getModuleService().getLoadedModules());

			if (ListFindNoCase(loadedModules, "InterviewSchedulePL") eq 0) {
				controller.getModuleService().rebuildModuleRegistry();
				controller.getModuleService().registerModule("InterviewSchedulePL");
				controller.getModuleService().activateModule("InterviewSchedulePL");
			}

			prc.xeh.AccessDenied = "InterviewSchedulePL:Main.AccessDenied";
			rc.layout="Layout.Module";
			prc.pageTitle = "Interview Schedule";
		}
	}

	function onLoad() {
		binder.map("Security@InterviewSchedulePL").to("#moduleMapping#.model.Security");
		binder.map("BreadCrumb@InterviewSchedule").to("sis_core.model.BreadCrumb").initArg(name="xmlObj", value=settings.moduleMapXML);
	}
</cfscript>
</cfcomponent>