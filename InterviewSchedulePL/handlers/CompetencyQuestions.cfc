component output="false" {
	property name="Security" inject="modules.InterviewSchedule.InterviewSchedulePL.model.Security";
	property name="InterviewScheduleAPI" inject="InterviewScheduleAPI";
	property name="MsgBox" inject="coldbox:myPlugin:MsgBox" scope="variables";
	
	function preHandler(event){
		prc.MsgBox = variables.MsgBox;
		prc.xeh.title = "InterviewSchedulePL:General.title";
		prc.xeh.tabs = "InterviewSchedulePL:General.buildTabs";
		prc.xeh.frame = "InterviewSchedulePL:General.modalIframe";
		prc.xeh.showmessage = "InterviewSchedulePL:General.messageBox";
		
		prc.userAccess = variables.Security.checkUserAccess(session.user_id,"Competency Questions");
		if (prc.userAccess eq "DENIED") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		
		prc.activetab = "Competency Questions";
		prc.uiFilter = variables.InterviewScheduleAPI.getQuestionService().createFilter(rc, cookie);
	}
	
	function index(event) {
		prc.activetab = "Competency Questions";
		prc.xeh.load = "InterviewSchedulePL:CompetencyQuestions.load";
		prc.xeh.self = "InterviewSchedulePL:CompetencyQuestions.index";
	}
	
	function load(event) {
		prc.xeh.editQuestion = "InterviewSchedulePL:CompetencyQuestions.edit";
		prc.qQuestions = variables.InterviewScheduleAPI.getQuestionService().getByCompetencyID(prc.uiFilter.getCompetencyValue());
		event.setView("CompetencyQuestions/results");
		event.setLayout("Layout.Ajax");
	}
	
	function edit(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}	
		prc.resize = false;
		prc.xeh.save = "InterviewSchedulePL:CompetencyQuestions.save";
		prc.xeh.delete = "InterviewSchedulePL:CompetencyQuestions.delete";
		prc.sessionQry = variables.InterviewScheduleAPI.getHelper().getSession(rc.sessionID);
		prc.questionTypesQry = variables.InterviewScheduleAPI.getHelper().getQuestionTypes();
		
		if (not isDefined("rc.questionData")){
			rc.questionData = variables.InterviewScheduleAPI.getQuestionService().get(rc.questionID);
		}

		rc.competencyData = variables.InterviewScheduleAPI.getCompetencyService().get(rc.competencyID);
		event.setLayout("Layout.Ajax");
	}
	
	function save(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		prc.xeh.edit = "InterviewSchedulePL:CompetencyQuestions.edit";
		var QuestionService = variables.InterviewScheduleAPI.getQuestionService();
		prc.questionData = QuestionService.createBean();
		prc.questionData.initFromStruct(rc);
		param name="rc.ISREQUIRED" default="";
		prc.questionData.setIS_REQUIRED(rc.ISREQUIRED);//COLDBOX does not accept form field to end in _REQUIRED
		bSuccess = QuestionService.save(prc.questionData);
		prc.sessionID = rc.sessionID;

		prc.competencyID = rc.COMPETENCY_ID;
		if (!bSuccess or rc.question_id neq "") {
			setNextEvent(event=prc.xeh.edit, persistStruct=prc);	
		} else {
			event.noRender();
		}			
	}
	
	function delete(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		var bSuccess = variables.InterviewScheduleAPI.getQuestionService().delete(rc.questionID);
		return bSuccess;
	}
}