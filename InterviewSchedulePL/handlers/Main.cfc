/**
 * InterviewSchedule
 *
 * @author zoltan.stir
 * @date 9/11/18
 **/
component accessors=true output=false persistent=false {


	property name="wirebox" inject="wirebox";
	property name="populator" inject="wirebox:populator";
	property name="SessionStorage" inject="coldbox:plugin:SessionStorage";
	property name="BreadCrumb" inject="BreadCrumb@InterviewSchedule";
	property name="UserDirectoryAPI" inject="id:API@UserDirectory";

	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {};

	function preHandler(event){
		var rc = event.getCollection();
		var prc = event.getCollection(private=true);

		event.paramValue("activeMenuOption", "Interview Schedule");
		rc.layout="Layout.Main";
		sessionStorage = getPlugin('SessionStorage');
		accessNodeId = variables.UserDirectoryAPI.getAccessNodeByCode(application.cbController.getSetting('ud_instance_id'), "ISCH_MODULE").access_node_id;
		prc.UserSecurity = variables.UserDirectoryAPI.getUserSecurity(application.cbController.getSetting('ud_instance_id'), sessionStorage.getVar('LoggedInUser').user_id, accessNodeId);
	}

	function index(event) {
		event.paramValue("breadCrumbCode", "ISCH_INTERVIEW_SCHEDULE");

		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadcrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");

		prc.tabListCodes = "ISCH_SETTINGS,ISCH_DAYS,ISCH_COMPETENCIES,ISCH_COMPETENCY_QUESTIONS,ISCH_INTERVIEWERS";
		prc.tabListCodes = prc.UserSecurity.checkAccessByCodeList(prc.tabListCodes);

		if (prc.tabListCodes eq "")
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");
		else
			prc.activeTab = ListFirst(prc.tabListCodes);

		prc.xeh.home = "App.index";
		prc.xeh.interviewSchedule = "InterviewSchedulePL:Main.index";
		prc.xeh.import = "InterviewSchedulePL:Import.showForm";

		prc.xmlContent = variables.BreadCrumb.getXmlObj();
		event.paramValue('active','');
		prc.active = rc.active;
		event.setView("index");
	}

	function accessdenied(event) {
		prc.xeh.login = "AdminPortal:Main.AccessDenied";
		rc.layout="";
		event.paramValue("isAjax", false);

		if (rc.isAjax)
			return renderView("AccessDenied");

		event.setView("AccessDenied");
	}

	function buildUserMenu(event) {
		return renderView("menu");
	}

	function buildBreadCrumb(event) {
		prc.xeh.home = "App.index";
		prc.breadCrumbArr = variables.BreadCrumb.getTreeElem(arguments.code);

		return renderView("breadCrumb");
	}

}