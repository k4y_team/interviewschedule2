<cfcomponent output="false">
	<cfproperty name="Security" inject="modules.InterviewSchedule.InterviewSchedulePL.model.Security">
	<cfproperty name="InterviewScheduleAPI" inject="InterviewScheduleAPI">
	<cfproperty name="MsgBox" inject="coldbox:myPlugin:MsgBox" scope="variables">
	<cfscript>
		function preHandler(event){
			prc.MsgBox = variables.MsgBox;
			prc.xeh.title = "InterviewSchedulePL:General.title";
			prc.xeh.tabs = "InterviewSchedulePL:General.buildTabs";
			prc.xeh.frame = "InterviewSchedulePL:General.modalIframe";
			prc.xeh.showmessage = "InterviewSchedulePL:General.messageBox";

			prc.userAccess = variables.Security.checkUserAccess(session.user_id,"Schedule");
			if (prc.userAccess eq "DENIED") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.uiFilter = variables.InterviewScheduleAPI.getScheduleService().createFilter(rc, cookie);
		}

		function index(event) {
			prc.activeTab = "Schedule";
			prc.xeh.self = "InterviewSchedulePL:Schedule.index";
			prc.xeh.filter = "InterviewSchedulePL:Schedule.filter";

			event.paramValue("interviewer","");
			event.paramValue("search_string","");

			event.setView("Schedule/schedule");
		}

		function data(event) {
			var Helper = variables.InterviewScheduleAPI.getHelper();
			var ScheduleService = variables.InterviewScheduleAPI.getScheduleService();
			filter = Helper.setupFilter(rc,'interviewScheduleID,dayID,circuitID,search_string');
			circuitQry = ScheduleService.getCircuits(filter);

			dataArr = ArrayNew(1);
			for (var i=1; i LTE circuitQry.recordCount; i++) {
				data = {"id" = circuitQry.CIRCUIT_ID[i], "label" = circuitQry.CIRCUIT_NAME[i] & ' - ' & circuitQry.DAY_NAME[i] & ' (' & DateFormat(circuitQry.DAY_DT[i],"dd-mmm-yyyy") & ')'};
				ArrayAppend(dataArr, data);
			}

	        event.renderData(type="JSON",data=dataArr);
		}

		function content(event) {
			prc.xeh.scheduleCandidate = "InterviewSchedulePL:Schedule.scheduleCandidate";

			var Helper = variables.InterviewScheduleAPI.getHelper();
			var ScheduleService = variables.InterviewScheduleAPI.getScheduleService();
			prc.circuitQry = ScheduleService.getCircuitByPK(rc.circuitID);

			event.paramValue("schedule_mode","setup");
			if (prc.userAccess eq "READ-ONLY") {
				rc.schedule_mode = "details";
			}

			if (rc.schedule_mode eq "setup") {
				prc.detailStruct = StructNew();

				competenciesQry = Helper.getAvailableCompetencies(rc.interviewScheduleID, prc.circuitQry.CIRCUIT_ID);
				prc.detailStruct['detail_' & prc.circuitQry.CIRCUIT_ID] = ScheduleService.getCircuitDetails(prc.circuitQry.CIRCUIT_ID);
				prc.detailStruct['avail_station_' & prc.circuitQry.CIRCUIT_ID] = competenciesQry;

				event.setView("Schedule/setup");
			} else {
				if (FindNoCase("A_",rc.search_string)) {
					applicantID = Replace(rc.search_string,"A_","","all");
					prc.schedule = Helper.getApplicantSchedule(applicantID);
				} else
				if (FindNoCase("I_",rc.search_string)) {
					interviewerID = Replace(rc.search_string,"I_","","all");
					prc.schedule = Helper.getScheduledApplicantsByInterviewerID(interviewerID);
				} else {
					prc.detailsArr = ArrayNew(1);
					circuitDetails = Helper.getScheduledApplicantsByCircuitID(prc.circuitQry.CIRCUIT_ID);
					if (circuitDetails.recordCount GT 0) {
						ArrayAppend(prc.detailsArr,circuitDetails);
					}
				}

				event.setView("Schedule/details");
			}
			event.setLayout("Layout.Ajax");
		}

		function filter(event) {
			prc.xeh.editCircuit = "InterviewSchedulePL:Schedule.editCircuit";
			prc.xeh.editStation = "InterviewSchedulePL:Schedule.editStation";
			prc.xeh.editShift = "InterviewSchedulePL:Schedule.editShift";

			prc.xeh.reorder = "InterviewSchedulePL:Schedule.reorderStation";
			prc.xeh.getApplicants = "InterviewSchedulePL:Schedule.getApplicants";
			prc.xeh.schedule = "InterviewSchedulePL:Schedule.scheduleCandidate";
			prc.xeh.unSchedule = "InterviewSchedulePL:Schedule.unScheduleCandidate";

			prc.xeh.filter = "InterviewSchedulePL:Schedule.filter";
			prc.xeh.scheduleAll = "InterviewSchedulePL:Schedule.scheduleAll";
			prc.xeh.getData4Search = "InterviewSchedulePL:Schedule.getData4Search";
			prc.xeh.createCircuit = "InterviewSchedulePL:Schedule.editCircuit";
			prc.xeh.export = "InterviewSchedulePL:Schedule.export";
			prc.xeh.data = "InterviewSchedulePL:Schedule.data";
			prc.xeh.content = "InterviewSchedulePL:Schedule.content";

			event.paramValue("interviewerID","");
			event.paramValue("schedule_mode","setup");

			if (prc.userAccess eq "READ-ONLY") {
				rc.schedule_mode = "details";
			}

			event.setView("Schedule/filter");
			event.setLayout("Layout.Ajax");
		}

		function editCircuit(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.save = "InterviewSchedulePL:Schedule.saveCircuit";
			prc.xeh.delete = "InterviewSchedulePL:Schedule.deleteCircuit";

			var ScheduleService = variables.InterviewScheduleAPI.getScheduleService();
			if (!isDefined("rc.circuitData")) {
				prc.circuitData = ScheduleService.getCircuit(rc.interviewScheduleID, rc.circuitID);
			} else {
				prc.circuitData = rc.circuitData;
			}
			prc.competenciesQry = variables.InterviewScheduleAPI.getCompetencyService().getByInterviewScheduleID(rc.interviewScheduleID);
			prc.shiftsQry = ScheduleService.getShifts(rc.interviewScheduleID);
			prc.sessionQry = variables.InterviewScheduleAPI.getHelper().getSession(rc.sessionID);
			prc.dayQry = variables.InterviewScheduleAPI.getDayService().getByInterviewScheduleID(rc.interviewScheduleID);
			event.setLayout("Layout.Ajax");
		}

		function saveCircuit(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.edit = "InterviewSchedulePL:Schedule.editCircuit";
			var ScheduleService = variables.InterviewScheduleAPI.getScheduleService();
			prc.CircuitData = ScheduleService.createBean("CircuitData");
			prc.CircuitData.initFromStruct(rc);
			bSuccess = ScheduleService.saveCircuit(prc.CircuitData);
			if (!bSuccess or rc.circuit_id neq "") {
				setNextEvent(event=prc.xeh.edit, queryString="interviewScheduleID=" & rc.interviewScheduleID, persistStruct=prc);
			} else {
				event.noRender();
			}
		}

		function deleteCircuit(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			var bSuccess = variables.InterviewScheduleAPI.getScheduleService().deleteCircuit(rc.circuit_id);
			return bSuccess;
		}

		function editStation(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.save = "InterviewSchedulePL:Schedule.saveStation";
			prc.xeh.delete = "InterviewSchedulePL:Schedule.deleteStation";

			if (!isDefined("rc.StationData")) {
				prc.StationData = variables.InterviewScheduleAPI.getScheduleService().getStation(rc.circuitID, rc.stationID);
			} else {
				prc.StationData = rc.StationData;
			}

			var Helper = variables.InterviewScheduleAPI.getHelper();
			prc.competencyQry = Helper.getAvailableCompetencies(rc.interviewScheduleID, prc.StationData.getCIRCUIT_ID(), prc.StationData.getCOMPETENCY_ID());
			if (prc.StationData.getSTATION_ID() eq "") {
				prc.interviewerQry = Helper.getAvailableInterviewers(prc.StationData.getDAY_ID(), -1);
			} else {
				prc.interviewerQry = Helper.getAvailableInterviewers(prc.StationData.getDAY_ID(), prc.StationData.getSTATION_ID());
			}
			prc.sessionQry = Helper.getSession(rc.sessionID);
			event.setLayout("Layout.Ajax");
		}

		function saveStation(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.edit = "InterviewSchedulePL:Schedule.editStation";
			var ScheduleService = variables.InterviewScheduleAPI.getScheduleService();
			prc.StationData = ScheduleService.createBean("StationData");
			prc.StationData.initFromStruct(rc);
			bSuccess = ScheduleService.saveStation(prc.StationData);
			if (!bSuccess or rc.station_id neq "") {
				setNextEvent(event=prc.xeh.edit, queryString="interviewScheduleID=" & rc.interviewScheduleID & "&circuitID=" & prc.StationData.getCIRCUIT_ID() & "&stationID=" & prc.StationData.getSTATION_ID(), persistStruct=prc);
			} else {
				event.noRender();
			}
		}

		function deleteStation(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			var bSuccess = variables.InterviewScheduleAPI.getScheduleService().deleteStation(rc.station_id);
			return bSuccess;
		}

		function reorderStation(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			var result = StructNew();
			result.success = variables.InterviewScheduleAPI.getScheduleService().reorderStation(rc.station_id);
			event.renderData(type="JSON", data=result);
		}

		function editShift(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.save = "InterviewSchedulePL:Schedule.saveShift";
			prc.xeh.delete = "InterviewSchedulePL:Schedule.deleteShift";

			if (!isDefined("rc.ShiftData")) {
				prc.ShiftData = variables.InterviewScheduleAPI.getScheduleService().getShift(rc.circuitID, rc.shiftID);
			} else {
				prc.ShiftData = rc.ShiftData;
			}

			var Helper = variables.InterviewScheduleAPI.getHelper();
			prc.sessionQry = Helper.getSession(rc.sessionID);
			prc.hourQry = Helper.getHours();
			prc.minutesQry = Helper.getMinutes();
			event.setLayout("Layout.Ajax");
		}

		function saveShift(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.edit = "InterviewSchedulePL:Schedule.editShift";
			var ScheduleService = variables.InterviewScheduleAPI.getScheduleService();
			prc.ShiftData = ScheduleService.createBean("ShiftData");
			prc.ShiftData.initFromStruct(rc);
			bSuccess = ScheduleService.saveShift(prc.ShiftData);
			if (!bSuccess or rc.shift_id neq "") {
				setNextEvent(event=prc.xeh.edit, queryString="interviewScheduleID=" & prc.ShiftData.getINTERVIEW_SCHEDULE_ID() & "&circuitID=" & prc.ShiftData.getCIRCUIT_ID() & "&shiftID=" & prc.ShiftData.getSHIFT_ID(), persistStruct=prc);
			} else {
				event.noRender();
			}
		}

		function deleteShift(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			bSuccess = variables.InterviewScheduleAPI.getScheduleService().deleteShift(rc.circuit_id, rc.shift_id);
			return bSuccess;
		}

		function getData4Search(event) {
			var interviewers = [];
			var Helper = variables.InterviewScheduleAPI.getHelper();
			filter = Helper.setupFilter(rc,'interviewScheduleID,dayID,circuitID,term');
			interviewerQry = Helper.getData4Search(filter);

			for (var i = 1; i <= interviewerQry.recordCount; i++) {
				ArrayAppend(interviewers, {"label"=interviewerQry.NAME[i], "id"=interviewerQry.ID[i]});
			}

	        event.renderData(type="JSON",data=interviewers);
		}

		function getApplicants(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			var applicants = [];
			var Helper = variables.InterviewScheduleAPI.getHelper();
			filter = Helper.setupFilter(rc,'interviewScheduleID,dayID,circuitID,term');
			applicantQry = Helper.getAvailableApplicants(filter);

			for (var i = 1; i <= applicantQry.recordCount; i++) {
				ArrayAppend(applicants, {"label"=applicantQry.LAST_NAME[i] & ", " & applicantQry.FIRST_NAME[i], "value"=applicantQry.FIRST_NAME[i] & " " & applicantQry.LAST_NAME[i], "id"=applicantQry.APPLICANT_ID[i], "referenceId"=applicantQry.REFERENCE_ID[i]});
			}

	        event.renderData(type="JSON",data=applicants);
		}

		function scheduleCandidate(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			var result = StructNew();
			var CandidateService = variables.InterviewScheduleAPI.getCandidateService();
			ScheduleCandidateData = CandidateService.createBean();
			ScheduleCandidateData.initFromStruct(rc);
			result.success = CandidateService.schedule(ScheduleCandidateData);
			event.renderData(type="JSON", data=result);
		}

		function unScheduleCandidate(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			var result = StructNew();
			result.success = variables.InterviewScheduleAPI.getCandidateService().unSchedule(rc.applicant_id);
			event.renderData(type="JSON", data=result);
		}

		function scheduleAll(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.autoSchedule = "InterviewSchedulePL:Schedule.autoSchedule";
			filter = variables.InterviewScheduleAPI.getHelper().setupFilter(rc,'interviewScheduleID,dayID,circuitID');
			prc.scheduleInfo = variables.InterviewScheduleAPI.getCandidateService().getScheduleInfo(filter);
			event.setLayout("Layout.Ajax");
		}

		function autoSchedule(event) {
			if (prc.userAccess neq "READ-WRITE") {
				setNextEvent(event=prc.xeh.AccessDenied);
			}
			prc.xeh.callback = "InterviewSchedulePL:Schedule.scheduleAll";
			filter = variables.InterviewScheduleAPI.getHelper().setupFilter(rc,'interviewScheduleID,dayID,circuitID');
			variables.InterviewScheduleAPI.getCandidateService().scheduleAll(filter);
			setNextEvent(url=event.buildLink(prc.xeh.callback)&"?interviewScheduleID=" & rc.interviewScheduleID & "&circuitID=" & rc.circuitID & "&dayID=" & rc.dayID);
		}
	</cfscript>

	<cffunction name="export" returntype="void">
		<cfargument name="event" type="any" required="true">
		
		<cfsetting requesttimeout="1800">
		<cfset var Helper = variables.InterviewScheduleAPI.getHelper()>
		<cfset filter = Helper.setupFilter(rc,'interviewScheduleID,dayID,circuitID,search_string')>
		<cfset dataQry = Helper.getData4Export(filter)>

		<cfset theSheet = SpreadsheetNew()>
		<cfset header = "OMSAS##,Last Name,First Name,Email,Date,Cicuit,Shift,Interviewer">
		<cfset spreadsheetAddRow(theSheet, header)>
		<cfset SpreadsheetFormatRow(theSheet, {bold=TRUE, textWrap=TRUE, alignment="center"}, 1)>
		<cfset spreadsheetAddRows(theSheet, dataQry)>

		<cfheader name="Set-Cookie" value="fileDownload=true; path=/" />
		<cfheader name="Content-Disposition" value="attachment;filename=ApplicantExport.xls">
		<cfcontent type="application/msexcel" reset="yes" variable="#spreadsheetReadBinary(theSheet)#">
	</cffunction>

</cfcomponent>