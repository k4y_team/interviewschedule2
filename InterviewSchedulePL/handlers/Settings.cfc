component output="false" {
	property name="Security" inject="modules.InterviewSchedule.InterviewSchedulePL.model.Security";
	property name="InterviewScheduleAPI" inject="InterviewScheduleAPI";
	property name="MsgBox" inject="coldbox:myPlugin:MsgBox" scope="variables";
	
	function preHandler(event){
		prc.MsgBox = variables.MsgBox;
		prc.xeh.title = "InterviewSchedulePL:General.title";
		prc.xeh.tabs = "InterviewSchedulePL:General.buildTabs";
		prc.xeh.frame = "InterviewSchedulePL:General.modalIframe";
		prc.xeh.showmessage = "InterviewSchedulePL:General.messageBox";
		
		prc.userAccess = variables.Security.checkUserAccess(session.user_id,"Settings");
		if (prc.userAccess eq "DENIED") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
			
		prc.uiFilter = variables.InterviewScheduleAPI.getScheduleService().createFilter(rc, cookie);
		prc.activetab = "Settings";
	}
	
	function index(event) {
		prc.activetab = "Settings";
		prc.xeh.self = "InterviewSchedulePL:Settings.index";
		prc.xeh.editInterview = "InterviewSchedulePL:Settings.edit";
		prc.xeh.load = "InterviewSchedulePL:Settings.load";
		prc.qInterviewSchedule = variables.InterviewScheduleAPI.getScheduleService().getAll();
	}
	
	function load(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		prc.xeh.save = "InterviewSchedulePL:Settings.save";
		rc.interviewQry = variables.InterviewScheduleAPI.getScheduleService().getInterviewScheduleByID(rc.interviewScheduleID);
		event.setView("Settings/edit");
		event.setLayout("Layout.Ajax");
	}
	
	function save(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		prc.xeh.load = "InterviewSchedulePL:Settings.load";
		var scheduleService = variables.InterviewScheduleAPI.getScheduleService();
		prc.InterviewScheduleData = scheduleService.createBean('InterviewScheduleData');
		prc.InterviewScheduleData.initFromStruct(rc);
		bSuccess = scheduleService.saveInterviewSchedule(prc.InterviewScheduleData);
		if (!bSuccess) {
			setNextEvent(event=prc.xeh.load, persistStruct=prc);	
		} else {
			event.noRender();		
		}			
	}
}