component output="false" {
	property name="Security" inject="modules.InterviewSchedule.InterviewSchedulePL.model.Security";
	property name="InterviewScheduleAPI" inject="InterviewScheduleAPI";
	property name="MsgBox" inject="coldbox:myPlugin:MsgBox" scope="variables";
	
	function preHandler(event){
		prc.MsgBox = variables.MsgBox;
		prc.xeh.title = "InterviewSchedulePL:General.title";
		prc.xeh.tabs = "InterviewSchedulePL:General.buildTabs";
		prc.xeh.frame = "InterviewSchedulePL:General.modalIframe";
		prc.xeh.showmessage = "InterviewSchedulePL:General.messageBox";
		
		prc.userAccess = variables.Security.checkUserAccess(session.user_id,"Competencies");
		if (prc.userAccess eq "DENIED") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		
		prc.activetab = "Competencies";
		prc.uiFilter = variables.InterviewScheduleAPI.getCompetencyService().createFilter(rc, cookie);
	}
	
	function index(event) {
		prc.activetab = "Competencies";
		prc.xeh.load = "InterviewSchedulePL:Competencies.load";
		prc.xeh.self = "InterviewSchedulePL:Competencies.index";
	}

	function load(event) {
		prc.xeh.editCompetency = "InterviewSchedulePL:Competencies.edit";
		prc.qCompetencies = variables.InterviewScheduleAPI.getCompetencyService().getByInterviewScheduleID(rc.interviewScheduleID);
		event.setView("Competencies/results");
		event.setLayout("Layout.Ajax");
	}

	function edit(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		prc.resize = false;
		prc.xeh.save = "InterviewSchedulePL:Competencies.save";
		prc.xeh.delete = "InterviewSchedulePL:Competencies.delete";
		prc.sessionQry = variables.InterviewScheduleAPI.getHelper().getSession(rc.sessionID);
		
		if (not isDefined("rc.competencyData")){
			rc.competencyData = variables.InterviewScheduleAPI.getCompetencyService().get(rc.competencyCD);
		}
		event.setLayout("Layout.Ajax");
	}
	
	function save(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		prc.xeh.edit = "InterviewSchedulePL:Competencies.edit";
		var CompetencyService = variables.InterviewScheduleAPI.getCompetencyService();
		prc.CompetencyData = CompetencyService.createBean();
		prc.CompetencyData.initFromStruct(rc);
		bSuccess = CompetencyService.save(prc.CompetencyData);
		prc.sessionID = rc.sessionID;
		if (!bSuccess or rc.competency_id neq "") {
			setNextEvent(event=prc.xeh.edit, persistStruct=prc);	
		} else {
			event.noRender();		
		}			
	}
	
	function delete(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		var bSuccess = variables.InterviewScheduleAPI.getCompetencyService().delete(rc.competencyCD);
		return bSuccess;
	}
}