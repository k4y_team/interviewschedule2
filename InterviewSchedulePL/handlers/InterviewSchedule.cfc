/**
 * InterviewSchedule
 *
 * @author zoltan.stir
 * @date 9/17/18
 **/
component  {

	property name="wirebox" inject="wirebox";
	property name="populator" inject="wirebox:populator";
	property name="SessionStorage" inject="coldbox:plugin:SessionStorage";
	property name="BreadCrumb" inject="BreadCrumb@InterviewSchedule";
	property name="UserDirectoryAPI" inject="id:API@UserDirectory";
	property name="MsgBox" inject="coldbox:myPlugin:MsgBox";

	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {};

	function preHandler(event){
		var rc = event.getCollection();
		var prc = event.getCollection(private=true);

		event.paramValue("activeMenuOption", "Interview Schedule");
		rc.layout="Layout.Main";
		sessionStorage = getPlugin('SessionStorage');
		accessNodeId = variables.UserDirectoryAPI.getAccessNodeByCode(application.cbController.getSetting('ud_instance_id'), "ISCH_MODULE").access_node_id;
		prc.UserSecurity = variables.UserDirectoryAPI.getUserSecurity(application.cbController.getSetting('ud_instance_id'), sessionStorage.getVar('LoggedInUser').user_id, accessNodeId);

		prc.xeh.home = "App.index";
		prc.xeh.interviewSchedule = "InterviewSchedulePL:Main.index";
	}

	function settings(event) {
		event.paramValue("breadCrumbCode", "ISCH_SETTINGS");

		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");
		if (prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode("ISCH_SETTINGS"),"READ_WRITE"))
			prc.readonly = false;
		else
			prc.readonly = true;

        event.paramValue("action", "LIST");

        prc.argStruct = {};
        prc.argStruct.entityName = "Settings";
        prc.argStruct.datasource = getSetting('DATASOURCE');
        prc.argStruct.callerUrl  = event.buildLink("InterviewSchedulePL:InterviewSchedule.settings");
        prc.argStruct.action     = rc.action;
        prc.argStruct.readonly   = prc.readOnly;
        prc.argStruct.path       = "sis_modules.InterviewSchedule.InterviewSchedulePL.model.entity";
        argStruct.rc = StructCopy(rc);

		return renderView('InterviewSchedule/settings');
	}

	function interviewDays(event) {

		event.paramValue("breadCrumbCode", "ISCH_DAYS");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");
		if (prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode("ISCH_DAYS"),"READ_WRITE"))
			prc.readonly = false;
		else
			prc.readonly = true;

		event.paramValue("action", "LIST");

        prc.argStruct = {};
        prc.argStruct.entityName = "InterviewDays";
        prc.argStruct.datasource = getSetting('DATASOURCE');
        prc.argStruct.callerUrl  = event.buildLink("InterviewSchedulePL:InterviewSchedule.interviewDays");
        prc.argStruct.action     = rc.action;
        prc.argStruct.readonly   = prc.readOnly;
        prc.argStruct.path       = "sis_modules.InterviewSchedule.InterviewSchedulePL.model.entity";
        argStruct.rc = StructCopy(rc);

		 return renderView('InterviewSchedule/interviewDays');
	}

	function competencies(event) {
		event.paramValue("breadCrumbCode", "ISCH_COMPETENCIES");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");
		if (prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode("ISCH_COMPETENCIES"),"READ_WRITE"))
			prc.readonly = false;
		else
			prc.readonly = true;

        event.paramValue("action", "LIST");

        prc.argStruct = {};
        prc.argStruct.entityName = "Competencies";
        prc.argStruct.datasource = getSetting('DATASOURCE');
        prc.argStruct.callerUrl  = event.buildLink("InterviewSchedulePL:InterviewSchedule.competencies");
        prc.argStruct.action     = rc.action;
        prc.argStruct.readonly   = prc.readOnly;
        prc.argStruct.path       = "sis_modules.InterviewSchedule.InterviewSchedulePL.model.entity";
        argStruct.rc = StructCopy(rc);

		return renderView('InterviewSchedule/competencies');
	}

	function competencyQuestions(event) {
		event.paramValue("breadCrumbCode", "ISCH_COMPETENCY_QUESTIONS");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");
		if (prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode("ISCH_COMPETENCY_QUESTIONS"),"READ_WRITE"))
			prc.readonly = false;
		else
			prc.readonly = true;

        event.paramValue("action", "LIST");

        prc.argStruct = {};
        prc.argStruct.entityName = "CompetencyQuestions";
        prc.argStruct.datasource = getSetting('DATASOURCE');
        prc.argStruct.callerUrl  = event.buildLink("InterviewSchedulePL:InterviewSchedule.competencyQuestions");
        prc.argStruct.action     = rc.action;
        prc.argStruct.readonly   = prc.readOnly;
        prc.argStruct.path       = "sis_modules.InterviewSchedule.InterviewSchedulePL.model.entity";
        argStruct.rc = StructCopy(rc);

		return renderView('InterviewSchedule/competencyQuestions');
	}

	function schedule(event) {
		event.paramValue("breadCrumbCode", "ISCH_SCHEDULE");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");

		return renderView('InterviewSchedule/schedule');
	}

	function interviewers(event) {
		event.paramValue("breadCrumbCode", "ISCH_INTERVIEWERS");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");

		var interviewScheduleAPI = variables.wirebox.getInstance("InterviewScheduleAPI");
		var interviewSchedule = interviewScheduleAPI.getScheduleService();
		prc.interviewScheduleQry = interviewSchedule.getAll();

		prc.xeh.loadInterviewers = "InterviewSchedulePL:InterviewSchedule.loadInterviewers";
		prc.xeh.editInterviewer = "InterviewSchedulePL:InterviewSchedule.editInterviewer";

		return renderView('InterviewSchedule/interviewers');
	}

	function loadInterviewers(event) {
		event.paramValue("breadCrumbCode", "ISCH_INTERVIEWERS");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");

		var initializer = wirebox.getInstance("ObjectStructInitializer").init("sis_core.model.ui.datatables.Request");
		initializer.read(rc);
		var gridRequest = initializer.getObject();
		var filter = wirebox.getInstance("SearchFilter");
		var interviewScheduleAPI = variables.wirebox.getInstance("InterviewScheduleAPI");
		filter.initFromStruct(rc);

		gridResponse = interviewScheduleAPI.getInterviewerService().interviewerDatatable(filter, gridRequest);
		event.renderData(type="json",data=gridResponse.toStruct());
	}

	function editInterviewer(event) {
		event.paramValue("breadCrumbCode", "ISCH_INTERVIEWERS");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");

		prc.tabListCodes = "ISCH_SETTINGS,ISCH_DAYS,ISCH_COMPETENCIES,ISCH_COMPETENCY_QUESTIONS,ISCH_INTERVIEWERS";
		prc.tabListCodes = prc.UserSecurity.checkAccessByCodeList(prc.tabListCodes);

		if (ListFind(prc.tabListCodes, "ISCH_INTERVIEWERS") eq 0)
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");
		else
			prc.activeTab = "ISCH_INTERVIEWERS";


		prc.xmlContent = variables.BreadCrumb.getXmlObj();
		prc.xeh.interviewers = "InterviewSchedulePL:InterviewSchedule.interviewers";
		prc.xeh.save = "InterviewSchedulePL:InterviewSchedule.saveInterviewer";
		prc.xeh.delete = "InterviewSchedulePL:InterviewSchedule.deleteInterviewer";

		var interviewScheduleAPI = variables.wirebox.getInstance("InterviewScheduleAPI");
		if (not isDefined("prc.interviewerData")) {
			prc.interviewerData = InterviewScheduleAPI.getInterviewerService().get(rc.interviewerId);
		}

		prc.days = InterviewScheduleAPI.getDayService().getByInterviewScheduleID(prc.interviewerData.getINTERVIEW_SCHEDULE_ID());
		prc.availableDays = InterviewScheduleAPI.getInterviewerService().getAvailabilityByInterviewerID(prc.interviewerData.getINTERVIEWER_ID());
	}

	function saveInterviewer(event) {
		event.paramValue("breadCrumbCode", "ISCH_INTERVIEWERS");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");

		prc.xeh.edit = "InterviewSchedulePL:InterviewSchedule.editInterviewer";
		prc.xeh.list = "InterviewSchedulePL:Main.index";

		var interviewScheduleAPI = variables.wirebox.getInstance("InterviewScheduleAPI");
		var InterviewerService = InterviewScheduleAPI.getInterviewerService();
		prc.interviewerData = InterviewerService.createBean();
		prc.interviewerData.initFromStruct(rc);

		bSuccess = InterviewerService.save(prc.interviewerData);

		prc.interviewScheduleID = prc.interviewerData.getINTERVIEW_SCHEDULE_ID();

		var response = StructNew();
		response.success = bSuccess;
		response.msgArray = variables.msgBox.getMessage();

		event.renderData(type="json", data=response);
		/*if (!bSuccess) {
			setNextEvent(event=prc.xeh.edit,persistStruct=prc);
		} else {
			event.noRender();
		}*/
	}

	function deleteInterviewer(event) {
		event.paramValue("breadCrumbCode", "ISCH_INTERVIEWERS");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode)))
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");

		var interviewScheduleAPI = variables.wirebox.getInstance("InterviewScheduleAPI");
		var bSuccess = InterviewScheduleAPI.getInterviewerService().delete(rc.interviewer_id);

		var response = StructNew();
		response.success = bSuccess;
		response.msgArray = variables.msgBox.getMessage();

		event.renderData(type="json", data=response);
	}
}