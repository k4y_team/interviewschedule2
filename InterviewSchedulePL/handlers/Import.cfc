component  {
	property name="BreadCrumb" inject="BreadCrumb@InterviewSchedule";
	property name="UserDirectoryAPI" inject="id:API@UserDirectory";
	property name="InterviewScheduleAPI" inject="InterviewScheduleAPI";
	property name="MsgBox" inject="coldbox:myPlugin:MsgBox";

	function preHandler(event){
		var rc = event.getCollection();
		var prc = event.getCollection(private=true);

		event.paramValue("activeMenuOption", "Import");
		rc.layout="Layout.Main";
		sessionStorage = getPlugin('SessionStorage');
		accessNodeId = variables.UserDirectoryAPI.getAccessNodeByCode(application.cbController.getSetting('ud_instance_id'), "ISCH_MODULE").access_node_id;

		prc.UserSecurity = variables.UserDirectoryAPI.getUserSecurity(application.cbController.getSetting('ud_instance_id'), sessionStorage.getVar('LoggedInUser').user_id, accessNodeId);

		prc.xeh.home = "App.index";
		prc.xeh.interviewSchedule = "InterviewSchedulePL:Main.index";
		prc.xeh.import = "InterviewSchedulePL:Import.showForm";
	}

	function showForm(event) {
		event.paramValue("breadCrumbCode", "ISCH_IMPORT_GENERIC");
		if (not prc.UserSecurity.checkAccess(variables.breadcrumb.getSecurityCode(rc.breadCrumbCode))) {
			setNextEvent("InterviewSchedulePL:Main.AccessDenied");
		}

		var arg = {};
		arg.rc = {};
		arg.rc.CODE= "IMPORT";
		arg.rc.restrictions = {};
		arg.rc.restrictions.entity_id= "IschInterviewer";
		arg.rc.restrictions.handlers= "IMPORT";
		arg.rc.noLayout= "true";
		arg.rc.checkSecurity= "false";
		prc.arg = arg;
		event.setView("Import/importForm");
	}
/*
	function uploadXLS(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.preview = true;
		prc.xeh.importForm = "InterviewSchedulePL:Interviewers.importForm";
		prc.xeh.submit = "InterviewSchedulePL:Interviewers.submitImportedInterviewers";

		var ImportService = variables.InterviewScheduleAPI.getImportService("INTERVIEWERS", rc.interviewScheduleID);
		prc.importFileName = ImportService.getService().uploadFile();
		if (prc.importFileName eq "") {
			setNextEvent(event="InterviewSchedulePL:Interviewers.importForm", persist="rc");
		}

		prc.importTitle = StructFind(ImportService.getService().getImportDesc(), "title");
		prc.fields = Replace(ImportService.getService().getImportFileHeader(), ",", ", ", "all");

		if (not ImportService.validateImport(prc.importFileName)) {
			setNextEvent(event="InterviewSchedulePL:Interviewers.importForm", persist="rc");
		}

		ImportService.validateImportData();
		prc.importResult = ImportService.getService().getImportResult();
		event.setView("Interviewers/importResult");
	}

	function submitImportedInterviewers(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.xeh.importForm = "InterviewSchedulePL:Interviewers.importForm";
		var ImportService = variables.InterviewScheduleAPI.getImportService("INTERVIEWERS", rc.interviewScheduleID);
		prc.importTitle = StructFind(ImportService.getService().getImportDesc(), "title");
		ImportService.validateImport(rc.filename);
		ImportService.validateImportData();
		ImportService.runImport();
		prc.importResult = ImportService.getService().getImportResult();
		event.setView("Interviewers/importConfirm");
	}
	<cffunction name="downloadImportTemplate" returntype="void">
		<cfargument name="event" type="any" required="true">

		<cfset var ImportService = variables.InterviewScheduleAPI.getImportService("INTERVIEWERS", -1)>

		<cfset theSheet = SpreadsheetNew()>
		<cfset SpreadsheetAddRow(theSheet,ImportService.getService().getImportFileHeader(),1,1)>
		<cfset SpreadsheetFormatRow(theSheet,{textWrap="true"},1)>

		<cfheader name="Content-Disposition" value="attachment;filename=Interviewers.xls">
		<cfcontent type="application/msexcel" reset="yes" variable="#spreadsheetReadBinary(theSheet)#">
	</cffunction>
*/
}