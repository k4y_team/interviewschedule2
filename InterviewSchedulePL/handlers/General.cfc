﻿component output="false" {
	property name="Security" inject="modules.InterviewSchedule.InterviewSchedulePL.model.Security"; 
	
	function index(event) {
		var userAccess = variables.Security.checkUserAccess(session.user_id,"Schedule");
		if (userAccess neq "DENIED") {
			setNextEvent(event="InterviewSchedulePL:Schedule.index");
		}
		userAccess = variables.Security.checkUserAccess(session.user_id,"Interview Days");
		if (userAccess neq "DENIED") {
			setNextEvent(event="InterviewSchedulePL:Days.index");
		}
		userAccess = variables.Security.checkUserAccess(session.user_id,"Competencies");
		if (userAccess neq "DENIED") {
			setNextEvent(event="InterviewSchedulePL:Competencies.index");
		}
		userAccess = variables.Security.checkUserAccess(session.user_id,"Competency Questions");
		if (userAccess neq "DENIED") {
			setNextEvent(event="InterviewSchedulePL:CompetencyQuestions.index");
		}
		userAccess = variables.Security.checkUserAccess(session.user_id,"Interviewers");
		if (userAccess neq "DENIED") {
			setNextEvent(event="InterviewSchedulePL:Interviewers.index");
		}
		userAccess = variables.Security.checkUserAccess(session.user_id,"Applicants/Scores");
		if (userAccess neq "DENIED") {
			setNextEvent(event="InterviewSchedulePL:Applicants.index");
		}
		userAccess = variables.Security.checkUserAccess(session.user_id,"Settings");
		if (userAccess neq "DENIED") {
			setNextEvent(event="InterviewSchedulePL:Settings.index");
		}
	}
	
	function buildTabs(event) {
		prc.tabList = "Schedule,Interview Days,Competencies,Competency Questions,Interviewers,Applicants/Scores,Settings";
		prc.linksList = "InterviewSchedulePL:Schedule.index,InterviewSchedulePL:Days.index,InterviewSchedulePL:Competencies.index,InterviewSchedulePL:CompetencyQuestions.index,InterviewSchedulePL:Interviewers.index,InterviewSchedulePL:Applicants.index,InterviewSchedulePL:Settings.index";
		prc.xeh.tab = 'InterviewSchedulePL:General.tab';
		prc.Security = variables.Security;
		if (isDefined("prc.tabList") and prc.tabList NEQ "") {
			return renderView('General/tabBar');
		}
	}
	
	function title(event) {
		if (!isDefined('prc.resize'))
			prc.resize = true;
		if (isDefined("prc.pageTitle") and prc.pageTitle NEQ "") {
			return renderView('General/pageTitle');
		}
	}
	
	function tab(event) {
		return renderView('General/tab');
	}
	
	function messageBox(event) {
		if (!isDefined('rc.message'))
			rc.message = '';
		if (!isDefined('rc.messageType'))
			rc.messageType = '';
		if (!isDefined('rc.timeOut'))
			rc.timeOut = 0;
		return renderView('General/messageBox');
	}
	
	function modalIframe(event) {
		return renderView('General/modalIframe');
	}
	
	function AccessDenied(event) {
		return renderView('General/AccessDenied');
	}
}