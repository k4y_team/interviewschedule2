<cfcomponent output="false">
	<cfproperty name="Security" inject="modules.InterviewSchedule.InterviewSchedulePL.model.Security">
	<cfproperty name="InterviewScheduleAPI" inject="InterviewScheduleAPI">
	<cfproperty name="MsgBox" inject="coldbox:myPlugin:MsgBox" scope="variables">
<cfscript>
	function preHandler(event){
		prc.MsgBox = variables.MsgBox;
		prc.xeh.title = "InterviewSchedulePL:General.title";
		prc.xeh.tabs = "InterviewSchedulePL:General.buildTabs";
		prc.xeh.frame = "InterviewSchedulePL:General.modalIframe";
		prc.xeh.showmessage = "InterviewSchedulePL:General.messageBox";

		prc.userAccess = variables.Security.checkUserAccess(session.user_id,"Interviewers");
		if (prc.userAccess eq "DENIED") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.activetab = "Interviewers";
		prc.uiFilter = variables.InterviewScheduleAPI.getInterviewerService().createFilter(rc, cookie);
	}

	function edit(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.resize = false;
		prc.xeh.self="InterviewSchedulePL:Interviewers.edit";
		prc.pageTitle = "Edit Interviewer";
		prc.xeh.delete = "InterviewSchedulePL:Interviewers.delete";
		prc.xeh.save = "InterviewSchedulePL:Interviewers.save";

		if (not isDefined("rc.interviewerData")) {
			rc.interviewerData = variables.InterviewScheduleAPI.getInterviewerService().get(rc.interviewerCD);
		}

		prc.days = variables.InterviewScheduleAPI.getDayService().getByInterviewScheduleID(rc.interviewScheduleID);
		prc.availableDays = variables.InterviewScheduleAPI.getInterviewerService().getAvailabilityByInterviewerID(rc.interviewerData.getINTERVIEWER_ID());
		event.setLayout("Layout.Ajax");
	}

	function index(event) {
		prc.xeh.import = "InterviewSchedulePL:Interviewers.importForm";
		prc.xeh.self = "InterviewSchedulePL:Interviewers.index";
		prc.xeh.load = "InterviewSchedulePL:Interviewers.load";
		event.paramValue("interviewerStatus", "all");
	}

	function load(event) {
		prc.xeh.schedule = "InterviewSchedulePL:Schedule.index";
		prc.xeh.editInterviewer = "InterviewSchedulePL:Interviewers.edit";
		event.paramValue("current_page", 1);
		var pageRows = 25;

		var Helper = variables.InterviewScheduleAPI.getHelper();
		filter = Helper.setupFilter(rc,'interviewScheduleID,interviewerStatus');
		filter.getTABLE_PAGE().setRECORDS_PER_PAGE(pageRows);
		filter.getTABLE_PAGE().setPAGE_NO(rc.current_page);

		prc.qInterviewers = variables.InterviewScheduleAPI.getInterviewerService().getByFilter(filter);
		return renderView('Interviewers/results');
	}

	function save(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.xeh.edit = "InterviewSchedulePL:Interviewers.edit";

		var InterviewerService = variables.InterviewScheduleAPI.getInterviewerService();
		prc.interviewerData = InterviewerService.createBean();
		prc.interviewerData.initFromStruct(rc);

		bSuccess = InterviewerService.save(prc.interviewerData);

		prc.interviewScheduleID = prc.interviewerData.getINTERVIEW_SCHEDULE_ID();

		if (!bSuccess or rc.interviewer_id neq "") {
			setNextEvent(event=prc.xeh.edit,persistStruct=prc);
		} else {
			event.noRender();
		}
	}

	function delete(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		var bSuccess = variables.InterviewScheduleAPI.getInterviewerService().delete(rc.interviewerCD);
		return bSuccess;
	}

	</cfscript>
	<cffunction name="downloadImportTemplate" returntype="void">
		<cfargument name="event" type="any" required="true">

		<cfset var ImportService = variables.InterviewScheduleAPI.getImportService("INTERVIEWERS", -1)>

		<cfset theSheet = SpreadsheetNew()>
		<cfset SpreadsheetAddRow(theSheet,ImportService.getService().getImportFileHeader(),1,1)>
		<cfset SpreadsheetFormatRow(theSheet,{textWrap="true"},1)>

		<cfheader name="Content-Disposition" value="attachment;filename=Interviewers.xls">
		<cfcontent type="application/msexcel" reset="yes" variable="#spreadsheetReadBinary(theSheet)#">
	</cffunction>
</cfcomponent>