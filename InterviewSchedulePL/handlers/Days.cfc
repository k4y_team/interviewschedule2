component output="false" {
	property name="Security" inject="modules.InterviewSchedule.InterviewSchedulePL.model.Security";
	property name="InterviewScheduleAPI" inject="InterviewScheduleAPI";
	property name="MsgBox" inject="coldbox:myPlugin:MsgBox" scope="variables";

	function preHandler(event){
		prc.MsgBox = variables.MsgBox;
		prc.xeh.title = "InterviewSchedulePL:General.title";
		prc.xeh.tabs = "InterviewSchedulePL:General.buildTabs";
		prc.xeh.frame = "InterviewSchedulePL:General.modalIframe";
		prc.xeh.showmessage = "InterviewSchedulePL:General.messageBox";
		
		prc.userAccess = variables.Security.checkUserAccess(session.user_id,"Interview Days");
		if (prc.userAccess eq "DENIED") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}		
		prc.activetab = "Interview Days";
		prc.uiFilter = variables.InterviewScheduleAPI.getDayService().createFilter(rc, cookie);
	}
	
	function index(event) {
		prc.activetab = "Interview Days";
		prc.xeh.load = "InterviewSchedulePL:Days.load";		
		prc.xeh.self = "InterviewSchedulePL:Days.index";
	}

	function load(event) {
		prc.xeh.editDay = "InterviewSchedulePL:Days.edit";
		prc.qDays = variables.InterviewScheduleAPI.getDayService().getByInterviewScheduleID(rc.interviewScheduleID);
		event.setView("Days/results");
		event.setLayout("Layout.Ajax");
	}
	function edit(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.resize = false;
		prc.xeh.save = "InterviewSchedulePL:Days.save";
		prc.xeh.delete = "InterviewSchedulePL:Days.delete";
		prc.sessionQry = variables.InterviewScheduleAPI.getHelper().getSession(rc.sessionID);
		
		if (not isDefined("rc.dayData")) {
			rc.dayData = variables.InterviewScheduleAPI.getDayService().get(rc.dayCD);
		}

		if (rc.dayData.getDAY_ID EQ '') {
			prc.pageTitle = "Add Interview Day";
		} else {
			prc.pageTitle = "Edit Interview Day";
		}
		event.setLayout("Layout.Ajax");
	}
		
	function save(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.xeh.edit = "InterviewSchedulePL:Days.edit";
		var DayService = variables.InterviewScheduleAPI.getDayService();
		prc.dayData = DayService.createBean();
		prc.dayData.initFromStruct(rc);
		bSuccess = DayService.save(prc.dayData);
		prc.sessionID = rc.sessionID;
		if (!bSuccess or rc.day_id neq "") {
			setNextEvent(event=prc.xeh.edit,persistStruct=prc);		
		} else {
			event.noRender();		
		}	
	}
	
	function delete(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		var bSuccess = variables.InterviewScheduleAPI.getDayService().delete(rc.dayCD);
		return bSuccess;
	}	
}