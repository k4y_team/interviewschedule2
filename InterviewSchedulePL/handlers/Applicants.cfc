<cfcomponent output="false">
	<cfproperty name="Security" inject="Security@InterviewSchedulePL">
	<cfproperty name="InterviewScheduleAPI" inject="InterviewScheduleAPI">
	<cfproperty name="MsgBox" inject="coldbox:myPlugin:MsgBox" scope="variables">
<cfscript>
	function preHandler(event){
		prc.MsgBox = variables.MsgBox;
		prc.xeh.title = "InterviewSchedulePL:General.title";
		prc.xeh.tabs = "InterviewSchedulePL:General.buildTabs";
		prc.xeh.frame = "InterviewSchedulePL:General.modalIframe";

		if (structKeyExists(arguments, 'eventArguments')) {
			if (structKeyExists(arguments.eventArguments, 'sessionCD'))
				rc.sessionID =	arguments.eventArguments.sessionCD;
			if (structKeyExists(arguments.eventArguments, 'reference_no'))
				rc.reference_no =	arguments.eventArguments.reference_no;
		}
		prc.userAccess = variables.Security.checkUserAccess(session.user_id,"Applicants/Scores");
		if (prc.userAccess eq "DENIED") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.activetab = "Applicants/Scores";
		prc.uiFilter = variables.InterviewScheduleAPI.getCandidateService().createFilter(rc, cookie);
	}
	
	function index(event) {
		prc.xeh.self = "InterviewSchedulePL:Applicants.index";
		event.paramValue("applicantStatus", "all");
		prc.xeh.load = "InterviewSchedulePL:Applicants.load";
		prc.xeh.import = "InterviewSchedulePL:Applicants.importForm";
	}

	function load(event) {
		prc.xeh.schedule = "InterviewSchedulePL:Schedule.index";
		prc.xeh.scoreDetails = "InterviewSchedulePL:Applicants.scoreDetails";
		prc.xeh.save = "InterviewSchedulePL:Applicants.saveScores";
		event.paramValue("current_page", 1);
		var pageRows = 25;

		var Helper = variables.InterviewScheduleAPI.getHelper();
		filter = Helper.setupFilter(rc,'interviewScheduleID,applicantStatus');
		filter.getTABLE_PAGE().setRECORDS_PER_PAGE(pageRows);
		filter.getTABLE_PAGE().setPAGE_NO(rc.current_page);
		
		prc.competencies = variables.InterviewScheduleAPI.getCompetencyService().getByInterviewScheduleID(rc.interviewScheduleID);
		prc.applicants = variables.InterviewScheduleAPI.getCandidateService().getApplicants(filter);
		event.paramValue("prc.total_pages", prc.applicants.recordcount/pageRows);
		event.paramValue("prc.total_rows", prc.applicants.recordcount);
		return renderView('Applicants/results');
	}

	function score(event) {
		/* It's used in the fusebox application 'runEvent', do not delete */
		if (NOT structKeyExists(rc, 'reference_no')) {
			return '';
		}
		prc.scheduleDetailsQry = variables.InterviewScheduleAPI.getCandidateService().getApplicantScores(rc.reference_no, rc.interviewScheduleID);
		return renderView(view="Applicants/score", module="InterviewSchedulePL", noLayout=true);
	}
	
	function scoreDetails(event) {
		prc.scoresQry = variables.InterviewScheduleAPI.getQuestionScoreService().getCompetencyScores(rc.applicantId, rc.competencyID);
		return renderView(view="Applicants/scoreDetails", module="InterviewSchedulePL");
	}

	function saveScores(event) {
		var qScoreService = variables.InterviewScheduleAPI.getQuestionScoreService();
		var prc.scoreData = qScoreService.createBean();
		prc.scoreData.initFromStruct(rc);
		bSuccess = qScoreService.saveCompetencyScore(prc.scoreData);
		event.noRender();
	}

	function importForm(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.xeh.applicants = "InterviewSchedulePL:Applicants.index";
		prc.xeh.upload = "InterviewSchedulePL:Applicants.uploadXLS";
		prc.xeh.run = "InterviewSchedulePL:Applicants.runImportXLS";
		prc.xeh.downloadTemplate = "InterviewSchedulePL:Applicants.downloadImportTemplate";
		if (isDefined("rc.interviewScheduleID")) {
			var ImportService = variables.InterviewScheduleAPI.getImportService();
			ImportService.init("APPLICANTS", rc.interviewScheduleID);
			prc.importTitle = "Scores Import";
			prc.fields = ImportService.getService().getImportFileHeader();
		}	
		event.setView("Applicants/importForm");
	}

	function uploadXLS(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.preview = true;
		prc.xeh.importForm = "InterviewSchedulePL:Applicants.importForm"; 
		prc.xeh.submit = "InterviewSchedulePL:Applicants.submitImportedApplicants";

		var ImportService = variables.InterviewScheduleAPI.getImportService("APPLICANTS", rc.interviewScheduleID);
		prc.importFileName = ImportService.getService().uploadFile(); 
		if (prc.importFileName eq "") {
			setNextEvent(event="InterviewSchedulePL:Applicants.importForm", persist="rc");
		}
		
		prc.importTitle = "Scores Import";
		prc.fields = ImportService.getService().getImportFileHeader();
		
		if (not ImportService.validateImport(prc.importFileName)) {
			setNextEvent(event="InterviewSchedulePL:Applicants.importForm", persist="rc");
		}
		ImportService.validateImportData();
		prc.importResult = ImportService.getService().getImportResult();
		event.setView("Applicants/importResult");
	}

	function submitImportedApplicants(event) {
		if (prc.userAccess neq "READ-WRITE") {
			setNextEvent(event=prc.xeh.AccessDenied);
		}
		prc.xeh.importForm = "InterviewSchedulePL:Applicants.importForm";		
		var ImportService = variables.InterviewScheduleAPI.getImportService("APPLICANTS", rc.interviewScheduleID);
		prc.importTitle = "Scores Import";
		prc.fields = ImportService.getService().getImportFileHeader();

		ImportService.validateImport(rc.filename);
		ImportService.validateImportData();
		ImportService.runImport();
		prc.importResult = ImportService.getService().getImportResult();
		event.setView("Applicants/importConfirm");
	}
	
	function recalculateScores(event) {
		var CandidateService = variables.InterviewScheduleAPI.getCandidateService();
		CandidateService.recalculateScores();	
	}
	</cfscript>	
		<cffunction name="downloadImportTemplate" returntype="void">
			<cfargument name="event" type="any" required="true">
			<cfset var ImportService = variables.InterviewScheduleAPI.getImportService("APPLICANTS", rc.interviewScheduleID)>
			
			<cfset theSheet = SpreadsheetNew()>
			<cfset SpreadsheetAddRow(theSheet,ImportService.getService().getImportFileHeader(),1,1)>
			<cfset SpreadsheetFormatRow(theSheet,{textWrap="true"},1)>
	
			<cfheader name="Content-Disposition" value="attachment;filename=Applicants.xls">
			<cfcontent type="application/msexcel" reset="yes" variable="#spreadsheetReadBinary(theSheet)#">
		</cffunction>
</cfcomponent>