<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<link href="css/custom2.css" rel="stylesheet" type="text/css" />
	<K4Y:includesFile fileName="module.js" moduleName="#event.getCurrentModule()#" fileType="js"/>
	<K4Y:includesFile fileName="jquery.file-Download.js" moduleName="#event.getCurrentModule()#" fileType="js"/>
	<K4Y:includesFile fileName="styles.css" moduleName="#event.getCurrentModule()#" fileType="css"/>
	<script type="text/javascript" src="#getSetting('applicationBaseURL')#/includes/js/plugins/angular/tabBars.js"></script>

	<cfset userSessionData = getPlugin('SessionStorage').getVar('LoggedInUser')>
	<aside id="left-panel" class="">
		<div class="login-info">
			<span>
				<img src="img/icons/user-icon.png" alt="">
				<div class="user-name">
					<div class="aligned-user-name">
						#userSessionData.name#
					</div>
				</div>
			</span>
		</div>
		#runEvent(event="InterviewSchedulePL:Main.BuildUserMenu",prepostExempt=false)#
		<span class="minifyme">
			<i class="fa fa-arrow-circle-left hit"></i>
		</span>
	</aside>

	<div class="wrapper">
		<div class="content" id="content">
			<div id="ribbon">
				#runEvent(event="InterViewSchedulePL:Main.BuildBreadCrumb",prepostExempt=false,eventArguments={code=rc.breadCrumbCode})#
			</div>
		    <div class="workplace">
	   			<div id="message-area"></div>
		    	#renderView()#
		    </div>
		</div>
	</div>
</cfoutput>