<cfcomponent name="CircuitData" output="false" extends="sis_core.model.ModelBase">
	<cfproperty name="CIRCUIT_ID" type="string"/>
	<cfproperty name="CIRCUIT_NAME" type="string"/>
	<cfproperty name="DAY_ID" type="string"/>
	<cfproperty name="LOCATION" type="string"/>
	<cfproperty name="SHIFT_ID_LIST" type="string">
	<cfproperty name="COMPETENCY_ID_LIST" type="string">
	<cfproperty name="COLOR" type="string">
	
	<cffunction name="init" access="public" returntype="CircuitData" output="false">
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.CIRCUIT_NAME = "" />
		<cfset variables.DAY_ID = "" />
		<cfset variables.LOCATION = "" />
		<cfset variables.SHIFT_ID_LIST = "" />
		<cfset variables.COMPETENCY_ID_LIST = "" />
		<cfset variables.COLOR = "" />

		<cfreturn this />
 	</cffunction>

	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CIRCUIT_ID = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>

	<cffunction name="setCIRCUIT_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CIRCUIT_NAME = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_NAME" access="public" returntype="string" output="false">
		<cfreturn variables.CIRCUIT_NAME />
	</cffunction>

	<cffunction name="setDAY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DAY_ID = arguments.val />
	</cffunction>
	<cffunction name="getDAY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_ID />
	</cffunction>

	<cffunction name="setLOCATION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LOCATION = arguments.val />
	</cffunction>
	<cffunction name="getLOCATION" access="public" returntype="any" output="false">
		<cfreturn variables.LOCATION />
	</cffunction>

	<cffunction name="setSHIFT_ID_LIST" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.SHIFT_ID_LIST = arguments.val />
	</cffunction>
	<cffunction name="getSHIFT_ID_LIST" access="public" returntype="any" output="false">
		<cfreturn variables.SHIFT_ID_LIST />
	</cffunction>

	<cffunction name="setCOMPETENCY_ID_LIST" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.COMPETENCY_ID_LIST = arguments.val />
	</cffunction>
	<cffunction name="getCOMPETENCY_ID_LIST" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_ID_LIST />
	</cffunction>

	<cffunction name="setCOLOR" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.COLOR = arguments.val />
	</cffunction>
	<cffunction name="getCOLOR" access="public" returntype="any" output="false">
		<cfreturn variables.COLOR />
	</cffunction>
	
</cfcomponent>