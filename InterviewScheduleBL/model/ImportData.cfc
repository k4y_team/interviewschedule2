<cfcomponent displayname="importData" output="false">

	<cfproperty name="INTERVIEW_SCHEDULE_ID" displayname="INTERVIEW_SCHEDULE_ID" type="string">
	<cfproperty name="FILE_NAME" displayname="FILE_NAME" type="string">
	<cfproperty name="ERROR" displayname="ERROR" type="string">
	
	<cffunction name="init" access="public" output="false" returntype="importData">
		<cfset variables.INTERVIEW_SCHEDULE_ID = "">
		<cfset variables.FILE_NAME = "">
		<cfset variables.ERROR = "">
		<cfreturn this />
	</cffunction>
		
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="string" required="true">
		<cfset variables.INTERVIEW_SCHEDULE_ID = arguments.val>
	</cffunction>
		
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="string" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID/>
	</cffunction>

	<cffunction name="setFILE_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="string" required="true">
		<cfset variables.FILE_NAME = arguments.val/>
	</cffunction>

	<cffunction name="getFILE_NAME" access="public" returntype="string" output="false">
		<cfreturn variables.FILE_NAME/>
	</cffunction>

	<cffunction name="setERROR" access="public" returntype="void" output="false">
		<cfargument name="val" type="string" required="true">
		<cfset variables.ERROR = arguments.val/>
	</cffunction>

	<cffunction name="getERROR" access="public" returntype="string" output="false">
		<cfreturn variables.ERROR />
	</cffunction>
	
</cfcomponent>