<cfcomponent name="ImportApplicantValidator" extends="importValidator">
	<cfproperty name="interviewScheduleID" type="numeric">
	
	<cffunction name="init" access="public" output="false" returntype="ImportApplicantValidator">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		
		<cfset variables.interviewScheduleID = arguments.interviewScheduleID>
		<cfset variables.invalidRefNoList = "">

		<cfset super.init()>
		<cfreturn this>
	</cffunction>

	<cffunction name="getCompQuestionsDetails" access="private" returntype="Struct">
		<cfset var competencyQuestionGtw = variables.dbFactory.create("IschCompetencyQuestionGTW")>
		<cfset var compQuestionsDetailsQry  = "">
		
		<cfif not isDefined("variables.compQuestions")>
			<cfset compQuestionsDetailsQry = competencyQuestionGtw.getCompetencyQuestionsDetails(variables.interviewScheduleID)>
			<cfset variables.compQuestions = StructNew()>
			<cfloop query="compQuestionsDetailsQry">
				<cfif not StructKeyExists(compQuestions,UCase(compQuestionsDetailsQry.competency_name))>
					<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)] = StructNew()>
				<cfelseif not StructKeyExists(compQuestions[UCase(compQuestionsDetailsQry.competency_name)],compQuestionsDetailsQry.question_rank)>
					<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank] = StructNew()>
				</cfif>	
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["QUESTION_ID"]  = compQuestionsDetailsQry.question_id>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["IS_REQUIRED"]  = compQuestionsDetailsQry.is_required>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["QUESTION_TYPE_CODE"]  = compQuestionsDetailsQry.question_type_code>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["RATING_ID"]  = compQuestionsDetailsQry.rating_id>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["MIN_VALUE"]  = compQuestionsDetailsQry.min_value>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["MAX_VALUE"]  = compQuestionsDetailsQry.max_value>
			</cfloop>
		</cfif>	
		<cfreturn variables.compQuestions>
	</cffunction>
	
	<cffunction name="validateQuestion" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		<cfargument name="record" required="true">
		<cfargument name = "headername" required="true"> 
		
		<cfset var questionRank = Replace(headername,"q_","")>
		<cfset var competencyName = UCase(arguments.record["COMPETENCY"][arguments.record.currentrow])>
		<cfset var compQuestions = getCompQuestionsDetails()>
		
		<cfif StructKeyExists(compQuestions,competencyName)>
			<cfif not StructKeyExists(compQuestions[competencyName],questionRank)>
				<cfthrow message="is not defined in competency" type="validation_warning">
			<cfelse>
				<cfif compQuestions[competencyName][questionRank].is_required eq "Y" && Trim(val) eq "">
					<cfthrow message="Field is required." type="validation_error">
				<cfelseif Trim(val) neq "">
					<cfif isNumeric(compQuestions[competencyName][questionRank].rating_id)>   <!--- rating scale type--->
						<cfset super.validateNumeric(arguments.val)>
						<cfif not (arguments.val gte  compQuestions[competencyName][questionRank].MIN_VALUE  AND  arguments.val lte compQuestions[competencyName][questionRank].MAX_VALUE)>
							<cfthrow message="'#NumberFormat(arguments.val,'_')#' has to be a numeric value between #compQuestions[competencyName][questionRank].MIN_VALUE# and #compQuestions[competencyName][questionRank].MAX_VALUE#" type="validation_error">
						</cfif>
					<cfelseif compQuestions[competencyName][questionRank].question_type_code eq "YES_NO">
						<cfset super.validateNumeric(arguments.val)>
						<cfif Val(arguments.val) neq  0  &&   Val(arguments.val) neq 1>
							<cfthrow message="'#NumberFormat(arguments.val,'_')#' has to be a numeric value: 0 or 1" type="validation_error">
						</cfif>
					<cfelse>
						<cfif Len(Trim(arguments.val)) gt 4000>
							<cfthrow message="Text exceeded 4000 chars" type="validation_error">
						</cfif>
					</cfif>		
				</cfif>	
			</cfif>
		</cfif>
	</cffunction>

	<cffunction name="validateCompetency" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		
		<cfset var compQuestions = getCompQuestionsDetails()>
		<cfif arguments.val neq "" >
			<cfif not StructKeyExists(compQuestions,Ucase(arguments.val))>
				<cfthrow message="'#arguments.val#' has not a valid competency." type="validation_error">
			</cfif>
		</cfif>
	</cffunction>		

	<cffunction name="validateReferanceNo" access="public" output="false" returntype="void">
	 	<cfargument name="val" required="true">
		<cfset var applicantsGtw = "">
			
		<cfif Trim(val) eq "">
			<cfthrow message="Field is required." type="validation_error">
		<cfelse>
			<cfset super.validateNumeric(arguments.val)>
			<cfif not ListFindNoCase(variables.invalidRefNoList,arguments.val)>
				<cfset applicantsGtw = variables.dbFactory.create("ApplicantsGTW")>
				<cfset existingApplicant = applicantsGtw.getByReferenceId(variables.interviewScheduleID, arguments.val)>
				<cfif existingApplicant.recordcount eq 0>
					<cfset variables.invalidRefNoList = ListAppend(variables.invalidRefNoList,arguments.val)>
					<cfthrow message="'#NumberFormat(arguments.val,'_')#' has  not a valid OMSAS##." type="validation_error">
				</cfif>
			<cfelse>	
				<cfthrow message="'#NumberFormat(arguments.val,'_')#' has  not a valid OMSAS##." type="validation_error">
			</cfif>	
		</cfif>
	</cffunction>		
</cfcomponent>