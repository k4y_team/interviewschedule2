<cfcomponent name="CompetencyData" extends="sis_core.model.ModelBase" output="false">
	<cfproperty name="COMPETENCY_ID" type="string" />
	<cfproperty name="COMPETENCY_NAME" type="string" />
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="string" />
	<cfproperty name="DURATION_MINS" type="string" />
		
	<cffunction name="init" access="public" returntype="CompetencyData" output="false">
		<cfset variables.COMPETENCY_ID = "" />
		<cfset variables.COMPETENCY_NAME = "" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		<cfset variables.DURATION_MINS = "" />
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setCOMPETENCY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.COMPETENCY_ID = val/>
	</cffunction>
	<cffunction name="getCOMPETENCY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_ID />
	</cffunction>

	<cffunction name="setCOMPETENCY_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.COMPETENCY_NAME = arguments.val />
	</cffunction>
	<cffunction name="getCOMPETENCY_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_NAME />
	</cffunction>
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = val/>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>

	<cffunction name="setDURATION_MINS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.DURATION_MINS = val/>
	</cffunction>
	<cffunction name="getDURATION_MINS" access="public" returntype="any" output="false">
		<cfreturn variables.DURATION_MINS />
	</cffunction>

</cfcomponent>