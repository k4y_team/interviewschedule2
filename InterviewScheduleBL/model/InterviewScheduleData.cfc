<cfcomponent name="InterviewScheduleData" displayname="InterviewScheduleData" extends="sis_core.model.ModelBase" output="false">
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="string" />
	<cfproperty name="INTERVIEW_SCHEDULE_NAME" type="string" />
	<cfproperty name="SESSION_ID" type="string" />
	<cfproperty name="BREAK_BETWEEN_STATIONS" type="string" />
	<cfproperty name="SCORE_WEIGHT" type="string" />
		
	<cffunction name="init" access="public" returntype="InterviewScheduleData" output="false" displayname="init" hint="I initialize a IschInterviewSchedule">
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		<cfset variables.INTERVIEW_SCHEDULE_NAME = "" />
		<cfset variables.SESSION_ID = "" />
		<cfset variables.BREAK_BETWEEN_STATIONS = "" />
		<cfset variables.SCORE_WEIGHT = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.INTERVIEW_SCHEDULE_ID = val/>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>
	<cffunction name="setINTERVIEW_SCHEDULE_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEW_SCHEDULE_NAME = arguments.val />
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_NAME />
	</cffunction>
	<cffunction name="setSESSION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.SESSION_ID = val/>
	</cffunction>
	<cffunction name="getSESSION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.SESSION_ID />
	</cffunction>
	<cffunction name="setBREAK_BETWEEN_STATIONS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.BREAK_BETWEEN_STATIONS = val/>
	</cffunction>
	<cffunction name="getBREAK_BETWEEN_STATIONS" access="public" returntype="any" output="false">
		<cfreturn variables.BREAK_BETWEEN_STATIONS />
	</cffunction>
	<cffunction name="setSCORE_WEIGHT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.SCORE_WEIGHT = val/>
	</cffunction>
	<cffunction name="getSCORE_WEIGHT" access="public" returntype="any" output="false">
		<cfreturn variables.SCORE_WEIGHT />
	</cffunction>
</cfcomponent>