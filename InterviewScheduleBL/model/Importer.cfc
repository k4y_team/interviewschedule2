<cfcomponent name="Importer" displayname="Importer" extends="ServiceBase" output="false">
	<cfproperty name="importData" inject="importData">
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="numeric">
	
	<cffunction name="init" access="public" returntype="Importer" output="false" displayname="init">
		<cfargument name="importCode" type="string" required="false" default="">
		<cfargument name="interviewScheduleID" type="string" required="false" default="">

		<cfset variables.INTERVIEW_SCHEDULE_ID = arguments.interviewScheduleID />
		<cfswitch expression="#Ucase(arguments.importCode)#">
			<cfcase value="INTERVIEWERS">
				<cfset variables.importXLS = variables.wirebox.getInstance(name="ImportInterviewers",initArguments={interviewScheduleID=arguments.interviewScheduleID})>
 				<cfset variables.importValidator = variables.wirebox.getInstance(name="ImportInterviewerValidator",initArguments={interviewScheduleID=arguments.interviewScheduleID})>
				<cfset variables.dbFactory = variables.wirebox.getInstance(name="dbFactory@InterviewSchedule")>

				<cfset ischInterviewScheduleGtw = variables.dbFactory.create("ischInterviewScheduleGtw")>
				<cfset ischInterviewScheduleQry = ischInterviewScheduleGtw.getByPK(variables.INTERVIEW_SCHEDULE_ID)>
				<cfset variables.importXLS.setImportTitle("Import Interviewers for #ischInterviewScheduleQry.interview_schedule_name#")>
				
				<cfset variables.importXLS.setValidator(variables.importValidator)>
			</cfcase>
				<cfcase value="APPLICANTS">
				<cfset variables.importXLS = variables.wirebox.getInstance("ImportApplicants")>
				<cfset variables.importXLS.init(arguments.interviewScheduleID)>
 				<cfset variables.importValidator = variables.wirebox.getInstance(name="ImportApplicantValidator",initArguments={interviewScheduleID=arguments.interviewScheduleID})>
				<cfset variables.dbFactory = variables.wirebox.getInstance(name="dbFactory@InterviewSchedule")>
				<cfset variables.importXLS.setImportTitle("Import Applicant/Scores")>
				<cfset variables.importXLS.setValidator(variables.importValidator)>
			</cfcase>

			<cfcase value="SCORES">
				<!--- <cfset variables.importXLS = variables.wirebox.getInstance("ImportScores").init(arguments.interviewScheduleID)> --->
			</cfcase>
		</cfswitch>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getService" access="public" returntype="any" output="false">
		<cfreturn variables.ImportXLS />
	</cffunction>
	
	<cffunction name="validateImport" access="public" returntype="boolean" output="false">
		<cfargument name="fileName" type="string" required="false" default="">
		
		<cfset var success = true>
		<cfset variables.importData.setINTERVIEW_SCHEDULE_ID(variables.INTERVIEW_SCHEDULE_ID)>
		<cftry>
			<cfset variables.importXLS.validateFileStruct(variables.importData, arguments.fileName)>
			<cfcatch>
				<cfset success =  false>
				<cfset variables.MsgBox.setMessage("error", "The file provided is not valid.")>
			</cfcatch>
		</cftry>
		
		<cfreturn success>
	</cffunction>

	<cffunction name="validateImportData" access="public" returntype="boolean" output="false">
		<cfset var success = true>
		<cfset variables.importData.setINTERVIEW_SCHEDULE_ID(variables.INTERVIEW_SCHEDULE_ID)>
		
		<cftry>
			<cfset variables.importXLS.validateFileData(variables.importData)>
			<cfcatch>
				<cfset success =  false>
				<cfrethrow>
			</cfcatch>
		</cftry>
		
		<cfreturn success>
	</cffunction>
	
	<cffunction name="runImport" access="public" returntype="boolean" output="false">
		<cfset var success = true>
		
		<cftry>
			<cfset variables.importXLS.processData(variables.importData)>
			<cfcatch>
				<cfset success = false>
				<cfrethrow>
			</cfcatch>
		</cftry>
		
		<cfreturn success>
	</cffunction>

</cfcomponent>