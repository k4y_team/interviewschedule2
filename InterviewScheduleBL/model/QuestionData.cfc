<cfcomponent name="QuestionData" extends="sis_core.model.ModelBase" output="false">
		<cfproperty name="QUESTION_ID" type="string" />
		<cfproperty name="COMPETENCY_ID" type="string" />
		<cfproperty name="QUESTION_NAME" type="string" />
		<cfproperty name="IS_REQUIRED" type="string" />
		<cfproperty name="IS_EDITABLE" type="string" />
		<cfproperty name="QUESTION_RANK" type="string" />
		<cfproperty name="QUESTION_TYPE_ID" type="string" />
		<cfproperty name="CREATION_DT" type="string" />
		<cfproperty name="CREATION_ID" type="string" />
		<cfproperty name="MODIFICATION_DT" type="string" />
		<cfproperty name="MODIFICATION_ID" type="numeric" />
		
	<cffunction name="init" access="public" returntype="QuestionData" output="false" displayname="init" hint="I initialize a QuestionData">
		<cfset variables.QUESTION_ID = "" />
		<cfset variables.COMPETENCY_ID = "" />
		<cfset variables.QUESTION_NAME = "" />
		<cfset variables.IS_REQUIRED = "" />
		<cfset variables.IS_EDITABLE = "" />
		<cfset variables.QUESTION_RANK = "" />
		<cfset variables.QUESTION_TYPE_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setQUESTION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.QUESTION_ID = val/>
	</cffunction>
	<cffunction name="getQUESTION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_ID />
	</cffunction>

	<cffunction name="setCOMPETENCY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.COMPETENCY_ID = val/>
	</cffunction>
	<cffunction name="getCOMPETENCY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_ID />
	</cffunction>

	<cffunction name="setQUESTION_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.QUESTION_NAME = arguments.val />
	</cffunction>
	<cffunction name="getQUESTION_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_NAME />
	</cffunction>

	<cffunction name="setIS_REQUIRED" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.IS_REQUIRED = arguments.val />
	</cffunction>
	<cffunction name="getIS_REQUIRED" access="public" returntype="any" output="false">
		<cfreturn variables.IS_REQUIRED />
	</cffunction>

	<cffunction name="setIS_EDITABLE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.IS_EDITABLE = arguments.val />
	</cffunction>
	<cffunction name="getIS_EDITABLE" access="public" returntype="any" output="false">
		<cfreturn variables.IS_EDITABLE />
	</cffunction>

	<cffunction name="setQUESTION_RANK" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.QUESTION_RANK = val/>
	</cffunction>
	<cffunction name="getQUESTION_RANK" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_RANK />
	</cffunction>

	<cffunction name="setQUESTION_TYPE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.QUESTION_TYPE_ID = val/>
	</cffunction>
	<cffunction name="getQUESTION_TYPE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_TYPE_ID />
	</cffunction>

	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.CREATION_DT = val/>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>

	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.CREATION_ID = val/>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>

	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.MODIFICATION_DT = val/>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>

	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
			<cfset variables.MODIFICATION_ID = val/>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>

</cfcomponent>
