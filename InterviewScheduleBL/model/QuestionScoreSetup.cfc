<cfcomponent name="QuestionScoreSetup" output="false" extends="ServiceBase">
	
	<cffunction name="createBean" returntype="QuestionScoreData" output="false" access="public">
		<cfset QuestionScoreData = variables.wirebox.getInstance("QuestionScoreData").init()>
		<cfreturn QuestionScoreData>
	</cffunction>
	
	<cffunction name="getCompetencyScores" access="public" returntype="query" output="false" displayname="getCompetencyScores">
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfargument name="competencyID" type="numeric" required="true" />

		<cfset qScoreGTW = variables.dbFactory.create("IschQuestionScoreGTW")>
		<cfset scoresQry = qScoreGTW.getCompetencyScores(arguments.applicantID,arguments.competencyID)>
		<cfreturn scoresQry>
	</cffunction>
	
	<cffunction name="saveCompetencyScore" access="public" returntype="boolean" output="false" displayname="saveCompetencyScore">
		<cfargument name="scoreData" required="true" type="QuestionScoreData">

		<cfset qScoreDAO = variables.dbFactory.create("IschQuestionScoreDAO")>
		<cfset qScoreVO = variables.dbFactory.create("IschQuestionScoreVO")>
		<cfset qScoreVO = qScoreDAO.read(arguments.scoreData.getQUESTION_ID(),arguments.scoreData.getAPPLICANT_ID())>
		<cfset success = validateCompetencyScore(arguments.scoreData)>
		<cfif success>
			<cftry>
				<cfset qScoreVO.setAPPLICANT_ID(arguments.scoreData.getAPPLICANT_ID())>
				<cfset qScoreVO.setANSWER_TEXT(arguments.scoreData.getANSWER_TEXT())>
				<cfif qScoreVO.getQUESTION_ID() eq "">
					<cfset qScoreVO.setQUESTION_ID(arguments.scoreData.getQUESTION_ID())>
					<cfset qScoreVO.setCREATION_ID(session.user_id)>
					<cfset qScoreVO.setCREATION_DT(Now())>
					<cfset qScoreDAO.add(qScoreVO)>
				<cfelse>
					<cfset qScoreVO.setMODIFICATION_ID(session.user_id)>>
					<cfset qScoreVO.setMODIFICATION_DT(Now())>
					<cfset qScoreDAO.update(qScoreVO)>
				</cfif>
				<cfset success = true>
				<cfset variables.MsgBox.success("Competency Score was successfully updated.")>
				<cfcatch>
					<cfset success = false>
					<cfset variables.MsgBox.error("Competency Score was not updated.")>
				</cfcatch>
			</cftry>
		</cfif>
		<cfreturn success>
	</cffunction>

	<cffunction name="validateCompetencyScore" access="public" returntype="boolean" output="false" displayname="validateCompetencyScore">
		<cfargument name="scoreData" required="true" type="QuestionScoreData">
		<cfset success = true>
		<cfif Len(arguments.scoreData.getANSWER_TEXT()) GT 4000>
			<cfset success = false>
			<cfset variables.MsgBox.error("You have exceeded the character limit of 4000 characters.")>
		</cfif>
		<cfreturn success>
	</cffunction>
</cfcomponent>