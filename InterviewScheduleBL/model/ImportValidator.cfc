<cfcomponent name="ImportValidator" extends="ServiceBase">
	<cffunction name="init" access="public" output="false" returntype="ImportValidator">
		<cfreturn this>
	</cffunction>
	
	<cffunction name="isTime" access="private" output="false" returntype="boolean">
		<cfargument name="time" type="string" required="true">
		<cfargument name="format" type="string" required="false">
		
		<cfset var found = 0>
		
		<cfif isDefined("arguments.format") and arguments.format eq "12">
			<cfset found = reFindNoCase("^((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [ap]m))$", arguments.time)>
		<cfelseif isDefined("arguments.format") and arguments.format eq "24">
	        <cfset found = reFindNoCase("^([01]\d|2[0-3])(:[0-5]\d){0,2}$", arguments.time)>
		<cfelse>
			<cfset found = reFindNoCase("^([01]\d|2[0-3])(:[0-5]\d){0,2}$|^((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [ap]m))$", arguments.time)>
		</cfif>
	    
	    <cfif found gt 0>
	    	<cfreturn true>
	    <cfelse>
	    	<cfreturn false>
	    </cfif>
	</cffunction>

	
	<cffunction name="validateNumeric" access="public" output="false" returntype="void">
		<cfargument name="val" required="true">
		
		<cfif not IsNumeric(arguments.val) AND arguments.val neq "">
			<cfthrow message="'#arguments.val#' is not a valid numeric." type="validation_error" />
		</cfif>
	</cffunction>
	
	<cffunction name="validateGender" access="public" output="false" retturntype="void">
		<cfargument name="val" required="true">
		
		<cfif ListFindNoCase('F,M',arguments.val) EQ 0>
			<cfthrow message="'#arguments.val#' is not a valid gender." type="validation_error" />
		</cfif>
	</cffunction>
	
	<cffunction name="validateTime" access="public" output="false" returntype="void">
		<cfargument name="val" required="true">
		
		<cfif arguments.val neq "">
		<cftry>
			<cfset tmp = ParseDateTime("01/01/2000 " & arguments.val)>
			<cfcatch>
				<cfthrow message="'#arguments.val#' is not a valid time format." type="validation_error">
			</cfcatch>
		</cftry>
		</cfif>
	</cffunction>
	
	<cffunction name="validateDate" access="public" output="false" returntype="void">
		<cfargument name="val" required="true">

		<cfif arguments.val neq "" AND not isDate(Trim(arguments.val))>
			<cfthrow message="'#arguments.val#' is not a valid date format." type="validation_error">
		</cfif>
	</cffunction>

	<cffunction name="validateEmail" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		
		<cfset var emailExp = "^['_a-z0-9-]+(\.['_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.(([a-z]{2,3})|(aero|coop|info|museum|name|jobs|travel))$">
		
		<cfif arguments.val neq '' and not REFindNoCase(emailExp, arguments.val)>
			<cfthrow message="'#arguments.val#' is not a valid email." type="validation_error">
		</cfif>
	</cffunction>
</cfcomponent>