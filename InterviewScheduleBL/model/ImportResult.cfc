<cfcomponent displayname="importResult" output="false">

	<cfproperty name="TOTAL_RECORDS" displayname="TOTAL_RECORDS" type="numeric">
	<cfproperty name="BAD_RECORDS" displayname="BAD_RECORDS" type="numeric">
	<cfproperty name="GOOD_RECORDS" displayname="GOOD_RECORDS" type="numeric">
	<cfproperty name="WARNING_RECORDS" displayname="WARNING_RECORDS" type="numeric">
	<cfproperty name="BAD_RECORDS_DATA" displayname="BAD_RECORDS_DATA" type="query">
	<cfproperty name="WARNING_RECORDS_DATA" displayname="WARNING_RECORDS_DATA" type="query">
	<cfproperty name="GOOD_RECORDS_DATA" displayname="GOOD_RECORDS_DATA" type="query">
	
	<cfproperty name="HAVE_MORE_COLUMNS" displayname="HAVE_MORE_COLUMNS" type="boolean">
	
	<cffunction name="init" access="public" output="false" returntype="importResult">
		<cfset variables.TOTAL_RECORDS = 0>
		<cfset variables.BAD_RECORDS = 0>
		<cfset variables.GOOD_RECORDS = 0>
		<cfset variables.WARNING_RECORDS = 0>
		<cfset variables.BAD_RECORDS_DATA = "">
		<cfset variables.WARNING_RECORDS_DATA = "">
		<cfset variables.GOOD_RECORDS_DATA = "">
		<cfset variables.HAVE_MORE_COLUMNS = "">
		<cfreturn this />
	</cffunction>
		
	<cffunction name="setTOTAL_RECORDS" access="public" returntype="void" output="false">
		<cfargument name="val" type="string" required="true">
		<cfset variables.TOTAL_RECORDS = arguments.val>
	</cffunction>
		
	<cffunction name="getTOTAL_RECORDS" access="public" returntype="string" output="false">
		<cfreturn variables.TOTAL_RECORDS/>
	</cffunction>
	
	<cffunction name="setBAD_RECORDS" access="public" returntype="void" output="false">
		<cfargument name="val" type="string" required="true">
		<cfset variables.BAD_RECORDS = arguments.val/>
	</cffunction>

	<cffunction name="getBAD_RECORDS" access="public" returntype="string" output="false">
		<cfreturn variables.BAD_RECORDS/>
	</cffunction>

	<cffunction name="setGOOD_RECORDS" access="public" returntype="void" output="false">
		<cfargument name="val" type="string" required="true">
		<cfset variables.GOOD_RECORDS = arguments.val/>
	</cffunction>

	<cffunction name="getGOOD_RECORDS" access="public" returntype="string" output="false">
		<cfreturn variables.GOOD_RECORDS />
	</cffunction>

	<cffunction name="setWARNING_RECORDS" access="public" returntype="void" output="false">
		<cfargument name="val" type="string" required="true">
		<cfset variables.WARNING_RECORDS = arguments.val/>
	</cffunction>

	<cffunction name="getWARNING_RECORDS" access="public" returntype="string" output="false">
		<cfreturn variables.WARNING_RECORDS />
	</cffunction>

	<cffunction name="setBAD_RECORDS_DATA" access="public" returntype="void" output="false">
		<cfargument name="val" type="query" required="true">
		<cfset variables.BAD_RECORDS_DATA = arguments.val>
	</cffunction>
		
	<cffunction name="getBAD_RECORDS_DATA" access="public" returntype="query" output="false">
		<cfreturn variables.BAD_RECORDS_DATA/>
	</cffunction>

	<cffunction name="setWARNING_RECORDS_DATA" access="public" returntype="void" output="false">
		<cfargument name="val" type="query" required="true">
		<cfset variables.WARNING_RECORDS_DATA = arguments.val>
	</cffunction>
		
	<cffunction name="getWARNING_RECORDS_DATA" access="public" returntype="query" output="false">
		<cfreturn variables.WARNING_RECORDS_DATA/>
	</cffunction>
	<cffunction name="setGOOD_RECORDS_DATA" access="public" returntype="void" output="false">
		<cfargument name="val" type="query" required="true">
		<cfset variables.GOOD_RECORDS_DATA = arguments.val>
	</cffunction>
		
	<cffunction name="getGOOD_RECORDS_DATA" access="public" returntype="query" output="false">
		<cfreturn variables.GOOD_RECORDS_DATA/>
	</cffunction>

	<cffunction name="setHAVE_MORE_COLUMNS" access="public" returntype="void" output="false">
		<cfargument name="val" type="boolean" required="true">
		<cfset variables.HAVE_MORE_COLUMNS = arguments.val>
	</cffunction>
		
	<cffunction name="getHAVE_MORE_COLUMNS" access="public" returntype="boolean" output="false">
		<cfreturn variables.HAVE_MORE_COLUMNS/>
	</cffunction>
	
</cfcomponent>