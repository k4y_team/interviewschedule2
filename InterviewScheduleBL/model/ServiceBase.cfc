<cfcomponent displayname="ServiceBase" name="ServiceBase" output="false" extends="sis_core.model.ServiceBase">
	<cfproperty name="dbFactory" inject="dbFactory@InterviewSchedule" scope="variables" />

	<cffunction name="init" access="public" returntype="ServiceBase">
		<cfreturn this />
	</cffunction>

</cfcomponent>