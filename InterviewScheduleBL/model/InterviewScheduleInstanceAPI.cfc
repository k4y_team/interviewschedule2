<cfcomponent name="InterviewScheduleInstanceAPI" extends="ServiceBase" output="false">

	<cffunction name="init" returntype="InterviewScheduleInstanceAPI" output="false" access="public">
		<cfreturn this>
	</cffunction>

	<cffunction name="getInstanceData" returntype="query" output="false" access="public">
		<cfargument name="instanceCD" type="numeric" required="true">

		<cfset var tblGtw = variables.dbFactory.create("omsasInstanceGTW")>
		<cfset var returnQry = tblGtw.getByPK(arguments.instanceCD)>
		<cfreturn returnQry>
	</cffunction>

	<cffunction name="getAllInstances" returntype="query" output="false" access="public">
		<cfset var tblGtw = variables.dbFactory.create("omsasInstanceGTW")>
		<cfset var returnQry = tblGtw.getAll()>
		<cfreturn returnQry>
	</cffunction>
</cfcomponent>

</cfcomponent>