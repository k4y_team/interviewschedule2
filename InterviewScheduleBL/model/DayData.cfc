<cfcomponent name="DayData" extends="sis_core.model.ModelBase" output="false">
	<cfproperty name="DAY_ID" type="numeric" />
	<cfproperty name="DAY_NAME" type="string" />
	<cfproperty name="DAY_DT" type="date" />
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="string" />
		
	<cffunction name="init" access="public" returntype="DayData" output="false" displayname="init">
		<cfset variables.DAY_ID = "" />
		<cfset variables.DAY_NAME = "" />
		<cfset variables.DAY_DT = Now() />
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setDAY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
	
		<cfset variables.DAY_ID = val/>
	</cffunction>
	<cffunction name="getDAY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_ID />
	</cffunction>

	<cffunction name="setDAY_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.DAY_NAME = arguments.val />
	</cffunction>
	<cffunction name="getDAY_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_NAME />
	</cffunction>

	<cffunction name="setDAY_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.DAY_DT = val/>
	</cffunction>
	<cffunction name="getDAY_DT" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_DT />
	</cffunction>
	
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = val/>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>

</cfcomponent>