<cfcomponent name="InterviewerSetup" output="false" extends="ServiceBase">

	<cfproperty name="sessionStorage" inject="coldbox:plugin:SessionStorage" />

	<cffunction name="getByInterviewScheduleId" returntype="Query" output="false" access="public">
		<cfargument name="interviewScheduleID" type="numeric" required="true" >
		<cfset var qInterviewers = "">
		<cfset interviewerGTW = variables.dbFactory.create("IschInterviewerGTW")>
		<cfset qInterviewers = interviewerGTW.getbyInterviewScheduleID(arguments.interviewScheduleID)>

		<cfreturn qInterviewers>
	</cffunction>

	<cffunction name="getByFilter" returntype="query" output="false" access="public">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">

		<cfset var qInterviewers = "">
		<cfset interviewerGTW = variables.dbFactory.create("IschInterviewerGTW")>
		<cfset qInterviewers = interviewerGTW.getByFilter(arguments.filter)>

		<cfreturn qInterviewers>
	</cffunction>

	<cffunction name="get" returntype="InterviewerData" output="false" access="public">
		<cfargument name="interviewerID" type="any" required="true" >

		<cfset InterviewerData = variables.wirebox.getInstance("InterviewerData")>
		<cfif isNumeric(arguments.interviewerID)>
			<cfset var InterviewerDAO = variables.dbFactory.create("IschInterviewerDAO")>
			<cfset var InterviewerVO = InterviewerDAO.read(arguments.interviewerID)>
			<cfset InterviewerData.initFromStruct(InterviewerVO.toStruct())>
		</cfif>

		<cfreturn InterviewerData>
	</cffunction>

	<cffunction name="validate" returntype="boolean" output="false" access="private">
		<cfargument name="interviewerData" type="struct" required="true">

		<cfset var bValid = true>

		<cfif Trim(arguments.interviewerData.getREGISTRANT_ID()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Registrant ID is required.")>
		<cfelseif not isValid("Integer",arguments.interviewerData.getREGISTRANT_ID())>
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Registrant ID must be a numeric value.")>
		</cfif>

		<cfif Trim(arguments.interviewerData.getFIRST_NAME()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("First Name is required.")>
		</cfif>

		<cfif Trim(arguments.interviewerData.getLAST_NAME()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Last Name is required.")>
		</cfif>

		<cfif Trim(arguments.interviewerData.getEMAIL()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Email is required.")>
		<cfelseif not IsValid("email", Trim(arguments.interviewerData.getEMAIL()))>
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Email is not valid.")>
		</cfif>

		<cfif UCase(arguments.interviewerData.getTYPE()) eq "STUDENT" and Trim(arguments.interviewerData.getSTUDENT_ID()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Student ID is required.")>
		<cfelseif not isValid("Integer",arguments.interviewerData.getSTUDENT_ID()) and not Trim(arguments.interviewerData.getSTUDENT_ID()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Student ID must be a numeric value.")>
		</cfif>

		<!--- <cfif (UCase(arguments.interviewerData.getTYPE()) eq "FACULTY" or UCase(arguments.interviewerData.getTYPE()) eq "RESIDENT") and Trim(arguments.interviewerData.getCPSO()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("CPSO ## is required.")>
		</cfif> --->

		<!--validate availability dates -->

		<cfset var interviewerAvailabilityGTW = variables.dbFactory.create("IschInterviewerAvailabilityGTW")>
		<cfset var assignedDays = interviewerAvailabilityGTW.getAssignedDays(arguments.interviewerData.getINTERVIEWER_ID(),arguments.interviewerData.getINTERVIEW_SCHEDULE_ID())>
		<cfset var availabilityArray = ListToArray(arguments.interviewerData.getAVAILABLE_DATES())>
		<cfset var usedDaysList = "">
		<cfloop query="assignedDays">
			<cfset day_dt=DateFormat(assignedDays.day_dt,"dd-mmm-yyyy")>
			<cfif not ArrayContains(availabilityArray,day_dt)>
				<cfset bValid = false>
				<cfset usedDaysList = ListAppend(usedDaysList,assignedDays.day_name)>
			</cfif>
		</cfloop>
		<cfif not bValid>
			<cfif ListLen(usedDaysList) eq 1>
				<cfset variables.MsgBox.error("#usedDaysList# can't be removed because the interviewer is already scheduled on it.")>
			<cfelse>
				<cfset variables.MsgBox.error("#usedDaysList# can't be removed because the interviewer is already scheduled on them.")>
			</cfif>
		</cfif>

		<cfreturn bValid>
	</cffunction>

	<cffunction name="save" returntype="boolean" output="false" access="public">
		<cfargument name="interviewerData" type="struct" required="true">

		<cfset var success = validate(arguments.interviewerData)>

		<cfif success>
		<cftransaction action="begin">
			<cftry>
				<!-- save interviewer-->
				<cfset var interviewerVO = variables.dbFactory.create("IschInterviewerVO")>
				<cfset var interviewerDAO = variables.dbFactory.create("IschInterviewerDAO")>
				<cfset interviewerVO.initFromForm(arguments.interviewerData.toStruct())>
				<cfif interviewerVO.getINTERVIEWER_ID() EQ ''>
					<cfset interviewerVO.setCREATION_ID(variables.sessionStorage.getVar('LoggedInUser').user_id)>
					<cfset interviewerVO.setCREATION_DT(Now())>
					<cfset interviewerID = interviewerDAO.add(interviewerVO)>
					<cfset arguments.interviewerData.setINTERVIEWER_ID(interviewerID)>
`					<cfset variables.MsgBox.success("Interviewer successfully created.")>
				<cfelse>
					<cfset interviewerVO.setMODIFICATION_ID(variables.sessionStorage.getVar('LoggedInUser').user_id)>
					<cfset interviewerVO.setMODIFICATION_DT(Now())>
					<cfset interviewerDAO.update(interviewerVO)>
					<cfset variables.MsgBox.success("Interviewer successfully updated.")>
				</cfif>
				<!-- save availability dates-->
				<cfset var InterviewerAvailabilityVO = variables.dbFactory.create("IschInterviewerAvailabilityVO")>
				<cfset var InterviewerAvailabilityDAO = variables.dbFactory.create("IschInterviewerAvailabilityDAO")>
				<cfset var InterviewerAvailabilityGTW = variables.dbFactory.create("IschInterviewerAvailabilityGTW")>
				<cfset InterviewerAvailabilityGTW.deleteByInterviewerID(arguments.interviewerData.getINTERVIEWER_ID())>
				<cfloop list="#arguments.interviewerData.getAVAILABLE_DATES()#" index="date">
					<cfset InterviewerAvailabilityVO.setINTERVIEWER_ID(arguments.interviewerData.getINTERVIEWER_ID())>
					<cfset InterviewerAvailabilityVO.setDAY_DT(date)>
					<cfset InterviewerAvailabilityDAO.add(InterviewerAvailabilityVO)>
				</cfloop>
				<cftransaction action="commit"/>
				<cfcatch type="any">
					<cfset success = false>
					<cftransaction action="rollback"/>
						<cfdump var="#cfcatch#"><cfabort>

				</cfcatch>
			</cftry>
			</cftransaction>
		</cfif>
		<cfreturn success>
	</cffunction>

	<cffunction name="delete" returntype="boolean" output="false" access="public">
		<cfargument name="interviewerID" type="string" required="true">

		<cfset var status = true>
		<cftransaction action="begin">
			<cftry>
				<cfset var interviewerAvailabilityDAO = variables.dbFactory.create("IschInterviewerAvailabilityDAO")>
				<cfset var interviewerDAO = variables.dbFactory.create("IschInterviewerDAO")>
				<cfset interviewerAvailabilityDAO.deleteByInterviewerID(arguments.interviewerID)>
				<cfset interviewerDAO.delete(arguments.interviewerID)>
				<cfset variables.MsgBox.success("Interviewer successfully removed.")>
				<cftransaction action="commit"/>
				<cfcatch type="any">
					<cfset variables.MsgBox.error("Interviewer is already used and it can not be removed.")>
					<cftransaction action="rollback"/>
					<cfset status = false>
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn status>
	</cffunction>

	<!--- Interviewer Availability --->
	<cffunction name="getAvailabilityByInterviewerID" returntype="Query" output="false" access="public">
		<cfargument name="interviewerID" type="numeric" required="true">

		<cfset resultQry = "">
		<cfset interviewerAvailabilityGTW = variables.dbFactory.create("IschInterviewerAvailabilityGTW")>
		<cfset resultQry = interviewerAvailabilityGTW.getByInterviewerID(arguments.interviewerID)>

		<cfreturn resultQry>
	</cffunction>

	<cffunction name="createBean" returntype="InterviewerData" output="false" access="public">
		<cfset InterviewerData = variables.wirebox.getInstance("InterviewerData").init()>
		<cfreturn InterviewerData>
	</cffunction>

	<cffunction name="createFilter" returntype="uiInterviewScheduleFilter" output="false" access="public">
		<cfargument name="formContent" required="true" />
		<cfargument name="cookiesContent" required="true" />

		<cfset uiInterviewScheduleFilter = variables.wirebox.getInstance("uiInterviewScheduleFilter").init(arguments.formContent, arguments.cookiesContent)>
		<cfreturn uiInterviewScheduleFilter>
	</cffunction>

	<cffunction name="interviewerDatatable" access="public" output="false" returntype="sis_core.model.ui.datatables.Response">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true" hint="column filter options." />
		<cfargument name="interviewRequest" type="sis_core.model.ui.datatables.Request" required="true" hint="request received from the grid." />

		<cfset var interviewResponse = CreateObject("component", "sis_core.model.ui.datatables.Response").init() />
		<cfset var interviewScheduleAPI = application.wirebox.getInstance("InterviewScheduleAPI") />
		<cfset var service = interviewScheduleAPI.getInterviewerService() />
		<cfset var interviewers = service.getByFilter(arguments.filter) />
		<cfset var rows = ArrayNew(1) />
		<cfset var dateFormat = variables.wirebox.getInstance(name="", dsl="coldbox:setting:formatStruct").dateFormat />

		<cfset interviewResponse.setSEcho(arguments.interviewRequest.getSEcho())>
		<cfif interviewers.RecordCount gt 0>
			<cfloop query="interviewers">
				<cfset row = {
					  'INTERVIEWER_ID'		= interviewers.interviewer_id
					, 'REGISTRANT_ID'		= interviewers.registrant_id
					, 'LAST_NAME' 		    = interviewers.last_name
					, 'FIRST_NAME' 		    = interviewers.first_name
					, 'EMAIL' 		    	= interviewers.email
					, 'TYPE' 				= interviewers.type
					, 'CPSO' 				= interviewers.cpso
					, 'STUDENT_ID' 			= interviewers.student_id
					, 'STATUS' 				= interviewers.status
					} />
				<cfset ArrayAppend(rows, row) />
			</cfloop>
			<cfset interviewResponse.setITotalRecords( interviewers.TOTAL_rows ) />
			<cfset interviewResponse.setITotalDisplayRecords( IIF(arguments.interviewRequest.getIDisplayLength() gt 0, 'interviewers.TOTAL_rows', '-1') ) />
			<cfset interviewResponse.setAaData( rows ) />
		</cfif>
		<cfreturn interviewResponse />
	</cffunction>


</cfcomponent>