<cfcomponent name="ImportInterviewers" output="false" extends="Import">
	<cfproperty name="columnData" type="array" />
	<cfproperty name="importQry" type="query" />
	<cfproperty name="importDesc" type="struct">
	<cfproperty name="interviewScheduleID" type="numeric">
	<cfproperty name="dbFactory" inject="dbFactory@InterviewSchedule" scope="variables" />
			
	<cffunction name="init" returntype="ImportInterviewers" output="false" access="public">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		
		<cfset variables.columnData = DeserializeJSON('
				[{"ColumnName":"Registrant ID","Required":"Y","Validation":"numeric"},
				 {"ColumnName":"First Name","Required":"Y","Validation":""},
				 {"ColumnName":"Last Name","Required":"Y","Validation":""},
				 {"ColumnName":"Email","Required":"Y","Validation":"email"},
				 {"ColumnName":"Type","Required":"Y","Validation":"type"},
				 {"ColumnName":"Department","Required":"N","Validation":""},
				 {"ColumnName":"CPSO","Required":"N","Validation":"cpso_num"},
				 {"ColumnName":"Student ID","Required":"N","Validation":"student_id"},
				 {"ColumnName":"Availability","Required":"N","Validation":"availability"}]')>
		
		<cfset variables.importQry = "">
		<cfset variables.interviewScheduleID = arguments.interviewScheduleID>
		<cfset variables.importDesc = { code = "INTERVIEWERS", title = "Import Interviewers"}>
		<cfreturn this>
	</cffunction>
	
	<cffunction name="setImportTitle" displayname="setImportDesc" access="public" output="false" returntype="void">
		<cfargument name="importTitle" type="string" required="true">
		
		<cfset variables.importDesc.title = arguments.importTitle>
	</cffunction>
	
	<cffunction name="processData" displayname="processData" access="public" output="false" returntype="boolean">
		<cfargument name="importVO" type="ImportData" required="true">
		
		<cfset var ischInterviewerDAO = variables.dbFactory.create("ischInterviewerDAO")>
		<cfset var ischInterviewerAvailabilityDAO = variables.dbFactory.create("ischInterviewerAvailabilityDAO")>		
		<cfset var ischInterviewerVO = "">		
		<cfset var ischInterviewerAvailabilityVO = "">
		
		<cfset var result = true>
		<cfset var totalImportedRecords = 0>
		<cftransaction  action="begin">
		<cftry>
			<!--- start from line 2, since first one has the column names --->
			<cfloop query="variables.importQry" startRow="2">
				<cfif variables.importQry.STATUS eq "OK">
					<cfset totalImportedRecords = totalImportedRecords + 1>
					<cfset ischInterviewerVO = ischInterviewerDAO.readByEmail(variables.interviewScheduleID,variables.importQry["Email"][variables.importQry.currentrow])>

					<!--- Libre office, remove decimal --->
					<cfset fStudentID = variables.importQry["Student ID"][variables.importQry.currentrow]>
					<cfset fRegistraintID = variables.importQry["Registrant ID"][variables.importQry.currentrow]>
					<cfset fCpso = variables.importQry["CPSO"][variables.importQry.currentrow]>
					<cfif isNumeric(fStudentID)>
						<cfset fStudentID = NumberFormat(fStudentID, '99999999')>
					</cfif>
					<cfif isNumeric(fRegistraintID)>
						<cfset fRegistraintID = NumberFormat(fRegistraintID, '99999999')>
					</cfif>
						<!--- <cfset fCpso = cpso> --->
					<cfif isNumeric(fCpso)>
						<cfset fCpso = NumberFormat(fCpso, '99999999')>
					</cfif>
					<cfset ischInterviewerVO.setFIRST_NAME(variables.importQry["First Name"][variables.importQry.currentrow])>
					<cfset ischInterviewerVO.setLAST_NAME(variables.importQry["Last Name"][variables.importQry.currentrow])>
					<cfset ischInterviewerVO.setEMAIL(variables.importQry["Email"][variables.importQry.currentrow])>
					<cfset ischInterviewerVO.setTYPE(variables.importQry["Type"][variables.importQry.currentrow])>
					<cfset ischInterviewerVO.setCPSO(fCpso)>
					<cfset ischInterviewerVO.setSTUDENT_ID(fStudentID)>
					<cfset ischInterviewerVO.setREGISTRANT_ID(fRegistraintID)>
					<cfset ischInterviewerVO.setDEPARTMENT(variables.importQry["Department"][variables.importQry.currentrow])>
					<cfset ischInterviewerVO.setINTERVIEW_SCHEDULE_ID(variables.interviewScheduleID)>

					<cfif ischInterviewerVO.getINTERVIEWER_ID() eq "">
						<cfset ischInterviewerVO.setCREATION_ID(session.user_id)>
						<cfset ischInterviewerVO.setCREATION_DT(Now())>					
						<cfset interviewerID = ischInterviewerDAO.add(ischInterviewerVO)>
					<cfelse>
						<cfset ischInterviewerVO.setCREATION_ID(session.user_id)>
						<cfset ischInterviewerVO.setCREATION_DT(Now())>					
						<cfset ischInterviewerDAO.update(ischInterviewerVO)>
						<cfset interviewerID = ischInterviewerVO.getINTERVIEWER_ID()>
					</cfif>
					
					<!--- update available dates --->
					<cfset ischInterviewerAvailabilityDAO.deleteByInterviewerID(interviewerID)>
					<cfset availableDates = variables.importQry["Availability"][variables.importQry.currentrow]>
					<cfloop list="#availableDates#" index="fDate">
						<cfset date = ParseDateTime(fDate)>
						<cfset ischInterviewerAvailabilityVO = variables.dbFactory.create("ischInterviewerAvailabilityVO")>
						<cfset ischInterviewerAvailabilityVO.setINTERVIEWER_ID(interviewerID)>
						<cfset ischInterviewerAvailabilityVO.setDAY_DT(date)>
						<cfset ischInterviewerAvailabilityDAO.add(ischInterviewerAvailabilityVO)>
					</cfloop>
				</cfif>	
			</cfloop>
			<cfif totalImportedRecords gt 1>
				<cfset variables.MsgBox.success("#totalImportedRecords# interviewers successfully imported.")>
			<cfelse>
				<cfset variables.MsgBox.success("#totalImportedRecords# interviewer successfully imported.")>
			</cfif>	
			<cftransaction action="commit"/>
		<cfcatch type="any">
			<cfset result = false>
			<cftransaction action="rollback"/>
			<cfrethrow />
		</cfcatch>
		</cftry>
		</cftransaction>
		
		<cfreturn result>
	</cffunction>

</cfcomponent>