<cfcomponent name="ScheduleCandidateData" extends="sis_core.model.ModelBase" output="false">
	<cfproperty name="APPLICANT_ID" type="string" />
	<cfproperty name="APPLICANT_NAME" type="string" />
	<cfproperty name="CIRCUIT_ID" type="string" />
	<cfproperty name="STATION_ID" type="string" />
	<cfproperty name="SHIFT_ID" type="string" />
		
	<cffunction name="init" access="public" returntype="ScheduleCandidateData" output="false">
		<cfset variables.APPLICANT_ID = "" />
		<cfset variables.APPLICANT_NAME = "" />
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.STATION_ID = "" />
		<cfset variables.SHIFT_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setAPPLICANT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.APPLICANT_ID = val/>
	</cffunction>
	<cffunction name="getAPPLICANT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.APPLICANT_ID />
	</cffunction>

	<cffunction name="setAPPLICANT_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.APPLICANT_NAME = arguments.val />
	</cffunction>
	<cffunction name="getAPPLICANT_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.APPLICANT_NAME />
	</cffunction>

	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.CIRCUIT_ID = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>

	<cffunction name="setSTATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		
		<cfset variables.STATION_ID = val/>
	</cffunction>
	<cffunction name="getSTATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.STATION_ID />
	</cffunction>

	<cffunction name="setSHIFT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.SHIFT_ID = val/>
	</cffunction>
	<cffunction name="getSHIFT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.SHIFT_ID />
	</cffunction>

</cfcomponent>