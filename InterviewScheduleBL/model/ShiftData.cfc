<cfcomponent name="ShiftData" output="false" extends="sis_core.model.ModelBase">
	<cfproperty name="SHIFT_ID" type="string"/>
	<cfproperty name="START_HOUR" type="string"/>
	<cfproperty name="START_MINUTE" type="string"/>
	<cfproperty name="END_HOUR" type="string"/>
	<cfproperty name="END_MINUTE" type="string"/>
	<cfproperty name="CIRCUIT_ID" type="string"/>
	<cfproperty name="CIRCUIT_NAME" type="string"/>
	<cfproperty name="DAY_NAME" type="string"/>
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="string"/>
	
	<cffunction name="init" access="public" returntype="ShiftData" output="false">
		<cfset variables.SHIFT_ID = "" />
		<cfset variables.START_HOUR = "" />
		<cfset variables.START_MINUTE = "" />
		<cfset variables.END_HOUR = "" />
		<cfset variables.END_MINUTE = "" />
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.CIRCUIT_NAME = "" />
		<cfset variables.DAY_NAME = "" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		
		<cfreturn this />
 	</cffunction>

	<cffunction name="setSHIFT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.SHIFT_ID = arguments.val />
	</cffunction>
	<cffunction name="getSHIFT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.SHIFT_ID />
	</cffunction>

	<cffunction name="setSTART_HOUR" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.START_HOUR = arguments.val />
	</cffunction>
	<cffunction name="getSTART_HOUR" access="public" returntype="any" output="false">
		<cfreturn variables.START_HOUR />
	</cffunction>

	<cffunction name="setSTART_MINUTE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.START_MINUTE = arguments.val />
	</cffunction>
	<cffunction name="getSTART_MINUTE" access="public" returntype="string" output="false">
		<cfreturn variables.START_MINUTE />
	</cffunction>

	<cffunction name="setEND_HOUR" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.END_HOUR = uCase(arguments.val) />
	</cffunction>
	<cffunction name="getEND_HOUR" access="public" returntype="any" output="false">
		<cfreturn variables.END_HOUR />
	</cffunction>

	<cffunction name="setEND_MINUTE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.END_MINUTE = arguments.val />
	</cffunction>
	<cffunction name="getEND_MINUTE" access="public" returntype="string" output="false">
		<cfreturn variables.END_MINUTE />
	</cffunction>

	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CIRCUIT_ID = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>

	<cffunction name="setCIRCUIT_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CIRCUIT_NAME = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_NAME />
	</cffunction>

	<cffunction name="setDAY_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DAY_NAME = arguments.val />
	</cffunction>
	<cffunction name="getDAY_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_NAME />
	</cffunction>

	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = arguments.val />
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>
	
</cfcomponent>