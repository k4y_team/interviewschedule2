<cfcomponent name="InterviewScheduleCandidates" output="false" extends="ServiceBase">

	<cffunction access="public" name="schedule" output="false" returntype="boolean">
		<cfargument name="data" type="ScheduleCandidateData" required="true">

		<cfset success = true>
		<cftry>
			<cfset var ApplicantDAO = variables.dbFactory.create("IschApplicantDAO")>
			<cfset var ApplicantVO = variables.dbFactory.create("IschApplicantVO")>
			<cfset ApplicantDAO.deleteByStationAndShift(arguments.data.getSTATION_ID(),arguments.data.getSHIFT_ID())>
			<cfset ApplicantVO.initFromForm(arguments.data.toStruct())>
			<cfset ApplicantVO.setCREATION_ID(session.user_id)>
			<cfset ApplicantVO.setCREATION_DT(Now())>
			<cfset ApplicantDAO.add(ApplicantVO)>
			<cfcatch>
				<cfset success = false>
				<cfset variables.MsgBox.error("An error occurred while scheduling <strong>#arguments.data.getAPPLICANT_NAME()#</strong>.")>
			</cfcatch>
		</cftry>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="public" name="unSchedule" output="false" returntype="boolean">
		<cfargument name="applicantID" type="numeric" required="true">

		<cfset success = true>
		<cftransaction action="begin">
			<cftry>
				<cfset var qScoreGTW = variables.dbFactory.create("IschQuestionScoreGTW")>
				<cfset qScoreGTW.deleteApplicantQuestions(arguments.applicantID)>
				
				<cfset var ApplicantDAO = variables.dbFactory.create("IschApplicantDAO")>
				<cfset ApplicantDAO.delete(arguments.applicantID)>
				<cftransaction action="commit"/>
				<cfcatch>
					<cfdump var="#cfcatch#"><cfabort>

					<cfset success = false>
					<cfset variables.MsgBox.error("An error occurred while scheduling the student.")>
					<cftransaction action="rollback"/>
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn success>
	</cffunction>

	<cffunction access="public" name="getApplicants" output="false" returntype="query">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">		
		<cfset getQry = "">
		
		<cfset var ApplicantsGTW = variables.dbFactory.create("IschApplicantsGTW")>
		<cfset getQry = ApplicantsGTW.getApplicants(arguments.filter)>
		
		<cfreturn getQry>
	</cffunction>
	
	<cffunction access="public" name="getScheduleInfo" output="false" returntype="struct">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfset temp = StructNew()>
		<cfset var ApplicantGTW = variables.dbFactory.create("IschApplicantGTW")>
		<cfset var ApplicantsGTW = variables.dbFactory.create("IschApplicantsGTW")>
		<cfset var CircuitGTW = variables.dbFactory.create("IschCircuitGTW")>
		<cfset temp["SCHEDULED_APPLICANTS"] = ApplicantGTW.getScheduledByFilter(arguments.filter).recordCount>
		<cfset temp["REMAINING_APPLICANTS"] = ApplicantsGTW.getRemaining(arguments.filter.getFILTER_ITEM('interviewScheduleID')).recordCount>
		<cfset temp["AVAILABLE_SPOTS"] = CircuitGTW.getAvailableSpots(arguments.filter).recordCount>

		<cfreturn temp>
	</cffunction>

	<cffunction access="public" name="getApplicantScores" output="false" returntype="query">
		<cfargument name="referenceID" type="numeric" required="true">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		
		<cfset var ApplicantsGTW = variables.dbFactory.create("IschApplicantsGTW")>
		<cfreturn ApplicantsGTW.getScores(arguments.referenceID, arguments.interviewScheduleID)>
	</cffunction>

	<cffunction access="public" name="scheduleAll" output="false" returntype="boolean">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">

		<cfset success = true>
		<cftry>
			<cfset var ApplicantDAO = variables.dbFactory.create("IschApplicantDAO")>
			<cfset ApplicantDAO.scheduleAll(filter)>
			<cfcatch>
				<cfset success = false>
				<cfset variables.MsgBox.error("An error occurred during automatic scheduling.")>
			</cfcatch>
		</cftry>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="public" name="createBean" returntype="ScheduleCandidateData" output="false">
		<cfset ScheduleCandidateData = variables.wirebox.getInstance("ScheduleCandidateData").init()>
		<cfreturn ScheduleCandidateData>
	</cffunction>
	
	<cffunction access="public" name="createFilter" returntype="uiInterviewScheduleFilter" output="false">
		<cfargument name="formContent" required="true" />
		<cfargument name="cookiesContent" required="true" />

		<cfset uiInterviewScheduleFilter = variables.wirebox.getInstance("uiInterviewScheduleFilter").init(arguments.formContent, arguments.cookiesContent)>
		<cfreturn uiInterviewScheduleFilter>
	</cffunction>

	<cffunction access="public" name="getCompetencyScores" output="false" returntype="query">
		<cfargument name="applicantID" type="numeric" required="true">
		<cfargument name="competencyID" type="numeric" required="true">
		
		<cfset questionGTW = variables.dbFactory.create("IschCompetencyQuestionGTW")>
		<cfset qScores = questionGTW.getCompetencyScores(arguments.applicantID, arguments.competencyID)>
		
		<cfreturn qScores>
	</cffunction>

	<cffunction access="public" name="calculateScore" output="false" returntype="void">
		<cfargument name="applicantID" type="numeric" required="true">

		<cfset applicantDAO = variables.dbFactory.create("IschApplicantDAO")>
		<cfset applicantDAO.calculateScore(arguments.applicantID)>
	</cffunction>

	<cffunction access="public" name="recalculateScores" output="false" returntype="void">
		<cfset var ischApplicantGtw = variables.dbFactory.create("IschApplicantGTW")>
		<cfset var ischApplicantsQry = ischApplicantGtw.getApplicants4ScoreCalculation()>

		<cfloop query="ischApplicantsQry">
			<cfset calculateScore(ischApplicantsQry.applicant_id)>
		</cfloop>
	</cffunction>
</cfcomponent>