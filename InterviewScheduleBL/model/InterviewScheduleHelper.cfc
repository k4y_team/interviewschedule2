<cfcomponent name="InterviewScheduleHelper" output="false" extends="ServiceBase">

	<cffunction access="public" name="init" output="false" returntype="InterviewScheduleHelper">
		<cfreturn this>
	</cffunction>

	<cffunction access="public" name="getSessions" output="false" returntype="query">
		<cfset var SessionGTW = variables.dbFactory.create("IschSessionGTW")>
		<cfset var SessionQry = SessionGTW.getAll()>
		
		<cfreturn SessionQry>
	</cffunction>

	<cffunction access="public" name="getSession" output="false" returntype="query">
		<cfargument name="sessionID" type="numeric" required="true">
		
		<cfset var SessionGTW = variables.dbFactory.create("IschSessionGTW")>
		<cfset var SessionQry = SessionGTW.getByPK(arguments.sessionID)>
		
		<cfreturn SessionQry>
	</cffunction>

	<cffunction access="public" name="getCurrentSession" output="false" returntype="query">
		<cfset var SessionGTW = variables.dbFactory.create("IschSessionGTW")>
		<cfset var SessionQry = SessionGTW.getCurrentSession()>
		<cfreturn SessionQry>
	</cffunction>

	<cffunction access="public" name="getAvailableCompetencies" output="false" returntype="query">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		<cfargument name="circuitID" type="numeric" required="true">
		<cfargument name="competencyID" type="any" required="false" default="">

		<cfset var CompetencyGTW = variables.dbFactory.create("IschCompetencyGTW")>
		<cfset var CompetencyQry = CompetencyGTW.getAvailableByCircuit(arguments.interviewScheduleID, arguments.circuitID, arguments.competencyID)>
		
		<cfreturn CompetencyQry>
	</cffunction>

	<cffunction access="public" name="getAvailableInterviewers" output="false" returntype="query">
		<cfargument name="dayId" type="numeric" required="true">
		<cfargument name="stationID" type="numeric" required="true">
		
		<cfset var InterviewerGTW = variables.dbFactory.create("IschInterviewerGTW")>
		<cfset var InterviewerQry = InterviewerGTW.getAvailable(arguments.dayID, arguments.stationId)>
		
		<cfreturn InterviewerQry>
	</cffunction>

	<cffunction access="public" name="getData4Search" output="false" returntype="query">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfset var InterviewScheduleGTW = variables.dbFactory.create("IschInterviewScheduleGTW")>
		<cfset var data4SearchQry = InterviewScheduleGTW.getData4Search(arguments.filter)>
		
		<cfreturn data4SearchQry>
	</cffunction>

	<cffunction access="public" name="getData4Export" output="false" returntype="query">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfset var ApplicantsGTW = variables.dbFactory.create("ApplicantsGTW")>
		<cfset var data4ExportQry = ApplicantsGTW.getData4Export(arguments.filter)>
		
		<cfreturn data4ExportQry>
	</cffunction>

	<cffunction access="public" name="getAvailableApplicants" output="false" returntype="query">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfset var ApplicantsGTW = variables.dbFactory.create("IschApplicantsGTW")>
		<cfset var ApplicantsQry = ApplicantsGTW.getByFilter(arguments.filter)>
		
		<cfreturn ApplicantsQry>
	</cffunction>

	<cffunction access="public" name="setupFilter" output="false" returntype="sis_core.model.SearchFilter">
		<cfargument name="data" type="struct" required="true">
		<cfargument name="fieldNames" type="string" required="true">
		
		<cfset var SearchFilter = variables.wirebox.getInstance("SearchFilter")>
		<cfset var filter = ArrayNew(1)>
		<cfloop list="#arguments.fieldNames#" index="fieldName">
			<cfset SearchFilterItem = variables.wirebox.getInstance("SearchFilterItem")>
			<cfset SearchFilterItem.setKey(fieldName)>
			<cfset SearchFilterItem.setValue(Evaluate('arguments.data.#fieldName#'))>
			<cfset Arrayappend(filter, SearchFilterItem)>
		</cfloop>
		<cfset SearchFilter.setFILTER_DATA(filter)>

		<cfreturn SearchFilter>
	</cffunction>

	<cffunction access="public" name="getHours" output="false" returntype="query">
		<cfset hourQry = queryNew("ID, VALUE")>
		
		<cfset QueryAddRow(hourQry, 1)>
		<cfset QuerySetCell(hourQry, "ID", 0)>
		<cfset QuerySetCell(hourQry, "VALUE", "00")>
		<cfloop from="1" to="23" step="1" index="i">
			<cfset QueryAddRow(hourQry, 1)>
			<cfset QuerySetCell(hourQry, "ID", i)>
			<cfset hours = ToString(i)>
			<cfset QuerySetCell(hourQry, "VALUE", hours)>
		</cfloop>
		
		<cfreturn hourQry>
	</cffunction>

	<cffunction access="public" name="getMinutes" output="false" returntype="query">
		<cfset minuteQry = queryNew("ID, VALUE")>
		
		<cfset QueryAddRow(minuteQry, 1)>
		<cfset QuerySetCell(minuteQry, "ID", 0)>
		<cfset QuerySetCell(minuteQry, "VALUE", "00")>
		<cfloop from="1" to="59" step="1" index="i">
			<cfset QueryAddRow(minuteQry, 1)>
			<cfset QuerySetCell(minuteQry, "ID", i)>
			<cfset minutes = ToString(i)>
			<cfset QuerySetCell(minuteQry, "VALUE", minutes)>
		</cfloop>
		
		<cfreturn minuteQry>
	</cffunction>

	<cffunction access="public" name="getScheduledApplicantsByCircuitID" output="false" returntype="query">
		<cfargument name="circuitId" type="numeric" required="true">
		
		<cfset var ApplicantsGTW = variables.dbFactory.create("ApplicantsGTW")>
		<cfset var qry = ApplicantsGTW.getByCircuitId(arguments.circuitId)>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction access="public" name="getScheduledApplicantsByInterviewerID" output="false" returntype="query">
		<cfargument name="interviewerId" type="numeric" required="true">
		
		<cfset var ApplicantsGTW = variables.dbFactory.create("ApplicantsGTW")>
		<cfset var qry = ApplicantsGTW.getByInterviewerId(arguments.interviewerId)>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction access="public" name="getApplicantSchedule" output="false" returntype="query">
		<cfargument name="applicantId" type="numeric" required="true">
		
		<cfset var ApplicantsGTW = variables.dbFactory.create("ApplicantsGTW")>
		<cfset var qry = ApplicantsGTW.getByApplicantId(arguments.applicantId)>
		
		<cfreturn qry>
	</cffunction>
	
	<cffunction access="public" name="getQuestionTypes" output="false" returntype="query">
		<cfset var questionTypeGTW = variables.dbFactory.create("IschQuestionTypeGTW")>
		<cfset var qry = questionTypeGTW.getAll()>
		
		<cfreturn qry>
	</cffunction>
</cfcomponent>