<cfcomponent name="ImportInterviewerValidator" extends="importValidator">
	<cfproperty name="interviewScheduleID" type="numeric">
	
	<cffunction name="init" access="public" output="false" returntype="ImportInterviewerValidator">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		<cfset variables.interviewScheduleID = arguments.interviewScheduleID>
		<cfset super.init()>
		<cfreturn this>
	</cffunction>
	
	<cffunction name="validateEmail" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		
		<cfset super.validateEmail(arguments.val)>
		<!--- validate for existing interviewers --->		
	</cffunction>
	
	<cffunction name="validateType" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		
		<cfif arguments.val neq "STUDENT" and arguments.val neq "FACULTY" and arguments.val neq "RESIDENT">
			<cfthrow message="'#arguments.val#' is not a valid interviewer type (Accepted values are STUDENT,RESIDENT,FACULTY)." type="validation_error">
		</cfif>
	</cffunction>
	
	<cffunction name="validateCPSO_NUM" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		<cfargument name="record" required="true">
		
		<!--- <cfset typeVal = arguments.record["TYPE"][arguments.record.currentrow]>
		<cfif (Trim(typeVal) eq "FACULTY" or Trim(typeVal) eq "RESIDENT") and Trim(val) eq "">
			<cfthrow message="Field is required." type="validation_error">
		</cfif> --->
	</cffunction>
	
	<cffunction name="validateSTUDENT_ID" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		<cfargument name="record" required="true">

		<cfset typeVal = arguments.record["TYPE"][arguments.record.currentrow]>
		<cfif Trim(typeVal) eq "STUDENT" and Trim(val) eq "">
			<cfthrow message="Field is required." type="validation_error">
		<cfelse>
			<cfset super.validateNumeric(arguments.val)>
		</cfif>
	</cffunction>
	
	<cffunction name="validateAVAILABILITY" access="public" output="false" returntype="void">
 		<cfargument name="val" required="true">
		<cfargument name="record" required="true">
		
		<cfset var invalidDates = "">
		<cfif arguments.val neq "">
			<cfloop list="#arguments.val#" index="fDate">
				<cftry>
					<cfset date = ParseDateTime(fDate)>
				<cfcatch>
					<cfset invalidDates = ListAppend(invalidDates,fDate)>
				</cfcatch>	
				</cftry>
			</cfloop>
			<cfif invalidDates neq "">
				<cfthrow message="Invalid dates: #invalidDates#." type="validation_error">
			</cfif>
		</cfif>	
	</cffunction>	
</cfcomponent>