<cfcomponent name="QuestionScoreData" displayname="QuestionScoreData" extends="sis_core.model.ModelBase" output="false">
		<cfproperty name="QUESTION_ID" type="string" />
		<cfproperty name="APPLICANT_ID" type="string" />
		<cfproperty name="SCORE_VALUE" type="string" />
		<cfproperty name="ANSWER_TEXT" type="string" />
		
	<cffunction name="init" access="public" returntype="QuestionScoreData" output="false" displayname="init" hint="I initialize a IschQuestionScore">
		<cfset variables.QUESTION_ID = "" />
		<cfset variables.APPLICANT_ID = "" />
		<cfset variables.SCORE_VALUE = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.ANSWER_TEXT = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setQUESTION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.QUESTION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getQUESTION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_ID />
	</cffunction>
	<cffunction name="setAPPLICANT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.APPLICANT_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getAPPLICANT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.APPLICANT_ID />
	</cffunction>
	<cffunction name="setSCORE_VALUE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.SCORE_VALUE = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getSCORE_VALUE" access="public" returntype="any" output="false">
		<cfreturn variables.SCORE_VALUE />
	</cffunction>
	<cffunction name="setANSWER_TEXT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ANSWER_TEXT = arguments.val />
	</cffunction>
	<cffunction name="getANSWER_TEXT" access="public" returntype="any" output="false">
		<cfreturn variables.ANSWER_TEXT />
	</cffunction>
</cfcomponent>
