<cfcomponent name="StationData" output="false" extends="sis_core.model.ModelBase">
	<cfproperty name="STATION_ID" type="string"/>
	<cfproperty name="CIRCUIT_ID" type="string"/>
	<cfproperty name="CIRCUIT_NAME" type="string"/>
	<cfproperty name="DAY_ID" type="string"/>
	<cfproperty name="DAY_NAME" type="string"/>
	<cfproperty name="COMPETENCY_ID" type="string"/>
	<cfproperty name="ROOM" type="string"/>
	<cfproperty name="INTERVIEWER_ID" type="string"/>
	<cfproperty name="ORDER_NUMBER" type="string"/>
	<cfproperty name="QUESTION_ID" type="string" />
	
	<cffunction name="init" access="public" returntype="StationData" output="false">
		<cfset variables.STATION_ID = "" />
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.CIRCUIT_NAME = "" />
		<cfset variables.SESSION_NAME = "" />
		<cfset variables.DAY_ID = "" />
		<cfset variables.DAY_NAME = "" />
		<cfset variables.COMPETENCY_ID = "" />
		<cfset variables.ROOM = "" />
		<cfset variables.INTERVIEWER_ID = "" />
		<cfset variables.ORDER_NUMBER = "" />
		<cfset variables.QUESTION_ID = "" />

		<cfreturn this />
 	</cffunction>

	<cffunction name="setSTATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.STATION_ID = arguments.val />
	</cffunction>
	<cffunction name="getSTATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.STATION_ID />
	</cffunction>

	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CIRCUIT_ID = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>

	<cffunction name="setCIRCUIT_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CIRCUIT_NAME = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_NAME" access="public" returntype="string" output="false">
		<cfreturn variables.CIRCUIT_NAME />
	</cffunction>

	<cffunction name="setSESSION_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.SESSION_NAME = uCase(arguments.val) />
	</cffunction>
	<cffunction name="getSESSION_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.SESSION_NAME />
	</cffunction>

	<cffunction name="setDAY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DAY_ID = arguments.val />
	</cffunction>
	<cffunction name="getDAY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_ID />
	</cffunction>

	<cffunction name="setDAY_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DAY_NAME = arguments.val />
	</cffunction>
	<cffunction name="getDAY_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_NAME />
	</cffunction>

	<cffunction name="setCOMPETENCY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.COMPETENCY_ID = arguments.val />
	</cffunction>
	<cffunction name="getCOMPETENCY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_ID />
	</cffunction>

	<cffunction name="setROOM" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ROOM = arguments.val />
	</cffunction>
	<cffunction name="getROOM" access="public" returntype="any" output="false">
		<cfreturn variables.ROOM />
	</cffunction>

	<cffunction name="setINTERVIEWER_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEWER_ID = arguments.val />
	</cffunction>
	<cffunction name="getINTERVIEWER_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEWER_ID />
	</cffunction>

	<cffunction name="setORDER_NUMBER" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ORDER_NUMBER = arguments.val />
	</cffunction>
	<cffunction name="getORDER_NUMBER" access="public" returntype="any" output="false">
		<cfreturn variables.ORDER_NUMBER />
	</cffunction>

	<cffunction name="setQUESTION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.QUESTION_ID = arguments.val />
	</cffunction>
	<cffunction name="getQUESTION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_ID />
	</cffunction>

</cfcomponent>