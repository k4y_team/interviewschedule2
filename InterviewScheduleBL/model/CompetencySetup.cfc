<cfcomponent name="CompetencySetup" output="false" extends="ServiceBase">
	
	<cffunction name="getByInterviewScheduleID" returntype="Query" output="false" access="public">
		<cfargument name="interviewScheduleID" type="any" required="true">
		
		<cfset var qCompetency = "">
		<cfset competencyGTW = variables.dbFactory.create("IschCompetencyGTW")>
		<cfset qCompetency = competencyGTW.getByInterviewScheduleID(arguments.interviewScheduleID)>
		
		<cfreturn qCompetency>
	</cffunction>
	
	<cffunction name="get" returntype="CompetencyData" output="false" access="public">
		<cfargument name="competencyID" type="any" required="true">

		<cfset CompetencyData = variables.wirebox.getInstance("CompetencyData")>
		<cfif isNumeric(arguments.competencyID)>
			<cfset var CompetencyDAO = variables.dbFactory.create("IschCompetencyDAO")>
			<cfset var CompetencyVO = CompetencyDAO.read(arguments.competencyID)>
			<cfset CompetencyData.initFromStruct(CompetencyVO.toStruct())>
		</cfif>
		
		<cfreturn CompetencyData>
	</cffunction>
	
	<cffunction name="validate" returntype="boolean" output="false" access="private">
		<cfargument name="competencyData" type="struct" required="true">

		<cfset var competencyGtw = "">
		<cfset var existingCompetencies = "">
		<cfset var bValid = true>
		
		<cfif Trim(arguments.competencyData.getCOMPETENCY_NAME()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Name is required.")>
		<cfelse>
			<cfset competencyGtw = variables.dbFactory.create("IschCompetencyGtw")>
			<cfset existingCompetencies = competencyGtw.getByName(arguments.competencyData.getINTERVIEW_SCHEDULE_ID(),arguments.competencyData.getCOMPETENCY_NAME())>
			<cfif existingCompetencies.recordcount neq 0 and UCase(existingCompetencies.competency_id) neq UCase(arguments.competencyData.getCOMPETENCY_ID())>
				<cfset bValid = false>
				<cfset variables.MsgBox.error("Name <i>#Trim(arguments.competencyData.getCOMPETENCY_NAME())#</i> is already used.")>
			</cfif>
		</cfif>
		
		
		<cfif Trim(arguments.competencyData.getDURATION_MINS()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Duration is required.")>
		<cfelseif not isValid("Integer",arguments.competencyData.getDURATION_MINS())>
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Duration must be a numeric value expressed in minutes.")>
		</cfif>
		
		<cfreturn bValid>
	</cffunction>
	
	<cffunction name="save" returntype="boolean" output="false" access="public">
		<cfargument name="competencyData" type="CompetencyData" required="true">
		
		<cfset var status = true>
		<cfset competencyVO = variables.dbFactory.create("IschCompetencyVO")>
		<cfset competencyDAO = variables.dbFactory.create("IschCompetencyDAO")>
		
		<cftry>
			<cfif validate(arguments.competencyData)>
				<cfset competencyVO.initFromForm(arguments.competencyData.toStruct())>
				<cfif competencyVO.getCOMPETENCY_ID() EQ ''>
					<cfset competencyVO.setCREATION_ID(session.user_id)>
					<cfset competencyVO.setCREATION_DT(Now())>

					<cfset competencyID = competencyDAO.add(competencyVO)>
					<cfset arguments.competencyData.setCOMPETENCY_ID(competencyID)>
					<cfset variables.MsgBox.success("Competency successfully created.")>
				<cfelse>
					<cfset competencyVO.setMODIFICATION_ID(session.user_id)>>
					<cfset competencyVO.setMODIFICATION_DT(Now())>
					<cfset competencyDAO.update(competencyVO)>
					<cfset variables.MsgBox.success("Competency successfully updated.")>
				</cfif>
			<cfelse>
				<cfset status = false>
			</cfif>
		<cfcatch type="any">
			<cfset status = false>

		</cfcatch>
		</cftry>
		<cfreturn status>
	</cffunction>
	
	<cffunction name="delete" returntype="boolean" output="false" access="public">
		<cfargument name="competencyID" type="string" required="true">

		<cfset var status = true>
		<cfset competencyDAO = variables.dbFactory.create("IschCompetencyDAO")>
		
		<cftry>
			<cfset competencyDAO.delete(arguments.competencyID)>
			<cfset variables.MsgBox.success("Competency successfully removed.")>
			<cfcatch type="any">
				<cfset status = false>
				<cfset variables.MsgBox.error("Competency is already used and it can not be removed.")>
			</cfcatch>
		</cftry>
		<cfreturn status>
	</cffunction>

	<cffunction name="createBean" returntype="CompetencyData" output="false" access="public">
		<cfset CompetencyData = variables.wirebox.getInstance("CompetencyData").init()>
		<cfreturn CompetencyData>
	</cffunction>
	
	<cffunction name="createFilter" returntype="uiInterviewScheduleFilter" output="false" access="public">
		<cfargument name="formContent" required="true" />
		<cfargument name="cookiesContent" required="true" />

		<cfset uiInterviewScheduleFilter = variables.wirebox.getInstance(name="uiInterviewScheduleFilter").init(arguments.formContent, arguments.cookiesContent)>
		<cfreturn uiInterviewScheduleFilter>
	</cffunction>
	
</cfcomponent>