<cfcomponent name="InterviewScheduleSetup" output="false" extends="ServiceBase">
	<cfproperty name="breakBetweenStations" inject/>

	<cffunction name="init" access="public" output="false" returntype="InterviewScheduleSetup">
		<cfreturn this>
	</cffunction>
	
	<cffunction access="public" name="getAll" output="false" returntype="query">
		
		<cfset var InterviewScheduleGTW = variables.dbFactory.create("IschInterviewScheduleGTW")>
		<cfset var InterviewScheduleQry = InterviewScheduleGTW.getAll()>
		
		<cfreturn InterviewScheduleQry>
	</cffunction>
	
	<cffunction access="public" name="getInterviewScheduleBySession" output="false" returntype="query">
		<cfargument name="sessionID" type="numeric" required="true">
		
		<cfset var InterviewScheduleGTW = variables.dbFactory.create("IschInterviewScheduleGTW")>
		<cfset var InterviewScheduleQry = InterviewScheduleGTW.getBySessionID(arguments.sessionID)>
		<cfif InterviewScheduleQry.recordCount EQ 0>
			<cfset InterviewScheduleID = createInterviewSchedule(arguments.sessionID)>
			<cfset InterviewScheduleQry = InterviewScheduleGTW.getByPK(InterviewScheduleID)>
		</cfif>
		
		<cfreturn InterviewScheduleQry>
	</cffunction>
	
	<cffunction access="public" name="getInterviewScheduleByID" output="false" returntype="query">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		
		<cfset var InterviewScheduleGTW = variables.dbFactory.create("IschInterviewScheduleGTW")>
		<cfset var InterviewScheduleQry = InterviewScheduleGTW.getByPK(arguments.interviewScheduleID)>
		
		<cfreturn InterviewScheduleQry>
	</cffunction>

	<cffunction access="private" name="createInterviewSchedule" output="false" returntype="numeric">
		<cfargument name="sessionID" type="numeric" required="true">

		<cfset var InterviewScheduleDAO = variables.dbFactory.create("IschInterviewScheduleDAO")>
		<cfset var SessionGTW = variables.dbFactory.create("IschSessionGTW")>
		<cfset var sessionQry = SessionGTW.getByPK(arguments.sessionID)>
		
		<cfset var InterviewScheduleVO = variables.dbFactory.create("IschInterviewScheduleVO")>
		<cfset InterviewScheduleVO.setSESSION_ID(arguments.sessionID)>
		<cfset InterviewScheduleVO.setINTERVIEW_SCHEDULE_NAME("INTERVIEW SCHEDULE " & sessionQry.SESSION_NAME)>
		<cfset InterviewScheduleVO.setBREAK_BETWEEN_STATIONS(variables.breakBetweenStations)>
		<cfset InterviewScheduleID = InterviewScheduleDAO.add(InterviewScheduleVO)>

		<cfreturn InterviewScheduleID>
	</cffunction>

	<cffunction access="public" name="saveInterviewSchedule" output="false" returntype="boolean">
		<cfargument name="bean" type="InterviewScheduleData" required="true">

		<cfset var InterviewScheduleDAO = variables.dbFactory.create("IschInterviewScheduleDAO")>
		<cftry>
			<cfset var InterviewScheduleVO = InterviewScheduleDAO.read(arguments.bean.getINTERVIEW_SCHEDULE_ID())>
			<cfset InterviewScheduleVO.setSCORE_WEIGHT(arguments.bean.getSCORE_WEIGHT())>
			<cfset InterviewScheduleVO.setINTERVIEW_SCHEDULE_NAME(arguments.bean.getINTERVIEW_SCHEDULE_NAME())>
			<cfset InterviewScheduleVO.setBREAK_BETWEEN_STATIONS(arguments.bean.getBREAK_BETWEEN_STATIONS())>
			<cfset InterviewScheduleID = InterviewScheduleDAO.update(InterviewScheduleVO)>
			<cfset status = true>
			<cfset variables.MsgBox.success("Circuit successfully saved.")>
		<cfcatch>
			<cfset status = false>
			<cfset variables.MsgBox.success("Circuit successfully saved.")>
		</cfcatch>
		</cftry>
		<cfreturn status>
	</cffunction>


	<!--- Circuits --->
	<cffunction access="public" name="getCircuits" output="false" returntype="query">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfset var CircuitGTW = variables.dbFactory.create("IschCircuitGTW")>
		<cfset var circuitQry = CircuitGTW.getByFilter(arguments.filter)>
		
		<cfreturn circuitQry>
	</cffunction>

	<cffunction access="public" name="getCircuitByPK" output="false" returntype="query">
		<cfargument name="circuitID" type="numeric" required="true">
		
		<cfset var circuitGTW = variables.dbFactory.create("IschCircuitGTW")>
		<cfset var circuitQry = circuitGTW.getCircuitByPK(arguments.circuitID)>
		
		<cfreturn circuitQry>
	</cffunction>

	<cffunction access="public" name="getCircuitDetails" output="false" returntype="query">
		<cfargument name="circuitID" type="numeric" required="true">
		
		<cfset var circuitGTW = variables.dbFactory.create("IschCircuitGTW")>
		<cfset var circuitQry = circuitGTW.getDetails(arguments.circuitID)>
		
		<cfreturn circuitQry>
	</cffunction>

	<cffunction access="public" name="getCircuit" output="false" returntype="CircuitData">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		<cfargument name="circuitID" type="any" required="true">

		<cfset CircuitData = CreateObject("component", "CircuitData").init()>
		<cfif isNumeric(arguments.circuitID)>
			<cfset var CircuitDAO = variables.dbFactory.create("IschCircuitDAO")>
			<cfset var StationGTW = variables.dbFactory.create("IschStationGTW")>
			<cfset var ShiftGTW = variables.dbFactory.create("IschShiftGTW")>
			
			<cfset var CircuitVO = CircuitDAO.read(arguments.circuitID)>
			<cfset var StationQry = StationGTW.getByCircuit(arguments.circuitID)>
			<cfset var ShiftQry = ShiftGTW.getByCircuit(arguments.circuitID)>

			<cfset CircuitData.initFromStruct(CircuitVO.toStruct())>
			<cfset CircuitData.setCOMPETENCY_ID_LIST(ValueList(StationQry.COMPETENCY_ID))>
			<cfset CircuitData.setSHIFT_ID_LIST(ValueList(ShiftQry.SHIFT_ID))>
		</cfif>

		<cfreturn CircuitData>
	</cffunction>

	<cffunction access="public" name="saveCircuit" output="false" returntype="boolean">
		<cfargument name="bean" type="CircuitData" required="true">

		<cfset var success = validateCircuit(bean)>
		<cfif success>
			<cftransaction action="begin">
				<cftry>
					<cfset var CircuitDAO = variables.dbFactory.create("IschCircuitDAO")>
					<cfset var CircuitVO = variables.dbFactory.create("IschCircuitVO")>
					<cfset var ShiftGTW = variables.dbFactory.create("IschShiftGTW")>
					<cfset var StationGTW = variables.dbFactory.create("IschStationGTW")>
					<cfset var CircuitShiftsDAO = variables.dbFactory.create("IschCircuitShiftsDAO")>
					<cfset var StationDAO = variables.dbFactory.create("IschStationDAO")>
					<cfset CircuitVO.initFromForm(arguments.bean.toStruct())>
					<cfif CircuitVO.getCIRCUIT_ID() EQ ''>
						<cfset CircuitVO.setCREATION_ID(session.user_id)>
						<cfset CircuitVO.setCREATION_DT(Now())>
						<cfset circuitID = CircuitDAO.add(CircuitVO)>
						<cfset arguments.bean.setCIRCUIT_ID(circuitID)>

						<!--- Manage Circuit Stations --->
					    <cfset var orderNumber = 0>
						<cfloop list="#arguments.bean.getCOMPETENCY_ID_LIST()#" index="competencyID">
							<cfset orderNumber++>
							<cfset StationVO = variables.dbFactory.create("IschStationVO")>
							<cfset StationVO.setCOMPETENCY_ID(competencyID)>
							<cfset StationVO.setCIRCUIT_ID(arguments.bean.getCIRCUIT_ID())>
							<cfset StationVO.setORDER_NUMBER(orderNumber)>
							<cfset StationVO.setCREATION_ID(session.user_id)>
							<cfset StationVO.setCREATION_DT(Now())>
							<cfset StationDAO.add(stationVO)>
						</cfloop>
						
						<!--- Manage Circuit Shifts --->
						<cfloop list="#arguments.bean.getSHIFT_ID_LIST()#" index="shiftID">
							<cfset CircuitShiftsVO = variables.dbFactory.create("IschCircuitShiftsVO")>
							<cfset CircuitShiftsVO.setSHIFT_ID(shiftID)>
							<cfset CircuitShiftsVO.setCIRCUIT_ID(arguments.bean.getCIRCUIT_ID())>
							<cfset CircuitShiftsDAO.add(CircuitShiftsVO)>
						</cfloop>
					<cfelse>
						<cfset CircuitVO.setMODIFICATION_ID(session.user_id)>
						<cfset CircuitVO.setMODIFICATION_DT(Now())>
						<cfset CircuitDAO.update(CircuitVO)>
					</cfif>

					<cfset variables.MsgBox.success("Circuit successfully saved.")>
					<cftransaction action="commit" />
					<cfcatch>
						<cfset success = false>
						<cftransaction action="rollback" />
						<cfset variables.MsgBox.error("An error occurred while saving Circuit.")>
					</cfcatch>
				</cftry>
			</cftransaction>
		</cfif>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="public" name="deleteCircuit" output="false" returntype="boolean">
		<cfargument name="circuitID" type="numeric" required="true">
		
		<cfset success = true>
		<cftransaction action="begin">
			<cftry>
				<cfset var CircuitDAO = variables.dbFactory.create("IschCircuitDAO")>
				<cfset var CircuitShiftsDAO = variables.dbFactory.create("IschCircuitShiftsDAO")>
				<cfset var ShiftGTW = variables.dbFactory.create("IschShiftGTW")>
				<cfset var StationDAO = variables.dbFactory.create("IschStationDAO")>
				<cfset var StationGTW = variables.dbFactory.create("IschStationGTW")>
				<cfset var ApplicantScheduleDAO = variables.dbFactory.create("IschApplicantDAO")>

				<cfset var SationQry = StationGTW.getByCircuit(arguments.circuitID)>

				<cfloop query="SationQry">
					<cfset ApplicantScheduleDAO.deleteByStationID(SationQry.STATION_ID)>
					<cfset StationDAO.delete(SationQry.STATION_ID)>
				</cfloop>

				<cfset var ShiftQry = ShiftGTW.getByCircuit(arguments.circuitID)>
				<cfloop query="ShiftQry">
					<cfset CircuitShiftsDAO.delete(arguments.circuitID, ShiftQry.SHIFT_ID)>
				</cfloop>

				<cfset CircuitDAO.delete(arguments.circuitID)>

<!--- 				<cfset variables.MsgBox.success("Circuit successfully deleted.")> --->
				<cftransaction action="commit" />
				<cfcatch>
					<cfset success = false>
					<cftransaction action="rollback" />
					<cfset variables.MsgBox.error("An error occurred while deleting Circuit.")>
				</cfcatch>
			</cftry>
		</cftransaction>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="private" name="validateCircuit" output="false" returntype="boolean">
		<cfargument name="bean" type="CircuitData" required="true">

		<cfset var bValid = true>
		<cfif Trim(arguments.bean.getDAY_ID()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Day is required.")>
		</cfif>
		
		<cfif Trim(arguments.bean.getCIRCUIT_NAME()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Name is required.")>
		</cfif>

		<cfif Trim(arguments.bean.getLOCATION()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Location is required.")>
		</cfif>
		
		<cfreturn bValid>
	</cffunction>


	<!--- Stations --->
	<cffunction access="public" name="getStation" output="false" returntype="StationData">
		<cfargument name="circuitID" type="any" required="true">
		<cfargument name="stationID" type="any" required="true">

		<cfset var StationData = CreateObject("component", "StationData").init()>
		<cfset circuitQry = getCircuitDetails(arguments.circuitID)>
		<cfset StationData.setCIRCUIT_ID(circuitQry.CIRCUIT_ID)>
		<cfset StationData.setCIRCUIT_NAME(circuitQry.CIRCUIT_NAME)>
		<cfset StationData.setDAY_ID(circuitQry.DAY_ID)>
		<cfset StationData.setDAY_NAME(circuitQry.DAY_NAME)>
		<cfset StationData.setQUESTION_ID(circuitQry.QUESTION_ID)>
		<cfif isNumeric(arguments.stationID)>
			<cfset var StationDAO = variables.dbFactory.create("IschStationDAO")>
			<cfset var StationVO = StationDAO.read(arguments.stationID)>
			
			<cfset StationData.initFromStruct(StationVO.toStruct())>
		</cfif>

		<cfreturn StationData>
	</cffunction>

	<cffunction access="public" name="saveStation" output="false" returntype="boolean">
		<cfargument name="bean" type="StationData" required="true">

		<cfset var success = validateStation(bean)>
		<cfif success>
			<cftry>
				<cfset var StationDAO = variables.dbFactory.create("IschStationDAO")>
				<cfset var StationVO = variables.dbFactory.create("IschStationVO")>
				
				<cfset StationVO.initFromForm(arguments.bean.toStruct())>
				<cfif StationVO.getORDER_NUMBER() EQ "">
					<cfset var StationGTW = variables.dbFactory.create("IschStationGTW")>
					<cfset var StationQry = StationGTW.getByCircuit(StationVO.getCIRCUIT_ID())>
				    <cfset orderNumber = 1>
					<cfif isNumeric(StationQry.MAX_NUMBER)>
					    <cfset orderNumber = StationQry.MAX_NUMBER + 1>
					</cfif>
					<cfset StationVO.setORDER_NUMBER(orderNumber)>
				</cfif>
				<cfif StationVO.getSTATION_ID() EQ "">
					<cfset StationVO.setCREATION_ID(session.user_id)>
					<cfset StationVO.setCREATION_DT(Now())>
					<cfset stationID = StationDAO.add(StationVO)>
					<cfset arguments.bean.setSTATION_ID(stationID)>
				<cfelse>
					<cfset StationVO.setMODIFICATION_ID(session.user_id)>
					<cfset StationVO.setMODIFICATION_DT(Now())>
					<cfset StationDAO.update(StationVO)>
				</cfif>
				<cfif ArrayLen(variables.MsgBox.getMessage()) EQ 0>
					<cfset variables.MsgBox.success("Station successfully saved.")>
				</cfif>
				<cfcatch>
					<cfset success = false>
					<cfset variables.MsgBox.error("An error occurred while saving Station.")>
				</cfcatch>
			</cftry>
		</cfif>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="public" name="deleteStation" output="false" returntype="boolean">
		<cfargument name="stationID" type="numeric" required="true">
		
		<cfset success = true>
		<cftry>
			<cfset var StationDAO = variables.dbFactory.create("IschStationDAO")>
			<cfset var StationVO = StationDAO.read(arguments.stationID)>
			<cfset var ApplicantScheduleDAO = variables.dbFactory.create("IschApplicantDAO")>
			
			<cfset ApplicantScheduleDAO.deleteByStationID(arguments.stationID)>
			<cfset StationDAO.delete(arguments.stationID)>
			
			<!--- Reorder Station --->
			<cfset var StationGTW = variables.dbFactory.create("IschStationGTW")>
			<cfset var StationQry = StationGTW.getByCircuit(StationVO.getCIRCUIT_ID())>
			<cfset success = reorderStation(ValueList(StationQry.STATION_ID))>
			<cfset reorderStation(ValueList(StationQry.STATION_ID))>
			
			<cfset variables.MsgBox.success("Station successfully deleted.")>
			<cfcatch>
				<cfset success = false>
				<cfset variables.MsgBox.error("An error occurred while deleting Station.")>
			</cfcatch>
		</cftry>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="public" name="reorderStation" output="false" returntype="boolean">
		<cfargument name="stationIDList" type="string" required="true">

		<cfset var success = true>
		<cftransaction action="begin">
			<cftry>
				<cfset var StationDAO = variables.dbFactory.create("IschStationDAO")>
				<cfset i = 0>
				<cfloop list="#arguments.stationIDList#" index="stationID">
					<cfset i++>
					<cfset StationVO = StationDAO.read(stationID)>
					<cfset StationVO.setORDER_NUMBER(i)>
					<cfset StationVO.setMODIFICATION_ID(session.user_id)>
					<cfset StationVO.setMODIFICATION_DT(Now())>
					<cfset StationDAO.update(StationVO)>
				</cfloop>
				
				<cfcatch>
					<cfset success = false>
				<cfset variables.MsgBox.error("An error occurred while reordering Stations.")>
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn success>
	</cffunction>

	<cffunction access="private" name="validateStation" output="false" returntype="boolean">
		<cfargument name="bean" type="StationData" required="true">

		<cfset var bValid = true>
		<cfif Trim(arguments.bean.getROOM()) neq "">
			<cfset var StationGTW = variables.dbFactory.create("IschStationGTW")>
			<cfset roomQry = StationGTW.checkIfRoomOccupied(arguments.bean.getDAY_ID(), arguments.bean.getROOM())>
			<cfif roomQry.recordCount neq 0 and roomQry.STATION_ID neq arguments.bean.getSTATION_ID()>
				<cfset bValid = false>
				<cfset variables.MsgBox.error("Room #roomQry.ROOM# is already used on #roomQry.DAY_NAME# / #DateFormat(roomQry.DAY_DT,'dd-Mmm-yyyy')#.")>
			</cfif>
		</cfif>
		
		<cfif arguments.bean.getCOMPETENCY_ID() eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Competency is required.")>
		<cfelse>
			<cfif arguments.bean.getINTERVIEWER_ID() neq "">
				<cfset var CompetencyGTW = variables.dbFactory.create("IschCompetencyGTW")>
				<cfset var InterviewerGTW = variables.dbFactory.create("IschInterviewerGTW")>
				<cfset CompetencyQry = CompetencyGTW.getByPK(arguments.bean.getCOMPETENCY_ID())>
				<cfset InterviewerQry = InterviewerGTW.getByPK(arguments.bean.getINTERVIEWER_ID())>
				<cfif lCase(CompetencyQry.COMPETENCY_NAME) NEQ 'self-reflection' AND InterviewerQry.TYPE EQ 'STUDENT'>
					<cfset variables.MsgBox.warn("Warning: You have scheduled a student for a competency other than Self-reflection.")>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn bValid>
	</cffunction>


	<!--- Shifts --->
	<cffunction access="public" name="getShifts" output="false" returntype="query">
		<cfargument name="interviewScheduleID" type="numeric" required="true">

		<cfset var ShiftGTW = variables.dbFactory.create("IschShiftGTW")>
		<cfset var ShiftQry = ShiftGTW.getByInterviewSchedule(arguments.interviewScheduleID)>
		
		<cfreturn ShiftQry>
	</cffunction>

	<cffunction access="public" name="getShift" output="false" returntype="ShiftData">
		<cfargument name="circuitID" type="numeric" required="true">
		<cfargument name="shiftID" type="any" required="true">

		<cfset var ShiftData = CreateObject("component", "ShiftData").init()>
		<cfset circuitQry = getCircuitDetails(arguments.circuitID)>
		<cfset ShiftData.setCIRCUIT_ID(circuitQry.CIRCUIT_ID)>
		<cfset ShiftData.setCIRCUIT_NAME(circuitQry.CIRCUIT_NAME)>
		<cfset ShiftData.setINTERVIEW_SCHEDULE_ID(circuitQry.INTERVIEW_SCHEDULE_ID)>
		<cfset ShiftData.setDAY_NAME(circuitQry.DAY_NAME)>
		<cfif isNumeric(arguments.shiftID)>
			<cfset var ShiftDAO = variables.dbFactory.create("IschShiftDAO")>
			<cfset var ShiftVO = ShiftDAO.read(arguments.shiftID)>
			<cfset ShiftData.initFromStruct(ShiftVO.toStruct())>
		</cfif>

		<cfreturn ShiftData>
	</cffunction>

	<cffunction access="public" name="saveShift" output="false" returntype="boolean">
		<cfargument name="bean" type="ShiftData" required="true">

		<cfset var success = validateShift(bean)>
		<cfif success>
			<cftransaction action="begin">
				<cftry>
					<cfset var CircuitGTW = variables.dbFactory.create("IschCircuitGTW")>
					<cfset var ShiftGTW = variables.dbFactory.create("IschShiftGTW")>
					<cfset var CircuitShiftsGTW = variables.dbFactory.create("IschCircuitShiftsGTW")>
					<cfset var ShiftDAO = variables.dbFactory.create("IschShiftDAO")>
					<cfset var CircuitShiftsDAO = variables.dbFactory.create("IschCircuitShiftsDAO")>
					<cfset var ShiftVO = variables.dbFactory.create("IschShiftVO")>
					<cfset var CircuitShiftsVO = variables.dbFactory.create("IschCircuitShiftsVO")>
					<cfset var ApplicantScheduleDAO = variables.dbFactory.create("IschApplicantDAO")>
					
					<cfset ShiftVO.initFromForm(arguments.bean.toStruct())>
					<cfset shiftQry = ShiftGTW.getByTime(arguments.bean.getINTERVIEW_SCHEDULE_ID(), ShiftVO.getSTART_HOUR(), ShiftVO.getSTART_MINUTE(), ShiftVO.getEND_HOUR(), ShiftVO.getEND_MINUTE())>
					<cfset ShiftVO.setSHIFT_ID(shiftQry.SHIFT_ID)>

					<!--- Add Shift if it's a new one --->
					<cfif ShiftVO.getSHIFT_ID() EQ ''>
						<cfset ShiftVO.setCREATION_ID(session.user_id)>
						<cfset ShiftVO.setCREATION_DT(Now())>
						<cfset shiftID = ShiftDAO.add(ShiftVO)>
						<cfset ShiftVO.setSHIFT_ID(shiftID)>
					</cfif>

					<!--- Establish connection between Circuit and Shift --->
					<cfset CircuitShiftsVO.setCIRCUIT_ID(arguments.bean.getCIRCUIT_ID())>
					<cfset CircuitShiftsVO.setSHIFT_ID(ShiftVO.getSHIFT_ID())>
					<cfset CircuitShiftsQry = CircuitShiftsGTW.getByPK(CircuitShiftsVO.getCIRCUIT_ID(), CircuitShiftsVO.getSHIFT_ID())>
					<cfif CircuitShiftsQry.recordCount EQ 0>
						<cfset CircuitShiftsDAO.add(CircuitShiftsVO)>
					<cfelseif arguments.bean.getSHIFT_ID() eq "">
						<cfthrow type="ISCH_ERROR" detail="Shift #NumberFormat(ShiftVO.getSTART_HOUR(),'00')#:#NumberFormat(ShiftVO.getSTART_MINUTE(),'00')# - #NumberFormat(ShiftVO.getEND_HOUR(),'00')#:#NumberFormat(ShiftVO.getEND_MINUTE(),'00')# already exists.">
					</cfif>

					<!--- Delete old connection between Circuit and Shift --->
					<cfif arguments.bean.getSHIFT_ID() NEQ '' AND arguments.bean.getSHIFT_ID() NEQ ShiftVO.getSHIFT_ID()>
						<cfset ApplicantScheduleDAO.updateShift(arguments.bean.getCIRCUIT_ID(),arguments.bean.getSHIFT_ID(),ShiftVO.getSHIFT_ID())>
						<cfset CircuitShiftsDAO.delete(arguments.bean.getCIRCUIT_ID(), arguments.bean.getSHIFT_ID())>
					</cfif>
					
					<!--- Delete shift if it's not used --->
					<cfif arguments.bean.getSHIFT_ID() NEQ ''>
						<cfset circuitQry = CircuitGTW.getByShift(arguments.bean.getSHIFT_ID())>
						<cfif circuitQry.recordCount EQ 0>
							<cfset ShiftDAO.delete(arguments.bean.getSHIFT_ID())>
						</cfif>
					</cfif>
		
					<cfset arguments.bean.setSHIFT_ID(ShiftVO.getSHIFT_ID())>
					<cfset variables.MsgBox.success("Shift successfully saved.")>
					<cftransaction action="commit" />
					<cfcatch type="ISCH_ERROR">
						<cfset success = false>
						<cftransaction action="rollback" />
						<cfset variables.MsgBox.error("#cfcatch.detail#")>
					</cfcatch>
					<cfcatch>
						<cfset success = false>
						<cftransaction action="rollback" />
						<cfset variables.MsgBox.error("An error occurred while saving Shift.")>
					</cfcatch>
				</cftry>
			</cftransaction>
		</cfif>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="public" name="deleteShift" output="false" returntype="boolean">
		<cfargument name="circuitID" type="numeric" required="true">
		<cfargument name="shiftID" type="numeric" required="true">
		
		<cfset success = true>
		<cftransaction action="begin">
			<cftry>
				<cfset var CircuitGTW = variables.dbFactory.create("IschCircuitGTW")>
				<cfset var CircuitShiftsDAO = variables.dbFactory.create("IschCircuitShiftsDAO")>
				<cfset var ApplicantScheduleDAO = variables.dbFactory.create("IschApplicantDAO")>
				<cfset var ShiftDAO = variables.dbFactory.create("IschShiftDAO")>

				<cfset ApplicantScheduleDAO.deleteByCircuitAndShift(arguments.circuitID, arguments.shiftID)>
				<cfset CircuitShiftsDAO.delete(arguments.circuitID, arguments.shiftID)>
				
				<cfset circuitQry = CircuitGTW.getByShift(arguments.shiftID)>
				<cfif circuitQry.recordCount EQ 0>
					<cfset ShiftDAO.delete(arguments.shiftID)>
				</cfif>
	
				<cfset variables.MsgBox.success("Shift successfully deleted.")>
				<cftransaction action="commit" />
				<cfcatch>
					<cfset success = false>
					<cftransaction action="rollback" />
					<cfset variables.MsgBox.error("An error occurred while deleting Shift.")>
				</cfcatch>
			</cftry>
		</cftransaction>
		
		<cfreturn success>
	</cffunction>

	<cffunction access="private" name="validateShift" output="false" returntype="boolean">
		<cfargument name="bean" type="ShiftData" required="true">

		<cfset var bValid = true>
		<cfif arguments.bean.getSTART_HOUR() GT arguments.bean.getEND_HOUR()>
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Start time must precede End time.")>
		</cfif>

		<cfif arguments.bean.getSTART_HOUR() EQ arguments.bean.getEND_HOUR() AND arguments.bean.getSTART_MINUTE() GTE arguments.bean.getEND_MINUTE()>
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Start time must precede End time.")>
		</cfif>
		
		<cfreturn bValid>
	</cffunction>

	<cffunction name="createBean" returntype="any" output="false" access="public">
		<cfargument name="type" type="string" required="true">
		<cfswitch expression="#arguments.type#">
			<cfcase value="CircuitData">
				<cfreturn variables.wirebox.getInstance("CircuitData").init()>
			</cfcase>
			<cfcase value="ShiftData">
				<cfreturn variables.wirebox.getInstance("ShiftData").init()>
			</cfcase>
			<cfcase value="StationData">
				<cfreturn variables.wirebox.getInstance("StationData").init()>
			</cfcase>
			<cfcase value="InterviewScheduleData">
				<cfreturn variables.wirebox.getInstance("InterviewScheduleData").init()>
			</cfcase>
			<cfdefaultcase>
				<cfthrow detail="Accepted types are: InterviewScheduleData, CircuitData, ShiftData and StationData">
			</cfdefaultcase>
		</cfswitch>
	</cffunction>
	
	<cffunction name="createFilter" returntype="uiInterviewScheduleFilter" output="false" access="public">
		<cfargument name="formContent" required="true" />
		<cfargument name="cookiesContent" required="true" />

		<cfset uiInterviewScheduleFilter = variables.wirebox.getInstance("uiInterviewScheduleFilter").init(arguments.formContent, arguments.cookiesContent)>
		<cfreturn uiInterviewScheduleFilter>
	</cffunction>

</cfcomponent>