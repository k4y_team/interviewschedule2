<cfcomponent name="IschDayDAO" displayname="IschDayDAO" hint="I abstract data access for ISCH_DAY" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschDayDAO" output="false" displayname="init" hint="I initialize a IschDayDAO">
		<cfargument name="datasource" type="string" required="true" />	
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschDayVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="dayID" type="numeric" required="true" />
		
		<cfset var IschDayVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				DAY_ID,
				DAY_NAME,
				DAY_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_DAY
			WHERE		
				DAY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.dayID#"/> 
		</cfquery>
		<cfset IschDayVO = CreateObject("component","IschDayVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschDayVO.setDAY_ID(getQry.DAY_ID) />
			<cfset IschDayVO.setDAY_NAME(getQry.DAY_NAME) />
			<cfset IschDayVO.setDAY_DT(getQry.DAY_DT) />
			<cfset IschDayVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschDayVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschDayVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschDayVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschDayVO.setINTERVIEW_SCHEDULE_ID(getQry.INTERVIEW_SCHEDULE_ID) />
		</cfif>
		
		<cfreturn IschDayVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschDayVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_DAY_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_DAY (
				DAY_ID ,DAY_NAME ,DAY_DT ,CREATION_ID ,CREATION_DT ,INTERVIEW_SCHEDULE_ID)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getDAY_NAME()#" maxLength="50" null="#iif((arguments.bean.getDAY_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getDAY_DT()#" maxLength="26" null="#iif((arguments.bean.getDAY_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschDayVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_DAY
			SET
				DAY_NAME = <cfqueryparam value="#arguments.bean.getDAY_NAME()#" cfsqltype="cf_sql_varchar" maxLength="50" null="#iif((arguments.bean.getDAY_NAME() eq ''),de("true"), de("false"))#" />,
				DAY_DT = <cfqueryparam value="#arguments.bean.getDAY_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getDAY_DT() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				INTERVIEW_SCHEDULE_ID = <cfqueryparam value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" /> 
			WHERE
				DAY_ID = <cfqueryparam value="#arguments.bean.getDAY_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="dayID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_DAY
			WHERE 
				DAY_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.dayID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>
