<cfcomponent name="IschCompetencyQuestionGTW" displayname="IschCompetencyQuestionGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschCompetencyQuestionGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				QUESTION_ID,
				COMPETENCY_ID,
				QUESTION_NAME,
				IS_REQUIRED,
				IS_EDITABLE,
				QUESTION_RANK,
				QUESTION_TYPE_ID,
				CREATION_DT,
				CREATION_ID,
				MODIFICATION_DT,
				MODIFICATION_ID
			FROM ISCH_COMPETENCY_QUESTION
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="questionID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				QUESTION_ID,
				COMPETENCY_ID,
				QUESTION_NAME,
				IS_REQUIRED,
				IS_EDITABLE,
				QUESTION_RANK,
				QUESTION_TYPE_ID,
				CREATION_DT,
				CREATION_ID,
				MODIFICATION_DT,
				MODIFICATION_ID
			FROM ISCH_COMPETENCY_QUESTION
			WHERE
				QUESTION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.questionID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
		
	<cffunction name="getByCompetencyID" access="public" returntype="query" output="false" displayname="getByCompetencyID" hint="">
		<cfargument name="competencyID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				Q.*,
				QT.QUESTION_TYPE_NAME
			FROM 
				ISCH_COMPETENCY_QUESTION Q,
				ISCH_QUESTION_TYPE QT
			WHERE
				Q.QUESTION_TYPE_ID = QT.QUESTION_TYPE_ID
				AND Q.COMPETENCY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#" />
			ORDER BY QUESTION_RANK
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
		
	<cffunction name="getByCompetencyIDandRank" access="public" returntype="query" output="false" displayname="getByCompetencyID" hint="">
		<cfargument name="competencyID" type="numeric" required="true" />
		<cfargument name="rank" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				Q.*,
				QT.QUESTION_TYPE_NAME
			FROM 
				ISCH_COMPETENCY_QUESTION Q,
				ISCH_QUESTION_TYPE QT
			WHERE
				Q.QUESTION_TYPE_ID = QT.QUESTION_TYPE_ID
				AND Q.COMPETENCY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#" />
				AND Q.QUESTION_RANK = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.rank#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getCompetencyScores" access="public" returntype="query" output="false" displayname="getCompetencyScores">
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfargument name="competencyID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				cq.question_name,
				qs.score_value,
				cq.is_editable,
				cq.question_rank,
				c.competency_name,
				qt.question_type_code,
				aso.score_value as total_score,
				qs.answer_text
			FROM
				isch_question_score qs,
				isch_competency_question cq,
				isch_competency c,
				isch_question_type qt,
				isch_applicant_score_out aso
			WHERE
				cq.competency_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#" />
				AND qs.question_id = cq.question_id
				AND cq.competency_id = c.competency_id
				AND cq.question_type_id = qt.question_type_id
				AND aso.applicant_id = qs.applicant_id
				AND aso.competency_id = cq.competency_id
				AND qs.applicant_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#" />
			ORDER BY cq.question_rank
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getCompetencyQuestions" access="public" returntype="query" output="false" displayname="getCompetencyScores">
		<cfargument name="interview_schedule_id" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
		select DISTINCT
			  CQ.QUESTION_RANK
			from 
			  ISCH_COMPETENCY C,
			  isch_competency_question CQ,
			  ISCH_QUESTION_TYPE QT
			WHERE
			  C.COMPETENCY_ID = cq.competency_id
			  AND CQ.question_type_ID = QT.QUESTION_TYPE_ID
			  AND C.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interview_schedule_id#" />
			ORDER BY CQ.QUESTION_RANK
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getCompetencyQuestionsDetails" access="public" returntype="query" output="false" displayname="getCompetencyQuestionsDetails">
		<cfargument name="interview_schedule_id" type="numeric" required="true" />
		
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			select distinct
				C.COMPETENCY_ID,
				C.COMPETENCY_NAME,
				CQ.QUESTION_RANK,
				CQ.QUESTION_ID,
				CQ.IS_REQUIRED,
				QT.QUESTION_TYPE_ID,
				QT.QUESTION_TYPE_CODE,
				QTR.RATING_ID,	
				DECODE(QTR.MIN_VALUE, NULL, '0',QTR.MIN_VALUE) AS MIN_VALUE,
				DECODE(QTR.MAX_VALUE, NULL, '1',QTR.MAX_VALUE) AS MAX_VALUE
			from 
				ISCH_COMPETENCY C,	
				ISCH_COMPETENCY_QUESTION CQ,
				ISCH_QUESTION_TYPE QT,
				ISCH_QUESTION_TYPE_RATING QTR
			WHERE
				C.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interview_schedule_id#" />
				AND C.COMPETENCY_ID = CQ.COMPETENCY_ID
				AND CQ.QUESTION_TYPE_ID = QT.QUESTION_TYPE_ID
				AND QTR.RATING_ID(+) = QT.RATING_ID
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getAllCompetencyQuestions" access="public" returntype="query" output="false" displayname="getAllCompetencyQuestions">
		<cfargument name="trainingSessionCD" type="numeric" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
			  CO.COMPETENCY_NAME,
			  CQ.QUESTION_NAME,
			  CQ.QUESTION_ID,
			  CQ.QUESTION_RANK
			FROM 
			  ISCH_COMPETENCY_QUESTION CQ,
			  ISCH_COMPETENCY CO,
			  ISCH_QUESTION_TYPE QT,
			  ISCH_INTERVIEW_SCHEDULE IIS
			WHERE
				CQ.COMPETENCY_ID = CO.COMPETENCY_ID AND
				CQ.QUESTION_TYPE_ID = QT.QUESTION_TYPE_ID AND
				QT.QUESTION_TYPE_CODE <> 'TEXT' AND
				IIS.INTERVIEW_SCHEDULE_ID = CO.INTERVIEW_SCHEDULE_ID AND
				IIS.SESSION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.trainingSessionCD#" />
			ORDER BY
			  	 CO.COMPETENCY_NAME,  CQ.QUESTION_RANK
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
		
</cfcomponent>
