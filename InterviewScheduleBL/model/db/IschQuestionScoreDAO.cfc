<cfcomponent name="IschQuestionScoreDAO" displayname="IschQuestionScoreDAO" hint="I abstract data access for ISCH_QUESTION_SCORE" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschQuestionScoreDAO" output="false" displayname="init" hint="I initialize a IschQuestionScoreDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschQuestionScoreVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="questionID" type="numeric" required="true" />
		<cfargument name="applicantID" type="numeric" required="true" />
		
		<cfset var IschQuestionScoreVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				QUESTION_ID,
				APPLICANT_ID,
				SCORE_VALUE,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				ANSWER_TEXT
			FROM ISCH_QUESTION_SCORE
			WHERE		
				QUESTION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.questionID#"/>  AND
				APPLICANT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#"/> 
		</cfquery>
		<cfset IschQuestionScoreVO = CreateObject("component","IschQuestionScoreVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschQuestionScoreVO.setQUESTION_ID(getQry.QUESTION_ID) />
			<cfset IschQuestionScoreVO.setAPPLICANT_ID(getQry.APPLICANT_ID) />
			<cfset IschQuestionScoreVO.setSCORE_VALUE(getQry.SCORE_VALUE) />
			<cfset IschQuestionScoreVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschQuestionScoreVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschQuestionScoreVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschQuestionScoreVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschQuestionScoreVO.setANSWER_TEXT(getQry.ANSWER_TEXT) />
		</cfif>
		
		<cfreturn IschQuestionScoreVO />
	</cffunction>

	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschQuestionScoreVO" required="true" />
		<cfset var seqQry = "">	
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_QUESTION_SCORE (
				QUESTION_ID ,APPLICANT_ID ,SCORE_VALUE ,CREATION_ID ,CREATION_DT ,ANSWER_TEXT)
			VALUES (<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getQUESTION_ID()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getAPPLICANT_ID()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSCORE_VALUE()#" maxLength="22" null="#iif((arguments.bean.getSCORE_VALUE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getANSWER_TEXT()#" maxLength="4000" null="#iif((arguments.bean.getANSWER_TEXT() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn 0/>
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschQuestionScoreVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_QUESTION_SCORE
			SET
				SCORE_VALUE = <cfqueryparam value="#arguments.bean.getSCORE_VALUE()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSCORE_VALUE() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				ANSWER_TEXT = <cfqueryparam value="#arguments.bean.getANSWER_TEXT()#" cfsqltype="cf_sql_varchar" maxLength="4000" null="#iif((arguments.bean.getANSWER_TEXT() eq ''),de("true"), de("false"))#" /> 
			WHERE
				QUESTION_ID = <cfqueryparam value="#arguments.bean.getQUESTION_ID()#" cfsqltype="CF_SQL_NUMERIC" />  AND
				APPLICANT_ID = <cfqueryparam value="#arguments.bean.getAPPLICANT_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="questionID" type="numeric" required="true" />
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_QUESTION_SCORE
			WHERE 
				QUESTION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.questionID#"/>
				AND APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.applicantID#"/>
		</cfquery>
	</cffunction>

</cfcomponent>