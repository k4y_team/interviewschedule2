<cfcomponent name="IschCircuitVO" displayname="IschCircuitVO" extends="getSetObjectValue" output="false">
	<cfproperty name="CIRCUIT_ID" type="numeric" />
	<cfproperty name="CIRCUIT_NAME" type="string" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />
	<cfproperty name="DAY_ID" type="numeric" />
	<cfproperty name="LOCATION" type="string" />
	<cfproperty name="COLOR" type="string" />
		
	<cffunction name="init" access="public" returntype="IschCircuitVO" output="false" displayname="init" hint="I initialize a IschCircuit">
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.CIRCUIT_NAME = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.DAY_ID = "" />
		<cfset variables.LOCATION = "" />
		<cfset variables.COLOR = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CIRCUIT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>
	<cffunction name="setCIRCUIT_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CIRCUIT_NAME = arguments.val />
	</cffunction>
	<cffunction name="getCIRCUIT_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_NAME />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
	<cffunction name="setDAY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.DAY_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getDAY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_ID />
	</cffunction>
	<cffunction name="setLOCATION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LOCATION = arguments.val />
	</cffunction>
	<cffunction name="getLOCATION" access="public" returntype="any" output="false">
		<cfreturn variables.LOCATION />
	</cffunction>
	<cffunction name="setCOLOR" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.COLOR = arguments.val />
	</cffunction>
	<cffunction name="getCOLOR" access="public" returntype="any" output="false">
		<cfreturn variables.COLOR />
	</cffunction>
</cfcomponent>