<cfcomponent name="IschShiftGTW" displayname="IschShiftGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschShiftGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				SHIFT_ID,
				START_HOUR,
				START_MINUTE,
				END_HOUR,
				END_MINUTE,
				CREATION_ID,
				CREATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_SHIFT
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="shiftID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				SHIFT_ID,
				START_HOUR,
				START_MINUTE,
				END_HOUR,
				END_MINUTE,
				CREATION_ID,
				CREATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_SHIFT
			WHERE
				SHIFT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.shiftID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByInterviewSchedule" access="public" returntype="query" output="false" displayname="getByID" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				SHIFT_ID,
				START_HOUR,
				START_MINUTE,
				START_HOUR || ':' || START_MINUTE AS START_TIME,
				END_HOUR,
				END_MINUTE,
				END_HOUR || ':' || END_MINUTE AS END_TIME,
				CREATION_ID,
				CREATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_SHIFT
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" />
			ORDER BY
				START_HOUR, START_MINUTE				
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByCircuit" access="public" returntype="query" output="false" displayname="getByID" hint="">
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				S.SHIFT_ID,
				S.START_HOUR,
				S.START_MINUTE,
				S.START_HOUR || ':' || S.START_MINUTE AS START_TIME,
				S.END_HOUR,
				S.END_MINUTE,
				S.END_HOUR || ':' || S.END_MINUTE AS END_TIME,
				S.CREATION_ID,
				S.CREATION_DT,
				S.INTERVIEW_SCHEDULE_ID
			FROM
				ISCH_SHIFT S,
				ISCH_CIRCUIT_SHIFTS CS
			WHERE
				S.SHIFT_ID = CS.SHIFT_ID
				AND CS.CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByTime" access="public" returntype="query" output="false" displayname="getByTime" hint="">
		<cfargument name="interview_schedule_id" type="numeric" required="true">
		<cfargument name="start_hour" type="numeric" required="true">
		<cfargument name="start_minute" type="numeric" required="true">
		<cfargument name="end_hour" type="numeric" required="true">
		<cfargument name="end_minute" type="numeric" required="true">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				SHIFT_ID,
				START_HOUR,
				START_MINUTE,
				START_HOUR || ':' || START_MINUTE AS START_TIME,
				END_HOUR,
				END_MINUTE,
				END_HOUR || ':' || END_MINUTE AS END_TIME,
				CREATION_ID,
				CREATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM
				ISCH_SHIFT
			WHERE
				START_HOUR = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.start_hour#" />
				AND START_MINUTE = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.start_minute#" />
				AND END_HOUR = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.end_hour#" />
				AND END_MINUTE = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.end_minute#" />
				AND INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interview_schedule_id#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
</cfcomponent>