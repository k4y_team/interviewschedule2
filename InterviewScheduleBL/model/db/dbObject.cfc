<cfcomponent name="dbObject" displayname="dbObject">

	<cfproperty name="datasource" type="string"/>

	<cffunction name="init" access="public" returntype="dbObject" output="false" hint="dbObject Constructor.">
		<cfargument name="datasource" type="string" required="true">
		<cfset variables.instance.datasource   =  arguments.datasource />
		<cfreturn this />
	</cffunction>

	<cffunction name="getDatasource" access="private" returntype="string">
		<cfreturn variables.instance.datasource />
	</cffunction>

</cfcomponent>