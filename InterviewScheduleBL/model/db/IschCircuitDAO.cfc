<cfcomponent name="IschCircuitDAO" displayname="IschCircuitDAO" hint="I abstract data access for ISCH_CIRCUIT" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschCircuitDAO" output="false" displayname="init" hint="I initialize a IschCircuitDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschCircuitVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="circuitID" type="numeric" required="true" />
		
		<cfset var IschCircuitVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				CIRCUIT_ID,
				CIRCUIT_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				DAY_ID,
				COLOR,
				LOCATION
			FROM ISCH_CIRCUIT
			WHERE		
				CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#"/> 
		</cfquery>
		<cfset IschCircuitVO = CreateObject("component","IschCircuitVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschCircuitVO.setCIRCUIT_ID(getQry.CIRCUIT_ID) />
			<cfset IschCircuitVO.setCIRCUIT_NAME(getQry.CIRCUIT_NAME) />
			<cfset IschCircuitVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschCircuitVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschCircuitVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschCircuitVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschCircuitVO.setDAY_ID(getQry.DAY_ID) />
			<cfset IschCircuitVO.setLOCATION(getQry.LOCATION) />
			<cfset IschCircuitVO.setCOLOR(getQry.COLOR) />
		</cfif>
		
		<cfreturn IschCircuitVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschCircuitVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_CIRCUIT_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_CIRCUIT (
				CIRCUIT_ID ,CIRCUIT_NAME ,CREATION_ID ,CREATION_DT ,DAY_ID ,COLOR ,LOCATION)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getCIRCUIT_NAME()#" maxLength="255" null="#iif((arguments.bean.getCIRCUIT_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDAY_ID()#" maxLength="22" null="#iif((arguments.bean.getDAY_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getCOLOR()#" maxLength="7" null="#iif((arguments.bean.getCOLOR() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getLOCATION()#" maxLength="255" null="#iif((arguments.bean.getLOCATION() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschCircuitVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_CIRCUIT
			SET
				CIRCUIT_NAME = <cfqueryparam value="#arguments.bean.getCIRCUIT_NAME()#" cfsqltype="cf_sql_varchar" maxLength="255" null="#iif((arguments.bean.getCIRCUIT_NAME() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				DAY_ID = <cfqueryparam value="#arguments.bean.getDAY_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDAY_ID() eq ''),de("true"), de("false"))#" />,
				COLOR = <cfqueryparam value="#arguments.bean.getCOLOR()#" cfsqltype="cf_sql_varchar" maxLength="7" null="#iif((arguments.bean.getCOLOR() eq ''),de("true"), de("false"))#" />,
				LOCATION = <cfqueryparam value="#arguments.bean.getLOCATION()#" cfsqltype="cf_sql_varchar" maxLength="255" null="#iif((arguments.bean.getLOCATION() eq ''),de("true"), de("false"))#" /> 
			WHERE
				CIRCUIT_ID = <cfqueryparam value="#arguments.bean.getCIRCUIT_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_CIRCUIT
			WHERE 
				CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.circuitID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>