<cfcomponent name="IschInterviewerVO" displayname="IschInterviewerVO" extends="getSetObjectValue" output="false">
	<cfproperty name="INTERVIEWER_ID" type="numeric" />
	<cfproperty name="FIRST_NAME" type="string" />
	<cfproperty name="LAST_NAME" type="string" />
	<cfproperty name="EMAIL" type="string" />
	<cfproperty name="CPSO" type="string" />
	<cfproperty name="STUDENT_ID" type="numeric" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="numeric" />
	<cfproperty name="TYPE" type="string" />
	<cfproperty name="REGISTRANT_ID" type="numeric" />
	<cfproperty name="DEPARTMENT" type="string" />	
		
	<cffunction name="init" access="public" returntype="IschInterviewerVO" output="false" displayname="init" hint="I initialize a IschInterviewer">
		<cfset variables.INTERVIEWER_ID = "" />
		<cfset variables.FIRST_NAME = "" />
		<cfset variables.LAST_NAME = "" />
		<cfset variables.EMAIL = "" />
		<cfset variables.CPSO = "" />
		<cfset variables.STUDENT_ID = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		<cfset variables.TYPE = "" />
		<cfset variables.REGISTRANT_ID = "" />
		<cfset variables.DEPARTMENT = "" />		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setINTERVIEWER_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.INTERVIEWER_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getINTERVIEWER_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEWER_ID />
	</cffunction>
	<cffunction name="setFIRST_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.FIRST_NAME = arguments.val />
	</cffunction>
	<cffunction name="getFIRST_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.FIRST_NAME />
	</cffunction>
	<cffunction name="setLAST_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LAST_NAME = arguments.val />
	</cffunction>
	<cffunction name="getLAST_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.LAST_NAME />
	</cffunction>
	<cffunction name="setEMAIL" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.EMAIL = arguments.val />
	</cffunction>
	<cffunction name="getEMAIL" access="public" returntype="any" output="false">
		<cfreturn variables.EMAIL />
	</cffunction>
	<cffunction name="setCPSO" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CPSO = arguments.val />
	</cffunction>
	<cffunction name="getCPSO" access="public" returntype="any" output="false">
		<cfreturn variables.CPSO />
	</cffunction>
	<cffunction name="setSTUDENT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.STUDENT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSTUDENT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.STUDENT_ID />
	</cffunction>
	
	<cffunction name="setREGISTRANT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.REGISTRANT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getREGISTRANT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.REGISTRANT_ID />
	</cffunction>

	<cffunction name="setDEPARTMENT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DEPARTMENT = val/>
	</cffunction>
	<cffunction name="getDEPARTMENT" access="public" returntype="any" output="false">
		<cfreturn variables.DEPARTMENT />
	</cffunction>
		
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.INTERVIEW_SCHEDULE_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>
	<cffunction name="setTYPE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.TYPE = arguments.val />
	</cffunction>
	<cffunction name="getTYPE" access="public" returntype="any" output="false">
		<cfreturn variables.TYPE />
	</cffunction>
</cfcomponent>