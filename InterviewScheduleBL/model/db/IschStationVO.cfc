<cfcomponent name="IschStationVO" displayname="IschStationVO" extends="getSetObjectValue" output="false">
	<cfproperty name="STATION_ID" type="numeric" />
	<cfproperty name="ROOM" type="string" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />
	<cfproperty name="CIRCUIT_ID" type="numeric" />
	<cfproperty name="INTERVIEWER_ID" type="numeric" />
	<cfproperty name="COMPETENCY_ID" type="numeric" />
	<cfproperty name="ORDER_NUMBER" type="numeric" />
	<cfproperty name="QUESTION_ID" type="string" />
		
	<cffunction name="init" access="public" returntype="IschStationVO" output="false" displayname="init" hint="I initialize a IschStation">
		<cfset variables.STATION_ID = "" />
		<cfset variables.ROOM = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.INTERVIEWER_ID = "" />
		<cfset variables.COMPETENCY_ID = "" />
		<cfset variables.ORDER_NUMBER = "" />
		<cfset variables.QUESTION_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setSTATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.STATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSTATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.STATION_ID />
	</cffunction>
	<cffunction name="setROOM" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ROOM = arguments.val />
	</cffunction>
	<cffunction name="getROOM" access="public" returntype="any" output="false">
		<cfreturn variables.ROOM />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CIRCUIT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>
	<cffunction name="setINTERVIEWER_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.INTERVIEWER_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getINTERVIEWER_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEWER_ID />
	</cffunction>
	<cffunction name="setCOMPETENCY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.COMPETENCY_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCOMPETENCY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_ID />
	</cffunction>
	<cffunction name="setORDER_NUMBER" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.ORDER_NUMBER = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getORDER_NUMBER" access="public" returntype="any" output="false">
		<cfreturn variables.ORDER_NUMBER />
	</cffunction>
	<cffunction name="setQUESTION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.QUESTION_ID = arguments.val />
	</cffunction>
	<cffunction name="getQUESTION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_ID />
	</cffunction>
</cfcomponent>