<cfcomponent name="IschInterviewScheduleDAO" displayname="IschInterviewScheduleDAO" hint="I abstract data access for ISCH_INTERVIEW_SCHEDULE" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschInterviewScheduleDAO" output="false" displayname="init" hint="I initialize a IschInterviewScheduleDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschInterviewScheduleVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		
		<cfset var IschInterviewScheduleVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEW_SCHEDULE_ID,
				INTERVIEW_SCHEDULE_NAME,
				SESSION_ID,
				BREAK_BETWEEN_STATIONS,
				SCORE_WEIGHT
			FROM ISCH_INTERVIEW_SCHEDULE
			WHERE		
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#"/> 
		</cfquery>
		<cfset IschInterviewScheduleVO = CreateObject("component","IschInterviewScheduleVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschInterviewScheduleVO.setINTERVIEW_SCHEDULE_ID(getQry.INTERVIEW_SCHEDULE_ID) />
			<cfset IschInterviewScheduleVO.setINTERVIEW_SCHEDULE_NAME(getQry.INTERVIEW_SCHEDULE_NAME) />
			<cfset IschInterviewScheduleVO.setSESSION_ID(getQry.SESSION_ID) />
			<cfset IschInterviewScheduleVO.setBREAK_BETWEEN_STATIONS(getQry.BREAK_BETWEEN_STATIONS) />
			<cfset IschInterviewScheduleVO.setSCORE_WEIGHT(getQry.SCORE_WEIGHT) />
		</cfif>
		
		<cfreturn IschInterviewScheduleVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschInterviewScheduleVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_INTERVIEW_SCHEDULE_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_INTERVIEW_SCHEDULE (
				INTERVIEW_SCHEDULE_ID ,INTERVIEW_SCHEDULE_NAME ,SESSION_ID ,BREAK_BETWEEN_STATIONS)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getINTERVIEW_SCHEDULE_NAME()#" maxLength="100" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSESSION_ID()#" maxLength="22" null="#iif((arguments.bean.getSESSION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getBREAK_BETWEEN_STATIONS()#" maxLength="22" null="#iif((arguments.bean.getBREAK_BETWEEN_STATIONS() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschInterviewScheduleVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_INTERVIEW_SCHEDULE
			SET
				INTERVIEW_SCHEDULE_NAME = <cfqueryparam value="#arguments.bean.getINTERVIEW_SCHEDULE_NAME()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_NAME() eq ''),de("true"), de("false"))#" />,
				SESSION_ID = <cfqueryparam value="#arguments.bean.getSESSION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSESSION_ID() eq ''),de("true"), de("false"))#" />,
				BREAK_BETWEEN_STATIONS = <cfqueryparam value="#arguments.bean.getBREAK_BETWEEN_STATIONS()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getBREAK_BETWEEN_STATIONS() eq ''),de("true"), de("false"))#" />,
				SCORE_WEIGHT = <cfqueryparam value="#arguments.bean.getSCORE_WEIGHT()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSCORE_WEIGHT() eq ''),de("true"), de("false"))#" /> 
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_INTERVIEW_SCHEDULE
			WHERE 
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.interviewScheduleID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>