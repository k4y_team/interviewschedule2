<cfcomponent name="IschInterviewerAvailabilityDAO" displayname="IschInterviewerAvailabilityDAO" hint="I abstract data access for ISCH_INTERVIEWER_AVAILABILITY" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschInterviewerAvailabilityDAO" output="false" displayname="init" hint="I initialize a IschInterviewerAvailabilityDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschInterviewerAvailabilityVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfargument name="date" type="date" required="true" />
		
		<cfset var IschInterviewerAvailabilityVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEWER_ID,
				DAY_DT
			FROM ISCH_INTERVIEWER_AVAILABILITY
			WHERE		
				INTERVIEWER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewerID#"/>  AND
				DAY_DT = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.date#"/> 
		</cfquery>
		<cfset IschInterviewerAvailabilityVO = CreateObject("component","IschInterviewerAvailabilityVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschInterviewerAvailabilityVO.setINTERVIEWER_ID(getQry.INTERVIEWER_ID) />
			<cfset IschInterviewerAvailabilityVO.setDAY_DT(getQry.DAY_DT) />
		</cfif>
		
		<cfreturn IschInterviewerAvailabilityVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschInterviewerAvailabilityVO" required="true" />
		
		<cfset var seqQry = "">	
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_INTERVIEWER_AVAILABILITY (
				INTERVIEWER_ID ,DAY_DT)
			VALUES (<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINTERVIEWER_ID()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getDAY_DT()#" maxLength="26" null="false" />)
		</cfquery>
		
		<cfreturn 0/>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfargument name="date" type="date" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_INTERVIEWER_AVAILABILITY
			WHERE 
				INTERVIEWER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.interviewerID#"/>
				AND DAY_DT = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.date#"/>
		</cfquery>
	</cffunction>
	
	<cffunction name="deleteByInterviewerID" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewerID" type="numeric" required="true" />

		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_INTERVIEWER_AVAILABILITY
			WHERE 
				INTERVIEWER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.interviewerID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>