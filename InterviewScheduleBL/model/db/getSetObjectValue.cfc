<cfcomponent name="getSetObjectValue" displayName="getSetObjectValue">		
	<cffunction name="init" access="public" output="true" returntype="getSetObjectValue">
		
		<cfset variables.formMsg = CreateObject("component","medsisOLD.Model.form.formMsg").init() />
		<cfset propertiesArray = GetMetaData(this).properties>
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">
			<cftry>
			<cfset PropStr =  "set#propertiesArray[i].name#('')">
				<cfset ExecuteProp = Evaluate(PropStr)>
			<cfcatch>
				<cfset This[propertiesArray[i].name] = "">
			</cfcatch>
			</cftry>
		</cfloop>
		<cfreturn this>
	</cffunction>

	<cffunction access="public" name="initFromForm" displayname="initFromForm" hint="" output="true" returntype="getSetObjectValue" roles="">
		<cfargument name="formStruct" type="struct" required="true">
		
		<cfset var field = "">
		<cfset propertiesArray = GetMetaData(This).properties>
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">	
			<cfif StructKeyExists(formStruct,propertiesArray[i].name)>
			    <cfset field = propertiesArray[i].name>	
			    <cfset PropStr = 'set#field#("#escapeString(formStruct[field])#")'>
			    <cfset Evaluate(PropStr)>
			<cfelseif propertiesArray[i].type EQ "Date"> 		
				<cfset yearField = propertiesArray[i].name & "Year">
				<cfset monthField = propertiesArray[i].name & "Month">
				<cfset dayField = propertiesArray[i].name & "Day">
				<cfset FormDate = "">
				<cfif StructKeyExists(formStruct,yearField) and StructKeyExists(formStruct,monthField) and StructKeyExists(formStruct,dayField)>
					<cftry>
						<cfset FormDate = CreateDate(formStruct[yearField], formStruct[monthField], formStruct[dayField])>
					<cfcatch>
						<!---do nothing --->
					</cfcatch>	
					</cftry>	
				</cfif>	
			    <cfset PropStr =  "set#propertiesArray[i].name#(FormDate)">	
		    	<cfset Evaluate(PropStr)>
			</cfif>
		</cfloop>
    	<cfreturn this>
	</cffunction>

	<cffunction access="public" name="initFromGrid" displayname="initFromGrid" hint="" output="false" returntype="getSetObjectValue" roles="">
		<cfargument name="gridStruct" type="struct" required="true">
		<cfargument name="index" type="numeric" required="true">
		<cfset propertiesArray = GetMetaData(This).properties>
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">
			<cfset fieldName = propertiesArray[i].name & "#index#">	
			<cfif StructKeyExists(gridStruct,fieldName)>
				<cfset grid = gridStruct[fieldName]>
			    <cfset PropStr =  'set#propertiesArray[i].name#("#escapeString(grid)#")'>	
			    <cfset Evaluate(PropStr)>
			</cfif>
		</cfloop>
		<cfreturn this>
	</cffunction>

	<cffunction access="public" name="initFromQuery" displayname="initFromQuery" hint="" output="false" returntype="getSetObjectValue" roles="">
		<cfargument name="queryData" type="query" required="true">
		<cfset propertiesArray = GetMetaData(This).properties>
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">
			<cfset keyColumn = propertiesArray[i].name>
			<cfif isDefined("queryData.#keyColumn#")>
				<cfset row = queryData.currentRow>
				<cfset qryData = Evaluate("queryData[keyColumn][row]")>
				<cfset PropStr =  'set#keyColumn#("#escapeString(qryData)#")'>
				<cfset Evaluate(PropStr)>
			</cfif>
		</cfloop>
		<cfreturn this>
	</cffunction>

	<cffunction access="private" name="escapeString" displayname="escapeString" hint="" output="true" returntype="string" roles="">
		<cfargument name="formFieldValue" type="string" hint="" required="true">
		
		<cfset formFieldValue = Trim(arguments.formFieldValue)>
		<cfset formFieldValue = Replace(formFieldValue,"""","""""",'ALL')>
		<cfset formFieldValue = Replace(formFieldValue,'##','####','ALL')>
		
		<cfreturn formFieldValue>
	</cffunction>
	
	<cffunction name="clone" access="public" displayname="clone">
		<cfargument name="source" type="getSetObjectValue">
		<cfset propertiesArray = GetMetaData(This).properties>
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">
			<!---<cfset SourcePropStr = Evaluate("source.get#propertiesArray[i].name#()")>--->
			<cfset SourcePropStr = "source.get#propertiesArray[i].name#()">
			<cfset PropStr =  "set#propertiesArray[i].name#(#SourcePropStr#)">						
			<cfset Evaluate(PropStr)>
		</cfloop>
     <cfreturn This>
	</cffunction>
	
	<cffunction name="getHTMLCode" access="public" displayname="clone">
		<cfset var HTMLcode = "">
		<cfset propertiesArray = GetMetaData(This).properties>
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">
			<cfset ValuePropStr = Evaluate("get#propertiesArray[i].name#()")>
			<cfset HTMLcode = HTMLcode & "<input type='hidden' name='" & propertiesArray[i].name & "' id='" & propertiesArray[i].name & "' value='" & ValuePropStr &"'>">
		</cfloop>
	    <cfreturn HTMLcode>
	</cffunction>	

	<cffunction name="getVoValue" access="public" returntype="struct">
		<cfset propValStruct = StructNew()>
		<cfset propertiesArray = GetMetaData(This).properties>
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">
			<cfset propVal = Evaluate("get#propertiesArray[i].name#()")>
			<cfset StructInsert(propValStruct,"#propertiesArray[i].name#","#propVal#")>
		</cfloop>
		<cfreturn propValStruct>
	</cffunction>

	<cffunction name="getFormMsg" access="public" returntype="medsisOLD.Model.form.formMsg" output="false">

		<cfreturn variables.formMsg>
	</cffunction>

	<cffunction name="setFormMsg" access="public" returntype="void" output="false">
		<cfargument name="formMsg" type="formMsg" required="true">
		<cfset variables.formMsg = arguments.formMsg>
	</cffunction>
	
	<cffunction name="getErrorMsg" access="public" returntype="array">
		<cfreturn getFormMsg().getMessages()>
	</cffunction>
	
	<cffunction name="toStruct" access="public" returntype="struct">
		<cfset var propValStruct = StructNew()>
		<cfset var propertiesArray = GetMetaData(This).properties>
		<cfset var propVal = "">
		<cfloop from="1" to="#ArrayLen(propertiesArray)#" index="i">
			<cfset propVal = Evaluate("get#propertiesArray[i].name#()")>
			<cfset StructInsert(propValStruct,"#propertiesArray[i].name#","#propVal#")>
		</cfloop>
		<cfreturn propValStruct>
	</cffunction>
</cfcomponent>