<cfcomponent name="IschCircuitShiftsDAO" displayname="IschCircuitShiftsDAO" hint="I abstract data access for ISCH_CIRCUIT_SHIFTS" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschCircuitShiftsDAO" output="false" displayname="init" hint="I initialize a IschCircuitShiftsDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschCircuitShiftsVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfargument name="shiftID" type="numeric" required="true" />
		
		<cfset var IschCircuitShiftsVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				CIRCUIT_ID,
				SHIFT_ID
			FROM ISCH_CIRCUIT_SHIFTS
			WHERE		
				CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#"/>  AND
				SHIFT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.shiftID#"/> 
		</cfquery>
		<cfset IschCircuitShiftsVO = CreateObject("component","IschCircuitShiftsVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschCircuitShiftsVO.setCIRCUIT_ID(getQry.CIRCUIT_ID) />
			<cfset IschCircuitShiftsVO.setSHIFT_ID(getQry.SHIFT_ID) />
		</cfif>
		
		<cfreturn IschCircuitShiftsVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschCircuitShiftsVO" required="true" />
		
		<cfset var seqQry = "">	
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_CIRCUIT_SHIFTS (
				CIRCUIT_ID ,SHIFT_ID)
			VALUES (<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCIRCUIT_ID()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSHIFT_ID()#" maxLength="22" null="false" />)
		</cfquery>
		
		<cfreturn 0/>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfargument name="shiftID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_CIRCUIT_SHIFTS
			WHERE 
				CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.circuitID#"/>
				AND SHIFT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.shiftID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>