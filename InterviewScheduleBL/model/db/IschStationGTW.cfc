<cfcomponent name="IschStationGTW" displayname="IschStationGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschStationGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				STATION_ID,
				ROOM,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				CIRCUIT_ID,
				INTERVIEWER_ID,
				COMPETENCY_ID,
				QUESTION_ID,
				ORDER_NUMBER
			FROM ISCH_STATION
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="stationID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				STATION_ID,
				ROOM,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				CIRCUIT_ID,
				INTERVIEWER_ID,
				COMPETENCY_ID,
				QUESTION_ID,
				ORDER_NUMBER
			FROM ISCH_STATION
			WHERE
				STATION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.stationID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByCircuit" access="public" returntype="query" output="false" displayname="getByID" hint="">
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				STATION_ID,
				ROOM,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				CIRCUIT_ID,
				INTERVIEWER_ID,
				COMPETENCY_ID,
				QUESTION_ID,
				ORDER_NUMBER,
				(SELECT MAX(ORDER_NUMBER) FROM ISCH_STATION WHERE CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />) AS MAX_NUMBER
			FROM ISCH_STATION
			WHERE
				CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />
			ORDER BY ORDER_NUMBER
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByFPK" access="public" returntype="query" output="false" displayname="getByFPK" hint="">
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfargument name="compentencyID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				STATION_ID,
				ROOM,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				CIRCUIT_ID,
				INTERVIEWER_ID,
				COMPETENCY_ID,
				QUESTION_ID,
				ORDER_NUMBER
			FROM ISCH_STATION
			WHERE
				CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />
				AND COMPETENCY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.compentencyID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="checkIfRoomOccupied" access="public" returntype="query" output="false" displayname="checkIfOccupied" hint="">
		<cfargument name="dayID" type="numeric" required="true">
		<cfargument name="room" type="string" required="true">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				S.*,
		        C.CIRCUIT_NAME,
		        D.DAY_NAME,
		        D.DAY_DT
			FROM
				ISCH_DAY D,
				ISCH_CIRCUIT C,
				ISCH_STATION S
			WHERE
				C.DAY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.dayID#" />
				AND LOWER(S.ROOM) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#lCase(arguments.room)#" />
				AND C.CIRCUIT_ID = S.CIRCUIT_ID
				AND C.DAY_ID = D.DAY_ID
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
</cfcomponent>