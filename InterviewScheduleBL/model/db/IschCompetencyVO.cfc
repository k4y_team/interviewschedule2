<cfcomponent name="IschCompetencyVO" displayname="IschCompetencyVO" extends="getSetObjectValue" output="false">
	<cfproperty name="COMPETENCY_ID" type="numeric" />
	<cfproperty name="COMPETENCY_NAME" type="string" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="numeric" />
	<cfproperty name="DURATION_MINS" type="numeric" />
		
	<cffunction name="init" access="public" returntype="IschCompetencyVO" output="false" displayname="init" hint="I initialize a IschCompetency">
		<cfset variables.COMPETENCY_ID = "" />
		<cfset variables.COMPETENCY_NAME = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		<cfset variables.DURATION_MINS = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setCOMPETENCY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.COMPETENCY_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCOMPETENCY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_ID />
	</cffunction>
	<cffunction name="setCOMPETENCY_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.COMPETENCY_NAME = arguments.val />
	</cffunction>
	<cffunction name="getCOMPETENCY_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_NAME />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.INTERVIEW_SCHEDULE_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>
	<cffunction name="setDURATION_MINS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DURATION_MINS = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDURATION_MINS" access="public" returntype="any" output="false">
		<cfreturn variables.DURATION_MINS />
	</cffunction>
</cfcomponent>