<cfcomponent name="IschShiftVO" displayname="IschShiftVO" extends="getSetObjectValue" output="false">
	<cfproperty name="SHIFT_ID" type="numeric" />
	<cfproperty name="START_HOUR" type="numeric" />
	<cfproperty name="START_MINUTE" type="numeric" />
	<cfproperty name="END_HOUR" type="numeric" />
	<cfproperty name="END_MINUTE" type="numeric" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="numeric" />
		
	<cffunction name="init" access="public" returntype="IschShiftVO" output="false" displayname="init" hint="I initialize a IschShift">
		<cfset variables.SHIFT_ID = "" />
		<cfset variables.START_HOUR = "" />
		<cfset variables.START_MINUTE = "" />
		<cfset variables.END_HOUR = "" />
		<cfset variables.END_MINUTE = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setSHIFT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.SHIFT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSHIFT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.SHIFT_ID />
	</cffunction>
	<cffunction name="setSTART_HOUR" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.START_HOUR = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSTART_HOUR" access="public" returntype="any" output="false">
		<cfreturn variables.START_HOUR />
	</cffunction>
	<cffunction name="setSTART_MINUTE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.START_MINUTE = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSTART_MINUTE" access="public" returntype="any" output="false">
		<cfreturn variables.START_MINUTE />
	</cffunction>
	<cffunction name="setEND_HOUR" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
	<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.END_HOUR = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getEND_HOUR" access="public" returntype="any" output="false">
		<cfreturn variables.END_HOUR />
	</cffunction>
	<cffunction name="setEND_MINUTE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.END_MINUTE = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getEND_MINUTE" access="public" returntype="any" output="false">
		<cfreturn variables.END_MINUTE />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.INTERVIEW_SCHEDULE_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>
</cfcomponent>
