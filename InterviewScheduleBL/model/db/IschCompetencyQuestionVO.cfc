<cfcomponent name="IschCompetencyQuestionVO" displayname="IschCompetencyQuestionVO" extends="getSetObjectValue" output="false">
		<cfproperty name="QUESTION_ID" type="numeric" />
		<cfproperty name="COMPETENCY_ID" type="numeric" />
		<cfproperty name="QUESTION_NAME" type="string" />
		<cfproperty name="IS_REQUIRED" type="string" />
		<cfproperty name="IS_EDITABLE" type="string" />
		<cfproperty name="QUESTION_RANK" type="numeric" />
		<cfproperty name="QUESTION_TYPE_ID" type="numeric" />
		<cfproperty name="CREATION_DT" type="date" />
		<cfproperty name="CREATION_ID" type="numeric" />
		<cfproperty name="MODIFICATION_DT" type="date" />
		<cfproperty name="MODIFICATION_ID" type="numeric" />
		
	<cffunction name="init" access="public" returntype="IschCompetencyQuestionVO" output="false" displayname="init" hint="I initialize a IschCompetencyQuestion">
		<cfset variables.QUESTION_ID = "" />
		<cfset variables.COMPETENCY_ID = "" />
		<cfset variables.QUESTION_NAME = "" />
		<cfset variables.IS_REQUIRED = "" />
		<cfset variables.IS_EDITABLE = "" />
		<cfset variables.QUESTION_RANK = "" />
		<cfset variables.QUESTION_TYPE_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setQUESTION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.QUESTION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getQUESTION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_ID />
	</cffunction>
	<cffunction name="setCOMPETENCY_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.COMPETENCY_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCOMPETENCY_ID" access="public" returntype="any" output="false">
		<cfreturn variables.COMPETENCY_ID />
	</cffunction>
	<cffunction name="setQUESTION_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.QUESTION_NAME = arguments.val />
	</cffunction>
	<cffunction name="getQUESTION_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_NAME />
	</cffunction>
	<cffunction name="setIS_REQUIRED" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.IS_REQUIRED = arguments.val />
	</cffunction>
	<cffunction name="getIS_REQUIRED" access="public" returntype="any" output="false">
		<cfreturn variables.IS_REQUIRED />
	</cffunction>
	<cffunction name="setIS_EDITABLE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.IS_EDITABLE = arguments.val />
	</cffunction>
	<cffunction name="getIS_EDITABLE" access="public" returntype="any" output="false">
		<cfreturn variables.IS_EDITABLE />
	</cffunction>
	<cffunction name="setQUESTION_RANK" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.QUESTION_RANK = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getQUESTION_RANK" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_RANK />
	</cffunction>
	<cffunction name="setQUESTION_TYPE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.QUESTION_TYPE_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getQUESTION_TYPE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_TYPE_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
</cfcomponent>
