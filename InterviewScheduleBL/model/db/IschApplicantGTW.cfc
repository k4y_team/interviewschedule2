<cfcomponent name="IschApplicantGTW" displayname="IschApplicantGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschApplicantGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				APPLICANT_ID,
				CREATION_ID,
				CREATION_DT,
				STATION_ID,
				CIRCUIT_ID,
				SHIFT_ID
			FROM ISCH_APPLICANT
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				APPLICANT_ID,
				CREATION_ID,
				CREATION_DT,
				STATION_ID,
				CIRCUIT_ID,
				SHIFT_ID
			FROM ISCH_APPLICANT
			WHERE
				APPLICANT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getScheduledByFilter" access="public" returntype="query" output="false" displayname="getByFilter" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				A.*
			FROM
				ISCH_APPLICANT A,
				ISCH_CIRCUIT C,
				ISCH_DAY D
			WHERE
				<cfif arguments.filter.getFILTER_ITEM("circuitID") NEQ "">
					A.CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('circuitID')#"> AND
				</cfif>
				A.CIRCUIT_ID = C.CIRCUIT_ID
				<cfif arguments.filter.getFILTER_ITEM("dayID") NEQ "">
					AND C.DAY_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('dayID')#">
				</cfif>
				AND C.DAY_ID = D.DAY_ID
				AND D.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#">
		</cfquery>
			
		<cfreturn getQry>
	</cffunction>

	<cffunction name="getApplicants4ScoreCalculation" access="public" returntype="query" output="false" displayname="getApplicantsWithScores" hint="">
		<cfset var getQry = "">
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT
				APPLICANT_ID
			FROM
				ISCH_APPLICANT
		</cfquery>
			
		<cfreturn getQry>
	</cffunction>
	
	<cffunction name="getCompQuestScoresByApplicant" access="public" returntype="query" output="false" displayname="getCompQuestScoresByApplicant" hint="">
		<cfargument name="referenceID" type="numeric" required="true" />
		<cfargument name="sessionID" type="numeric" required="true" />
		<cfset var getQry = "">
		
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
			  AIN.REFERENCE_ID,
			  IC.CIRCUIT_NAME,
			  D.DAY_NAME,
			  CO.COMPETENCY_NAME,
			  CQ.QUESTION_NAME,
			  CQ.QUESTION_ID,
			  QS.SCORE_VALUE,
			  (SELECT 
			    SUM(B.SCORE_VALUE)
			  FROM
			    ISCH_COMPETENCY_QUESTION A,
			    ISCH_QUESTION_SCORE B,
			    ISCH_QUESTION_TYPE C
			  WHERE
			    A.QUESTION_ID = B.QUESTION_ID
			    AND A.COMPETENCY_ID = CO.COMPETENCY_ID
			    AND B.APPLICANT_ID = AIN.APPLICANT_ID
			    AND A.QUESTION_TYPE_ID = C.QUESTION_TYPE_ID
			    AND UPPER(C.QUESTION_TYPE_CODE) NOT IN ('TEXT','YES_NO')) AS COMPETENCY_SCORE,
			  I.FIRST_NAME,
			  I.LAST_NAME,
			  I.TYPE
			FROM 
			  ISCH_APPLICANTS_IN AIN,
			  ISCH_QUESTION_SCORE QS,
			  ISCH_COMPETENCY_QUESTION CQ,
			  ISCH_COMPETENCY CO,
			  ISCH_QUESTION_TYPE QT,
			  ISCH_APPLICANT IA,
			  ISCH_STATION S,
			  ISCH_STATION S2,
			  ISCH_INTERVIEWER I,
			  ISCH_CIRCUIT IC,
			  ISCH_DAY D
			WHERE
			  AIN.APPLICANT_ID = QS.APPLICANT_ID
			  AND QS.QUESTION_ID = CQ.QUESTION_ID
			  AND CQ.COMPETENCY_ID = CO.COMPETENCY_ID
			  AND CQ.QUESTION_TYPE_ID = QT.QUESTION_TYPE_ID
			  AND QT.QUESTION_TYPE_CODE <> 'TEXT'
			  AND AIN.APPLICANT_ID = IA.APPLICANT_ID
			  AND IA.STATION_ID = S.STATION_ID
			  AND S.CIRCUIT_ID = IC.CIRCUIT_ID
			  AND IC.DAY_ID = D.DAY_ID
			  AND IC.CIRCUIT_ID = S2.CIRCUIT_ID
			  AND CO.COMPETENCY_ID = S2.COMPETENCY_ID
			  AND S2.INTERVIEWER_ID = I.INTERVIEWER_ID(+)
			  AND AIN.REFERENCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.referenceID#"> 
			  AND AIN.SESSION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.sessionID#"> 
		</cfquery>
		<cfreturn getQry>
	</cffunction>
</cfcomponent>