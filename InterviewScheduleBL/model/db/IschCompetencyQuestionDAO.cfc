<cfcomponent name="IschCompetencyQuestionDAO" displayname="IschCompetencyQuestionDAO" hint="I abstract data access for ISCH_COMPETENCY_QUESTION" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschCompetencyQuestionDAO" output="false" displayname="init" hint="I initialize a IschCompetencyQuestionDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschCompetencyQuestionVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="questionID" type="numeric" required="true" />
		
		<cfset var IschCompetencyQuestionVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				QUESTION_ID,
				COMPETENCY_ID,
				QUESTION_NAME,
				IS_REQUIRED,
				IS_EDITABLE,
				QUESTION_RANK,
				QUESTION_TYPE_ID,
				CREATION_DT,
				CREATION_ID,
				MODIFICATION_DT,
				MODIFICATION_ID
			FROM ISCH_COMPETENCY_QUESTION
			WHERE		
				QUESTION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.questionID#"/> 
		</cfquery>
		<cfset IschCompetencyQuestionVO = CreateObject("component","IschCompetencyQuestionVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschCompetencyQuestionVO.setQUESTION_ID(getQry.QUESTION_ID) />
			<cfset IschCompetencyQuestionVO.setCOMPETENCY_ID(getQry.COMPETENCY_ID) />
			<cfset IschCompetencyQuestionVO.setQUESTION_NAME(getQry.QUESTION_NAME) />
			<cfset IschCompetencyQuestionVO.setIS_REQUIRED(getQry.IS_REQUIRED) />
			<cfset IschCompetencyQuestionVO.setIS_EDITABLE(getQry.IS_EDITABLE) />
			<cfset IschCompetencyQuestionVO.setQUESTION_RANK(getQry.QUESTION_RANK) />
			<cfset IschCompetencyQuestionVO.setQUESTION_TYPE_ID(getQry.QUESTION_TYPE_ID) />
			<cfset IschCompetencyQuestionVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschCompetencyQuestionVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschCompetencyQuestionVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschCompetencyQuestionVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
		</cfif>
		
		<cfreturn IschCompetencyQuestionVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschCompetencyQuestionVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_COMPETENCY_QUESTION_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_COMPETENCY_QUESTION (
				QUESTION_ID ,COMPETENCY_ID ,QUESTION_NAME ,IS_REQUIRED ,IS_EDITABLE ,QUESTION_RANK ,QUESTION_TYPE_ID ,CREATION_DT ,CREATION_ID)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCOMPETENCY_ID()#" maxLength="22" null="#iif((arguments.bean.getCOMPETENCY_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getQUESTION_NAME()#" maxLength="50" null="#iif((arguments.bean.getQUESTION_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getIS_REQUIRED()#" maxLength="1" null="#iif((arguments.bean.getIS_REQUIRED() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getIS_EDITABLE()#" maxLength="1" null="#iif((arguments.bean.getIS_EDITABLE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getQUESTION_RANK()#" maxLength="22" null="#iif((arguments.bean.getQUESTION_RANK() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getQUESTION_TYPE_ID()#" maxLength="22" null="#iif((arguments.bean.getQUESTION_TYPE_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschCompetencyQuestionVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_COMPETENCY_QUESTION
			SET
				COMPETENCY_ID = <cfqueryparam value="#arguments.bean.getCOMPETENCY_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getCOMPETENCY_ID() eq ''),de("true"), de("false"))#" />,
				QUESTION_NAME = <cfqueryparam value="#arguments.bean.getQUESTION_NAME()#" cfsqltype="cf_sql_varchar" maxLength="50" null="#iif((arguments.bean.getQUESTION_NAME() eq ''),de("true"), de("false"))#" />,
				IS_REQUIRED = <cfqueryparam value="#arguments.bean.getIS_REQUIRED()#" cfsqltype="cf_sql_varchar" maxLength="1" null="#iif((arguments.bean.getIS_REQUIRED() eq ''),de("true"), de("false"))#" />,
				IS_EDITABLE = <cfqueryparam value="#arguments.bean.getIS_EDITABLE()#" cfsqltype="cf_sql_varchar" maxLength="1" null="#iif((arguments.bean.getIS_EDITABLE() eq ''),de("true"), de("false"))#" />,
				QUESTION_RANK = <cfqueryparam value="#arguments.bean.getQUESTION_RANK()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getQUESTION_RANK() eq ''),de("true"), de("false"))#" />,
				QUESTION_TYPE_ID = <cfqueryparam value="#arguments.bean.getQUESTION_TYPE_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getQUESTION_TYPE_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" /> 
			WHERE
				QUESTION_ID = <cfqueryparam value="#arguments.bean.getQUESTION_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="questionID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_COMPETENCY_QUESTION
			WHERE 
				QUESTION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.questionID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>
