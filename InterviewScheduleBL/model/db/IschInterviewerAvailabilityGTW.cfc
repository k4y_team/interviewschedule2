<cfcomponent name="IschInterviewerAvailabilityGTW" displayname="IschInterviewerAvailabilityGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschInterviewerAvailabilityGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				INTERVIEWER_ID,
				DAY_DT
			FROM ISCH_INTERVIEWER_AVAILABILITY
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfargument name="date" type="date" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				INTERVIEWER_ID,
				DAY_DT
			FROM ISCH_INTERVIEWER_AVAILABILITY
			WHERE
				INTERVIEWER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewerID#" /> AND
				DAY_DT = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.date#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="deleteByInterviewerID" access="public" returntype="void" output="false" displayname="getByPK" hint="">
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfset var delQry = ""/>
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_INTERVIEWER_AVAILABILITY
			WHERE 
				INTERVIEWER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.interviewerID#"/>
		</cfquery>
	</cffunction>
	
	<cffunction name="getByInterviewerID" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				INTERVIEWER_ID,
				DAY_DT
			FROM ISCH_INTERVIEWER_AVAILABILITY
			WHERE
				INTERVIEWER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewerID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getAssignedDays" access="public" returntype="query">
		<cfargument name="interviewerID" type="numeric" required="true">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT 
				d.day_dt,
				d.day_id,
				d.day_name
			FROM
			  isch_day d,
			  isch_circuit c,
			  isch_station s
			WHERE
			  c.day_id = d.day_id AND
			  c.circuit_id = s.circuit_id AND
			  s.interviewer_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewerID#" /> AND
			  d.interview_schedule_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" />
			 ORDER BY d.day_dt
		</cfquery>
		<cfreturn getQry>
	</cffunction>
</cfcomponent>