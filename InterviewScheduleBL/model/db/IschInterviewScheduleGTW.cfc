<cfcomponent name="IschInterviewScheduleGTW" displayname="IschInterviewScheduleGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschInterviewScheduleGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEW_SCHEDULE_ID,
				INTERVIEW_SCHEDULE_NAME,
				SESSION_ID,
				BREAK_BETWEEN_STATIONS,
				SCORE_WEIGHT
			FROM ISCH_INTERVIEW_SCHEDULE
			ORDER BY SESSION_ID DESC
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEW_SCHEDULE_ID,
				INTERVIEW_SCHEDULE_NAME,
				SESSION_ID,
				BREAK_BETWEEN_STATIONS,
				SCORE_WEIGHT
			FROM ISCH_INTERVIEW_SCHEDULE
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getBySessionID" access="public" returntype="query" output="false" displayname="getByID" hint="">
		<cfargument name="sessionID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEW_SCHEDULE_ID,
				INTERVIEW_SCHEDULE_NAME,
				SESSION_ID,
				BREAK_BETWEEN_STATIONS,
				SCORE_WEIGHT
			FROM ISCH_INTERVIEW_SCHEDULE
			WHERE
				SESSION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.sessionID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getData4Search" access="public" returntype="query" output="false" displayname="getData4Search" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		<cfset term = Trim(arguments.filter.getFILTER_ITEM('term'))>
		<cfif FindNoCase('interviewer', term)>
			<cfset term = Trim(ReplaceNoCase(arguments.filter.getFILTER_ITEM('term'),'interviewer',''))>
		</cfif>
		<cfif FindNoCase('applicant', term)>
			<cfset term = Trim(ReplaceNoCase(arguments.filter.getFILTER_ITEM('term'),'applicant',''))>
		</cfif>
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT
				'I_' || I.INTERVIEWER_ID AS ID,
				'Interviewer ' || I.LAST_NAME || ', ' || I.FIRST_NAME AS NAME
			FROM
				ISCH_INTERVIEWER I
			WHERE
				<cfif arguments.filter.getFILTER_ITEM("interviewScheduleID") NEQ "">
					I.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#" />
				</cfif>
				AND (1 = 0
				<cfif term NEQ "">
					OR LOWER(I.FIRST_NAME || ' ' || I.LAST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(term)#%" /> OR
					LOWER(I.LAST_NAME || ', ' || I.FIRST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(term)#%" />   OR
					LOWER(I.LAST_NAME || ' ' || I.FIRST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(term)#%" />
				</cfif>)
			UNION
			SELECT DISTINCT
				'A_' || AI.APPLICANT_ID AS ID,
				'Applicant ' || AI.LAST_NAME || ', ' || AI.FIRST_NAME AS NAME
			FROM
				ISCH_APPLICANTS_IN AI,
				ISCH_INTERVIEW_SCHEDULE I
			WHERE
				AI.SESSION_ID = I.SESSION_ID
				<cfif arguments.filter.getFILTER_ITEM("interviewScheduleID") NEQ "">
					AND I.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#" />
				</cfif>
				AND (1 = 0
				<cfif term NEQ "">
					OR LOWER(AI.FIRST_NAME || ' ' || AI.LAST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(term)#%" /> OR
					LOWER(AI.LAST_NAME || ', ' || AI.FIRST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(term)#%" />   OR
					LOWER(AI.LAST_NAME || ' ' || AI.FIRST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(term)#%" />
				</cfif>)
			UNION
			SELECT DISTINCT
				'R_' || S.ROOM AS ID,
				'Room ' || S.ROOM AS NAME
			FROM
				ISCH_CIRCUIT C,
				ISCH_DAY D,
				ISCH_STATION S
			WHERE
				C.CIRCUIT_ID = S.CIRCUIT_ID AND
				C.DAY_ID = D.DAY_ID
				<cfif arguments.filter.getFILTER_ITEM("interviewScheduleID") NEQ "">
					AND D.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#" />
				</cfif>
				AND (1 = 0
				<cfif term NEQ "">
					OR LOWER(S.ROOM) like <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(term)#%" />
				</cfif>)
			ORDER BY NAME
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
</cfcomponent>