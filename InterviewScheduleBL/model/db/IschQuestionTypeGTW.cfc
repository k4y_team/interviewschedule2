<cfcomponent name="IschQuestionTypeGTW" displayname="IschQuestionTypeGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschQuestionTypeGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				QUESTION_TYPE_ID,
				QUESTION_TYPE_NAME,
				QUESTION_TYPE_CODE,
				CREATION_DT,
				CREATION_ID,
				MODIFICATION_DT,
				MODIFICATION_ID,
				RATING_ID
			FROM ISCH_QUESTION_TYPE
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="questionTypeID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				QUESTION_TYPE_ID,
				QUESTION_TYPE_NAME,
				QUESTION_TYPE_CODE,
				CREATION_DT,
				CREATION_ID,
				MODIFICATION_DT,
				MODIFICATION_ID,
				RATING_ID
			FROM ISCH_QUESTION_TYPE
			WHERE
				QUESTION_TYPE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.questionTypeID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
</cfcomponent>
