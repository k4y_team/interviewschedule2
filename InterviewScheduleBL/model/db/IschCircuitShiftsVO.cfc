<cfcomponent name="IschCircuitShiftsVO" displayname="IschCircuitShiftsVO" extends="getSetObjectValue" output="false">
	<cfproperty name="CIRCUIT_ID" type="numeric" />
	<cfproperty name="SHIFT_ID" type="numeric" />
		
	<cffunction name="init" access="public" returntype="IschCircuitShiftsVO" output="false" displayname="init" hint="I initialize a IschCircuitShifts">
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.SHIFT_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CIRCUIT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>
	<cffunction name="setSHIFT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.SHIFT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSHIFT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.SHIFT_ID />
	</cffunction>
</cfcomponent>