<cfcomponent name="ApplicantsGTW" displayname="ApplicantsGTW" output="false" hint="" extends="dbObject">
	
	<cffunction name="init" access="public" returntype="ApplicantsGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT *
			FROM  ISCH_APPLICANT_SCHEDULE_OUT
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByCircuitId" access="public" returntype="query" output="false" displayname="getByCircuitId" hint="">
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT *
			FROM  ISCH_APPLICANT_SCHEDULE_OUT
			WHERE CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />
			ORDER BY
				DAY_DT,CIRCUIT_NAME,SHIFT_START_HOUR,SHIFT_START_MINUTE,START_HOUR,START_MINUTE,ORDER_NUMBER
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByInterviewerId" access="public" returntype="query" output="false" displayname="getByInterviewerId" hint="">
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT *
			FROM  ISCH_APPLICANT_SCHEDULE_OUT
			WHERE INTERVIEWER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewerID#" />
			ORDER BY
				DAY_DT,CIRCUIT_NAME,SHIFT_START_HOUR,SHIFT_START_MINUTE,START_HOUR,START_MINUTE,ORDER_NUMBER
		</cfquery>
		<cfreturn getQry/>
	</cffunction>	
	
	<cffunction name="getByApplicantId" access="public" returntype="query" output="false" displayname="getByApplicantId" hint="">
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT *
			FROM  ISCH_APPLICANT_SCHEDULE_OUT
			WHERE APPLICANT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#" />
			ORDER BY
				SHIFT_START_HOUR,SHIFT_START_MINUTE,START_HOUR,START_MINUTE,ORDER_NUMBER
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getData4Export" access="public" returntype="query" output="false" displayname="getData4Export" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">

		<cfset applicant_ID = ''>
		<cfif FindNoCase('A_', Trim(arguments.filter.getFILTER_ITEM('search_string')))>
			<cfset applicant_ID = Trim(ReplaceNoCase(arguments.filter.getFILTER_ITEM('search_string'),'A_',''))>
		</cfif>
		<!--- <cfset interviewer_ID = ''>
		<cfif FindNoCase('I_', Trim(arguments.filter.getFILTER_ITEM('search_string')))>
			<cfset interviewer_ID = Trim(ReplaceNoCase(arguments.filter.getFILTER_ITEM('search_string'),'I_',''))>
		</cfif> --->
		<cfset room = ''>
		<cfif FindNoCase('R_', Trim(arguments.filter.getFILTER_ITEM('search_string')))>
			<cfset room = Trim(ReplaceNoCase(arguments.filter.getFILTER_ITEM('search_string'),'R_',''))>
		</cfif>

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT
				AI.REFERENCE_ID,
				AI.LAST_NAME,
				AI.FIRST_NAME,
				AI.EMAIL,
				D.DAY_DT,
				C.CIRCUIT_NAME,
				CASE
					WHEN SH.START_HOUR < 10 THEN 0 || SH.START_HOUR
					ELSE TO_CHAR(SH.START_HOUR)
				END
				|| ':' ||
				CASE
					WHEN SH.START_MINUTE < 10 THEN 0 || SH.START_MINUTE
					ELSE TO_CHAR(SH.START_MINUTE)
				END
				|| ' - ' ||
				CASE
					WHEN SH.END_HOUR < 10 THEN 0 || SH.END_HOUR
					ELSE TO_CHAR(SH.END_HOUR)
				END
				|| ':' ||
				CASE
					WHEN SH.END_MINUTE < 10 THEN 0 || SH.END_MINUTE
					ELSE TO_CHAR(SH.END_MINUTE)
				END AS SHIFT,
				ISCA.INTERVIEWER_FNAME || ' ' || ISCA.INTERVIEWER_LNAME as INTERVIEWER
			FROM 
				ISCH_APPLICANT A,
				ISCH_APPLICANTS_IN AI ,
				ISCH_CIRCUIT C ,
				ISCH_DAY D,
				ISCH_CIRCUIT_SHIFTS CSH,
				ISCH_SHIFT SH,
				(
				    SELECT O.APPLICANT_ID, O.SESSION_ID, O.REFERENCE_ID, O.INTERVIEWER_FNAME, O.INTERVIEWER_LNAME
				    FROM
				    (
				    SELECT O.APPLICANT_ID, O.SESSION_ID, O.REFERENCE_ID, MIN(O.start_hour*60+O.start_minute) AS START_TIME
				    FROM ISCH_APPLICANT_SCHEDULE_OUT O
				    GROUP BY O.APPLICANT_ID, O.SESSION_ID, O.REFERENCE_ID
				    ) X,
				    ISCH_APPLICANT_SCHEDULE_OUT O
				    WHERE
				      X.APPLICANT_ID = O.APPLICANT_ID
				      AND X.SESSION_ID = O.SESSION_ID
				      AND X.REFERENCE_ID = O.REFERENCE_ID
				      AND X.START_TIME = O.start_hour*60+O.start_minute
				      <cfif room NEQ ''>
					  	  AND O.ROOM = <cfqueryparam cfsqltype="cf_sql_varchar" value="#room#">
					  </cfif>
					  <cfif applicant_ID NEQ ''>
						  AND O.APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#applicant_ID#">
					  </cfif>
					  <cfif arguments.filter.getFILTER_ITEM('circuitID') NEQ ''>
						  AND O.CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('circuitID')#">
				      </cfif>
				      <cfif arguments.filter.getFILTER_ITEM('interviewScheduleID') NEQ ''>
						  AND O.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#">
					  </cfif>
				) ISCA
			WHERE 
				A.APPLICANT_ID        = AI.APPLICANT_ID
				AND A.CIRCUIT_ID            = C.CIRCUIT_ID
				AND C.DAY_ID                = D.DAY_ID
				AND C.CIRCUIT_ID            = CSH.CIRCUIT_ID
				AND A.SHIFT_ID              = SH.SHIFT_ID
				AND CSH.SHIFT_ID            = SH.SHIFT_ID
				AND ISCA.APPLICANT_ID       = A.APPLICANT_ID
				AND ISCA.SESSION_ID         = AI.SESSION_ID
				AND ISCA.REFERENCE_ID       = AI.REFERENCE_ID
				<!--- <cfif interviewer_ID NEQ ''>
					AND APO.INTERVIEWER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#interviewer_ID#">
				</cfif> --->
				<cfif room NEQ ''>
					AND C.ROOM = <cfqueryparam cfsqltype="cf_sql_varchar" value="#room#">
				</cfif>
				<cfif applicant_ID NEQ ''>
					AND AI.APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#applicant_ID#">
				</cfif>
				<cfif arguments.filter.getFILTER_ITEM('circuitID') NEQ ''>
					AND C.CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('circuitID')#">
				</cfif>
				<cfif arguments.filter.getFILTER_ITEM('dayID') NEQ ''>
					AND C.DAY_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('dayID')#">
				</cfif>
				<cfif arguments.filter.getFILTER_ITEM('interviewScheduleID') NEQ ''>
					AND D.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#">
				</cfif>
			ORDER BY LAST_NAME, FIRST_NAME, CIRCUIT_NAME
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByReferenceId" access="public" returntype="query" output="false" displayname="getByReferenceId" hint="">
		<cfargument name="interview_schedule_id" type="numeric" required="true" />
		<cfargument name="referenceID" type="numeric" required="true" />
		
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
				select AI.* 
				from ISCH_Applicants_in AI, 
		    				ISCH_Applicant A
				where 
		  			AI.SESSION_ID  in  (select  session_id from ISCH_interview_schedule where interview_schedule_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interview_schedule_id#" />)
		  			and A.Applicant_id = AI.applicant_id
		  			and AI.reference_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.referenceID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
</cfcomponent>