<cfcomponent name="IschCompetencyGTW" displayname="IschCompetencyGTW" output="false" hint="" extends="dbObject">

	<cffunction name="init" access="public" returntype="IschCompetencyGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />	

		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				COMPETENCY_ID,
				COMPETENCY_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				DURATION_MINS
			FROM ISCH_COMPETENCY
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="competencyID" type="numeric" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				COMPETENCY_ID,
				COMPETENCY_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				DURATION_MINS
			FROM ISCH_COMPETENCY
			WHERE
				COMPETENCY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByInterviewScheduleID" access="public" returntype="query" output="false" displayname="getByID" hint="">
		<cfargument name="InterviewScheduleID" type="numeric" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				A.COMPETENCY_ID,
				A.COMPETENCY_NAME,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
				A.INTERVIEW_SCHEDULE_ID,
				A.DURATION_MINS,
				COUNT(B.COMPETENCY_ID) AS QUESTIONS_NUMBER
			FROM ISCH_COMPETENCY A,
				 ISCH_COMPETENCY_QUESTION B
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.InterviewScheduleID#" />
				AND A.COMPETENCY_ID = B.COMPETENCY_ID(+)
			group by 
				A.COMPETENCY_ID,
				A.COMPETENCY_NAME,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
				A.INTERVIEW_SCHEDULE_ID,
				A.DURATION_MINS
			ORDER BY LOWER(COMPETENCY_NAME)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByName" access="public" returntype="query" output="false" displayname="getByName" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfargument name="competencyName" type="string" required="true" />
		
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				COMPETENCY_ID,
				COMPETENCY_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				DURATION_MINS
			FROM ISCH_COMPETENCY
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" />
				AND LOWER(COMPETENCY_NAME) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Trim(LCase(arguments.competencyName))#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getAvailableByCircuit" access="public" returntype="query" output="false" displayname="getByCircuitID" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		<cfargument name="circuitID" type="numeric" required="true">
		<cfargument name="competencyID" type="any" required="true">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				COMPETENCY_ID,
				COMPETENCY_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				DURATION_MINS
			FROM
				ISCH_COMPETENCY
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" />
				AND COMPETENCY_ID NOT IN (SELECT S.COMPETENCY_ID FROM ISCH_STATION S WHERE S.CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#"/>)
			<cfif isNumeric(arguments.competencyID)>
				UNION
				
				SELECT
					COMPETENCY_ID,
					COMPETENCY_NAME,
					CREATION_ID,
					CREATION_DT,
					MODIFICATION_ID,
					MODIFICATION_DT,
					INTERVIEW_SCHEDULE_ID,
					DURATION_MINS
				FROM
					ISCH_COMPETENCY
				WHERE
					COMPETENCY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#" />
			</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
</cfcomponent>