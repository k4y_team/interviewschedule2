<cfcomponent name="IschApplicantDAO" displayname="IschApplicantDAO" hint="I abstract data access for ISCH_APPLICANT" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschApplicantDAO" output="false" displayname="init" hint="I initialize a IschApplicantDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschApplicantVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="applicantID" type="numeric" required="true" />
		
		<cfset var IschApplicantVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				APPLICANT_ID,
				CREATION_ID,
				CREATION_DT,
				STATION_ID,
				CIRCUIT_ID,
				SHIFT_ID
			FROM ISCH_APPLICANT
			WHERE		
				APPLICANT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#"/> 
		</cfquery>
		<cfset IschApplicantVO = CreateObject("component","IschApplicantVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschApplicantVO.setAPPLICANT_ID(getQry.APPLICANT_ID) />
			<cfset IschApplicantVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschApplicantVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschApplicantVO.setSTATION_ID(getQry.STATION_ID) />
			<cfset IschApplicantVO.setCIRCUIT_ID(getQry.CIRCUIT_ID) />
			<cfset IschApplicantVO.setSHIFT_ID(getQry.SHIFT_ID) />
		</cfif>
		
		<cfreturn IschApplicantVO />
	</cffunction>

	<cffunction name="add" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschApplicantVO" required="true" />
		<cfset var addQry = "">
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_APPLICANT (
				APPLICANT_ID ,CREATION_ID ,CREATION_DT ,STATION_ID ,CIRCUIT_ID ,SHIFT_ID)
			VALUES (<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getAPPLICANT_ID()#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSTATION_ID()#" maxLength="22" null="#iif((arguments.bean.getSTATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCIRCUIT_ID()#" maxLength="22" null="#iif((arguments.bean.getCIRCUIT_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSHIFT_ID()#" maxLength="22" null="#iif((arguments.bean.getSHIFT_ID() eq ''),de("true"), de("false"))#" />)
		</cfquery>
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschApplicantVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_APPLICANT
			SET
				STATION_ID = <cfqueryparam value="#arguments.bean.getSTATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSTATION_ID() eq ''),de("true"), de("false"))#" />,
				CIRCUIT_ID = <cfqueryparam value="#arguments.bean.getCIRCUIT_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getCIRCUIT_ID() eq ''),de("true"), de("false"))#" />,
				SHIFT_ID = <cfqueryparam value="#arguments.bean.getSHIFT_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSHIFT_ID() eq ''),de("true"), de("false"))#" /> 
			WHERE
				APPLICANT_ID = <cfqueryparam value="#arguments.bean.getAPPLICANT_ID()#" cfsqltype="cf_sql_numeric" /> 
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="applicantID" type="numeric" required="true" />

		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_APPLICANT
			WHERE 
				APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.applicantID#"/>
		</cfquery>
	</cffunction>
	
	<cffunction name="deleteByCircuitAndShift" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfargument name="shiftID" type="numeric" required="true" />		
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_APPLICANT
			WHERE 
				CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.circuitID#"/> AND
				SHIFT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.shiftID#"/>				
		</cfquery>
	</cffunction>

	<cffunction name="deleteByStationID" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="stationID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_APPLICANT
			WHERE 
				STATION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stationID#"/>
		</cfquery>
	</cffunction>
	
	<cffunction name="deleteByStationAndShift" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="stationID" type="numeric" required="true" />
		<cfargument name="shiftID" type="numeric" required="true" />
				
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_APPLICANT
			WHERE 
				STATION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stationID#"/> AND
				SHIFT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.shiftID#"/>
		</cfquery>
	</cffunction>
		
	<cffunction name="updateShift" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfargument name="oldShiftID" type="numeric" required="true" />
		<cfargument name="newShiftID" type="numeric" required="true" />				
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE ISCH_APPLICANT
			SET
				SHIFT_ID = <cfqueryparam value="#arguments.newShiftID#" cfsqltype="cf_sql_numeric" maxLength="22"/>
			WHERE
				CIRCUIT_ID = <cfqueryparam value="#arguments.circuitID#" cfsqltype="cf_sql_numeric" maxLength="22" /> AND
				SHIFT_ID = <cfqueryparam value="#arguments.oldShiftID#" cfsqltype="cf_sql_numeric" maxLength="22" /> 
		</cfquery>
	</cffunction>

	<cffunction name="scheduleAll" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">

		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_APPLICANT 
				(APPLICANT_ID ,CREATION_ID ,CREATION_DT ,STATION_ID ,CIRCUIT_ID ,SHIFT_ID)
			WITH UNSCH_APPLICANTS AS (
				SELECT
					X.*,
					ROWNUM AS IDX
				FROM 
					(SELECT 
						AI.*
					FROM 
						ISCH_INTERVIEW_SCHEDULE I,
						ISCH_APPLICANTS_IN AI,
						ISCH_APPLICANT A
					WHERE
						I.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#">
						AND I.SESSION_ID = AI.SESSION_ID
						AND AI.IS_ELIGIBLE = 'Y'
						AND AI.APPLICANT_ID = A.APPLICANT_ID(+)
						AND A.APPLICANT_ID IS NULL
					ORDER BY
						AI.APPLICANT_ID) X),
			AVAILABLE_SPOTS AS (
				SELECT 
					Z.*,
					ROWNUM AS IDX
				FROM
					(SELECT 
						X.*
					FROM
						(SELECT
							C.CIRCUIT_ID,
							S.STATION_ID,
							CS.SHIFT_ID,
							D.DAY_DT,
							S.ORDER_NUMBER,
							SH.START_HOUR,
							SH.START_MINUTE,
							C.CIRCUIT_NAME
						FROM 
							ISCH_INTERVIEW_SCHEDULE ISCH,
							ISCH_DAY D,
							ISCH_CIRCUIT C,
							ISCH_STATION S,
							ISCH_CIRCUIT_SHIFTS CS,
							ISCH_SHIFT SH
						WHERE
							ISCH.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#">
							AND ISCH.INTERVIEW_SCHEDULE_ID = D.INTERVIEW_SCHEDULE_ID
							<cfif arguments.filter.getFILTER_ITEM("dayID") NEQ "">
								AND D.DAY_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('dayID')#">
							</cfif>
							AND D.DAY_ID = C.DAY_ID
							<cfif arguments.filter.getFILTER_ITEM("circuitID") NEQ "">
								AND C.CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('circuitID')#">
							</cfif>
							AND C.CIRCUIT_ID = S.CIRCUIT_ID
							AND C.CIRCUIT_ID = CS.CIRCUIT_ID
							AND CS.SHIFT_ID = SH.SHIFT_ID) X,
						ISCH_APPLICANT Y
					WHERE
						X.CIRCUIT_ID = Y.CIRCUIT_ID(+)
						AND X.STATION_ID = Y.STATION_ID(+)
						AND X.SHIFT_ID = Y.SHIFT_ID(+)
						AND Y.APPLICANT_ID IS NULL
					ORDER BY
						DAY_DT, CIRCUIT_NAME, START_HOUR, START_MINUTE, ORDER_NUMBER) Z)
			SELECT 
				X1.APPLICANT_ID,
				#session.user_id# AS CREATION_ID,
				SYSDATE AS CREATION_DT,
				X2.STATION_ID,
				X2.CIRCUIT_ID,
				X2.SHIFT_ID
			FROM
				UNSCH_APPLICANTS X1,
				AVAILABLE_SPOTS X2
			WHERE
				X1.IDX = X2.IDX
		</cfquery>	
	</cffunction>

	<cffunction name="calculateScore" access="public" returntype="void" output="false" displayname="calculateScore" hint="">
		<cfargument name="applicantID" type="numeric" required="true">
		
		<cfquery name="calculateScoreQry" datasource="#getDatasource()#">
			SELECT
			    REFERENCE_ID,SESSION_ID,
				DECODE(
					SIGN(SUM(IS_NULL)-TOTAL_ONAPP),
					-1,NULL,
					SUM(COMPETENCY_SCORE)
				) AS TOTAL
			FROM
				(SELECT
					TOTAL_APP_SCORE.APPLICANT_ID,
					TOTAL_APP_SCORE.COMPETENCY_ID,
					COMPETENCY_SCORE,
					REFERENCE_ID,SESSION_ID,
					NVL2(TOTAL_APP_SCORE.COMPETENCY_SCORE,1,0) AS IS_NULL,
					COUNT(*) OVER (PARTITION BY TOTAL_APP_SCORE.APPLICANT_ID) AS TOTAL_ONAPP
				FROM
					(SELECT
						DECODE(
							SIGN(SUM(IS_NULL)-TOTAL_ONCOMP),
							-1,NULL,  
							SUM(DATAQ.SCORE_VALUE)
						) AS COMPETENCY_SCORE,
						APPLICANT_ID,
						REFERENCE_ID,
						SESSION_ID,
						COMPETENCY_ID
					FROM
						(SELECT
							QUESTION.*,
							SCORE.SCORE_VALUE,
							NVL2(SCORE.SCORE_VALUE,1,0) AS IS_NULL,
							COUNT(*) OVER (PARTITION BY QUESTION.APPLICANT_ID, QUESTION.COMPETENCY_ID) AS TOTAL_ONCOMP
						FROM
							(SELECT
								IQS.APPLICANT_ID,
								ICQ.COMPETENCY_ID,
								ICQ.QUESTION_RANK,
								ICQ.QUESTION_ID,
								IQS.SCORE_VALUE
							FROM 
								ISCH_QUESTION_SCORE IQS,
								ISCH_COMPETENCY_QUESTION ICQ,
								ISCH_QUESTION_TYPE IQT
							WHERE 
								IQS.APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.applicantID#">
								AND IQS.QUESTION_ID = ICQ.QUESTION_ID
								AND ICQ.QUESTION_TYPE_ID = IQT.QUESTION_TYPE_ID
								AND IQT.RATING_ID IS NOT NULL
							ORDER BY ICQ.COMPETENCY_ID, ICQ.QUESTION_RANK) SCORE,
							(SELECT 
								IA.APPLICANT_ID,
								ICO.COMPETENCY_ID,
								ICQ.QUESTION_RANK,
								ICQ.QUESTION_ID,
								IAI.REFERENCE_ID,
								IAI.SESSION_ID
							FROM 
								ISCH_APPLICANT IA,
								ISCH_CIRCUIT ICI,
								ISCH_STATION IST,
								ISCH_COMPETENCY ICO,
								ISCH_COMPETENCY_QUESTION ICQ,
								ISCH_QUESTION_TYPE IQT,
								ISCH_APPLICANTS_IN IAI
							WHERE
								IA.APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.applicantID#">
								AND IA.CIRCUIT_ID = ICI.CIRCUIT_ID
								AND ICI.CIRCUIT_ID = IST.CIRCUIT_ID
								AND IST.COMPETENCY_ID = ICO.COMPETENCY_ID
								AND ICO.COMPETENCY_ID = ICQ.COMPETENCY_ID
								AND ICQ.QUESTION_TYPE_ID = IQT.QUESTION_TYPE_ID
								AND IQT.RATING_ID IS NOT NULL
								AND IA.APPLICANT_ID = IAI.APPLICANT_ID
							ORDER BY
								ICQ.COMPETENCY_ID, ICQ.QUESTION_RANK) QUESTION
						WHERE
							QUESTION.QUESTION_ID = SCORE.QUESTION_ID(+)
							AND QUESTION.APPLICANT_ID = SCORE.APPLICANT_ID(+)
						) DATAQ
					GROUP BY APPLICANT_ID,REFERENCE_ID,SESSION_ID,COMPETENCY_ID,TOTAL_ONCOMP) TOTAL_APP_SCORE
				)
			GROUP BY APPLICANT_ID,REFERENCE_ID,SESSION_ID,TOTAL_ONAPP				
		</cfquery>

		<cfquery name="update1Qry" datasource="#getDatasource()#" >
			UPDATE ISCH_APPLICANT
			SET 
				<cfif isNumeric(calculateScoreQry.TOTAL)>
					TOTAL_SCORE = #calculateScoreQry.TOTAL#
				<cfelse>
					TOTAL_SCORE = NULL	
				</cfif>
			WHERE APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.applicantID#">
		</cfquery>
		
		<cfif isNumeric(calculateScoreQry.TOTAL)>	
			<cfquery name="interviewScoreQry" datasource="#getDatasource()#" >
				SELECT DISTINCT TOTAL_SCORE
		        FROM ISCH_APPLICANT_SCORE_OUT
		        WHERE APPLICANT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.applicantID#">
			</cfquery>
		</cfif>	
		
		<cfquery name="update2Qry" datasource="#getDatasource()#">
			UPDATE OMSAS_SCORE
			SET
			   <cfif isNumeric(calculateScoreQry.TOTAL)>
				   INTERVIEW_SCORE = #interviewScoreQry.TOTAL_SCORE#,
				   TOTAL_SCORE2 = Round(#interviewScoreQry.TOTAL_SCORE# + TOTAL_SCORE*50/100,2)
			   <cfelse>
			   	   INTERVIEW_SCORE = NULL,
			   	   TOTAL_SCORE2 = Round(TOTAL_SCORE*50/100,2)
			   </cfif>	   
			WHERE 
			   REFERENCE_NO = <cfqueryparam cfsqltype="cf_sql_numeric" value="#calculateScoreQry.reference_id#"> AND
			   TRAINING_SESSION_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#calculateScoreQry.session_id#">
		</cfquery>
	</cffunction>

</cfcomponent>