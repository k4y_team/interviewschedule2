<cfcomponent name="IschCompetencyDAO" displayname="IschCompetencyDAO" hint="I abstract data access for ISCH_COMPETENCY" extends="dbObject">

	<cffunction name="init" access="public" returntype="IschCompetencyDAO" output="false" displayname="init" hint="I initialize a IschCompetencyDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschCompetencyVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="competencyID" type="numeric" required="true" />
		
		<cfset var IschCompetencyVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				COMPETENCY_ID,
				COMPETENCY_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				DURATION_MINS
			FROM ISCH_COMPETENCY
			WHERE		
				COMPETENCY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#"/> 
		</cfquery>

		<cfset IschCompetencyVO = CreateObject("component","IschCompetencyVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschCompetencyVO.setCOMPETENCY_ID(getQry.COMPETENCY_ID) />
			<cfset IschCompetencyVO.setCOMPETENCY_NAME(getQry.COMPETENCY_NAME) />
			<cfset IschCompetencyVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschCompetencyVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschCompetencyVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschCompetencyVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschCompetencyVO.setINTERVIEW_SCHEDULE_ID(getQry.INTERVIEW_SCHEDULE_ID) />
			<cfset IschCompetencyVO.setDURATION_MINS(getQry.DURATION_MINS) />
		</cfif>
		
		<cfreturn IschCompetencyVO />
	</cffunction>

	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschCompetencyVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_COMPETENCY_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_COMPETENCY (
				COMPETENCY_ID ,COMPETENCY_NAME ,CREATION_ID ,CREATION_DT ,INTERVIEW_SCHEDULE_ID ,DURATION_MINS)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getCOMPETENCY_NAME()#" maxLength="100" null="#iif((arguments.bean.getCOMPETENCY_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDURATION_MINS()#" maxLength="22" null="#iif((arguments.bean.getDURATION_MINS() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschCompetencyVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_COMPETENCY
			SET
				COMPETENCY_NAME = <cfqueryparam value="#arguments.bean.getCOMPETENCY_NAME()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getCOMPETENCY_NAME() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				INTERVIEW_SCHEDULE_ID = <cfqueryparam value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" />,
				DURATION_MINS = <cfqueryparam value="#arguments.bean.getDURATION_MINS()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDURATION_MINS() eq ''),de("true"), de("false"))#" /> 
			WHERE
				COMPETENCY_ID = <cfqueryparam value="#arguments.bean.getCOMPETENCY_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="competencyID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_COMPETENCY
			WHERE 
				COMPETENCY_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.competencyID#"/>
		</cfquery>
	</cffunction>

</cfcomponent>