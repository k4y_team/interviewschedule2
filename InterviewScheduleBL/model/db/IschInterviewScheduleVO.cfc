<cfcomponent name="IschInterviewScheduleVO" displayname="IschInterviewScheduleVO" extends="getSetObjectValue" output="false">
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="numeric" />
	<cfproperty name="INTERVIEW_SCHEDULE_NAME" type="string" />
	<cfproperty name="SESSION_ID" type="numeric" />
	<cfproperty name="BREAK_BETWEEN_STATIONS" type="numeric" />
	<cfproperty name="SCORE_WEIGHT" type="numeric" />
		
	<cffunction name="init" access="public" returntype="IschInterviewScheduleVO" output="false" displayname="init" hint="I initialize a IschInterviewSchedule">
		<cfset variables.INTERVIEW_SCHEDULE_ID = "" />
		<cfset variables.INTERVIEW_SCHEDULE_NAME = "" />
		<cfset variables.SESSION_ID = "" />
		<cfset variables.BREAK_BETWEEN_STATIONS = "" />
		<cfset variables.SCORE_WEIGHT = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.INTERVIEW_SCHEDULE_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>
	<cffunction name="setINTERVIEW_SCHEDULE_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEW_SCHEDULE_NAME = arguments.val />
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_NAME />
	</cffunction>
	<cffunction name="setSESSION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.SESSION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSESSION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.SESSION_ID />
	</cffunction>
	<cffunction name="setBREAK_BETWEEN_STATIONS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.BREAK_BETWEEN_STATIONS = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getBREAK_BETWEEN_STATIONS" access="public" returntype="any" output="false">
		<cfreturn variables.BREAK_BETWEEN_STATIONS />
	</cffunction>
	<cffunction name="setSCORE_WEIGHT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.SCORE_WEIGHT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSCORE_WEIGHT" access="public" returntype="any" output="false">
		<cfreturn variables.SCORE_WEIGHT />
	</cffunction>
</cfcomponent>