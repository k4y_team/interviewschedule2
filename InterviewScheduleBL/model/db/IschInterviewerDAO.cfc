<cfcomponent name="IschInterviewerDAO" displayname="IschInterviewerDAO" hint="I abstract data access for ISCH_INTERVIEWER" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschInterviewerDAO" output="false" displayname="init" hint="I initialize a IschInterviewerDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschInterviewerVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewerID" type="numeric" required="true" />
		
		<cfset var IschInterviewerVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEWER_ID,
				FIRST_NAME,
				LAST_NAME,
				EMAIL,
				CPSO,
				STUDENT_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				TYPE,
				REGISTRANT_ID,
				DEPARTMENT
			FROM ISCH_INTERVIEWER
			WHERE		
				INTERVIEWER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewerID#"/> 
		</cfquery>
		<cfset IschInterviewerVO = CreateObject("component","IschInterviewerVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschInterviewerVO.setINTERVIEWER_ID(getQry.INTERVIEWER_ID) />
			<cfset IschInterviewerVO.setFIRST_NAME(getQry.FIRST_NAME) />
			<cfset IschInterviewerVO.setLAST_NAME(getQry.LAST_NAME) />
			<cfset IschInterviewerVO.setEMAIL(getQry.EMAIL) />
			<cfset IschInterviewerVO.setCPSO(getQry.CPSO) />
			<cfset IschInterviewerVO.setSTUDENT_ID(getQry.STUDENT_ID) />
			<cfset IschInterviewerVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschInterviewerVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschInterviewerVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschInterviewerVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschInterviewerVO.setINTERVIEW_SCHEDULE_ID(getQry.INTERVIEW_SCHEDULE_ID) />
			<cfset IschInterviewerVO.setTYPE(getQry.TYPE) />
			<cfset IschInterviewerVO.setREGISTRANT_ID(getQry.REGISTRANT_ID) />
			<cfset IschInterviewerVO.setDEPARTMENT(getQry.DEPARTMENT) />
		</cfif>
		
		<cfreturn IschInterviewerVO />
	</cffunction>
	
	<cffunction name="readByEmail" returntype="IschInterviewerVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfargument name="emailAddress" type="string" required="true" />
		
		<cfset var IschInterviewerVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEWER_ID,
				FIRST_NAME,
				LAST_NAME,
				EMAIL,
				CPSO,
				STUDENT_ID,
				TYPE,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				TYPE,
				REGISTRANT_ID,
				DEPARTMENT
			FROM ISCH_INTERVIEWER
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.interviewScheduleID#"/> AND
				EMAIL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.emailAddress#"/> 
		</cfquery>
		<cfset IschInterviewerVO = CreateObject("component","IschInterviewerVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschInterviewerVO.setINTERVIEWER_ID(getQry.INTERVIEWER_ID) />
			<cfset IschInterviewerVO.setFIRST_NAME(getQry.FIRST_NAME) />
			<cfset IschInterviewerVO.setLAST_NAME(getQry.LAST_NAME) />
			<cfset IschInterviewerVO.setEMAIL(getQry.EMAIL) />
			<cfset IschInterviewerVO.setCPSO(getQry.CPSO) />
			<cfset IschInterviewerVO.setSTUDENT_ID(getQry.STUDENT_ID) />
			<cfset IschInterviewerVO.setTYPE(getQry.TYPE) />			
			<cfset IschInterviewerVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschInterviewerVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschInterviewerVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschInterviewerVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschInterviewerVO.setINTERVIEW_SCHEDULE_ID(getQry.INTERVIEW_SCHEDULE_ID) />
			<cfset IschInterviewerVO.setTYPE(getQry.TYPE) />
			<cfset IschInterviewerVO.setREGISTRANT_ID(getQry.REGISTRANT_ID) />
			<cfset IschInterviewerVO.setDEPARTMENT(getQry.DEPARTMENT) />
		</cfif>
		
		<cfreturn IschInterviewerVO />
	</cffunction>	
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschInterviewerVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_INTERVIEWER_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_INTERVIEWER (
				INTERVIEWER_ID ,FIRST_NAME ,LAST_NAME ,EMAIL ,CPSO ,STUDENT_ID ,CREATION_ID ,CREATION_DT ,INTERVIEW_SCHEDULE_ID ,TYPE, REGISTRANT_ID, DEPARTMENT)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getFIRST_NAME()#" maxLength="50" null="#iif((arguments.bean.getFIRST_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getLAST_NAME()#" maxLength="50" null="#iif((arguments.bean.getLAST_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getEMAIL()#" maxLength="100" null="#iif((arguments.bean.getEMAIL() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getCPSO()#" maxLength="50" null="#iif((arguments.bean.getCPSO() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSTUDENT_ID()#" maxLength="22" null="#iif((arguments.bean.getSTUDENT_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#UCase(arguments.bean.getTYPE())#" maxLength="25" null="#iif((arguments.bean.getTYPE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getREGISTRANT_ID()#" maxLength="22" null="#iif((arguments.bean.getREGISTRANT_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#UCase(arguments.bean.getDEPARTMENT())#" maxLength="100" null="#iif((arguments.bean.getDEPARTMENT() eq ''),de("true"), de("false"))#" />					)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschInterviewerVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_INTERVIEWER
			SET
				FIRST_NAME = <cfqueryparam value="#arguments.bean.getFIRST_NAME()#" cfsqltype="cf_sql_varchar" maxLength="50" null="#iif((arguments.bean.getFIRST_NAME() eq ''),de("true"), de("false"))#" />,
				LAST_NAME = <cfqueryparam value="#arguments.bean.getLAST_NAME()#" cfsqltype="cf_sql_varchar" maxLength="50" null="#iif((arguments.bean.getLAST_NAME() eq ''),de("true"), de("false"))#" />,
				EMAIL = <cfqueryparam value="#arguments.bean.getEMAIL()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getEMAIL() eq ''),de("true"), de("false"))#" />,
				CPSO = <cfqueryparam value="#arguments.bean.getCPSO()#" cfsqltype="cf_sql_varchar" maxLength="50" null="#iif((arguments.bean.getCPSO() eq ''),de("true"), de("false"))#" />,
				STUDENT_ID = <cfqueryparam value="#arguments.bean.getSTUDENT_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSTUDENT_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				INTERVIEW_SCHEDULE_ID = <cfqueryparam value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" />,
				TYPE = <cfqueryparam value="#UCase(arguments.bean.getTYPE())#" cfsqltype="cf_sql_varchar" maxLength="25" null="#iif((arguments.bean.getTYPE() eq ''),de("true"), de("false"))#" />,
				REGISTRANT_ID = <cfqueryparam value="#arguments.bean.getREGISTRANT_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getREGISTRANT_ID() eq ''),de("true"), de("false"))#" />,
				DEPARTMENT = <cfqueryparam value="#UCase(arguments.bean.getDEPARTMENT())#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getDEPARTMENT() eq ''),de("true"), de("false"))#" />
			WHERE
				INTERVIEWER_ID = <cfqueryparam value="#arguments.bean.getINTERVIEWER_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_INTERVIEWER
			WHERE 
				INTERVIEWER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.interviewerID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>