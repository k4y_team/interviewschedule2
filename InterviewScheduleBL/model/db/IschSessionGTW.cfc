<cfcomponent name="IschSessionGTW" displayname="IschSessionGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschSessionGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				SESSION_ID,
				SESSION_NAME,
				IS_CURRENT
			FROM ISCH_SESSION_IN
			ORDER BY SESSION_NAME DESC
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="sessionID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				SESSION_ID,
				SESSION_NAME,
				IS_CURRENT
			FROM ISCH_SESSION_IN
			WHERE
				SESSION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.sessionID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getCurrentSession" access="public" returntype="query" output="false" hint="Returns systems curent training session">
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT 
				SESSION_ID,
				SESSION_NAME,
				IS_CURRENT
			FROM ISCH_SESSION_IN
			WHERE IS_CURRENT = 'Y'
		</cfquery>
		<cfif getQry.recordCount EQ 0>
			<cfquery name="getQry" datasource="#getDatasource()#">
				SELECT *
				FROM
					(SELECT 
						SESSION_ID,
						SESSION_NAME,
						IS_CURRENT,
						ROWNUM AS MY_ROWNUM
					FROM ISCH_SESSION_IN
					ORDER BY SESSION_NAME DESC)
				WHERE MY_ROWNUM = 1
				ORDER BY SESSION_NAME
			</cfquery>
			SELECT * FROM ISCH_SESSION_IN WHERE ROWNUM = 1 ORDER BY SESSION_NAME
		</cfif>
		<cfreturn getQry>
	</cffunction>
</cfcomponent>