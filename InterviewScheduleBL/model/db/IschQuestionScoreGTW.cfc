<cfcomponent name="IschQuestionScoreGTW" displayname="IschQuestionScoreGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschQuestionScoreGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				QUESTION_ID,
				APPLICANT_ID,
				SCORE_VALUE,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				ANSWER_TEXT
			FROM ISCH_QUESTION_SCORE
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="questionID" type="numeric" required="true" />
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				QUESTION_ID,
				APPLICANT_ID,
				SCORE_VALUE,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				ANSWER_TEXT
			FROM ISCH_QUESTION_SCORE
			WHERE
				QUESTION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.questionID#" /> AND
				APPLICANT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getCompetencyScores" access="public" returntype="query" output="false" displayname="getCompetencyScores">
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfargument name="competencyID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				x.*,
			  	y.score_value,
			  	y.answer_text,
			  	aso.score_value as total_score,
			  	aso.applicant_id
			FROM
				(SELECT 
					q.question_id,
					q.question_name,
					q.is_editable,
					q.question_rank,
					qt.question_type_code,
					c.competency_name,
					q.competency_id
				FROM
					isch_competency_question q,
					isch_competency c,
					isch_question_type qt
				WHERE 
					q.competency_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#" />
					AND q.competency_id = c.competency_id
					AND q.question_type_id = qt.question_type_id) x,
				(SELECT 
					qs.answer_text,
					qs.question_id,
					qs.score_value,
					qs.applicant_id
				FROM
					isch_question_score qs
				WHERE 
				    qs.applicant_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#" />) y,
				    isch_applicant_score_out aso
				WHERE
					x.question_id = y.question_id(+)
					AND aso.competency_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.competencyID#" />
					AND aso.applicant_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#" />
				ORDER BY x.question_rank
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="deleteApplicantQuestions" access="public" returntype="void" output="false" displayname="getCompetencyScores">
		<cfargument name="applicantID" type="numeric" required="true" />
		<cfquery name="getQry" datasource="#getDatasource()#">
				DELETE FROM isch_question_score WHERE applicant_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.applicantID#" />
		</cfquery>
	</cffunction>
	
</cfcomponent>
