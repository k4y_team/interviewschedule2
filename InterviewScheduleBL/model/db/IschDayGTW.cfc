<cfcomponent name="IschDayGTW" displayname="IschDayGTW" output="false" hint="" extends="dbObject">

	<cffunction name="init" access="public" returntype="IschDayGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />	

		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				DAY_ID,
				DAY_NAME,
				DAY_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_DAY
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="dayID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				DAY_ID,
				DAY_NAME,
				DAY_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_DAY
			WHERE
				DAY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.dayID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByInterviewScheduleID" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				DAY_ID,
				DAY_NAME,
				DAY_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_DAY
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" />
			ORDER BY
				DAY_DT
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByDate" access="public" returntype="query" output="false" displayname="getByDate" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfargument name="dayDt" type="date" required="true" />
		
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				DAY_ID,
				DAY_NAME,
				DAY_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_DAY
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" /> AND
				TO_CHAR(DAY_DT,'yyyymmdd') = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#DateFormat(arguments.dayDt,'yyyymmdd')#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getByName" access="public" returntype="query" output="false" displayname="getByName" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfargument name="dayName" type="string" required="true" />
		
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				DAY_ID,
				DAY_NAME,
				DAY_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_DAY
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" /> AND
				LOWER(DAY_NAME) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Trim(LCase(arguments.dayName))#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>