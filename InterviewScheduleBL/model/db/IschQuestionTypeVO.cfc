<cfcomponent name="IschQuestionTypeVO" displayname="IschQuestionTypeVO" extends="getSetObjectValue" output="false">
		<cfproperty name="QUESTION_TYPE_ID" type="numeric" />
		<cfproperty name="QUESTION_TYPE_NAME" type="string" />
		<cfproperty name="QUESTION_TYPE_CODE" type="string" />
		<cfproperty name="CREATION_DT" type="date" />
		<cfproperty name="CREATION_ID" type="numeric" />
		<cfproperty name="MODIFICATION_DT" type="date" />
		<cfproperty name="MODIFICATION_ID" type="numeric" />
		<cfproperty name="RATING_ID" type="numeric" />
		
	<cffunction name="init" access="public" returntype="IschQuestionTypeVO" output="false" displayname="init" hint="I initialize a IschQuestionType">
		<cfset variables.QUESTION_TYPE_ID = "" />
		<cfset variables.QUESTION_TYPE_NAME = "" />
		<cfset variables.QUESTION_TYPE_CODE = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.RATING_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setQUESTION_TYPE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.QUESTION_TYPE_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getQUESTION_TYPE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_TYPE_ID />
	</cffunction>
	<cffunction name="setQUESTION_TYPE_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.QUESTION_TYPE_NAME = arguments.val />
	</cffunction>
	<cffunction name="getQUESTION_TYPE_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_TYPE_NAME />
	</cffunction>
	<cffunction name="setQUESTION_TYPE_CODE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.QUESTION_TYPE_CODE = arguments.val />
	</cffunction>
	<cffunction name="getQUESTION_TYPE_CODE" access="public" returntype="any" output="false">
		<cfreturn variables.QUESTION_TYPE_CODE />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setRATING_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.RATING_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getRATING_ID" access="public" returntype="any" output="false">
		<cfreturn variables.RATING_ID />
	</cffunction>
</cfcomponent>
