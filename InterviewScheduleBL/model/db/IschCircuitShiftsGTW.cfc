<cfcomponent name="IschCircuitShiftsGTW" displayname="IschCircuitShiftsGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschCircuitShiftsGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				A.CIRCUIT_ID,
				A.SHIFT_ID,
				B.START_HOUR,
				B.START_MINUTE,
				B.END_HOUR,
				B.END_MINUTE
			FROM 
				ISCH_CIRCUIT_SHIFTS A,
				ISCH_SHIFT B
			WHERE
				A.SHIFT_ID = B.SHIFT_ID
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfargument name="shiftID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				A.CIRCUIT_ID,
				A.SHIFT_ID,
				B.START_HOUR,
				B.START_MINUTE,
				B.END_HOUR,
				B.END_MINUTE
			FROM 
				ISCH_CIRCUIT_SHIFTS A,
				ISCH_SHIFT B
			WHERE
				A.CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" /> AND
				A.SHIFT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.shiftID#" /> AND
				A.SHIFT_ID = B.SHIFT_ID
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
</cfcomponent>