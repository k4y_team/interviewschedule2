<cfcomponent name="IschApplicantsGTW" displayname="IschApplicantsGTW" output="false" hint="" extends="dbObject">

	<cffunction name="init" access="public" returntype="IschApplicantsGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />	

		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				APPLICANT_ID,
				SESSION_ID,
				FIRST_NAME,
				LAST_NAME
			FROM 
				ISCH_APPLICANTS_IN
			WHERE
				IS_ELIGIBLE = 'Y'
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
		
	<cffunction name="getByFilter" access="public" returntype="query" output="false" displayname="getByFilter" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT
				AI.APPLICANT_ID,
				AI.SESSION_ID,
				AI.FIRST_NAME,
				AI.LAST_NAME,
				AI.REFERENCE_ID
			FROM
				ISCH_INTERVIEW_SCHEDULE ISCH,
				ISCH_APPLICANTS_IN AI,
				ISCH_APPLICANT A
			WHERE
				AI.SESSION_ID = ISCH.SESSION_ID
				AND AI.IS_ELIGIBLE = 'Y'
				<cfif arguments.filter.getFILTER_ITEM("interviewScheduleID") NEQ "">
				AND ISCH.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#" />
				</cfif>
				<cfif arguments.filter.getFILTER_ITEM("term") NEQ "">
				AND (LOWER(AI.FIRST_NAME || ' ' || AI.LAST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(arguments.filter.getFILTER_ITEM('term'))#%" />
				OR LOWER(AI.LAST_NAME || ' ' || AI.FIRST_NAME) LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LCase(arguments.filter.getFILTER_ITEM('term'))#%" />)
				</cfif>
				AND AI.APPLICANT_ID = A.APPLICANT_ID (+)
				AND A.APPLICANT_ID IS NULL
			ORDER BY AI.LAST_NAME, AI.FIRST_NAME
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getApplicants" access="public" returntype="query" output="false">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfset var getQry = ""/>
		<cfset var tblPageObj = arguments.filter.getTABLE_PAGE()>
		
		<cfquery name="getCompetenciesQry" datasource="#getDatasource()#">
			select *
			from isch_competency
			where
				interview_schedule_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM("interviewScheduleID")#" />
		</cfquery>
	
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT *
			FROM
			(SELECT 
				x.*,
				rownum AS row_idx
			FROM
			(SELECT DISTINCT
				ai.applicant_id,
			  	ai.reference_id as reference_no,
			  	ai.first_name,
			  	ai.last_name,
			  	s.total_score,
		      	decode(a.applicant_id,null,'Not Scheduled','Scheduled') as status
		      	<cfloop query="getCompetenciesQry">
			      	,sum(decode(s.competency_id,#getCompetenciesQry.competency_id#,s.score_value,0)) as competency_#getCompetenciesQry.competency_id#_score
				</cfloop>
				,count(*) over() as total_rows,
				i.score_weight
			FROM
				isch_interview_schedule i,		
				isch_applicants_in ai,
				isch_applicant a,
				isch_applicant_score_out s
			WHERE
				i.interview_schedule_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM("interviewScheduleID")#" /> and		
		        i.session_id = ai.session_id and
		        ai.is_eligible = 'Y' and
				ai.applicant_id = a.applicant_id (+) and
				ai.applicant_id = s.applicant_id (+)
			<cfif arguments.filter.getFILTER_ITEM("applicantStatus") eq "not_scheduled">
				and a.applicant_id IS NULL
			<cfelseif arguments.filter.getFILTER_ITEM("applicantStatus") eq "scheduled">
				and a.applicant_id IS NOT NULL
			</cfif>
			GROUP BY
				ai.applicant_id,
				ai.reference_id,
			  	ai.first_name,
			  	ai.last_name,
			  	a.applicant_id,
			  	s.total_score,
			  	i.score_weight
			ORDER BY
				lower(last_name), lower(first_name)) X
			)
			WHERE
				row_idx >= 	<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#tblPageObj.getRECORDS_PER_PAGE()*(tblPageObj.getPAGE_NO()-1)+1#" /> and
				row_idx <= 	<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#tblPageObj.getRECORDS_PER_PAGE()*tblPageObj.getPAGE_NO()#" />
			ORDER BY
				lower(last_name), lower(first_name)				
		</cfquery>

		<cfreturn getQry>
	</cffunction>
	
	<cffunction name="getRemaining" access="public" returntype="query" output="false" displayname="getByInterviewScheduleID" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				I.INTERVIEW_SCHEDULE_ID,
				AI.*
			FROM 
				ISCH_INTERVIEW_SCHEDULE I,
				ISCH_APPLICANTS_IN AI,
				ISCH_APPLICANT A
			WHERE
				I.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.interviewScheduleID#"> AND
				I.SESSION_ID = AI.SESSION_ID AND
				AI.IS_ELIGIBLE = 'Y' AND
				AI.APPLICANT_ID = A.APPLICANT_ID (+) AND
				A.APPLICANT_ID IS NULL
			ORDER BY
				AI.APPLICANT_ID
		</cfquery>
		<cfreturn getQry>
	</cffunction>

	<cffunction name="getScores" access="public" returntype="query" output="false" displayname="getScoreByReferenceID" hint="optional CompetencyID">
		<cfargument name="referenceID" type="numeric" required="true">
		<cfargument name="interviewScheduleID" type="numeric" required="true">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				SCHOUT.APPLICANT_ID,
				SCHOUT.INTERVIEWER_LNAME,
				SCHOUT.INTERVIEWER_FNAME,
				SCHOUT.COMPETENCY_ID,
				SCHOUT.COMPETENCY_NAME,
				SCHOUT.ROOM,
				SCHOUT.CIRCUIT_NAME,
				SCHOUT.DAY_NAME,
				SCHOUT.DAY_DT,
				SCHOUT.START_HOUR,
				SCHOUT.START_MINUTE,
				SCHOUT.END_HOUR,
				SCHOUT.END_MINUTE,
				SCHOUT.SHIFT_START_HOUR,
				SCHOUT.SHIFT_START_MINUTE,
				SCHOUT.SHIFT_END_HOUR,
				SCHOUT.SHIFT_END_MINUTE,
				SCHOUT.LOCATION,
				SOUT.SCORE_VALUE,
				SOUT.ADMISSION_OFFER,
				SOUT.RED_FLAG,
				SOUT.TOTAL_SCORE
			FROM
				ISCH_APPLICANT_SCHEDULE_OUT SCHOUT,
				ISCH_APPLICANT_SCORE_OUT SOUT
			WHERE
				SCHOUT.REFERENCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.referenceID#">
				AND SCHOUT.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.interviewScheduleID#">
				AND SCHOUT.APPLICANT_ID = SOUT.APPLICANT_ID(+)
				AND SCHOUT.COMPETENCY_ID = SOUT.COMPETENCY_ID(+)
			ORDER BY
				SCHOUT.SHIFT_START_HOUR,
				SCHOUT.SHIFT_START_MINUTE,
				SCHOUT.START_HOUR,
				SCHOUT.START_MINUTE,
				SCHOUT.ORDER_NUMBER
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>