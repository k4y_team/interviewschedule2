<cfcomponent name="IschInterviewerAvailabilityVO" displayname="IschInterviewerAvailabilityVO" extends="getSetObjectValue" output="false">
	<cfproperty name="INTERVIEWER_ID" type="numeric" />
	<cfproperty name="DAY_DT" type="date" />
		
	<cffunction name="init" access="public" returntype="IschInterviewerAvailabilityVO" output="false" displayname="init" hint="I initialize a IschInterviewerAvailability">
		<cfset variables.INTERVIEWER_ID = "" />
		<cfset variables.DAY_DT = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setINTERVIEWER_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.INTERVIEWER_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getINTERVIEWER_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEWER_ID />
	</cffunction>
	<cffunction name="setDAY_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.DAY_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getDAY_DT" access="public" returntype="any" output="false">
		<cfreturn variables.DAY_DT />
	</cffunction>
</cfcomponent>