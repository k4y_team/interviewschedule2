<cfcomponent name="IschApplicantVO" displayname="IschApplicantVO" extends="getSetObjectValue" output="false">
	<cfproperty name="APPLICANT_ID" type="numeric" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="STATION_ID" type="numeric" />
	<cfproperty name="CIRCUIT_ID" type="numeric" />
	<cfproperty name="SHIFT_ID" type="numeric" />
		
	<cffunction name="init" access="public" returntype="IschApplicantVO" output="false" displayname="init" hint="I initialize a IschApplicant">
		<cfset variables.APPLICANT_ID = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.STATION_ID = "" />
		<cfset variables.CIRCUIT_ID = "" />
		<cfset variables.SHIFT_ID = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setAPPLICANT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.APPLICANT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getAPPLICANT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.APPLICANT_ID />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CREATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setSTATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.STATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSTATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.STATION_ID />
	</cffunction>
	<cffunction name="setCIRCUIT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.CIRCUIT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getCIRCUIT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CIRCUIT_ID />
	</cffunction>
	<cffunction name="setSHIFT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.SHIFT_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getSHIFT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.SHIFT_ID />
	</cffunction>
</cfcomponent>