<cfcomponent name="IschQuestionTypeDAO" displayname="IschQuestionTypeDAO" hint="I abstract data access for ISCH_QUESTION_TYPE" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschQuestionTypeDAO" output="false" displayname="init" hint="I initialize a IschQuestionTypeDAO">
		<cfargument name="datasource" type="string" required="true" />
	    <cfargument name="restrictions" type="medsis.Model.s4y.restrictions" required="true" />		
		<cfset super.init(arguments.datasource, arguments.restrictions)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="model.db.IschQuestionTypeVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="questionTypeID" type="numeric" required="true" />
		
		<cfset var IschQuestionTypeVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				QUESTION_TYPE_ID,
				QUESTION_TYPE_NAME,
				QUESTION_TYPE_CODE,
				CREATION_DT,
				CREATION_ID,
				MODIFICATION_DT,
				MODIFICATION_ID,
				RATING_ID
			FROM ISCH_QUESTION_TYPE
			WHERE		
				QUESTION_TYPE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.questionTypeID#"/> 
		</cfquery>
		<cfset IschQuestionTypeVO = CreateObject("component","model.db.IschQuestionTypeVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschQuestionTypeVO.setQUESTION_TYPE_ID(getQry.QUESTION_TYPE_ID) />
			<cfset IschQuestionTypeVO.setQUESTION_TYPE_NAME(getQry.QUESTION_TYPE_NAME) />
			<cfset IschQuestionTypeVO.setQUESTION_TYPE_CODE(getQry.QUESTION_TYPE_CODE) />
			<cfset IschQuestionTypeVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschQuestionTypeVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschQuestionTypeVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschQuestionTypeVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschQuestionTypeVO.setRATING_ID(getQry.RATING_ID) />
		</cfif>
		
		<cfreturn IschQuestionTypeVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="model.db.IschQuestionTypeVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_QUESTION_TYPE_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_QUESTION_TYPE (
				QUESTION_TYPE_ID ,QUESTION_TYPE_NAME ,QUESTION_TYPE_CODE ,CREATION_DT ,CREATION_ID ,RATING_ID)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getQUESTION_TYPE_NAME()#" maxLength="100" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getQUESTION_TYPE_CODE()#" maxLength="100" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getRATING_ID()#" maxLength="22" null="#iif((arguments.bean.getRATING_ID() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="model.db.IschQuestionTypeVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_QUESTION_TYPE
			SET
				QUESTION_TYPE_NAME = <cfqueryparam value="#arguments.bean.getQUESTION_TYPE_NAME()#" cfsqltype="cf_sql_varchar" maxLength="100" null="false" />,
				QUESTION_TYPE_CODE = <cfqueryparam value="#arguments.bean.getQUESTION_TYPE_CODE()#" cfsqltype="cf_sql_varchar" maxLength="100" null="false" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				RATING_ID = <cfqueryparam value="#arguments.bean.getRATING_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getRATING_ID() eq ''),de("true"), de("false"))#" /> 
			WHERE
				QUESTION_TYPE_ID = <cfqueryparam value="#arguments.bean.getQUESTION_TYPE_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="questionTypeID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_QUESTION_TYPE
			WHERE 
				QUESTION_TYPE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.questionTypeID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>
