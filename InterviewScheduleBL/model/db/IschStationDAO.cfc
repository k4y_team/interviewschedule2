<cfcomponent name="IschStationDAO" displayname="IschStationDAO" hint="I abstract data access for ISCH_STATION" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschStationDAO" output="false" displayname="init" hint="I initialize a IschStationDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschStationVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="stationID" type="numeric" required="true" />
		
		<cfset var IschStationVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				STATION_ID,
				ROOM,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				CIRCUIT_ID,
				INTERVIEWER_ID,
				COMPETENCY_ID,
				QUESTION_ID,
				ORDER_NUMBER
			FROM ISCH_STATION
			WHERE		
				STATION_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.stationID#"/> 
		</cfquery>
		<cfset IschStationVO = CreateObject("component","IschStationVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschStationVO.setSTATION_ID(getQry.STATION_ID) />
			<cfset IschStationVO.setROOM(getQry.ROOM) />
			<cfset IschStationVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschStationVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschStationVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset IschStationVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset IschStationVO.setCIRCUIT_ID(getQry.CIRCUIT_ID) />
			<cfset IschStationVO.setINTERVIEWER_ID(getQry.INTERVIEWER_ID) />
			<cfset IschStationVO.setCOMPETENCY_ID(getQry.COMPETENCY_ID) />
			<cfset IschStationVO.setQUESTION_ID(getQry.QUESTION_ID) />
			<cfset IschStationVO.setORDER_NUMBER(getQry.ORDER_NUMBER) />
		</cfif>
		
		<cfreturn IschStationVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschStationVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_STATION_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_STATION (
				STATION_ID ,ROOM ,CREATION_ID ,CREATION_DT ,CIRCUIT_ID ,INTERVIEWER_ID ,COMPETENCY_ID ,QUESTION_ID ,ORDER_NUMBER)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getROOM()#" maxLength="100" null="#iif((arguments.bean.getROOM() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCIRCUIT_ID()#" maxLength="22" null="#iif((arguments.bean.getCIRCUIT_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINTERVIEWER_ID()#" maxLength="22" null="#iif((arguments.bean.getINTERVIEWER_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCOMPETENCY_ID()#" maxLength="22" null="#iif((arguments.bean.getCOMPETENCY_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getQUESTION_ID()#" maxLength="25" null="#iif((arguments.bean.getQUESTION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getORDER_NUMBER()#" maxLength="22" null="#iif((arguments.bean.getORDER_NUMBER() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschStationVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_STATION
			SET
				ROOM = <cfqueryparam value="#arguments.bean.getROOM()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getROOM() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				CIRCUIT_ID = <cfqueryparam value="#arguments.bean.getCIRCUIT_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getCIRCUIT_ID() eq ''),de("true"), de("false"))#" />,
				INTERVIEWER_ID = <cfqueryparam value="#arguments.bean.getINTERVIEWER_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getINTERVIEWER_ID() eq ''),de("true"), de("false"))#" />,
				COMPETENCY_ID = <cfqueryparam value="#arguments.bean.getCOMPETENCY_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getCOMPETENCY_ID() eq ''),de("true"), de("false"))#" />,
				QUESTION_ID = <cfqueryparam value="#arguments.bean.getQUESTION_ID()#" cfsqltype="cf_sql_varchar" maxLength="25" null="#iif((arguments.bean.getQUESTION_ID() eq ''),de("true"), de("false"))#" />,
				ORDER_NUMBER = <cfqueryparam value="#arguments.bean.getORDER_NUMBER()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getORDER_NUMBER() eq ''),de("true"), de("false"))#" /> 
			WHERE
				STATION_ID = <cfqueryparam value="#arguments.bean.getSTATION_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="stationID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_STATION
			WHERE 
				STATION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stationID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>