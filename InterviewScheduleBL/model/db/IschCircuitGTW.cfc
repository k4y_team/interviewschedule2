<cfcomponent name="IschCircuitGTW" displayname="IschCircuitGTW" output="false" hint="" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschCircuitGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />	
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				CIRCUIT_ID,
				CIRCUIT_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				DAY_ID,
				COLOR,
				LOCATION
			FROM ISCH_CIRCUIT
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				CIRCUIT_ID,
				CIRCUIT_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				DAY_ID,
				COLOR,
				LOCATION
			FROM ISCH_CIRCUIT
			WHERE
				CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByShift" access="public" returntype="query" output="false" displayname="getByID" hint="">
		<cfargument name="shiftID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				C.CIRCUIT_ID,
				C.CIRCUIT_NAME,
				C.CREATION_ID,
				C.CREATION_DT,
				C.MODIFICATION_ID,
				C.MODIFICATION_DT,
				C.DAY_ID,
				C.COLOR,
				C.LOCATION
			FROM
				ISCH_CIRCUIT C,
				ISCH_CIRCUIT_SHIFTS CS
			WHERE
				C.CIRCUIT_ID = CS.CIRCUIT_ID
				AND CS.SHIFT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.shiftID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getDetails" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="circuitID" type="numeric" required="true">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				X.*,
				Y.APPLICANT_ID,
				Z.FIRST_NAME || ' ' || Z.LAST_NAME AS APPLICANT_NAME,
				Z.REFERENCE_ID
			FROM		
				(SELECT DISTINCT
					ISCH.INTERVIEW_SCHEDULE_ID,
					D.DAY_ID,
					D.DAY_NAME,
					CI.CIRCUIT_ID,
					CI.CIRCUIT_NAME,
					CI.COLOR,
					ST.STATION_ID,
					ST.ROOM,
					ST.ORDER_NUMBER,
					ST.QUESTION_ID,
					I.INTERVIEWER_ID,
					I.FIRST_NAME,
					I.LAST_NAME,
					CO.COMPETENCY_ID,
					CO.COMPETENCY_NAME,
					CO.DURATION_MINS,
					SH.SHIFT_ID,
					SH.START_HOUR,
					SH.START_MINUTE,
					SH.END_HOUR,
					SH.END_MINUTE
				FROM
					ISCH_INTERVIEW_SCHEDULE ISCH,
					ISCH_DAY D,
					ISCH_CIRCUIT CI,
					ISCH_STATION ST,
					ISCH_INTERVIEWER I,
					ISCH_COMPETENCY CO,
					ISCH_CIRCUIT_SHIFTS CS,
					ISCH_SHIFT SH
				WHERE
					ISCH.INTERVIEW_SCHEDULE_ID = D.INTERVIEW_SCHEDULE_ID
					AND D.DAY_ID = CI.DAY_ID
					AND CI.CIRCUIT_ID = ST.CIRCUIT_ID(+)
					AND ST.INTERVIEWER_ID = I.INTERVIEWER_ID(+)
					AND ST.COMPETENCY_ID = CO.COMPETENCY_ID(+)
					AND CI.CIRCUIT_ID = CS.CIRCUIT_ID(+)
					AND CS.SHIFT_ID = SH.SHIFT_ID(+)
					AND CI.CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />) X,
				ISCH_APPLICANT Y,
				ISCH_APPLICANTS_IN Z
			WHERE
				X.CIRCUIT_ID = Y.CIRCUIT_ID(+)
				AND X.STATION_ID = Y.STATION_ID(+)
				AND X.SHIFT_ID = Y.SHIFT_ID(+)
				AND Y.APPLICANT_ID = Z.APPLICANT_ID (+)	
			ORDER BY
				COMPETENCY_ID, START_HOUR, START_MINUTE, END_HOUR, END_MINUTE
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	
	<cffunction name="getCircuitByPK" access="public" returntype="query" output="false" displayname="getCircuitByPK" hint="">
		<cfargument name="circuitID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT
				D.DAY_ID,
				D.DAY_NAME,
				D.DAY_DT,
				C.CIRCUIT_ID,
				C.CIRCUIT_NAME,
				C.LOCATION,
				C.COLOR,
				I.INTERVIEWER_ID,
				S.STATION_ID,
				S.ROOM
			FROM
				ISCH_DAY D,
				ISCH_CIRCUIT C,
				ISCH_STATION S,
				ISCH_INTERVIEWER I
			WHERE
				D.DAY_ID = C.DAY_ID
				AND C.CIRCUIT_ID = S.CIRCUIT_ID (+)
				AND S.INTERVIEWER_ID = I.INTERVIEWER_ID (+)
				AND C.CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.circuitID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
			
	<cffunction name="getByFilter" access="public" returntype="query" output="false" displayname="getByFilter" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		<cfset var getQry = ""/>
		
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT 
				X.CIRCUIT_ID,
				X.COLOR,
				X.DAY_DT,
				X.DAY_NAME,
				X.CIRCUIT_NAME
			FROM
			(SELECT DISTINCT
				D.DAY_ID,
				D.DAY_NAME,
				D.DAY_DT,
				C.CIRCUIT_ID,
				C.CIRCUIT_NAME,
				C.LOCATION,
				C.COLOR,
				I.INTERVIEWER_ID,
				S.STATION_ID,
				S.ROOM
			FROM
				ISCH_DAY D,
				ISCH_CIRCUIT C,
				ISCH_STATION S,
				ISCH_INTERVIEWER I
			WHERE
				D.DAY_ID = C.DAY_ID
				AND C.CIRCUIT_ID = S.CIRCUIT_ID (+)
				AND S.INTERVIEWER_ID = I.INTERVIEWER_ID (+)
				<cfif arguments.filter.getFILTER_ITEM("interviewScheduleID") NEQ "">
				AND D.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#" />
				</cfif>
				<cfif arguments.filter.getFILTER_ITEM("dayID") NEQ "">
				AND D.DAY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('dayID')#" />
				</cfif>
				<cfif arguments.filter.getFILTER_ITEM("circuitID") NEQ "">
				AND C.CIRCUIT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('circuitID')#" />
				</cfif>
			) X,
				ISCH_APPLICANT Y,
				ISCH_APPLICANTS_IN Z
			WHERE
				X.CIRCUIT_ID = Y.CIRCUIT_ID (+) AND
				X.STATION_ID = Y.STATION_ID (+) AND
				Y.APPLICANT_ID = Z.APPLICANT_ID (+)
				<cfif Trim(arguments.filter.getFILTER_ITEM("search_string")) NEQ "">
				AND (
					'I_' || X.INTERVIEWER_ID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.filter.getFILTER_ITEM('search_string')#" /> OR
					'A_' || Y.APPLICANT_ID =  <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.filter.getFILTER_ITEM('search_string')#" /> OR
					'R_' || UPPER(X.ROOM) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#UCase(arguments.filter.getFILTER_ITEM('search_string'))#" />
				)	
				</cfif>
			ORDER BY X.DAY_DT, X.CIRCUIT_NAME
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getAvailableSpots" access="public" returntype="query" output="false" displayname="getAvailbleSpots" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">
		
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT 
				X.*
			FROM
				(SELECT
					C.CIRCUIT_ID,
					S.STATION_ID,
					CS.SHIFT_ID,
					D.DAY_DT,
					S.ORDER_NUMBER,
					SH.START_HOUR,
					SH.START_MINUTE,
					C.CIRCUIT_NAME,
					C.COLOR
				FROM 
					ISCH_INTERVIEW_SCHEDULE ISCH,
					ISCH_DAY D,
					ISCH_CIRCUIT C,
					ISCH_STATION S,
					ISCH_CIRCUIT_SHIFTS CS,
					ISCH_SHIFT SH
				WHERE
					ISCH.INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#">
					AND ISCH.INTERVIEW_SCHEDULE_ID = D.INTERVIEW_SCHEDULE_ID
					<cfif arguments.filter.getFILTER_ITEM("dayID") NEQ "">
						AND D.DAY_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('dayID')#">
					</cfif>
					AND D.DAY_ID = C.DAY_ID
					<cfif arguments.filter.getFILTER_ITEM("circuitID") NEQ "">
						AND C.CIRCUIT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filter.getFILTER_ITEM('circuitID')#">
					</cfif>
					AND C.CIRCUIT_ID = S.CIRCUIT_ID
					AND C.CIRCUIT_ID = CS.CIRCUIT_ID
					AND CS.SHIFT_ID = SH.SHIFT_ID) X,
				ISCH_APPLICANT Y
			WHERE
				X.CIRCUIT_ID = Y.CIRCUIT_ID (+) AND
				X.STATION_ID = Y.STATION_ID (+) AND
				X.SHIFT_ID = Y.SHIFT_ID (+) AND
				Y.APPLICANT_ID IS NULL
			ORDER BY
				DAY_DT,CIRCUIT_NAME,START_HOUR,START_MINUTE,ORDER_NUMBER
		</cfquery>
			
		<cfreturn getQry>
	</cffunction>
</cfcomponent>