<cfcomponent name="IschInterviewerGTW" displayname="IschInterviewerGTW" output="false" hint="" extends="dbObject">

	<cffunction name="init" access="public" returntype="IschInterviewerGTW" output="false" displayname="init" hint="constructor">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>

	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEWER_ID,
				FIRST_NAME,
				LAST_NAME,
				EMAIL,
				CPSO,
				STUDENT_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				TYPE
			FROM ISCH_INTERVIEWER
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="interviewerID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEWER_ID,
				FIRST_NAME,
				LAST_NAME,
				EMAIL,
				CPSO,
				STUDENT_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				TYPE
			FROM ISCH_INTERVIEWER
			WHERE
				INTERVIEWER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewerID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByInterviewScheduleID" access="public" returntype="query" output="false" displayname="getByInterviewScheduleID" hint="">
		<cfargument name="interviewScheduleID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				INTERVIEWER_ID,
				FIRST_NAME,
				LAST_NAME,
				EMAIL,
				CPSO,
				STUDENT_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INTERVIEW_SCHEDULE_ID,
				TYPE,
				DEPARTMENT,
				REGISTRANT_ID
			FROM ISCH_INTERVIEWER
			WHERE
				INTERVIEW_SCHEDULE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.interviewScheduleID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getAvailable" access="public" returntype="query" output="false" displayname="getAvailable" hint="">
		<cfargument name="dayID" type="numeric" required="true">
		<cfargument name="stationId" type="numeric" required="true">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT DISTINCT
				A.INTERVIEWER_ID,
				A.FIRST_NAME,
				A.LAST_NAME,
				A.TYPE
			FROM
				ISCH_INTERVIEWER A,
				ISCH_INTERVIEWER_AVAILABILITY B,
				ISCh_DAY C
			WHERE
				A.INTERVIEWER_ID = B.INTERVIEWER_ID
				AND B.DAY_DT = C.DAY_DT
				AND C.DAY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.dayID#" />
				AND C.INTERVIEW_SCHEDULE_ID = A.INTERVIEW_SCHEDULE_ID
				AND A.INTERVIEWER_ID NOT IN (
					SELECT
						X.INTERVIEWER_ID
					FROM
						ISCH_STATION X,
						ISCH_CIRCUIT Y
					WHERE
						X.INTERVIEWER_ID IS NOT NULL
						AND X.CIRCUIT_ID = Y.CIRCUIT_ID
						AND Y.DAY_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.dayID#" />
						<cfif arguments.stationId gt 0>
							AND X.STATION_ID != <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.stationID#" />
						</cfif>
				)
			ORDER BY
				LOWER(LAST_NAME), LOWER(FIRST_NAME)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByFilter" access="public" returntype="query" output="false" displayname="getByFilter" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="true">

		<cfset var getQry = ""/>
		<cfset var tblPageObj = arguments.filter.getTABLE_PAGE()>

		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT *
			FROM
			(SELECT
				x.*,
				rownum AS row_idx
			FROM
			(SELECT DISTINCT
				i.*,
     			decode(s.interviewer_id,null,'N','Y') AS status,
				count(*) over() as total_rows
			FROM
				isch_interviewer i,
				isch_station s
			WHERE
				1 = 1
				<cfif Trim(arguments.filter.getFILTER_ITEM("search_string")) NEQ "">
					AND (
						UPPER(I.FIRST_NAME) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="#UCase(arguments.filter.getFILTER_ITEM("search_string"))#%" /> OR
						UPPER(I.LAST_NAME) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="#UCase(arguments.filter.getFILTER_ITEM("search_string"))#%" />
					)
				</cfif>
				<cfif arguments.filter.getFILTER_ITEM("interviewScheduleID") neq "">
					and i.interview_schedule_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM('interviewScheduleID')#" />
				</cfif>
				AND s.interviewer_id(+) = i.interviewer_id
				<cfif arguments.filter.getFILTER_ITEM("interviewerStatus") eq "not_scheduled">
					and s.interviewer_id IS NULL
				<cfelseif arguments.filter.getFILTER_ITEM("interviewerStatus") eq "scheduled">
					and s.interviewer_id IS NOT NULL
				</cfif>

			) X
				ORDER BY
				<cfif arguments.filter.getSORT_OPTIONS().getField() neq "">
					#arguments.filter.getSORT_OPTIONS().getSortString_UC()# NULLS LAST
				<cfelse>
					lower(x.last_name), lower(x.first_name)
				</cfif>

			)
			WHERE
				row_idx >= 	<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#tblPageObj.getRECORDS_PER_PAGE()*(tblPageObj.getPAGE_NO()-1)+1#" /> and
				row_idx <= 	<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#tblPageObj.getRECORDS_PER_PAGE()*tblPageObj.getPAGE_NO()#" />
		</cfquery>

		<cfreturn getQry/>
	</cffunction>

</cfcomponent>