<cfcomponent name="IschShiftDAO" displayname="IschShiftDAO" hint="I abstract data access for ISCH_SHIFT" extends="dbObject">
	<cffunction name="init" access="public" returntype="IschShiftDAO" output="false" displayname="init" hint="I initialize a IschShiftDAO">
		<cfargument name="datasource" type="string" required="true" />
		<cfset super.init(arguments.datasource)>
		<cfreturn this />
	</cffunction>
	
	<cffunction name="read" returntype="IschShiftVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="shiftID" type="numeric" required="true" />
		
		<cfset var IschShiftVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				SHIFT_ID,
				START_HOUR,
				START_MINUTE,
				END_HOUR,
				END_MINUTE,
				CREATION_ID,
				CREATION_DT,
				INTERVIEW_SCHEDULE_ID
			FROM ISCH_SHIFT
			WHERE		
				SHIFT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.shiftID#"/> 
		</cfquery>
		<cfset IschShiftVO = CreateObject("component","IschShiftVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset IschShiftVO.setSHIFT_ID(getQry.SHIFT_ID) />
			<cfset IschShiftVO.setSTART_HOUR(getQry.START_HOUR) />
			<cfset IschShiftVO.setSTART_MINUTE(getQry.START_MINUTE) />
			<cfset IschShiftVO.setEND_HOUR(getQry.END_HOUR) />
			<cfset IschShiftVO.setEND_MINUTE(getQry.END_MINUTE) />
			<cfset IschShiftVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset IschShiftVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset IschShiftVO.setINTERVIEW_SCHEDULE_ID(getQry.INTERVIEW_SCHEDULE_ID) />
		</cfif>
		
		<cfreturn IschShiftVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschShiftVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">	
		
		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT ISCH_SHIFT_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		
		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO ISCH_SHIFT (
				SHIFT_ID ,START_HOUR ,START_MINUTE ,END_HOUR ,END_MINUTE ,CREATION_ID ,CREATION_DT ,INTERVIEW_SCHEDULE_ID)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSTART_HOUR()#" maxLength="22" null="#iif((arguments.bean.getSTART_HOUR() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getSTART_MINUTE()#" maxLength="22" null="#iif((arguments.bean.getSTART_MINUTE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getEND_HOUR()#" maxLength="22" null="#iif((arguments.bean.getEND_HOUR() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getEND_MINUTE()#" maxLength="22" null="#iif((arguments.bean.getEND_MINUTE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" />)
		</cfquery>
		
		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">	
		<cfargument name="bean" type="IschShiftVO" required="true" />
		<cfset var updateQry = "" />
		
		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	ISCH_SHIFT
			SET
				START_HOUR = <cfqueryparam value="#arguments.bean.getSTART_HOUR()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSTART_HOUR() eq ''),de("true"), de("false"))#" />,
				START_MINUTE = <cfqueryparam value="#arguments.bean.getSTART_MINUTE()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getSTART_MINUTE() eq ''),de("true"), de("false"))#" />,
				END_HOUR = <cfqueryparam value="#arguments.bean.getEND_HOUR()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getEND_HOUR() eq ''),de("true"), de("false"))#" />,
				END_MINUTE = <cfqueryparam value="#arguments.bean.getEND_MINUTE()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getEND_MINUTE() eq ''),de("true"), de("false"))#" />,
				INTERVIEW_SCHEDULE_ID = <cfqueryparam value="#arguments.bean.getINTERVIEW_SCHEDULE_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getINTERVIEW_SCHEDULE_ID() eq ''),de("true"), de("false"))#" /> 
			WHERE
				SHIFT_ID = <cfqueryparam value="#arguments.bean.getSHIFT_ID()#" cfsqltype="CF_SQL_NUMERIC" /> 
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="shiftID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	ISCH_SHIFT
			WHERE 
				SHIFT_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.shiftID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>