<cfcomponent name="InterviewerData" displayname="InterviewerData" extends="sis_core.model.ModelBase" output="false">
	<cfproperty name="INTERVIEWER_ID" type="string" />
	<cfproperty name="FIRST_NAME" type="string" />
	<cfproperty name="LAST_NAME" type="string" />
	<cfproperty name="EMAIL" type="string" />
	<cfproperty name="CPSO" type="string" />
	<cfproperty name="STUDENT_ID" type="string" />
	<cfproperty name="TYPE" type="string" />
	<cfproperty name="INTERVIEW_SCHEDULE_ID" type="string" />
	<cfproperty name="DEPARTMENT" type="string" />
	<cfproperty name="REGISTRANT_ID" type="string" />
	<cfproperty name="AVAILABLE_DATES" type="string" />

	<cffunction name="init" access="public" returntype="InterviewerData" output="false" displayname="init">
		<cfset variables.INTERVIEWER_ID = "" />
		<cfset variables.FIRST_NAME = "" />
		<cfset variables.LAST_NAME = "" />
		<cfset variables.EMAIL = "" />
		<cfset variables.CPSO = "" />
		<cfset variables.STUDENT_ID = "" />
		<cfset variables.TYPE = "" />
		<cfset variables.DEPARTMENT = "" />
		<cfset variables.REGISTRANT_ID = "" />
		<cfset variables.AVAILABLE_DATES = "" />
				
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setINTERVIEWER_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEWER_ID = val/>
	</cffunction>
	<cffunction name="getINTERVIEWER_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEWER_ID />
	</cffunction>
	
	<cffunction name="setFIRST_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.FIRST_NAME = arguments.val />
	</cffunction>
	<cffunction name="getFIRST_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.FIRST_NAME />
	</cffunction>
	
	<cffunction name="setLAST_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LAST_NAME = arguments.val />
	</cffunction>
	<cffunction name="getLAST_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.LAST_NAME />
	</cffunction>
	
	<cffunction name="setEMAIL" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.EMAIL = arguments.val />
	</cffunction>
	<cffunction name="getEMAIL" access="public" returntype="any" output="false">
		<cfreturn variables.EMAIL />
	</cffunction>
	
	<cffunction name="setCPSO" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.CPSO = arguments.val />
	</cffunction>
	<cffunction name="getCPSO" access="public" returntype="any" output="false">
		<cfreturn variables.CPSO />
	</cffunction>
	
	<cffunction name="setSTUDENT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.STUDENT_ID = val/>
	</cffunction>
	<cffunction name="getSTUDENT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.STUDENT_ID />
	</cffunction>
	
	<cffunction name="setTYPE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.TYPE = arguments.val />
	</cffunction>
	<cffunction name="getTYPE" access="public" returntype="any" output="false">
		<cfreturn variables.TYPE />
	</cffunction>
	
	<cffunction name="setINTERVIEW_SCHEDULE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INTERVIEW_SCHEDULE_ID = arguments.val/>
	</cffunction>
	<cffunction name="getINTERVIEW_SCHEDULE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INTERVIEW_SCHEDULE_ID />
	</cffunction>
	
	<cffunction name="setDEPARTMENT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DEPARTMENT = arguments.val/>
	</cffunction>
	<cffunction name="getDEPARTMENT" access="public" returntype="any" output="false">
		<cfreturn variables.DEPARTMENT />
	</cffunction>	
	
	<cffunction name="setREGISTRANT_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.REGISTRANT_ID = arguments.val/>
	</cffunction>
	<cffunction name="getREGISTRANT_ID" access="public" returntype="any" output="false">
		<cfreturn variables.REGISTRANT_ID />
	</cffunction>
	
	<cffunction name="setAVAILABLE_DATES" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.AVAILABLE_DATES = arguments.val/>
	</cffunction>
	<cffunction name="getAVAILABLE_DATES" access="public" returntype="any" output="false">
		<cfreturn variables.AVAILABLE_DATES />
	</cffunction>	
</cfcomponent>