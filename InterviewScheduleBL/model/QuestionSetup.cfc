<cfcomponent name="QuestionSetup" output="false" extends="ServiceBase">
	
	<cffunction name="getByCompetencyID" returntype="Query" output="false" access="public">
		<cfargument name="competencyID" type="any" required="true">
		
		<cfset var qQuestion = "">
		<cfset questionGTW = variables.dbFactory.create("IschCompetencyQuestionGTW")>
		<cfset qQuestion = questionGTW.getByCompetencyID(arguments.competencyID)>
		
		<cfreturn qQuestion>
	</cffunction>
	
	<cffunction name="get" returntype="QuestionData" output="false" access="public">
		<cfargument name="questionID" type="any" required="true">

		<cfset QuestionData = variables.wirebox.getInstance("QuestionData")>
		<cfif isNumeric(arguments.questionID)>
			<cfset var QuestionDAO = variables.dbFactory.create("IschCompetencyQuestionDAO")>
			<cfset var QuestionVO = QuestionDAO.read(arguments.questionID)>
			<cfset QuestionData.initFromStruct(QuestionVO.toStruct())>
		</cfif>

		<cfreturn QuestionData>
	</cffunction>
	
	<cffunction name="validate" returntype="boolean" output="false" access="private">
		<cfargument name="questionData" type="struct" required="true">

		<cfset var questionGtw = "">
		<cfset var existingCompetencies = "">
		<cfset var bValid = true>
		
		<cfif Trim(arguments.questionData.getQUESTION_NAME()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Name is required.")>
		</cfif>
		
		<cfif Trim(arguments.questionData.getQUESTION_RANK()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Rank is required.")>
		<cfelseif not isValid("Integer", arguments.questionData.getQUESTION_RANK())>
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Rank must be a numeric value.")>
		<cfelse>
			<cfset questionGtw = variables.dbFactory.create("IschCompetencyQuestionGTW")>
			<cfset existingQuestions = questionGtw.getByCompetencyIDandRank(arguments.questionData.getCOMPETENCY_ID(),arguments.questionData.getQUESTION_RANK())>
			<cfif existingQuestions.recordcount neq 0 and arguments.questionData.getQUESTION_ID() neq existingQuestions.question_id>
				<cfset bValid = false>
				<cfset variables.MsgBox.error("Rank <i>#arguments.questionData.getQUESTION_RANK()#</i> is already used.")>
			</cfif>
		</cfif>
						
		<cfreturn bValid>
	</cffunction>
	
	<cffunction name="save" returntype="boolean" output="false" access="public">
		<cfargument name="questionData" type="QuestionData" required="true">

		<cfset var status = true>
		<cfset questionVO = variables.dbFactory.create("IschCompetencyQuestionVO")>
		<cfset questionDAO = variables.dbFactory.create("IschCompetencyQuestionDAO")>
		
		<cftry>
			<cfif validate(arguments.questionData)>
				<cfset questionVO.initFromForm(arguments.questionData.toStruct())>
				<cfif questionVO.getIS_REQUIRED() eq "">
					<cfset questionVO.setIS_REQUIRED('N')>
				</cfif>
				<cfif questionVO.getIS_EDITABLE() eq "">
					<cfset questionVO.setIS_EDITABLE('N')>
				</cfif>
				<cfif questionVO.getQUESTION_ID() EQ ''>
					<cfset questionVO.setCREATION_ID(session.user_id)>
					<cfset questionVO.setCREATION_DT(Now())>
					<cfset questionID = questionDAO.add(questionVO)>
					<cfset arguments.questionData.setQUESTION_ID(questionID)>
					<cfset variables.MsgBox.success("Score Question successfully created.")>
				<cfelse>
					<cfset questionVO.setMODIFICATION_ID(session.user_id)>>
					<cfset questionVO.setMODIFICATION_DT(Now())>
					<cfset questionDAO.update(questionVO)>
					<cfset variables.MsgBox.success("Score Question successfully updated.")>
				</cfif>
			<cfelse>
				<cfset status = false>
			</cfif>
		<cfcatch type="any">
			<cfset status = false>
			<cfset variables.MsgBox.error("Score Question could not be saved.")>
		</cfcatch>
		</cftry>
		<cfreturn status>
	</cffunction>
	
	<cffunction name="delete" returntype="boolean" output="false" access="public">
		<cfargument name="questionID" type="string" required="true">

		<cfset var status = true>
		<cfset questionDAO = variables.dbFactory.create("IschCompetencyQuestionDAO")>
		
		<cftry>
			<cfset questionDAO.delete(arguments.questionID)>
			<cfset variables.MsgBox.success("Score question successfully removed.")>
			<cfcatch type="any">
				<cfset status = false>
				<cfset variables.MsgBox.error("Score question is already used and it can not be removed.")>
			</cfcatch>
		</cftry>
		<cfreturn status>
	</cffunction>

	<cffunction name="createBean" returntype="QuestionData" output="false" access="public">
		<cfset QuestionData = variables.wirebox.getInstance("QuestionData").init()>
		<cfreturn QuestionData>
	</cffunction>
	
	<cffunction name="createFilter" returntype="uiInterviewScheduleFilter" output="false" access="public">
		<cfargument name="formContent" required="true" />
		<cfargument name="cookiesContent" required="true" />

		<cfset uiInterviewScheduleFilter = variables.wirebox.getInstance(name="uiInterviewScheduleFilter").init(arguments.formContent, arguments.cookiesContent)>
		<cfreturn uiInterviewScheduleFilter>
	</cffunction>
	
</cfcomponent>