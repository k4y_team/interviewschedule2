<cfcomponent name="InterviewScheduleAPI" output="false" extends="ServiceBase">
	
	<cffunction name="getHelper" returntype="InterviewScheduleHelper" output="false" access="public">
		<cfset InterviewScheduleHelper = variables.wirebox.getInstance("InterviewScheduleHelper")>
		<cfreturn InterviewScheduleHelper>
	</cffunction>
	
	<cffunction name="getScheduleService" returntype="InterviewScheduleSetup" output="false" access="public">
		<cfset InterviewScheduleSetup = variables.wirebox.getInstance("InterviewScheduleSetup")>
		<cfreturn InterviewScheduleSetup>
	</cffunction>
	
	<cffunction name="getCandidateService" returntype="InterviewScheduleCandidates" output="false" access="public">
		<cfset InterviewScheduleCandidates = variables.wirebox.getInstance("InterviewScheduleCandidates")>
		<cfreturn InterviewScheduleCandidates>
	</cffunction>
	
	<cffunction name="getDayService" returntype="DaySetup" output="false" access="public">
		<cfset DaySetup = variables.wirebox.getInstance("DaySetup")>
		<cfreturn DaySetup>
	</cffunction>
	
	<cffunction name="getCompetencyService" returntype="CompetencySetup" output="false" access="public">
		<cfset CompetencySetup = variables.wirebox.getInstance("CompetencySetup")>
		<cfreturn CompetencySetup>
	</cffunction>
	
	<cffunction name="getQuestionService" returntype="QuestionSetup" output="false" access="public">
		<cfset QuestionSetup = variables.wirebox.getInstance("QuestionSetup")>
		<cfreturn QuestionSetup>
	</cffunction>
	
	<cffunction name="getInterviewerService" returntype="InterviewerSetup" output="false" access="public">
		<cfset InterviewerSetup = variables.wirebox.getInstance("InterviewerSetup")>
		<cfreturn InterviewerSetup>
	</cffunction>

	<cffunction name="getImportService" returntype="Importer" output="false" access="public">
		<cfargument name="importCode" type="string" required="false" default="">
		<cfargument name="interviewScheduleID" type="string" required="false" default="">

		<cfset Importer = variables.wirebox.getInstance("Importer").init(arguments.importCode, arguments.interviewScheduleID)>
		<cfreturn Importer>
	</cffunction>

	<cffunction name="getQuestionScoreService" returntype="QuestionScoreSetup" output="false" access="public">
		<cfset InterviewScheduleCandidates = variables.wirebox.getInstance("QuestionScoreSetup")>
		<cfreturn InterviewScheduleCandidates>
	</cffunction>
</cfcomponent>