<cfcomponent name="Import" output="false" extends="ServiceBase">
	<cffunction name="init" access="public" output="false" returntype="Import">
		<cfset variables.validator = variables.wirebox.getInstance("ImportValidator")>
		<cfreturn this>
	</cffunction>
	
	<cffunction name="setValidator" access="public" returntype="void" output="false">
		<cfargument name="validator" type="any">
		<cfset variables.validator = arguments.validator>
	</cffunction>

	<cffunction name="getValidator" access="public" returntype="any" output="false">
		<cfreturn variables.validator>
	</cffunction>	

	<cffunction name="uploadFile" displayname="uploadFile" access="public" output="false" returntype="string">
		<cfif form.xls_file eq "">
			<cfset variables.MsgBox.error(message="No file selected.",keep=true)>
			<cfreturn "">
		</cfif>
		<cffile action="upload"
			destination="#GetTempDirectory()#"
			nameConflict="MakeUnique"
			fileField="xls_file">

		<cfreturn cffile.serverFile>
	</cffunction>
	
	<cffunction name="validateFileStruct" displayname="validateFileStruct" access="public" output="false" returntype="boolean">
		<cfargument name="data" type="importData" required="true">
		<cfargument name="fileName" type="string" required="false" default="">
		
		<cfset var columnList = "">

		<cfif arguments.fileName eq ""> 
			<cfset arguments.data.setFILE_NAME(uploadFile())>
		<cfelse>
			<cfset arguments.data.setFILE_NAME(arguments.fileName)>
		</cfif>

		<!--- try to read the excel file --->
		<cfspreadsheet action="read" src="#GetTempDirectory()##arguments.data.getFILE_NAME()#" headerRow="1" query="variables.importQry"/>

		<cfif variables.importQry.recordCount eq 0>
			<cfthrow message="No data found in excel file.">
		</cfif>
		
		<cfset QueryAddColumn(variables.importQry, 'EXCEL_LINE', 'Integer', ArrayNew(1))>
		<cftry>
			<cfset headerList = variables.importQry.columnList>
			<cfloop array="#variables.columnData#" index="column">
				<cfset columnList = ListAppend(columnList, column.ColumnName)>
			</cfloop>

			<!--- try to see its a valid file - format, etc. --->
			<cfset valid = true>
			<cfset diffHeader = "">
			<cfloop list="#columnList#" index="cl">
				<cfif ListFindNoCase(headerList,cl) EQ 0>
					<cfset diffHeader = ListAppend(diffHeader, cl)>
					<cfset valid = false>
				</cfif>
			</cfloop>

			<cfif NOT valid>
				<cfthrow message="Spreadsheet has invalid header! Missing the following column(s):<br> #Replace(diffHeader,',',', ','all')#.<br>Please use the sample header from this page.">
				<cfreturn false>
			</cfif>
			<cfcatch>
				<cfthrow message="Unable to read xls file: #cfcatch.message#">
				<cfreturn false>
			</cfcatch>
		</cftry>
	
		<cfset QueryAddColumn(variables.importQry, 'status', 'VarChar', ArrayNew(1))>
		<cfset QueryAddColumn(variables.importQry, 'errorMessages', 'VarChar', ArrayNew(1))>
		<cfset QueryAddColumn(variables.importQry, 'errorFields', 'VarChar', ArrayNew(1))>
		
		<!--- Warning query --->
		
		<cfquery dbtype="query" name="variables.warningQry">
			SELECT * FROM importQry
		</cfquery>
		
		<cfreturn true/>
	</cffunction>
	
	<cffunction name="validateFileData" displayname="validateData" access="public" output="false" returntype="boolean">
		<cfargument name="data" type="ImportData" required="true">
		<cfset var error_msg = "">
		<cfset var warning_msg = "">
		<cfset var error_fields = "">
		<cfset var warning_fields = "">
		<cfset var rowIsValid = true>
		<cfset var status = "">
		<cfset var isValid = true>
		<cfset blank_fields = 0>
		
		<!--- start from line 2, since first one has the column names --->
		<cfloop query="variables.importQry" startRow="2">
			<cfset rowIsValid = true>
			<cfset status = "OK">
			<cfloop array="#variables.columnData#" index="column">
				<cfset columnVal = trim(variables.importQry['#column.ColumnName#'][currentRow])>
				<cfif column.Required eq "Y" AND columnVal eq "">
					<cfset error_msg = error_msg & column.ColumnName & ": Field is required." & "<br>">
					<cfset status="ERROR">
					<cfset error_fields = ListAppend(error_fields, column.ColumnName)>
					<cfset rowIsValid = false><!---
				<cfelseif column.Required eq "N" AND columnVal eq "">
					<cfset warning_msg = warning_msg & column.ColumnName & ": Field is blank." & "<br>">
					<cfset warning_fields = ListAppend(warning_fields, column.ColumnName)>--->
				</cfif>	
				<cfloop list="#column.Validation#" index="code">
					<cftry>
						<cfset Evaluate("variables.validator.validate#UCase(code)#(columnVal,variables.importQry,column.columnName)")>
					<cfcatch type="validation_warning">
						<cfset warning_msg = warning_msg & column.ColumnName & ": " & cfcatch.message & "<br>">
						<cfset warning_fields = ListAppend(warning_fields, column.ColumnName)>
					</cfcatch>
					<cfcatch type="validation_error">
						<cfset status="ERROR">
						<cfset error_msg = error_msg & column.ColumnName & ": " & cfcatch.message & "<br>">
						<cfset error_fields = ListAppend(error_fields, column.ColumnName)>
						<cfset rowIsValid = false>
						<cfbreak>
					</cfcatch>
					<cfcatch type="any">
						<cfset status="ERROR">
						<cfset error_msg = error_msg & column.ColumnName & ": " & cfcatch.message & "<br>">
						<cfset error_fields = ListAppend(error_fields, column.ColumnName)>
						<cfset rowIsValid = false>
						<cfbreak>
					</cfcatch>
					</cftry>
				</cfloop>
			</cfloop>
						
			<cfif error_msg neq "">
				<cfset QuerySetCell(variables.importQry, "errorMessages", error_msg, variables.importQry.currentRow)>
				<cfset QuerySetCell(variables.importQry, "errorFields", error_fields, variables.importQry.currentRow)>
				<cfset isValid = isValid AND rowIsValid>
			<cfelseif warning_msg neq "">
				<cfset QuerySetCell(variables.warningQry, "errorMessages", warning_msg, variables.importQry.currentRow)>
				<cfset QuerySetCell(variables.warningQry, "errorFields", warning_fields, variables.importQry.currentRow)>
				<cfset QuerySetCell(variables.warningQry, "STATUS", "WARNING", variables.importQry.currentRow)>
				<cfset QuerySetCell(variables.warningQry, "EXCEL_LINE", variables.importQry.currentRow, variables.importQry.currentRow)>
			</cfif>
			
			<cfset error_msg = "">
			<cfset error_fields = "">
			<cfset warning_msg = "">
			<cfset warning_fields = "">
			
			<cfset QuerySetCell(variables.importQry, "STATUS", status, variables.importQry.currentRow)>
			<cfset QuerySetCell(variables.importQry, "EXCEL_LINE", variables.importQry.currentRow, variables.importQry.currentRow)>
		</cfloop>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction name="getImportDataByFilter" displayname="getImportDataByFilter" access="public" output="false" returntype="query">
		<cfargument name="msgType" type="string" required="false" default="">

		<cfquery name="filterQry" dbtype="query">
			SELECT * FROM variables.importQry
			<cfswitch expression="#arguments.msgType#">
				<cfcase value="SUCCESS">
					WHERE STATUS = 'OK'
				</cfcase>
				<cfcase value="ERROR">
					WHERE STATUS = 'ERROR'
				</cfcase>
				<cfcase value="WARNING">
					WHERE STATUS = 'WARNING'
				</cfcase>
			</cfswitch>
		</cfquery>

		<cfreturn filterQry>
	</cffunction>

	<cffunction name="getWarningData" displayname="getWarningData" access="public" output="false" returntype="query">
		<cfquery name="filterQry" dbtype="query">
			SELECT * FROM variables.warningQry
			WHERE STATUS = 'WARNING'
		</cfquery>

		<cfreturn filterQry>
	</cffunction>

	<cffunction name="getImportData" displayname="getImportData" access="public" output="false" returntype="query">
		<cfreturn variables.importQry>
	</cffunction>
	
	<cffunction name="getImportDesc" displayname="getImportDesc" access="public" output="false" returntype="struct">
		<cfreturn variables.importDesc>
	</cffunction>
		
	<cffunction name="getImportFileHeader" displayname="getImportFileHeader" access="public" output="false" returntype="string">
		<cfset var columnList = "">
		
		<cfloop array="#variables.columnData#" index="column">
			<cfset columnList = ListAppend(columnList, column.ColumnName)>
		</cfloop>
		
		<cfreturn columnList>
	</cffunction>
	
	<cffunction name="getImportResult" displayname="getImportResult" access="public" output="false" returntype="ImportResult">
		<cfset var importResult = CreateObject("component","importResult").init()>
		<cfset var badDataQry = getImportDataByFilter("ERROR")>
		<cfset var warningDataQry = getWarningData()>
		<cfset var goodDataQry = getImportDataByFilter("SUCCESS")>

		<cfset importResult.setTOTAL_RECORDS(variables.importQry.recordCount - 1)>
		<cfset importResult.setBAD_RECORDS(badDataQry.recordCount)>
		<cfset importResult.setWARNING_RECORDS(warningDataQry.recordCount)>
		<cfset importResult.setGOOD_RECORDS(importResult.getTOTAL_RECORDS() - importResult.getBAD_RECORDS())>
		<cfset importResult.setBAD_RECORDS_DATA(badDataQry)>
		<cfset importResult.setWARNING_RECORDS_DATA(warningDataQry)>
		<cfset importResult.setGOOD_RECORDS_DATA(goodDataQry)>

		<cfset var columnList = "">
		<cfset headerList = variables.importQry.columnList>
		<cfloop array="#variables.columnData#" index="column">
			<cfset columnList = ListAppend(columnList, column.ColumnName)>
		</cfloop>

		<cfif ListLen(headerList)-4 NEQ ListLen(columnList)>
			<cfset importResult.setHAVE_MORE_COLUMNS(true)>
		<cfelse>
			<cfset importResult.setHAVE_MORE_COLUMNS(false)>
		</cfif>
		<cfreturn importResult>
	</cffunction>

</cfcomponent>