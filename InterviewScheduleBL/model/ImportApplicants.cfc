<cfcomponent name="ImportApplicants" output="false" extends="Import">
	<cfproperty name="columnData" type="array" />
	<cfproperty name="importQry" type="query" />
	<cfproperty name="importDesc" type="struct">
	<cfproperty name="interviewScheduleID" type="numeric">
	<cfproperty name="dbFactory" inject="dbFactory@InterviewSchedule" />
	<cfproperty name="InterviewScheduleAPI" inject="InterviewScheduleAPI">
	
	<cffunction name="init" returntype="ImportApplicants" output="false" access="public">
		<cfargument name="interviewScheduleID" type="numeric" required="true">
		
		<cfset variables.interviewScheduleID = arguments.interviewScheduleID>
		<cfset columnsName = '[{"ColumnName":"OMSAS##","Required":"Y","Validation":"ReferanceNo"}
				 ,{"ColumnName":"Competency","Required":"Y","Validation":"Competency"}'>
		<cfset var ischcompetencyGTW = variables.dbFactory.create("IschCompetencyQuestionGTW")>
		<cfset questionsQry = ischcompetencyGTW.getCompetencyQuestions(variables.interviewScheduleID)>
		<cfloop query="questionsQry" >
			<cfset columnsName = columnsName & ',{"ColumnName":"q_' & questionsQry.QUESTION_RANK &'","Required":"N","Validation":"Question"}' > 
		</cfloop>		
		<cfset columnsName = columnsName & ']'>
		<cfset variables.columnData = DeserializeJSON(columnsName)> 
		<cfset variables.importQry = "">
		<cfset variables.importDesc = { code = "APPLICANTS", title = "Import Applicants"}>
		<cfreturn this>
	</cffunction>
	
	<cffunction name="setImportTitle" displayname="setImportDesc" access="public" output="false" returntype="void">
		<cfargument name="importTitle" type="string" required="true">
		
		<cfset variables.importDesc.title = arguments.importTitle>
	</cffunction>

	<cffunction name="getCompQuestionsDetails" access="private" returntype="Struct">
		<cfset var competencyQuestionGtw = variables.dbFactory.create("IschCompetencyQuestionGTW")>
		<cfset var compQuestionsDetailsQry  = "">
		
		<cfif not isDefined("variables.compQuestions")>
			<cfset compQuestionsDetailsQry = competencyQuestionGtw.getCompetencyQuestionsDetails(variables.interviewScheduleID)>
			<cfset variables.compQuestions = StructNew()>
			<cfloop query="compQuestionsDetailsQry">
				<cfif not StructKeyExists(compQuestions,UCase(compQuestionsDetailsQry.competency_name))>
					<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)] = StructNew()>
				<cfelseif not StructKeyExists(compQuestions[UCase(compQuestionsDetailsQry.competency_name)],compQuestionsDetailsQry.question_rank)>
					<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank] = StructNew()>
				</cfif>	
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["QUESTION_ID"]  = compQuestionsDetailsQry.question_id>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["IS_REQUIRED"]  = compQuestionsDetailsQry.is_required>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["QUESTION_TYPE_CODE"]  = compQuestionsDetailsQry.question_type_code>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["RATING_ID"]  = compQuestionsDetailsQry.rating_id>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["MIN_VALUE"]  = compQuestionsDetailsQry.min_value>
				<cfset variables.compQuestions[UCase(compQuestionsDetailsQry.competency_name)][compQuestionsDetailsQry.question_rank]["MAX_VALUE"]  = compQuestionsDetailsQry.max_value>
			</cfloop>
		</cfif>	
		<cfreturn variables.compQuestions>
	</cffunction>
		
	<cffunction name="processData" displayname="processData" access="public" output="false" returntype="boolean">
		<cfargument name="importVO" type="ImportData" required="true">
	
		<cfset var ischScoreDAO = variables.dbFactory.create("IschQuestionScoreDAO")>
		<cfset var applicantsGtw = variables.dbFactory.create("ApplicantsGTW")>
		
		<cfset var ischQuestScoreVO = "">
		<cfset var result = true>
		<cfset var totalImportedRecords = 0>
		<cfset var competencyName = "">
		<cfset var rankNo = "">
		<cfset var compQuestions = getCompQuestionsDetails()>

			<!--- start from line 2, since first one has the column names --->
			<cfset colQuestList = "">
			<cfset applicantIdsList = "">
			
			<cfloop array="#variables.columnData#" index="colDataStruct">
				<cfif colDataStruct.Validation eq "Question" >
					<cfset colQuestList = ListAppend(colQuestList, colDataStruct.ColumnName)>
				</cfif>	
			</cfloop>
			<cfloop query="variables.importQry" startRow="2">
				<cftransaction>
				<cftry>
				<cfset competencyName = variables.importQry["Competency"][variables.importQry.currentrow]>
				<cfif variables.importQry.STATUS eq "OK">
					<cfset totalImportedRecords = totalImportedRecords + 1>

					<cfset applicantQry = applicantsGtw.getByReferenceId(variables.interviewScheduleID, variables.importQry["OMSAS##"][variables.importQry.currentrow])>
					<cfif ListFindNoCase(applicantIdsList,applicantQry.APPLICANT_ID) eq 0>
						<cfset	applicantIdsList = ListAppend( applicantIdsList, applicantQry.APPLICANT_ID)>
					</cfif>
					<cfset rankNo = "">
					<cfloop list="#colQuestList#" index="colNameQuest">	
						<cfset scoreValue = variables.importQry["#colNameQuest#"][variables.importQry.currentrow]>
						<cfset rankNo = Replace(colNameQuest,"q_","")>

						<cfif compQuestions[competencyName][rankNo].QUESTION_ID neq ''>
							<cfset ischQuestScoreVO = ischScoreDAO.read(compQuestions[competencyName][rankNo].QUESTION_ID, applicantQry.APPLICANT_ID)>
							<cfif compQuestions[competencyName][rankNo].question_type_code eq "TEXT">
								<cfset ischQuestScoreVO.setANSWER_TEXT(scoreValue)>
								<cfset ischQuestScoreVO.setSCORE_VALUE("")>
							<cfelse>	
								<cfset ischQuestScoreVO.setSCORE_VALUE(scoreValue)>
								<cfset ischQuestScoreVO.setANSWER_TEXT("")>
							</cfif>	
							
							<cfif ischQuestScoreVO.getAPPLICANT_ID() eq "">
								<cfset ischQuestScoreVO.setAPPLICANT_ID(applicantQry.APPLICANT_ID)>
								<cfset ischQuestScoreVO.setQUESTION_ID(compQuestions[competencyName][rankNo].QUESTION_ID)>
								<cfset ischQuestScoreVO.setCREATION_ID(session.user_id)>
								<cfset ischQuestScoreVO.setCREATION_DT(Now())>					
								<cfset ischScoreDAO.add(ischQuestScoreVO)>
							<cfelse>
								<cfset ischQuestScoreVO.setMODIFICATION_ID(session.user_id)>
								<cfset ischQuestScoreVO.setMODIFICATION_DT(Now())>					
								<cfset ischScoreDAO.update(ischQuestScoreVO)>
							</cfif>
						</cfif>
					</cfloop>
				</cfif> 	
				<cftransaction action="commit"/>
				<cfcatch type="any">
					<cftransaction action="rollback"/>
					<cfset result = false>
					<cfrethrow />
				</cfcatch>
				</cftry>
				</cftransaction>
			</cfloop>
			
			<cfloop list="#applicantIdsList#" index="applicantID"> 
				<cfset variables.InterviewScheduleAPI.getCandidateService().calculateScore(applicantID)>
			</cfloop>
			<cfif totalImportedRecords gt 1>
				<cfset variables.MsgBox.success("#totalImportedRecords# applicant scores successfully imported.")>
			<cfelse>
				<cfset variables.MsgBox.success("#totalImportedRecords# applicant scores successfully imported.")>
			</cfif>	
		<cfreturn result>
	</cffunction>
	
</cfcomponent>