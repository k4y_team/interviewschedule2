<!---
  --- Interviewers
  --- ------------
  ---
  --- author: zoltan.stir
  --- date:   9/20/18
  --->
<cfcomponent name="Interviewer" displayname="Interviewer" code="QB_ISCH_INTERVIEWER" output="false" extends="sis_core.model.blAutomation.EntityBase" accessors="true" >
	<cfproperty name="datasource" inject="coldbox:setting:pg_datasource" scope="variables">

	<cfproperty id="REGISTRANT_ID" name="REGISTRANT_ID" type="string" label="Registrant ID" >
	<cfproperty id="LAST_NAME" key="true" name="LAST_NAME" type="string" label="Last Name" >
	<cfproperty id="FIRST_NAME" name="FIRST_NAME" type="string" label="First NAME" >
	<cfproperty id="EMAIL" name="EMAIL" type="string" label="Email">
	<cfproperty id="TYPE" name="TYPE" type="string" label="Type">
	<cfproperty id="DEPARTMENT" name="DEPARTMENT" type="string" label="Department">
	<cfproperty id="CPSO" name="CPSO" type="numeric" label="CPSO ##">
	<cfproperty id="STUDENT_ID" name="STUDENT_ID" type="numeric" label="Student ID">
	<cfproperty id="AVAILABILITY" name="AVAILABILITY" type="string" label="Availability">

	<cffunction name="init" access="public" output="false" returntype="Interviewer">
		<cfset super.init() />
		<cfreturn this />
	</cffunction>

	<cffunction name="getSQL" access="public" output="false" returntype="string">
		<cfset var sql = "
			SELECT
				REGISTRANT_ID,
				LAST_NAME,
				FIRST_NAME,
				EMAIL,
				TYPE,
				DEPARTMENT,
				CPSO,
				STUDENT_ID,
				AVAILABILITY
			FROM
				ISCH_INTERVIEWER_STAGING
		" />
		<cfreturn sql>
	</cffunction>

</cfcomponent>