<cfcomponent name="uiInterviewScheduleFilter" displayName="uiInterviewScheduleFilter" extends="medsis.model.s4y.uiFilter">
	<cfproperty name="InterviewScheduleHelper" inject="InterviewScheduleHelper">
	<cfproperty name="InterviewScheduleSetup" inject="InterviewScheduleSetup">
	<cfproperty name="DaySetup" inject="DaySetup">
	<cfproperty name="CompetencySetup" inject="CompetencySetup">

	<cffunction access="public" name="init" returntype="uiInterviewScheduleFilter" displayname="init" hint="" output="false" roles="">
		<cfargument name="formContent" required="true" />
		<cfargument name="cookiesContent" required="true" />
		   
		<cfset variables.instance.formContent = formContent>
		<cfset variables.instance.cookiesContent = cookiesContent>
		<cfparam name="variables.instance.formContent.dayID" default="">
		<cfparam name="variables.instance.formContent.circuitID" default="">
		
		<cfset setupInterviewSchedule()>
		<cfset setupSession()>
		<cfset setupDay()>
		<cfset setupCircuit()>
		<cfset setupCompetency()>
   		<cfreturn this>
	</cffunction>
	
	<cffunction access="private" name="setupInterviewSchedule" returntype="void" output="false" hint="Setup queries and field values used in filter">
		<cfif isDefined("variables.instance.formContent.sessionID")>
			<cfset sessionID = variables.instance.formContent.sessionID>
		<cfelseif isDefined("variables.instance.cookiesContent.CM_sessionCD")>
			<cfset sessionID = variables.instance.cookiesContent.CM_sessionCD>
		</cfif>
			
		<cfif isDefined("sessionID")>
			<cfset variables.instance.formContent.sessionID = sessionID>
			<cfset variables.instance.formContent.interviewScheduleID = InterviewScheduleSetup.getInterviewScheduleBySession(variables.instance.formContent.sessionID).INTERVIEW_SCHEDULE_ID>
		<cfelse>
			<cfif isDefined("variables.instance.formContent.interviewScheduleID")>
				<cfset interviewScheduleID = variables.instance.formContent.interviewScheduleID>
			<cfelseif isDefined("variables.instance.cookiesContent.ISCH_interviewScheduleID")>
				<cfset interviewScheduleID = variables.instance.cookiesContent.ISCH_interviewScheduleID>
			</cfif>
			<cfif isDefined("interviewScheduleID")>
				<cfset variables.instance.formContent.interviewScheduleID = interviewScheduleID>
				<cfset variables.instance.formContent.sessionID = InterviewScheduleSetup.getInterviewScheduleByID(variables.instance.formContent.interviewScheduleID).SESSION_ID>
			<cfelse>
				<cfset currentSessionQry = variables.InterviewScheduleHelper.getCurrentSession()>
				<cfset variables.instance.formContent.sessionID = currentSessionQry.SESSION_ID>
				<cfset variables.instance.formContent.interviewScheduleID = InterviewScheduleSetup.getInterviewScheduleBySession(variables.instance.formContent.sessionID).INTERVIEW_SCHEDULE_ID>
			</cfif>
		</cfif>
		<cfset variables.instance.InterviewScheduleV = setupValue("interviewScheduleID", "ISCH_interviewScheduleID")>
	</cffunction>
	
	<cffunction access="public" name="getInterviewScheduleValue" returntype="string" displayname="getInterviewScheduleValue" hint="" output="false" roles="">
	 	<cfreturn variables.instance.InterviewScheduleV/>
	</cffunction>

	<cffunction access="private" name="setupSession" returntype="void" output="false" hint="Setup queries and field values used in filter">
		<cfset variables.instance.sessionQuery = variables.InterviewScheduleHelper.getSessions()/>
		<cfset variables.instance.sessionValue = setupValue("sessionID", "CM_sessionCD")>
	</cffunction>
	
	<cffunction access="private" name="setupDay" returntype="void" output="false" hint="Setup queries and field values used in filter">
		<cfset variables.instance.triggerTypeQ = variables.DaySetup.getByInterviewScheduleID(variables.instance.InterviewScheduleV)/>
		<cfset variables.instance.triggerTypeV = setupValue("dayID", "ISCH_dayID")>
	</cffunction>
	
	<cffunction access="public" name="getDayQuery" returntype="query" displayname="getDayQuery" hint="" output="false" roles="">
		<cfreturn variables.instance.triggerTypeQ/>
	</cffunction>
	
	<cffunction access="public" name="getDayValue" returntype="string" displayname="getDayValue" hint="" output="false" roles="">
	 	<cfreturn variables.instance.triggerTypeV/>
	</cffunction>
	
	<cffunction access="private" name="setupCircuit" returntype="void" output="false" hint="Setup queries and field values used in filter">
		<cfset var filter = variables.InterviewScheduleHelper.setupFilter(variables.instance.formContent,'interviewScheduleID,dayID')/>
		<cfset variables.instance.circuitQ = variables.InterviewScheduleSetup.getCircuits(filter)/>
		<cfset variables.instance.circuitV = setupValue("circuitID", "ISCH_circuitID")>
	</cffunction>
	
	<cffunction access="public" name="getCircuitQuery" returntype="query" displayname="getCircuitQuery" hint="" output="false" roles="">
		<cfreturn variables.instance.circuitQ/>
	</cffunction>
	
	<cffunction access="public" name="getCircuitValue" returntype="string" displayname="getCircuitValue" hint="" output="false" roles="">
	 	<cfreturn variables.instance.circuitV/>
	</cffunction>
	
	<cffunction access="private" name="setupCompetency" returntype="void" output="false" hint="Setup queries and field values used in filter">
		<cfset variables.instance.competencyQ = variables.CompetencySetup.getByInterviewScheduleID(variables.instance.InterviewScheduleV)/>
		<cfset variables.instance.competencyV = setupValue("competencyID", "ISCH_competencyID")>
	</cffunction>
	
	<cffunction access="public" name="getCompetencyQuery" returntype="query" displayname="getCompetencyQuery" hint="" output="false" roles="">
		<cfreturn variables.instance.competencyQ/>
	</cffunction>
	
	<cffunction access="public" name="getCompetencyValue" returntype="string" displayname="getCompetencyValue" hint="" output="false" roles="">
	 	<cfreturn variables.instance.competencyV/>
	</cffunction>
	
</cfcomponent>