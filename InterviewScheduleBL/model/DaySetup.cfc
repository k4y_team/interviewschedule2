<cfcomponent name="DaySetup" output="false" extends="ServiceBase">
	
	<cffunction name="getByInterviewScheduleID" returntype="Query" output="false" access="public">
		<cfargument name="interviewScheduleID" type="any" required="true">
		
		<cfset var qDay = "">
		<cfset dayGTW = variables.dbFactory.create("IschDayGTW")>
		<cfset qDay = dayGTW.getByInterviewScheduleID(arguments.interviewScheduleID)>
		
		<cfreturn qDay>
	</cffunction>
	
	<cffunction name="get" returntype="DayData" output="false" access="public">
		<cfargument name="dayID" type="any" required="true">

		<cfset DayData = variables.wirebox.getInstance("DayData")>
		<cfif isNumeric(arguments.dayID)>
			<cfset var DayDAO = variables.dbFactory.create("IschDayDAO")>
			<cfset var DayVO = DayDAO.read(arguments.dayID)>
			<cfset DayData.initFromStruct(DayVO.toStruct())>
		</cfif>
		
		<cfreturn DayData>
	</cffunction>

	<cffunction name="validate" returntype="boolean" output="false" access="private">
		<cfargument name="dayData" type="struct" required="true">

		<cfset var dayGtw = "">
		<cfset var existingDays = "">		
		<cfset var bValid = true>
		
		<cfif Trim(arguments.dayData.getDAY_NAME()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Name is required.")>
		<cfelse>
			<cfset dayGtw = variables.dbFactory.create("IschDayGtw")>
			<cfset existingDays = dayGtw.getByName(arguments.dayData.getINTERVIEW_SCHEDULE_ID(),arguments.dayData.getDAY_NAME())>
			<cfif existingDays.recordcount neq 0 and existingDays.day_id neq arguments.dayData.getDAY_ID()>
				<cfset bValid = false>
				<cfset variables.MsgBox.error("Name <i>#Trim(arguments.dayData.getDAY_NAME())#</i> is already used for <i>#DateFormat(existingDays.day_dt,'dd-mmm-yyyy')#</i>.")>
			</cfif>
		</cfif>

		<cfif Trim(arguments.dayData.getDAY_DT()) eq "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Date is required.")>
		<cfelseif not isDate(arguments.dayData.getDAY_DT())>
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Invalid date (e.g. 01-Jan-2013).")>
		<cfelse>
			<cfset dayGtw = variables.dbFactory.create("IschDayGtw")>
			<cfset existingDays = dayGtw.getByDate(arguments.dayData.getINTERVIEW_SCHEDULE_ID(),arguments.dayData.getDAY_DT())>
			<cfif existingDays.recordcount neq 0 and existingDays.day_id neq arguments.dayData.getDAY_ID()>
				<cfset bValid = false>
				<cfset variables.MsgBox.error("Date <i>#DateFormat(arguments.dayData.getDAY_DT(),'dd-mmm-yyyy')#</i> is already used by <i>#existingDays.day_name#</i>.")>
			</cfif>
		</cfif>
		
		<cfreturn bValid>
	</cffunction>

	<cffunction name="save" returntype="boolean" output="false" access="public">
		<cfargument name="dayData" type="struct" required="true">
		
		<cfset var status = true>
		<cfset dayVO = variables.dbFactory.create("IschDayVO")>
		<cfset dayDAO = variables.dbFactory.create("IschDayDAO")>
		
		<cfset dayVO.initFromForm(arguments.dayData.toStruct())>
		<cftry>
			<cfif validate(arguments.dayData)>
				<cfif dayVO.getDAY_ID() EQ ''>
					<cfset dayVO.setCREATION_ID(session.user_id)>
					<cfset dayVO.setCREATION_DT(Now())>
					<cfset dayID = dayDAO.add(dayVO)>
					<cfset arguments.dayData.setDAY_ID(dayID)>
					<cfset variables.MsgBox.success("Interview Day successfully created.")>
				<cfelse>
					<cfset dayVO.setMODIFICATION_ID(session.user_id)>
					<cfset dayVO.setMODIFICATION_DT(Now())>
					<cfset dayDAO.update(dayVO)>
					<cfset variables.MsgBox.success("Interview Day successfully updated.")>
				</cfif>
			<cfelse>	
				<cfset status = false>			
			</cfif>	
		<cfcatch type="any">
			<cfset status = false>
		</cfcatch>
		</cftry>
		
		<cfreturn status>
	</cffunction>
	
	<cffunction name="delete" returntype="boolean" output="false" access="public">
		<cfargument name="dayID" type="string" required="true">

		<cfset var status = true>
		
		<cfset dayDAO = variables.dbFactory.create("IschDayDAO")>
		<cftry>
			<cfset dayDAO.delete(arguments.dayID)>
			<cfset variables.MsgBox.success("Interview Day successfully removed.")>
			<cfcatch type="any">
				<cfset variables.MsgBox.error("Interview Day is already used and it can not be removed.")>
				<cfset status = false>
			</cfcatch>
		</cftry>
		
		<cfreturn status>
	</cffunction>
	
	<cffunction name="createBean" returntype="DayData" output="false" access="public">
		<cfset DayData = variables.wirebox.getInstance("DayData").init()>
		<cfreturn DayData>
	</cffunction>
	
	<cffunction name="createFilter" returntype="uiInterviewScheduleFilter" output="false" access="public">
		<cfargument name="formContent" required="true" />
		<cfargument name="cookiesContent" required="true" />

		<cfset uiInterviewScheduleFilter = variables.wirebox.getInstance("uiInterviewScheduleFilter").init(arguments.formContent, arguments.cookiesContent)>
		<cfreturn uiInterviewScheduleFilter>
	</cffunction>
	
</cfcomponent>