<!-----------------------------------------------------------------------
Author 	 :	Walkovszky Norbert
Date     :	November 15, 2013
Description : Interview Schedule business logic layer

----------------------------------------------------------------------->
<cfcomponent output="false" hint="Interview Schedule BL Configuration">
<cfscript>
	// Module Properties
	this.title 				= "Interview Schedule BL";
	this.author				= "Walkovszky Norbert";
	this.webUrl				= "";
	this.description 		= "";
	this.version			= "1.0";
	this.viewParentLookup 	= true;
	this.layoutParentLookup = true;
	this.entryPoint			= "InterviewSchedule";

	/**
	* Configure the UserDirectory Module
	*/
	function configure(){
		settings = {
			version = "1.0",
			breakBetweenStations = 3 // break time in minutes
		};
		wirebox = {
			enabled = true,
			binder = "#moduleMapping#.config.WireBox", // by convention this is the default
			singletonReload = true  // reload the singleton scope on each request, great for dev.
		};
	}

	private void function registerEntities() {
		QB = application.wirebox.getInstance('QueryBuilder@CORE');

		QB.unregisterAll(tags="Interview Schedule");
		QB.registerEntity("QB_ISCH_INTERVIEWER", "IschInterviewer", "sis_modules.InterviewSchedule.InterviewScheduleBL.model.entity.Interviewer", "Interview Schedule", {code="QB_ISCH_INTERVIEWER", STAGING_TABLE="ISCH_INTERVIEWER_STAGING", module="ISCH_MODULE"}, "Interviewer Import");
	}


	function onLoad() {
		binder.map("dbFactory@InterviewSchedule").to("sis_core.model.DBFactory").initArg(name="datasource", value="#application.cbcontroller.getsetting('datasource')#")
																				.initArg(name="path", value="#moduleMapping#/model/db");

		binder.map("InterviewScheduleAPI").to("#moduleMapping#.model.InterviewScheduleAPI").noInit();

		binder.map("uiInterviewScheduleFilter").to("#moduleMapping#.model.uiInterviewScheduleFilter").noInit();
		binder.map("InterviewScheduleHelper").to("#moduleMapping#.model.InterviewScheduleHelper");
		binder.map("InterviewScheduleSetup").to("#moduleMapping#.model.InterviewScheduleSetup");

		binder.map("InterviewScheduleCandidates").to("#moduleMapping#.model.InterviewScheduleCandidates");

		binder.map("SearchFilter").to("sis_core.model.SearchFilter");
		binder.map("SearchFilterItem").to("sis_core.model.SearchFilterItem");

		binder.map("breakBetweenStations").toDSL("coldbox:setting:breakBetweenStations@InterviewScheduleBL");

		binder.map("CompetencySetup").to("#moduleMapping#.model.CompetencySetup");
		binder.map("CompetencyData").to("#moduleMapping#.model.CompetencyData");

		binder.map("DaySetup").to("#moduleMapping#.model.DaySetup");
		binder.map("DayData").to("#moduleMapping#.model.DayData");

		binder.map("InterviewerSetup").to("#moduleMapping#.model.InterviewerSetup");
		binder.map("InterviewerData").to("#moduleMapping#.model.InterviewerData");

		binder.map("ImportApplicants").to("#moduleMapping#.model.ImportApplicants").noInit();

		if (StructKeyExists(url,"db_refresh")) {
			/*registerHandlers();
			registerEventBindings();*/
			registerEntities();
			/*registerActivityLogEntities();
			setupMappingEntities();*/
		}
	}
</cfscript>
</cfcomponent>